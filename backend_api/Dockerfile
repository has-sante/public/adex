ARG INSTALL_DEV=false
ARG POETRY_VERSION="1.2.2"
FROM python:3.8

ARG INSTALL_DEV
ARG POETRY_VERSION

# Setup Poetry for Python package and dependency management
ENV POETRY_HOME=/opt/poetry \
    POETRY_VIRTUALENVS_CREATE=true
ENV PATH="${POETRY_HOME}/bin:${PATH}"
# Install `poetry` via `pip` and system `python`
RUN pip install poetry==${POETRY_VERSION} && \
    poetry --version && \
    poetry config --list

WORKDIR /app/

COPY ./src/pyproject.toml ./src/poetry.lock /app/
RUN poetry install --no-interaction --no-root \
    $(test $INSTALL_DEV = "false" && echo "--no-dev")

COPY ./src/start.sh /start.sh
RUN chmod +x /start.sh

COPY ./src/gunicorn_conf.py /gunicorn_conf.py

COPY ./src/start-reload.sh /start-reload.sh
RUN chmod +x /start-reload.sh

COPY ./src/ /app
RUN poetry install --no-interaction $(test $INSTALL_DEV = "false" && echo "--no-dev")


CMD ["poetry", "run", "/start.sh"]
