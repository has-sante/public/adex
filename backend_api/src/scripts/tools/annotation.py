import click

from app.crud import (
    crud_ts_declaration_avantage,
    crud_ts_declaration_convention,
    crud_ts_declaration_remuneration,
)
from app.db.session import SessionLocal


@click.command()
@click.option("-h", "--hashes")
def cli(hashes):
    db = SessionLocal()
    hashes = hashes.split(",")
    conventions = crud_ts_declaration_convention.get_multi_by_hashes(
        hashes, db
    )
    remunerations = crud_ts_declaration_remuneration.get_multi_by_hashes(
        hashes, db
    )
    avantages = crud_ts_declaration_avantage.get_multi_by_hashes(hashes, db)
    declaration_ids = (
        [convention.declaration_id for convention in conventions]
        + [remuneration.declaration_id for remuneration in remunerations]
        + [avantage.declaration_id for avantage in avantages]
    )
    print(declaration_ids)


if __name__ == "__main__":
    cli()
