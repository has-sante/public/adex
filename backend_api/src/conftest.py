from app.tests.search_engine.fixtures.ans import (
    case_ascii_tests,
    cvdi_examples,
    homonyms,
    homonyms_with_place,
)
from app.tests.search_engine.fixtures.ts_reliable import (
    ts_reliable_declarations,
)


def pytest_addoption(parser):
    parser.addoption(
        "--search-engine-dev",
        action="store_true",
        help="Test search engine with few examples",
    )
    parser.addoption(
        "--search-engine-prod",
        action="store_true",
        default=False,
        help="Test search engine with all examples",
    )


def pytest_generate_tests(metafunc):
    if metafunc.config.getoption("search_engine_dev"):
        if "ans_search_query" in metafunc.fixturenames:
            metafunc.parametrize(
                "ans_search_query",
                case_ascii_tests + homonyms + homonyms_with_place,
            )
        if "ts_reliable_declarations" in metafunc.fixturenames:
            metafunc.parametrize(
                "ts_reliable_declarations", ts_reliable_declarations
            )
    if metafunc.config.getoption("search_engine_prod"):
        if "ans_search_query" in metafunc.fixturenames:
            metafunc.parametrize(
                "ans_search_query",
                case_ascii_tests
                + homonyms
                + homonyms_with_place
                + cvdi_examples,
            )
        if "ts_reliable_declarations" in metafunc.fixturenames:
            metafunc.parametrize(
                "ts_reliable_declarations", ts_reliable_declarations
            )
