from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from app.api.api_v1.api import api_router
from app.core.config import settings
from app.core.constants import APITags

tags_metadata = [
    {
        "name": APITags.annuaire_ans,
        "description": "Routes permettant la recherche de professionnels de santé dans l'Annuaire Santé de l'Agence du Numérique en Santé (ANS).",
        "externalDocs": {
            "description": "Documentation de l'annaire de sante de l'ANS",
            "url": "https://annuaire.sante.fr/",
        },
    },
    {
        "name": APITags.beneficiaires_transparence_sante,
        "description": "Routes permettant la recherche de bénéficiaires de Transparence Santé (TS)",
        "externalDocs": {
            "description": "Lien vers Transparence Santé",
            "url": "https://transparence.sante.gouv.fr",
        },
    },
]

app = FastAPI(
    title="ADEX",
    description="Un projet porté par la Haute Autorité de Santé et le programme Entrepreneurs d'Intérêt Général.",
    openapi_url=f"{settings.API_V1_STR}/openapi.json",
    openapi_tags=tags_metadata,
)

# Set all CORS enabled origins
if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[
            str(origin) for origin in settings.BACKEND_CORS_ORIGINS
        ],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


app.include_router(api_router, prefix=settings.API_V1_STR)
