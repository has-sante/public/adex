# pylint: disable=too-few-public-methods
from sqlalchemy import Column, Date, Enum, Float, String

from app.db.base_class import Base
from app.schemas import LienInteret


class Declaration(Base):
    __tablename__ = "ts_declarations"
    declaration_id = Column(String, primary_key=True)

    lien_interet = Column(Enum(LienInteret))
    entreprise_identifiant = Column(String)
    denomination_sociale = Column(String)
    identifiant_unique = Column(String)
    categorie_beneficiaire_code = Column(String)
    categorie_beneficiaire_libelle = Column(String)
    prenom = Column(String)
    profession = Column(String)
    pays_code = Column(String)
    pays = Column(String)
    type_identifiant = Column(String)
    numero_identifiant = Column(String)
    structure_exercice = Column(String)
    date = Column(Date)
    motif = Column(String)
    autre_motif = Column(String)
    date_debut = Column(Date)
    date_fin = Column(Date)
    montant = Column(Float)
    identite_nom = Column(String)
    code_postal = Column(String)
    adresse = Column(String)
    ville = Column(String)
    information_convention = Column(String)
    identite_nom_normalized = Column(String)
    prenom_normalized = Column(String)
    benef_hash = Column(String)
    convention_description = Column(String)
    convention_id = Column(String)
