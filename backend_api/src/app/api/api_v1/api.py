from fastapi import APIRouter

from app.api.api_v1.endpoints import (
    annuaire_ans,
    beneficiaires_ts,
    declaration_summaries,
    info,
    ts_declaration_avantage,
    ts_declaration_convention,
    ts_declaration_remuneration,
)
from app.core.constants import APITags

api_router = APIRouter()

api_router.include_router(
    declaration_summaries.router,
    prefix="/declaration_summaries",
    tags=["declaration_summaries"],
)

api_router.include_router(
    beneficiaires_ts.router,
    prefix="/beneficiaires_ts",
    tags=[APITags.beneficiaires_transparence_sante],
)

api_router.include_router(
    annuaire_ans.router, prefix="/annuaire_ans", tags=[APITags.annuaire_ans]
)

api_router.include_router(
    ts_declaration_avantage.router,
    prefix="/ts_declaration_avantages",
    tags=["declarations", "avantages"],
)
api_router.include_router(
    ts_declaration_remuneration.router,
    prefix="/ts_declaration_remunerations",
    tags=["declarations", "remunerations"],
)
api_router.include_router(
    ts_declaration_convention.router,
    prefix="/ts_declaration_conventions",
    tags=["declarations", "conventions"],
)

api_router.include_router(
    info.router,
    prefix="/info",
    tags=["métadonnées"],
)
