from fastapi import APIRouter

from app import schemas
from app.crud import crud_beneficiaires_ts

router = APIRouter()


@router.post(
    "/recherche",
    response_model=schemas.RecipientSearchResults,
    summary="Recherche directe de bénéficiaires de TS",
)
def search_beneficiares_ts(
    query: schemas.SearchQueryFree,
) -> schemas.RecipientSearchResults:
    """
    Lance une recherche dans les bénéficiaires de TS par nom / prénom.
    """
    recipients = crud_beneficiaires_ts.get_multi_by_search(
        search=query.query,
        page_current=query.page_current,
        page_size=query.page_size,
    )
    return recipients


@router.post(
    "/depuis_annuaire_ans",
    response_model=schemas.RecipientSearchResults,
    summary="Recherche dans les bénéficiaires de TS via une entrée de l'annuaire ANS",
)
def get_beneficiaires_ts_depuis_annuaire_ans(
    query: schemas.SearchQueryId,
) -> schemas.RecipientSearchResults:
    """
    À partir d'une entrée de l'annuaire ANS, lance une recherche à partir de l'identifiant et du nom / prénom.
    """
    recipients = crud_beneficiaires_ts.get_from_directory(
        _id=query.identification_nationale_pp,
        page_current=query.page_current,
        page_size=query.page_size,
    )
    return recipients


@router.post(
    "/surs_depuis_annuaire_ans",
    response_model=schemas.RecipientSearchResults,
    summary="Recherche de bénéficiaires de TS sûrs via une entrée de l'annuaire ANS",
)
def get_beneficiaires_ts_surs_depuis_annuaire_ans(
    query: schemas.SearchQueryId,
) -> schemas.RecipientSearchResults:
    """
    À partir d'une entrée de l'annuaire ANS, lance une recherche à partir de l'identifiant et du nom / prénom.
    Les résultats retournées sont très probablement la personne recherchée.
    """
    recipients = crud_beneficiaires_ts.get_reliable_from_directory(
        _id=query.identification_nationale_pp,
        page_current=query.page_current,
        page_size=query.page_size,
    )
    return recipients


@router.post(
    "/non_surs_depuis_annuaire_ans",
    response_model=schemas.RecipientSearchResults,
    summary="Recherche de bénéficiaires de TS non sûrs via une entrée de l'annuaire ANS",
)
def get_beneficiaires_ts_non_surs_depuis_annuaire_ans(
    query: schemas.SearchQueryId,
) -> schemas.RecipientSearchResults:
    """
    À partir d'une entrée de l'annuaire ANS, lance une recherche à partir de l'identifiant et du nom / prénom.
    Les résultats retournées peuvent être la personne recherchée.
    Mais aucun n'est évident.
    """
    not_reliable_recipients = (
        crud_beneficiaires_ts.get_not_reliable_from_directory(
            _id=query.identification_nationale_pp,
            page_current=query.page_current,
            page_size=query.page_size,
        )
    )
    return not_reliable_recipients
