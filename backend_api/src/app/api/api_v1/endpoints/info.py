from typing import List

from fastapi import APIRouter

from app import schemas
from app.crud import crud_info

router = APIRouter()


@router.get("/info", response_model=schemas.Metadata)
def get_info() -> schemas.Metadata:
    """
    retrieve infos
    """

    info = crud_info.get_info_from_es()
    return info
