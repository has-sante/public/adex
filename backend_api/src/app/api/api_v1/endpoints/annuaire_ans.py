from fastapi import APIRouter

from app import schemas
from app.crud import crud_annuaire_ans

router = APIRouter()


@router.post(
    "/recherche",
    response_model=schemas.ProfessionalSearchResult,
    summary="Recherche un professionnel",
)
def search_professionals(
    query: schemas.SearchQueryDirectory,
) -> schemas.ProfessionalSearchResult:
    """
    Cette route permet de recherche un Professionnel de Santé en mentionnant :
    - Un nom (Avec une certaine souplesse dans l'orthographe)
    - Un nom + ville
    - Un identifiant RPPS / ADELI

    Il y a deux paramêtres :
    - highlights : permet d'encapsuler entre les tags `<mark>` / `</mark>` les parties du résultats qui correspondent à la recherche
    - raw_data : retourne dans le champ "children" toutes les infos, sinon retourne uniquement les informations considérées comme discriminantes par ADEX
    """
    professionals = crud_annuaire_ans.get_multi_by_search(
        search=query.query,
        page_current=query.page_current,
        page_size=query.page_size,
        highlight=query.highlight,
        raw_data=query.raw_data,
    )
    return professionals


@router.get(
    "/{identification_nationale_pp}",
    response_model=schemas.ProfessionalElasticResult,
    summary="Récupère toutes les données liées à un professionnel",
)
def get_professional(
    identification_nationale_pp: str,
) -> schemas.ProfessionalElasticResult:
    """
    À partir de l'identifiant "identification_nationale_pp" récupéré dans les suggestions de la recherche
    """
    professional = crud_annuaire_ans.get_by_id(
        identification_nationale_pp=identification_nationale_pp
    )
    return professional
