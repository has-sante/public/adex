from typing import List

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app import schemas
from app.api import deps
from app.crud import crud_ts_declaration

router = APIRouter()


@router.post("/by_hashes", response_model=List[schemas.Remuneration])
def get_by_hashes(
    query: schemas.SearchQueryHashes,
    db: Session = Depends(deps.get_db),
) -> List[schemas.Remuneration]:
    """
    From recipients hashes will fetch remunerations from ts
    """
    hashes = query.hashes
    limit = query.limit
    skip = query.skip
    remunerations = crud_ts_declaration.get_remunerations_by_hashes(
        hashes=hashes, db=db, limit=limit, skip=skip
    )
    return remunerations
