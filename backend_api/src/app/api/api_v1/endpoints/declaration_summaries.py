from datetime import datetime
from typing import Any, Dict, List

from fastapi import APIRouter, Depends
from fastapi.responses import FileResponse
from sqlalchemy.orm import Session

from app import schemas
from app.api import deps
from app.crud import (
    crud_annuaire_ans,
    crud_declaration_summary,
    crud_declaration_summary_rows,
)

router = APIRouter()


@router.post(
    "/synthese/", response_model=List[schemas.RowSyntheseDeclarations]
)
def read_synthese_by_hashes(
    query: schemas.SearchQueryHashes,
    db: Session = Depends(deps.get_db),
) -> List[schemas.RowSyntheseDeclarations]:
    """
    Will fetch summary from avantages, rémunérations,conventions
    """
    hashes = query.hashes
    synthese = crud_declaration_summary_rows.get_synthese_by_hashes(hashes, db)
    return synthese


@router.get(
    "/synthese_export/",
)
def read_synthese_export(
    hashes: str,
    identification_nationale_pp: str = None,
    db: Session = Depends(deps.get_db),
) -> FileResponse:
    """
    Will fetch summary from avantages, rémunérations,conventions
    """
    hashes = hashes.split(",")
    date_now = datetime.now()
    formatted_date_now = datetime.strftime(date_now, "%d_%m_%Y_%H_%M")
    if identification_nationale_pp is not None:
        professional = crud_annuaire_ans.get_by_id(identification_nationale_pp)
        filename = (
            "_".join(
                (
                    "ADEX_EXPORT_"
                    + professional.noms_complets_exercice[0].nom_exercice,
                    professional.noms_complets_exercice[0].prenom_exercice,
                    professional.identification.libelle_type_identifiant_pp,
                    professional.identification.identifiant_pp,
                    formatted_date_now,
                )
            )
            + ".xlsx"
        )
    else:
        professional = None
        filename = "_".join(("ADEX_EXPORT_", formatted_date_now)) + ".xlsx"

    p_summary = crud_declaration_summary.generate_summary(
        hashes, professional=professional, db=db
    )
    return FileResponse(p_summary, filename=filename)
