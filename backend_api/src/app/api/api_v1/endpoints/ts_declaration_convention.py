from typing import List

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app import schemas
from app.api import deps
from app.crud import crud_ts_declaration

router = APIRouter()


@router.post("/by_hashes", response_model=List[schemas.Convention])
def get_by_hashes(
    query: schemas.SearchQueryHashes,
    db: Session = Depends(deps.get_db),
) -> List[schemas.Convention]:
    """
    From recipients hashes will fetch conventions from ts
    """
    hashes = query.hashes
    limit = query.limit
    skip = query.skip
    conventions = crud_ts_declaration.get_conventions_by_hashes(
        hashes=hashes, db=db, limit=limit, skip=skip
    )
    return conventions
