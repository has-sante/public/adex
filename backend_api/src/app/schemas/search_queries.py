# pylint: disable=no-name-in-module
# pylint: disable=too-few-public-methods
from typing import List

from pydantic import BaseModel


# Shared properties
class SearchQueryBase(BaseModel):
    page_current: int = 0
    page_size: int = 20


class SearchQueryFree(SearchQueryBase):
    query: str


class SearchQueryDirectory(SearchQueryBase):
    query: str
    highlight: bool = True
    raw_data: bool = False


class SearchQueryId(SearchQueryBase):
    identification_nationale_pp: str


class SearchQueryHashes(BaseModel):
    hashes: List[str]
    limit: int = 10
    skip: int = 0
