# pylint: disable=no-name-in-module
# pylint: disable=too-few-public-methods
from typing import List, Optional

from pydantic import BaseModel

from app.schemas.ts_declaration import Avantage, Convention, Remuneration


class AllDeclarationAggregate(BaseModel):
    conventions: Optional[List[Convention]]
    remunerations: Optional[List[Remuneration]]
    avantages: Optional[List[Avantage]]
