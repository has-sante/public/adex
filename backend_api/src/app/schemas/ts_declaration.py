# pylint: disable=no-name-in-module
# pylint: disable=too-few-public-methods

import enum
from datetime import date
from typing import Literal, Optional, Union

from pydantic import BaseModel


class LienInteret(enum.Enum):
    convention = "convention"
    remuneration = "remuneration"
    avantage = "avantage"


class DeclarationBase(BaseModel):
    declaration_id: Optional[str]

    entreprise_identifiant: Optional[str]
    denomination_sociale: Optional[str]
    identifiant_unique: Optional[str]
    categorie_beneficiaire_code: Optional[str]
    categorie_beneficiaire_libelle: Optional[str]
    prenom: Optional[str]
    profession: Optional[str]
    pays_code: Optional[str]
    pays: Optional[str]
    type_identifiant: Optional[str]
    numero_identifiant: Optional[str]
    structure_exercice: Optional[str]
    date: Optional[date]
    motif: Optional[str]
    autre_motif: Optional[str]
    date_debut: Optional[date]
    date_fin: Optional[date]
    montant: Optional[float]
    identite_nom: Optional[str]
    code_postal: Optional[str]
    adresse: Optional[str]
    ville: Optional[str]
    information_convention: Optional[str]
    identite_nom_normalized: Optional[str]
    prenom_normalized: Optional[str]
    benef_hash: Optional[str]
    convention_description: Optional[str]
    convention_id: Optional[str]

    class Config:
        orm_mode = True


class Convention(DeclarationBase):
    lien_interet: Literal[LienInteret.convention]


class Remuneration(DeclarationBase):
    lien_interet: Literal[LienInteret.remuneration]


class Avantage(DeclarationBase):
    lien_interet: Literal[LienInteret.avantage]


Declaration = Union[
    Convention,
    Remuneration,
    Avantage,
]
