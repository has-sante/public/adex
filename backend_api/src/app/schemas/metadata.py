# pylint: disable=no-name-in-module
# pylint: disable=too-few-public-methods
from pydantic import BaseModel


class MetadataBase(BaseModel):
    commit_sha: str
    date_maj_donnees: str
    version: str


# Properties to receive on item creation
class MetadataCreate(MetadataBase):
    pass


# Properties to receive on item update
class MetadataUpdate(MetadataBase):
    pass


# Properties to return to client
class Metadata(MetadataBase):
    pass
