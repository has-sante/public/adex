# pylint: disable=no-name-in-module
# pylint: disable=too-few-public-methods
from typing import List, Optional

from pydantic import BaseModel


# Shared properties
class RecipientBase(BaseModel):
    categorie_beneficiaire_libelle: Optional[str]
    prenom: Optional[str]
    profession: Optional[str]
    pays_code: Optional[str]
    pays: Optional[str]
    type_identifiant: Optional[str]
    numero_identifiant: Optional[str]
    structure_exercice: Optional[str]
    identite_nom: Optional[str]
    code_postal: Optional[str]
    adresse: Optional[str]
    ville: Optional[str]
    identite_nom_normalized: Optional[str]
    prenom_normalized: Optional[str]
    benef_hash: Optional[str]


class RecipientGroup(RecipientBase):
    group_size: int
    children: List[RecipientBase]


class RecipientSearch(RecipientGroup):
    score: float
    identification_nationale_pp: str


class RecipientSearchResults(BaseModel):
    total: int
    data: List[RecipientSearch]
