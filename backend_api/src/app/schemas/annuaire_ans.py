# pylint: disable=no-name-in-module
# pylint: disable=too-few-public-methods
from typing import List, Optional

from pydantic import BaseModel


class Professional(BaseModel):
    type_identifiant_pp: Optional[int]
    identifiant_pp: Optional[str]
    identification_nationale_pp: Optional[str]
    code_civilite_exercice: Optional[str]
    libelle_civilite_exercice: Optional[str]
    code_civilité: Optional[str]
    libelle_civilite: Optional[str]
    nom_exercice: str
    prenom_exercice: str
    code_profession: Optional[str]
    libelle_profession: Optional[str]
    code_categorie_professionnelle: Optional[str]
    libelle_categorie_professionnelle: Optional[str]
    code_type_savoir_faire: Optional[str]
    libelle_type_savoir_faire: Optional[str]
    code_savoir_faire: Optional[str]
    libelle_savoir_faire: Optional[str]
    code_mode_exercice: Optional[str]
    libelle_mode_exercice: Optional[str]
    numero_siret_site: Optional[str]
    numero_siren_site: Optional[str]
    numero_finess_site: Optional[str]
    numero_finess_etablissement_juridique: Optional[str]
    raison_sociale_site: Optional[str]
    enseigne_commerciale_site: Optional[str]
    numero_voie_coord_structure: Optional[str]
    indice_repetition_voie_coord_structure: Optional[str]
    code_type_voie_coord_structure: Optional[str]
    libelle_type_voie_coord_structure: Optional[str]
    libelle_voie_coord_structure: Optional[str]
    bureau_cedex_coord_structure: Optional[str]
    code_postal_coord_structure: Optional[str]
    code_commune_coord_structure: Optional[str]
    libelle_commune_coord_structure: Optional[str]
    code_pays_coord_structure: Optional[str]
    libelle_pays_coord_structure: Optional[str]
    telephone_coord_structure: Optional[str]
    telephone_2_coord_structure: Optional[str]
    telecopie_coord_structure: Optional[str]
    adresse_email_coord_structure: Optional[str]
    code_departement_structure: Optional[str]
    libelle_department_structure: Optional[str]
    complement_destinataire_coord_structure: Optional[str]
    complement_point_geographique_coord_structure: Optional[str]
    mention_distribution_coord_structure: Optional[str]
    identifiant_technique_structure: Optional[str]
    ancien_identifiant_structure: Optional[str]
    autorite_enregistrement: Optional[str]
    code_secteur_activite: Optional[str]
    libelle_secteur_activite: Optional[str]
    code_section_tableau_pharmaciens: Optional[str]
    libelle_section_tableau_pharmaciens: Optional[str]
    address: Optional[str]
    libelle_type_identifiant: Optional[str]


class ProfessionalIdentification(BaseModel):
    code_type_identifiant_pp: Optional[str]
    libelle_type_identifiant_pp: Optional[str]
    identifiant_pp: Optional[str]
    identification_nationale_pp: Optional[str]


class ProfessionalName(BaseModel):
    libelle_civilite: Optional[str]
    libelle_civilite_exercice: Optional[str]
    nom_exercice: Optional[str]
    prenom_exercice: Optional[str]


class ProfessionalActivityPlace(BaseModel):
    adresse_complete: Optional[str]
    numero_voie_coord_structure: Optional[str]
    indice_repetition_voie_coord_structure: Optional[str]
    libelle_type_voie_coord_structure: Optional[str]
    libelle_voie_coord_structure: Optional[str]
    code_postal_coord_structure: Optional[str]
    libelle_commune_coord_structure: Optional[str]
    libelle_pays_coord_structure: Optional[str]
    libelle_department_structure: Optional[str]
    raison_sociale_site: Optional[str]


# class ProfessionalGroup(BaseModel):
#     group_size: int
#     identification_nationale_pp: str
#     children: List[Professional]


class ProfessionalElasticResult(BaseModel):
    score: float
    identification_nationale_pp: str
    identification: ProfessionalIdentification
    lieux_activite: List[ProfessionalActivityPlace]
    noms_complets_exercice: List[ProfessionalName]
    professions: List[str]
    specialites: List[str]
    children: Optional[List[Professional]]


class ProfessionalSearchResult(BaseModel):
    total: int
    data: List[ProfessionalElasticResult]
