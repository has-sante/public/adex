# pylint: disable=no-name-in-module
# pylint: disable=too-few-public-methods
import datetime
from typing import List, Literal, Optional, Union

from pydantic import BaseModel

from app.schemas.ts_declaration import (
    Avantage,
    Convention,
    LienInteret,
    Remuneration,
)


class SyntheseDeclaration(BaseModel):
    date: datetime.date
    montant: float = 0


class SyntheseRemuneration(SyntheseDeclaration):
    source: Remuneration


class SyntheseAvantage(SyntheseDeclaration):
    source: Avantage


class SyntheseConvention(SyntheseDeclaration):
    remunerations: List[SyntheseRemuneration]
    avantages: List[SyntheseAvantage]
    source: Convention


class Montants(BaseModel):
    # Affichage colonne Montant avantages
    montants_avantages: float = None
    # Affichage colonne Montant remunerations
    montants_remunerations: float = None
    # Affichage colonne Montant conventions
    montant_convention: float = None

    # Pour avant / rému seuls, min et max seront les mêmes que le montant
    # de la déclaration.
    # Pour les convention : cf issue #44 sur gitlab
    # https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/44
    # Pour les avantages et rémunérations liées à la convention, on met max et min à 0.

    # Ainsi, pour calculer le min des liens d'intérêt : sum(min_montant)
    # Et pour calculer le max : sum(max_montant)

    # Dans le cas d'une convention : max (C, sum(A_liés, R_liés))
    max_montant: float = None
    # Dans le cas d'une convention : sum(A_liés, R_liés)
    min_montant: float = None


class RowSyntheseDeclarationsBase(BaseModel):
    benef_hash: str
    entreprise: str
    annee: Optional[str]
    annee_debut: str
    annee_fin: Optional[str]

    montants: Montants


class RowSyntheseDeclarationsConvention(RowSyntheseDeclarationsBase):
    declaration_type: Literal[LienInteret.convention]
    declaration: SyntheseConvention

    @property
    def full_convention_objet(self) -> str:
        if self.declaration.source.motif:
            convention_objet = f"{self.declaration.source.motif}"
        else:
            convention_objet = "Vide"

        if self.declaration.source.autre_motif:
            convention_objet += f" / {self.declaration.source.autre_motif}"

        if self.declaration.source.information_convention:
            convention_objet += (
                f"\n({self.declaration.source.information_convention})"
            )

        convention_objet += "\nDate de début : {date_debut}".format(
            date_debut=datetime.datetime.strftime(
                self.declaration.source.date_debut, "%Y-%m-%d"
            )
        )
        if self.declaration.source.date_fin:
            convention_objet += " - Date de fin : {date_fin}".format(
                date_fin=datetime.datetime.strftime(
                    self.declaration.source.date_fin, "%Y-%m-%d"
                )
            )

        return convention_objet


class RowSyntheseDeclarationsRemuneration(RowSyntheseDeclarationsBase):
    declaration_type: Literal[LienInteret.remuneration]
    declaration: SyntheseRemuneration


class RowSyntheseDeclarationsAvantage(RowSyntheseDeclarationsBase):
    declaration_type: Literal[LienInteret.avantage]
    declaration: SyntheseAvantage


RowSyntheseDeclarations = Union[
    RowSyntheseDeclarationsConvention,
    RowSyntheseDeclarationsRemuneration,
    RowSyntheseDeclarationsAvantage,
]
