import enum


class APITags(str, enum.Enum):
    annuaire_ans = "Annuaire Santé de l'ANS"
    beneficiaires_transparence_sante = "Bénéficiaires de Transparence Santé"
