from collections import defaultdict
from typing import List

from app import schemas


def all_declarations_to_synthese_rows(
    all_declarations: schemas.AllDeclarationAggregate,
    sort: bool = True,
) -> List[schemas.RowSyntheseDeclarations]:
    conventions = all_declarations.conventions
    remunerations = all_declarations.remunerations
    avantages = all_declarations.avantages

    remunerations_convention_id = defaultdict(list)
    remunerations_alone = []
    for remuneration in remunerations:
        if remuneration.convention_id:
            remunerations_convention_id[remuneration.convention_id].append(
                remuneration
            )
        else:
            remunerations_alone.append(remuneration)

    avantages_convention_id = defaultdict(list)
    avantages_alone = []
    for avantage in avantages:
        if avantage.convention_id:
            avantages_convention_id[avantage.convention_id].append(avantage)
        else:
            avantages_alone.append(avantage)

    synthese_rows = []

    for convention in conventions:
        synthese_convention = schemas.SyntheseConvention(
            date=convention.date,
            montant=convention.montant,
            source=convention,
            remunerations=[
                remuneration_to_synthese(r)
                for r in remunerations_convention_id[convention.declaration_id]
            ],
            avantages=[
                avantage_to_synthese(a)
                for a in avantages_convention_id[convention.declaration_id]
            ],
        )
        synthese_convention_row = schemas.RowSyntheseDeclarationsConvention(
            benef_hash=convention.benef_hash,
            annee_debut=convention.date_debut.year,
            entreprise=convention.denomination_sociale,
            declaration=synthese_convention,
            montants=get_montants_convention(synthese_convention),
            annee_fin=str(get_max_year_of_convention(synthese_convention)),
            declaration_type=schemas.LienInteret.convention,
        )

        # On a besoin du calcul de annee_fin pour créer annee_literal
        annee_literal = get_annee_literal(synthese_convention_row)
        synthese_convention_row.annee = annee_literal

        remunerations_convention_id.pop(convention.declaration_id)
        avantages_convention_id.pop(convention.declaration_id)

        synthese_rows.append(synthese_convention_row)

    for convention_id in remunerations_convention_id:
        remunerations_to_push = remunerations_convention_id[convention_id]
        for remuneration in remunerations_to_push:
            synthese_rows.append(remuneration_to_synthese_row(remuneration))

    for convention_id in avantages_convention_id:
        avantages_to_push = avantages_convention_id[convention_id]
        for avantage in avantages_to_push:
            synthese_rows.append(avantage_to_synthese_row(avantage))

    for remuneration in remunerations_alone:
        synthese_rows.append(remuneration_to_synthese_row(remuneration))

    for avantage in avantages_alone:
        synthese_rows.append(avantage_to_synthese_row(avantage))
    if sort:
        sort_key = lambda row: (
            row.entreprise,
            str(row.annee),
            row.declaration.date,
        )
        synthese_rows = sorted(synthese_rows, key=sort_key)
        for synthese_row in synthese_rows:
            if synthese_row.declaration_type == schemas.LienInteret.convention:
                synthese_row.declaration.remunerations = sorted(
                    synthese_row.declaration.remunerations,
                    key=lambda remuneration_synthese: remuneration_synthese.source.date,
                )
                synthese_row.declaration.avantages = sorted(
                    synthese_row.declaration.avantages,
                    key=lambda avantage_synthese: avantage_synthese.source.date,
                )

    return synthese_rows


def get_montants_convention(
    synthese_convention: schemas.SyntheseConvention,
) -> schemas.Montants:
    montant_convention = synthese_convention.montant
    sum_remunerations = sum(
        r.montant for r in synthese_convention.remunerations
    )
    sum_avantages = sum(a.montant for a in synthese_convention.avantages)
    return schemas.Montants(
        montant_convention=montant_convention,
        montants_remunerations=sum_remunerations,
        montants_avantages=sum_avantages,
        min_montant=sum_remunerations + sum_avantages,
        max_montant=max(montant_convention, sum_avantages + sum_remunerations),
    )


def get_max_year_of_convention(
    convention_synthese: schemas.SyntheseConvention,
) -> int:
    convention = convention_synthese.source
    if convention.date_fin is None:
        max_year = convention.date_debut.year
    else:
        max_year = max(convention.date_debut.year, convention.date_fin.year)

    for avantage in convention_synthese.avantages:
        if avantage.date.year > max_year:
            max_year = avantage.date.year
    for remuneration in convention_synthese.remunerations:
        if remuneration.date.year > max_year:
            max_year = remuneration.date.year
    return max_year


def get_annee_literal(
    synthese_row: schemas.RowSyntheseDeclarations,
) -> str:
    if (synthese_row.annee_fin is None) or (
        synthese_row.annee_fin == synthese_row.annee_debut
    ):
        return str(synthese_row.annee_debut)
    else:
        return " - ".join((synthese_row.annee_debut, synthese_row.annee_fin))


def remuneration_to_synthese_row(
    remuneration: schemas.Remuneration,
) -> schemas.RowSyntheseDeclarationsRemuneration:
    year = remuneration.date.year
    synthese_remuneration = remuneration_to_synthese(remuneration)
    synthese_remuneration_row = schemas.RowSyntheseDeclarationsRemuneration(
        benef_hash=remuneration.benef_hash,
        entreprise=remuneration.denomination_sociale,
        annee=year,
        annee_debut=year,
        annee_fin=year,
        montants=schemas.Montants(
            montants_remunerations=remuneration.montant,
            min_montant=remuneration.montant,
            max_montant=remuneration.montant,
        ),
        declaration_type=schemas.LienInteret.remuneration,
        declaration=synthese_remuneration,
    )

    return synthese_remuneration_row


def remuneration_to_synthese(
    remuneration: schemas.Remuneration,
) -> schemas.SyntheseRemuneration:
    return schemas.SyntheseRemuneration(
        date=remuneration.date,
        montant=remuneration.montant,
        source=remuneration,
    )


def avantage_to_synthese_row(
    avantage: schemas.Avantage,
) -> schemas.RowSyntheseDeclarationsAvantage:
    year = avantage.date.year
    synthese_avantage = avantage_to_synthese(avantage)

    synthese_avantage_row = schemas.RowSyntheseDeclarationsAvantage(
        benef_hash=avantage.benef_hash,
        entreprise=avantage.denomination_sociale,
        annee=year,
        annee_debut=year,
        annee_fin=year,
        montants=schemas.Montants(
            montants_avantages=avantage.montant,
            min_montant=avantage.montant,
            max_montant=avantage.montant,
        ),
        declaration=synthese_avantage,
        declaration_type=schemas.LienInteret.avantage,
    )
    return synthese_avantage_row


def avantage_to_synthese(
    avantage: schemas.Avantage,
) -> schemas.SyntheseAvantage:
    # TODO: Créer un champ "avantage_description" contenant les bonnes infos lors du
    # preprocessing
    if avantage.convention_description is not None:
        avantage.autre_motif = (
            f"{avantage.autre_motif or ''}"
            f" (Description convention liée : {avantage.convention_description})".strip()
        )

    return schemas.SyntheseAvantage(
        source=avantage,
        montant=avantage.montant,
        date=avantage.date,
    )
