import pathlib
from collections import defaultdict
from datetime import datetime
from itertools import groupby
from typing import Any, List, Tuple

import pandas as pd
import pytz
from openpyxl import Workbook, load_workbook
from openpyxl.styles import Alignment, Font, PatternFill
from openpyxl.styles.colors import Color
from openpyxl.utils import get_column_letter
from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl.worksheet.hyperlink import Hyperlink
from openpyxl.worksheet.table import Table, TableStyleInfo
from openpyxl.worksheet.worksheet import Worksheet
from sqlalchemy.orm import Session

from app import schemas

# Le fichier notice.md est un sous ensemble du fichier faq.md
# du frontend.
# Pour mettre à jour le template avec cette notice, il faut
# d'abord modifier le fichier notice.md.
# Puis l'intégrer dans le template.
# Par exemple en utilisant pandoc
#   pandoc notice.md -t docx -o notice.docx
# Puis en faisant un copier excel dans le template.
P_TEMPLATE = str(pathlib.Path(__file__).parent / "template.xlsx")

FONT_ENTREPRISE = Font(bold=True, color=Color(theme=0))
FONT_YEAR = Font(bold=True, color=Color(theme=0))
FONT_ROW = Font(color=Color(theme=1))
FONT_CONVENTION_ROW = Font(bold=True, color=Color(theme=1))

FILL_ENTREPRISE = PatternFill(
    start_color=Color(theme=3), end_color=Color(theme=3), fill_type="solid"
)
FILL_YEAR = PatternFill(
    start_color=Color(theme=6), end_color=Color(theme=6), fill_type="solid"
)
FILL_DARK_ROW = PatternFill(
    start_color=Color(theme=6, tint=0.8),
    end_color=Color(theme=6, tint=0.8),
    fill_type="solid",
)
FILL_LIGHT_ROW = PatternFill(
    start_color=Color(theme=0), end_color=Color(theme=6), fill_type="solid"
)
START_SUMMARY_ROW = 10

DATA_TAB_COLUMNS = [
    "identite_nom_normalized",
    "prenom_normalized",
    "categorie_beneficiaire_code",
    "categorie_beneficiaire_libelle",
    "profession",
    "type_identifiant",
    "numero_identifiant",
    "code_postal",
    "ville",
    "adresse",
    "structure_exercice",
    "entreprise_identifiant",
    "denomination_sociale",
    "pays_code",
    "pays",
    "date",
    "lien_interet",
    "montant",
    "motif",
    "autre_motif",
    "information_convention",
    "date_debut",
    "date_fin",
    "benef_hash",
    "prenom",
    "identite_nom",
    "identifiant_unique",
    "declaration_id",
    "convention_description",
    "convention_id",
]


class SummaryColumns:
    entreprise = 1
    annee = 2
    date = 3
    declaration_type = 4
    motif = 5
    montant_convention = 6
    montant_remunerations = 7
    montant_avantages = 8
    montant_max = 9


class ExcelSummaryCreator:
    def __init__(
        self,
        professional: schemas.ProfessionalElasticResult,
        declarations: pd.DataFrame,
        summary_rows: List[schemas.RowSyntheseDeclarations],
        metadata: schemas.Metadata,
    ):
        self.professional = professional
        self.declarations = declarations
        self.summary_rows = self.group_summary_rows(summary_rows)
        self.metadata = metadata

        self.current_summary_line = 1

        self.workbook = self.create_workbook()

    def create_workbook(self) -> Workbook:
        workbook = load_workbook(P_TEMPLATE)
        self._summary_sheet = workbook.create_sheet("Synthèse", index=0)
        self._data_sheet = workbook.create_sheet("Données brutes", index=1)
        return workbook

    def group_summary_rows(
        self,
        summary_rows: List[schemas.RowSyntheseDeclarations],
    ) -> schemas.RowSyntheseDeclarations:
        grouped_rows = defaultdict(dict)
        for entreprise, rows in groupby(
            summary_rows, key=lambda data: data.entreprise
        ):
            for annee, content in groupby(
                rows, key=lambda data: str(data.annee)
            ):
                grouped_rows[entreprise][annee] = list(content)
        return grouped_rows

    def create(self) -> Workbook:
        self.create_data_sheet()
        self.create_summary()
        # self.create_test_table()
        self.workbook.active = 0
        for sheet in self.workbook:
            if sheet.title == "Synthèse":
                sheet.sheet_view.tabSelected = True
            else:
                sheet.sheet_view.tabSelected = False

        return self.workbook

    def create_data_sheet(self) -> None:
        data_sheet = self._data_sheet

        for r in dataframe_to_rows(
            self.declarations[DATA_TAB_COLUMNS], index=False, header=True
        ):
            data_sheet.append(r)
        number_of_rows = self.declarations.shape[0]

        tab = Table(
            displayName="donnees_brutes",
            ref=f"A1:{get_column_letter(len(self.declarations.columns))}{number_of_rows+1}",
        )

        style = TableStyleInfo(name="TableStyleMedium7", showRowStripes=True)
        tab.tableStyleInfo = style
        data_sheet.add_table(tab)

    def create_summary(self) -> None:
        self.create_summary_header()
        self.create_summary_table_header()
        self.create_summary_table()
        self.create_summary_filters()
        self.adapt_columns_width()
        self.insert_totals()

    def get_formatted_date(
        self, date_to_format: datetime.date
    ) -> Tuple[str, str]:
        return (
            datetime.strftime(date_to_format, "%d/%m/%Y"),
            datetime.strftime(date_to_format, "%H:%M"),
        )

    def get_formatted_time_now(self) -> Tuple[str, str, str]:
        timezone_name = "Europe/Paris"
        timezone = pytz.timezone(timezone_name)
        date_now = datetime.now(timezone)
        date_formatted, time_formatted = self.get_formatted_date(date_now)
        return (
            date_formatted,
            time_formatted,
            timezone_name,
        )

    def _add_value_to_summary_cell(
        self,
        row: int,
        column: int,
        value: Any,
        font: Font = None,
        fill: PatternFill = None,
        alignment: Alignment = None,
    ) -> None:
        self._summary_sheet.cell(row=row, column=column).value = value
        if font:
            self._summary_sheet.cell(row=row, column=column).font = font
        if fill:
            self._summary_sheet.cell(row=row, column=column).fill = fill
        if alignment:
            self._summary_sheet.cell(
                row=row, column=column
            ).alignment = alignment

    def create_summary_header(self) -> None:
        if self.professional:
            self._add_value_to_summary_cell(
                row=1,
                column=1,
                value="Liens d'intérêts financiers recherchés pour {name} {firstname}".format(
                    name=self.professional.noms_complets_exercice[
                        0
                    ].nom_exercice,
                    firstname=self.professional.noms_complets_exercice[
                        0
                    ].prenom_exercice,
                ),
                font=Font(bold=True, size=18),
            )
        else:
            self._add_value_to_summary_cell(
                row=1,
                column=1,
                value="Liens d'intérêts financiers identifiés pour une recherche directe",
                font=Font(bold=True, size=18),
            )
        (
            formatted_export_creation_date_now,
            formatted_export_creation_time_now,
            timezone_name,
        ) = self.get_formatted_time_now()
        (
            formatted_data_processing_date_now,
            formatted_data_processing_time_now,
        ) = self.get_formatted_date(
            datetime.strptime(
                self.metadata["date_maj_donnees"], "%Y-%m-%dT%H:%M:%SZ"
            )
        )

        # TODO: move this below
        self._add_value_to_summary_cell(
            row=1,
            column=9,
            value=(
                f"{formatted_export_creation_date_now} {formatted_export_creation_time_now} ({timezone_name}): "
                "génération du fichier via ADEX à partir des déclarations de la base Transparence Santé"
            ),
            font=Font(italic=True, size=11, color=Color(theme=9)),
        )
        self._add_value_to_summary_cell(
            row=2,
            column=9,
            value=(
                f"{formatted_data_processing_date_now} {formatted_data_processing_time_now}: "
                "mise à jour des données de Transparence Santé dans ADEX"
            ),
            font=Font(italic=True, size=11, color=Color(theme=9)),
        )
        bold_font = Font(bold=True)

        if self.professional:
            self._add_value_to_summary_cell(
                row=2, column=1, value="Profession", font=bold_font
            )
            self._add_value_to_summary_cell(
                row=3, column=1, value="RPPS", font=bold_font
            )
            self._add_value_to_summary_cell(
                row=4, column=1, value="Lieu(x) d'exercice", font=bold_font
            )
            profession_specialite = (
                ", ".join(self.professional.professions)
                + " - "
                + ", ".join(self.professional.specialites)
            )
            self._add_value_to_summary_cell(
                row=2, column=3, value=profession_specialite
            )
            self._add_value_to_summary_cell(
                row=3,
                column=3,
                value=self.professional.identification.identification_nationale_pp,
            )
            self._add_value_to_summary_cell(
                row=4,
                column=3,
                value=" / ".join(
                    [
                        a.adresse_complete
                        for a in self.professional.lieux_activite
                    ]
                ),
            )

        self._add_value_to_summary_cell(
            row=6,
            column=1,
            value="Bénéficiaires sélectionnés dans Transparence-Santé",
            font=bold_font,
            alignment=Alignment(vertical="center"),
        )
        self._add_value_to_summary_cell(
            row=7,
            column=1,
            value='Voir détails dans l\'onglet "Données Brutes"',
            alignment=Alignment(vertical="center"),
            font=Font(italic=True, size=11, color=Color(theme=9)),
        )

        self._add_value_to_summary_cell(
            row=6,
            column=4,
            value="Noms prénoms",
            font=bold_font,
            alignment=Alignment(vertical="center"),
        )

        self._add_value_to_summary_cell(
            row=7,
            column=4,
            value="Professions",
            font=bold_font,
            alignment=Alignment(vertical="center"),
        )
        self._add_value_to_summary_cell(
            row=8,
            column=4,
            value="Identifiants",
            font=bold_font,
            alignment=Alignment(vertical="center"),
        )
        self._add_value_to_summary_cell(
            row=9,
            column=4,
            value="Villes",
            font=bold_font,
            alignment=Alignment(vertical="center"),
        )
        noms_prenoms = (
            self.declarations[["identite_nom_normalized", "prenom_normalized"]]
            .drop_duplicates()
            .to_dict(orient="records")
        )

        self._add_value_to_summary_cell(
            row=6,
            column=5,
            value=(
                ", ".join(
                    f'{b["identite_nom_normalized"]} {b["prenom_normalized"]}'
                    for b in noms_prenoms
                )
            ),
            alignment=Alignment(horizontal="left"),
        )

        professions = (
            self.declarations[["categorie_beneficiaire_libelle", "profession"]]
            .drop_duplicates()
            .to_dict(orient="records")
        )

        self._add_value_to_summary_cell(
            row=7,
            column=5,
            value=(
                ", ".join(
                    f'{(b["profession"]) if b["profession"] else b["categorie_beneficiaire_libelle"]}'
                    for b in professions
                )
            ),
            alignment=Alignment(horizontal="left"),
        )

        identifiants = (
            self.declarations[["type_identifiant", "numero_identifiant"]]
            .drop_duplicates()
            .to_dict(orient="records")
        )

        self._add_value_to_summary_cell(
            row=8,
            column=5,
            value=(
                ", ".join(
                    f'{b["type_identifiant"]} {b["numero_identifiant"] }'
                    for b in identifiants
                )
            ),
            alignment=Alignment(horizontal="left"),
        )
        villes = (
            self.declarations[["ville"]]
            .drop_duplicates()
            .to_dict(orient="records")
        )

        self._add_value_to_summary_cell(
            row=9,
            column=5,
            value=(", ".join(f'{b["ville"]}' for b in villes)),
            alignment=Alignment(horizontal="left"),
        )

    def insert_totals(self) -> None:
        self._summary_sheet.cell(row=1, column=6).value = "Montant total"
        self._summary_sheet.cell(row=1, column=6).font = Font(
            bold=True, size=14
        )

        montant_conv_col = "Montant maximal de la déclaration"
        montant_remu_col = "Montant de la rémunération"
        # On doit escape l'apostrophe dans le nom de la colonne dans la formule :
        # https://support.microsoft.com/en-gb/office/using-structured-references-with-excel-tables-f5ed2452-2337-4f71-bed3-c8ae6d2b276e
        # Use an escape character for some special characters in column headers
        # Some characters have special meaning and require the use of a single quotation mark (')
        # as an escape character.
        # For example: =DeptSalesFYSummary['#OfItems]
        montant_avant_col = "Montant de l''avantage"

        formula_total_remu_avant = f"=SUBTOTAL(109,tableau_synthese[{montant_remu_col}])+SUBTOTAL(109,tableau_synthese[{montant_avant_col}])"
        formula_total = f"=SUBTOTAL(109,tableau_synthese[{montant_conv_col}])"

        self._summary_sheet.cell(row=2, column=6).value = "Entre :"
        self._summary_sheet.cell(
            row=2, column=7
        ).value = formula_total_remu_avant
        self._summary_sheet.cell(row=2, column=7).font = Font(
            bold=True, size=14
        )

        self._summary_sheet.cell(row=3, column=6).value = "Et :"
        self._summary_sheet.cell(row=3, column=7).value = formula_total
        self._summary_sheet.cell(row=3, column=7).font = Font(
            bold=True, size=14
        )
        hyperlink_ref = f"{get_column_letter(6)}3"
        hyperlink_location = "#Notice!A1"
        hyperlink_name = "Comment est calculé ce montant ?"
        self._summary_sheet.cell(row=4, column=6).hyperlink = Hyperlink(
            display=hyperlink_name,
            ref=hyperlink_ref,
            target=hyperlink_location,
        )
        self._summary_sheet.cell(row=4, column=6).value = hyperlink_name
        self._summary_sheet.cell(row=4, column=6).font = Font(
            underline="single", color=Color(theme=6)
        )

    def create_summary_table(self) -> None:
        for entreprise in self.summary_rows:
            self.draw_row_entreprise(entreprise=entreprise)
            for annee in self.summary_rows[entreprise]:
                self.draw_row_annee(entreprise=entreprise, annee=annee)
                row_group = self.summary_rows[entreprise][annee]
                light_row = True
                for row in row_group:
                    if row.declaration_type == schemas.LienInteret.avantage:
                        self.draw_summary_avantage_row(row, light_row)
                        light_row = not light_row
                    elif (
                        row.declaration_type
                        == schemas.LienInteret.remuneration
                    ):
                        self.draw_summary_remuneration_row(row, light_row)
                        light_row = not light_row
                    elif (
                        row.declaration_type == schemas.LienInteret.convention
                    ):
                        self.draw_summary_convention_row(row, light_row)
                        for avantage in row.declaration.avantages:
                            self.draw_summary_avantage_partial_row(
                                entreprise=entreprise,
                                annee=annee,
                                avantage=avantage,
                                light_row=light_row,
                            )
                        for remuneration in row.declaration.remunerations:
                            self.draw_summary_remuneration_partial_row(
                                entreprise=entreprise,
                                annee=annee,
                                remuneration=remuneration,
                                light_row=light_row,
                            )
                        light_row = not light_row

    def create_summary_table_header(self):
        self.current_summary_line = START_SUMMARY_ROW
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.entreprise,
            value="Entreprise",
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.annee,
            value="Année",
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.date,
            value="Date",
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.declaration_type,
            value="Type declaration",
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.motif,
            value="Objet",
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.montant_convention,
            value="Montant déclaré dans la convention",
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.montant_remunerations,
            value="Montant de la rémunération",
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.montant_avantages,
            value="Montant de l'avantage",
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.montant_max,
            value="Montant maximal de la déclaration",
        )
        self._apply_style_to_row(
            row=self.current_summary_line,
            font=Font(color=Color(theme=9)),
            alignment=Alignment(horizontal="left", wrapText=True),
        )

        # Increase cell size
        self._summary_sheet.row_dimensions[
            self.current_summary_line
        ].height = 40

        self.increment_current_summary_line()

    def create_summary_filters(self) -> None:
        self._summary_sheet.freeze_panes = f"A{START_SUMMARY_ROW + 1}"
        tab = Table(
            displayName="tableau_synthese",
            ref="A{start_summary_row}:{last_column_number}{last_line}".format(
                start_summary_row=START_SUMMARY_ROW,
                last_column_number=get_column_letter(
                    SummaryColumns.montant_max
                ),
                last_line=self.current_summary_line - 1,
            ),
            totalsRowShown=True,
        )
        self._summary_sheet.add_table(tab)

    def draw_summary_avantage_row(
        self,
        row_avantage: schemas.RowSyntheseDeclarationsAvantage,
        light_row: bool,
    ) -> None:
        self._draw_summary_line(
            entreprise=row_avantage.entreprise,
            annee=row_avantage.annee,
            date=row_avantage.declaration.date,
            declaration_type=schemas.LienInteret.avantage.value,
            motif=row_avantage.declaration.source.autre_motif,
            montant_convention=None,
            montant_remunerations=None,
            montant_avantages=row_avantage.montants.montants_avantages,
            montant_max=row_avantage.montants.max_montant,
            light_row=light_row,
        )

        # Rémunérations non liées sont en gras
        self._apply_style_to_row(
            row=self.current_summary_line,
            font=Font(bold=True),
        )

        self.increment_current_summary_line()

    def draw_summary_avantage_partial_row(
        self,
        entreprise: str,
        annee: str,
        avantage: schemas.SyntheseAvantage,
        light_row: bool,
    ) -> None:
        self._draw_summary_line(
            entreprise=entreprise,
            annee=annee,
            date=avantage.date,
            declaration_type=schemas.LienInteret.avantage.value,
            motif=avantage.source.autre_motif,
            montant_convention=None,
            montant_remunerations=None,
            montant_avantages=avantage.montant,
            # Pour les avantages liées, on ignore le montant qui est contenu
            # dans le calcul du montant max de la convention
            montant_max=None,
            light_row=light_row,
        )

        self.increment_current_summary_line()

    def draw_summary_remuneration_partial_row(
        self,
        entreprise: str,
        annee: str,
        remuneration: schemas.SyntheseRemuneration,
        light_row: bool,
    ) -> None:
        self._draw_summary_line(
            entreprise=entreprise,
            annee=annee,
            date=remuneration.date,
            declaration_type=schemas.LienInteret.remuneration.value,
            motif=None,
            montant_convention=None,
            montant_remunerations=remuneration.montant,
            montant_avantages=None,
            # Pour les rémunérations liées, on ignore le montant qui est contenu
            # dans le calcul du montant max de la convention
            montant_max=None,
            light_row=light_row,
        )

        self.increment_current_summary_line()

    def draw_summary_remuneration_row(
        self,
        row_remuneration: schemas.RowSyntheseDeclarationsRemuneration,
        light_row: bool,
    ) -> None:
        self._draw_summary_line(
            entreprise=row_remuneration.entreprise,
            annee=row_remuneration.annee,
            date=row_remuneration.declaration.date,
            declaration_type=schemas.LienInteret.remuneration.value,
            motif=None,
            montant_convention=None,
            montant_remunerations=row_remuneration.montants.montants_remunerations,
            montant_avantages=None,
            montant_max=row_remuneration.montants.max_montant,
            light_row=light_row,
        )

        # Rémunérations non liées sont en gras
        self._apply_style_to_row(
            row=self.current_summary_line,
            font=Font(bold=True),
        )

        self.increment_current_summary_line()

    def draw_summary_convention_row(
        self,
        row_convention: schemas.RowSyntheseDeclarationsConvention,
        light_row: bool,
    ) -> None:
        """
        Avec le nouveau calcul du montant, il est nécessaire d'ignorer le
        montant aggrégé des rémunérations et des avantages dans la somme totale
        des avantages et des rémunérations.
        On utilise donc une formule SUBTOTAL afin de calculer ces sommes via des formules
        SUBTOTAL qui seront ignorées dans les formules globales.
        """

        if (len(row_convention.declaration.avantages) == 0) and (
            len(row_convention.declaration.remunerations) == 0
        ):
            # Si pas d'avantages ni de rémunérations, pas besoin d'insérer
            # une formule.
            montant_remunerations = 0
            montant_avantages = 0
        else:
            starting_row = self.current_summary_line + 1
            ending_row = (
                self.current_summary_line
                + len(row_convention.declaration.avantages)
                + len(row_convention.declaration.remunerations)
            )
            column_letter_remu = get_column_letter(
                SummaryColumns.montant_remunerations
            )
            column_letter_avant = get_column_letter(
                SummaryColumns.montant_avantages
            )
            montant_remunerations = f"=SUBTOTAL(109, {column_letter_remu}{starting_row}:{column_letter_remu}{ending_row})"
            montant_avantages = f"=SUBTOTAL(109, {column_letter_avant}{starting_row}:{column_letter_avant}{ending_row})"

        self._draw_summary_line(
            entreprise=row_convention.entreprise,
            annee=row_convention.annee,
            date=row_convention.declaration.date,
            declaration_type=schemas.LienInteret.convention.value,
            motif=row_convention.full_convention_objet,
            montant_convention=row_convention.montants.montant_convention,
            montant_remunerations=montant_remunerations,
            montant_avantages=montant_avantages,
            montant_max=row_convention.montants.max_montant,
            light_row=light_row,
        )

        self._apply_style_to_row(
            row=self.current_summary_line,
            font=Font(bold=True),
        )

        # Increase cell size
        self._summary_sheet.row_dimensions[
            self.current_summary_line
        ].height = 40

        self.increment_current_summary_line()

    def _draw_summary_line(
        self,
        entreprise: str,
        annee: str,
        date: datetime,
        declaration_type: str,
        motif: str,
        montant_convention: float,
        montant_remunerations: float,
        montant_avantages: float,
        montant_max: float,
        light_row: bool,
    ):
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.entreprise,
            value=entreprise,
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.annee,
            value=annee,
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.date,
            value=date,
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.declaration_type,
            value=declaration_type,
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.motif,
            value=motif,
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.montant_convention,
            value=montant_convention,
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.montant_remunerations,
            value=montant_remunerations,
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.montant_avantages,
            value=montant_avantages,
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.montant_max,
            value=montant_max,
        )
        if light_row:
            fill = FILL_LIGHT_ROW
        else:
            fill = FILL_DARK_ROW
        self._apply_style_to_row(
            row=self.current_summary_line,
            fill=fill,
        )

    def increment_current_summary_line(self):
        self.current_summary_line += 1

    def draw_row_entreprise(self, entreprise) -> None:
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.entreprise,
            value=entreprise,
        )
        self._apply_style_to_row(
            row=self.current_summary_line,
            font=FONT_ENTREPRISE,
            fill=FILL_ENTREPRISE,
        )
        self.increment_current_summary_line()

    def draw_row_annee(self, entreprise: str, annee: str) -> None:
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.entreprise,
            value=entreprise,
        )
        self._add_value_to_summary_cell(
            row=self.current_summary_line,
            column=SummaryColumns.annee,
            value=annee,
        )
        self._apply_style_to_row(
            row=self.current_summary_line,
            font=FONT_YEAR,
            fill=FILL_YEAR,
        )
        self.increment_current_summary_line()

    def adapt_columns_width(self):
        self.adapt_columns_width_data_sheet()
        self.adapt_columns_width_summary()

    def adapt_columns_width_data_sheet(self):
        self.resize_columns_width(self._data_sheet, min_row=1)

    def adapt_columns_width_summary(self):
        self.resize_columns_width(
            self._summary_sheet, min_row=START_SUMMARY_ROW
        )

    def resize_columns_width(self, worksheet: Worksheet, min_row: int):
        for col_index, col in enumerate(worksheet.iter_cols(min_row=min_row)):
            max_cell_size = max([len(str(s.value)) for s in col if s.value])
            if col_index > 4:
                max_cell_size = 15
            worksheet.column_dimensions[
                get_column_letter(col_index + 1)
            ].width = min(
                max_cell_size + 2, 60
            )  # max width for a column for legibility : 60

    def _apply_style_to_row(
        self,
        row: int,
        font: Font = None,
        fill: PatternFill = None,
        alignment: Alignment = None,
    ) -> None:
        for column in [
            SummaryColumns.entreprise,
            SummaryColumns.annee,
            SummaryColumns.date,
            SummaryColumns.declaration_type,
            SummaryColumns.motif,
            SummaryColumns.montant_convention,
            SummaryColumns.montant_remunerations,
            SummaryColumns.montant_avantages,
            SummaryColumns.montant_max,
        ]:
            self._apply_style_to_cell(
                row=row,
                column=column,
                font=font,
                fill=fill,
                alignment=alignment,
            )

    def _apply_style_to_cell(
        self,
        row: int,
        column: int,
        font: Font = None,
        fill: PatternFill = None,
        alignment: Alignment = None,
    ) -> None:
        if fill:
            self._summary_sheet.cell(row=row, column=column).fill = fill
        if font:
            self._summary_sheet.cell(row=row, column=column).font = font
        if alignment:
            self._summary_sheet.cell(
                row=row, column=column
            ).alignment = alignment
