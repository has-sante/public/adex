Qu’est-ce qu’ADEX ?
-------------------

ADEX est un outil qui permet de retrouver facilement un professionnel de
santé ou une personne morale (association de patients, établissement de
santé, organisme professionnel, etc.) de la base Transparence Santé et
d’afficher ses liens d’intérêts financiers avec les industries de
santé.

D’où viennent les informations d’ADEX ?
---------------------------------------

ADEX utilise des données ouvertes issues de l’*Annuaire Santé* de
l’Agence du Numérique en Santé (ANS) ainsi que de la base de données
publique [*Transparence Santé*](https://transparence.sante.gouv.fr/).

Quelles informations vais-je trouver sur ADEX ?
-----------------------------------------------

Vous trouverez sur ADEX des informations relatives aux professionnels de
santé (nom, prénom, identifiant professionnel et lieu(x) d’exercice) et
personnes morales (Nom de l’établissement, Siren, adresse), ainsi que les
conventions, avantages et rémunérations octroyées par des industriels à des
professionnels de santé ou des personnes morales.

ADEX ne contient pas d’informations issues de Déclarations Publiques
d’Intérêt (DPI).

Les données sont-elles à jour ?
-------------------------------

Les données d’ADEX sont synchronisées toutes les nuits avec celles de la
base Transparence Santé. Le décalage entre les deux sera donc de un jour
maximum. La date de mise à jour des données est indiquée en pied de page
ainsi que dans les [mentions légales](https://adex.has-sante.fr/mentions-legales).

Les déclarations des industriels sur Transparence Santé sont
semestrielles, avec une mise en ligne en mars et en septembre.
C’est pourquoi vous ne trouverez généralement pas de
liens d’intérêts financiers datant de moins de 6 mois. La base
Transparence Santé est cependant alimentée en continu, car des corrections
sur les déclarations sont régulièrement apportées.

Quelle est la différence entre ADEX et le site Transparence Santé ?
-------------------------------------------------------------------

ADEX croise les données de [*Transparence
Santé*](https://transparence.sante.gouv.fr/) avec celles de l’*Annuaire
Santé* de l’Agence du Numérique en Santé (ANS) afin de pouvoir retrouver
plus facilement un professionnel de santé et afficher ses liens
d’intérêt financiers.

Le croisement de ces deux sources de données permet plusieurs choses :

-   Rechercher une personne par nom, prénom, identifiant professionnel
    et/ou lieu d’activité, et non plus seulement avec son nom de famille
    comme c’est le cas sur le site Transparence Santé. Cela permet de
    gagner du temps et d’éviter de rater des bénéficiaires à cause
    d’erreurs dans les déclarations (un prénom et un nom de famille
    inversés par exemple).

-   Regrouper plusieurs bénéficiaires référencés sur le site
    Transparence Santé dont on est certains qu’ils correspondent à une
    seule et même personne (nom, prénom et identifiant professionnel
    identiques) pour avoir moins de lignes à sélectionner manuellement.

-   Ordonner la liste des bénéficiaires de manière à faire ressortir en
    premier les résultats les plus pertinents et ainsi éviter de
    parcourir de nombreuses pages de résultats.

Par ailleurs, les liens d’intérêts financiers issus de Transparence
Santé ont été traités de manière à être plus facilement exploitables :

-   Les données relatives aux montants perçus sont regroupées par
    industrie, par année et par type pour plus de lisibilité.

-   Lorsque c’est possible, les avantages et rémunérations sont
    regroupés avec la convention liée, pour vous permettre de mieux
    apprécier le contexte global de la convention.

-   Les totaux des montants perçus sont automatiquement calculés.

-   Une vue graphique permet d’apprécier l’intensité et la distribution
    des liens d’intérêts financiers sur la durée.

Enfin, l’ensemble de la synthèse peut être téléchargé sous forme de
tableur.

Comment en savoir plus sur le fonctionnement précis d’ADEX ?
------------------------------------------------------------

ADEX est édité en open source et le code ainsi que toute la
documentation technique et de conception sont consultables sur [le dépôt
Gitlab](https://gitlab.has-sante.fr/has-sante/public/adex/) du projet.

D’où viennent les données figurant sur le tableau de synthèse ?
---------------------------------------------------------------

Les données du tableau de synthèse ont été renseignées par les
industries de santé eux-mêmes sur le site Transparence Santé. Depuis la
loi Bertrand de 2011, ces entreprises ont l’obligation légale de
renseigner certains types d’accords qu’ils concluent avec des
professionnels de santé ou des personnes morales (institutions,
associations, etc.). Il existe 3 types de déclarations : les
conventions, les rémunérations et les avantages.

Qu’est-ce qu’une convention ?
-----------------------------

D’après Transparence Santé, « *les conventions entre les entreprises et
les acteurs de la santé sont des accords impliquant des obligations de
part et d’autre. Il s’agit, par exemple, de la participation à un
congrès en tant qu’orateur (obligation remplie par le professionnel),
avec prise en charge du transport et de l’hébergement (obligation
remplie par l’entreprise). Les conventions peuvent aussi avoir pour
objet une activité de recherche ou des essais cliniques sur un produit
de santé, la participation à un congrès scientifique, une action de
formation, etc.* »

Il existe plus de 5,5 millions de conventions dans la base Transparence
Santé, généralement reliées à des avantages et des rémunérations
correspondant à la contrepartie perçue par le professionnel de santé.

Qu’est-ce qu’une rémunération ?
-------------------------------

D’après Transparence Santé, « *les rémunérations sont les sommes versées
par les entreprises à un acteur de la santé (professionnel de santé ou
personne morale) en contrepartie de la réalisation d’un travail ou d’une
prestation.* »

Ainsi, les rémunérations sont versées dans le cadre de
[conventions](/questionsFrequentes/#quest-ce-quune-convention) qui établissent les
contreparties de ces rémunérations, c’est-à-dire ce à quoi s’engage le
professionnel de santé.

Qu’est-ce qu’un avantage ?
--------------------------

D’après Transparence Santé, « *les avantages pris en compte dans la base
de données Transparence Santé recouvrent tout ce qui est alloué ou versé
sans contrepartie par une entreprise à un acteur de la santé (don de
matériel, repas, transport, hébergement, etc.).* »

Il peut s’agir d’avantages en nature ou en espèce, directs ou indirects.

Pourquoi des déclarations de plus de 5 ans apparaissent-elles dans le tableau de synthèse ?
-------------------------------------------------------------------------------------------

Les conditions d’utilisations des données de Transparence Santé stipulent
que les déclarations de moins de 5 ans doivent être consultables. Et celles
antérieures à cette période ne doivent plus apparaître.

Il existe malheureusement beaucoup d’erreurs dans la base Transparence
Santé : un certain nombre de dates ne sont pas saisies de manière
standard et il y a parfois des incohérences entre les dates des
conventions et les avantages et rémunérations liés.

Les données exposées par Transparence Santé sur le site
[data.gouv](https://www.data.gouv.fr/en/datasets/transparence-sante-1/)
contiennent des déclarations qui respectent cette contrainte de 5 ans maximum
d’ancienneté. Mais, à cause des erreurs de saisies, il y a aussi des déclarations
dont il n’a pas été possible de déterminer si elles étaient antérieures à
la période de 5 ans (pas de date de fin sur une convention, date mal formatée...).

L’équipe ADEX a fait le choix d’exposer toutes les données proposées par l’export de
[data.gouv](https://www.data.gouv.fr/en/datasets/transparence-sante-1/) après avoir consulté
l’équipe de Transparence Santé.

Pourquoi le total des montants des conventions, rémunérations et avantages sont-ils calculés séparément ?
--------------------------------------------------------------------------------------------------------

Dans le cadre d’une convention, un industriel de santé va verser une
contrepartie financière ou en nature au bénéficiaire. Or, selon
l’entreprise qui a soumis la déclaration, le montant de cette
contrepartie peut être indiqué au niveau de la convention (de manière
anticipé ou non), au niveau de la rémunération ou de l’avantage lié, ou
les deux (générant ainsi des doublons). Qui plus est, les montants au
niveau de la convention et des avantages et rémunération liés peuvent ne
pas être identiques, par exemple si toutes les rémunérations relatives à
une convention n’ont pas été encore versées ou déclarées.

Afin d’éviter les erreurs, les totaux des conventions et ceux des
rémunérations et avantages sont donc calculés et présentés séparément,
afin de vous aider à apprécier les éventuels doublons et adapter votre
calcul.

Est-il possible de filtrer les données exportées ?
--------------------------------------------------

Il n’est pas possible de filtrer en amont les données qui seront
exportées.

Une fois exportées, il est possible de filtrer certaines données de la
synthèse depuis les en-têtes du tableau.

D’où viennent les informations sur l’en-tête du fichier exporté ?
-----------------------------------------------------------------

Les informations relatives au professionnel de santé (nom, prénom, RPPS
et lieu(x) d’exercice) sont issues de l’*Annuaire Santé* de l’Agence du
Numérique en Santé (ANS).

Pourquoi l’en-tête du fichier exporté est-il vide ?
---------------------------------------------------

Si vous n’avez pas sélectionné un professionnel de santé dans les
suggestions du moteur de recherche ou si votre synthèse concerne une
personne morale, alors aucune donnée ne pourra être affichée dans
l’en-tête. Néanmoins, vous pouvez si vous le souhaitez remplir ces
informations vous-même dans le fichier exporté.

Pourquoi l’export affiche-t-il une fourchette pour le montant total des liens d’intérêts financiers ?
------------------------------------------------------------------------------------------

Comme il existe de nombreux doublons de montants dus au fait que les
industriels de santé [ne renseignent pas tous les montants de
conventions de la même
manière](/questionsFrequentes/#pourquoi-le-total-des-conventions-remunerations-et-avantages-sont-ils-calcules-separment),
ADEX propose une fourchette calculée comme ceci :

-   **Montant minimum =** total des avantages + total des rémunérations

-   **Montant maximum =** total des avantages et rémunérations non liées
à des conventions + pour chaque convention, le montant le plus élevé entre
soit le montant de la convention, soit le montant total des avantages et
rémunérations associées.

Cette fourchette est automatiquement mise-à-jour si vous filtrez les
données.

Pourquoi la fourchette de montant des liens d’intérêts financiers ne s’affiche-t-elle pas dans le fichier exporté ?
-------------------------------------------------------------------------------------------------------------------

Une fois que vous avez ouvert le fichier exporté avec Excel ou tout
autre logiciel compatible, assurez-vous de bien avoir autorisé la
modification du fichier. Les sommes automatiques des formules seront
alors calculées.

Est-il possible de télécharger les données brutes relatives à un bénéficiaire de Transparence Santé ?
-----------------------------------------------------------------------------------------------------

Oui, les données brutes sont automatiquement exportées avec la synthèse,
dans l’onglet « Données brutes » du fichier généré.