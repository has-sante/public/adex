import os
from typing import Tuple

import elasticsearch


def get_elasticsearch_auth() -> Tuple[str, str]:
    user = os.getenv("ELASTICSEARCH_USER")
    pwd = os.getenv("ELASTICSEARCH_PWD")
    if not user:
        raise EnvironmentError("Env variable ELASTICSEARCH_USER not defined")
    if not pwd:
        raise EnvironmentError("Env variable ELASTICSEARCH_PWD not defined")
    return (user, pwd)


def get_elasticsearch() -> elasticsearch.Elasticsearch:
    auth = get_elasticsearch_auth()
    es = elasticsearch.Elasticsearch(
        [{"host": "elasticsearch"}], http_auth=auth
    )
    return es


def unpack_es_results(es_results: dict) -> dict:
    data = {}
    data["total"] = es_results["hits"]["total"]["value"]
    data["data"] = []
    for _, hit in enumerate(list(es_results["hits"]["hits"])):
        new_hit = hit["_source"]
        new_hit["score"] = hit["_score"]
        new_hit["identification_nationale_pp"] = hit["_id"]
        data["data"].append(new_hit)

    return data
