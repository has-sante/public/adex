from app import models, schemas
from app.crud import crud_all_declarations
from app.tests.utils.insertion import insert_declarations


def test_crud_all_declarations(db):
    declarations_to_insert = [
        models.Declaration(
            declaration_id="c1",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.convention,
        ),
        models.Declaration(
            declaration_id="c2",
            entreprise_identifiant="e2",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.convention,
        ),
        models.Declaration(
            declaration_id="other_c",
            entreprise_identifiant="e2",
            benef_hash="h2",
            lien_interet=schemas.LienInteret.convention,
        ),
        models.Declaration(
            declaration_id="r1",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.remuneration,
        ),
        models.Declaration(
            declaration_id="r2",
            entreprise_identifiant="e2",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.remuneration,
        ),
        models.Declaration(
            declaration_id="other_r",
            entreprise_identifiant="e2",
            benef_hash="h2",
            lien_interet=schemas.LienInteret.remuneration,
        ),
        models.Declaration(
            declaration_id="a1",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.avantage,
        ),
        models.Declaration(
            declaration_id="a2",
            entreprise_identifiant="e2",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.avantage,
        ),
        models.Declaration(
            declaration_id="other_a",
            entreprise_identifiant="e2",
            benef_hash="h2",
            lien_interet=schemas.LienInteret.avantage,
        ),
    ]

    insert_declarations(declarations=declarations_to_insert, db=db)

    fetched_synthese = crud_all_declarations.get_all_by_hashes(["h1"], db)
    assert ["c1", "c2"] == [
        convention.declaration_id
        for convention in fetched_synthese.conventions
    ]
    assert ["r1", "r2"] == [
        remuneration.declaration_id
        for remuneration in fetched_synthese.remunerations
    ]
    assert ["a1", "a2"] == [
        avantage.declaration_id for avantage in fetched_synthese.avantages
    ]
