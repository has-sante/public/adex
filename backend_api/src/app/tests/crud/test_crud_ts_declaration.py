from app import models, schemas
from app.crud import crud_ts_declaration
from app.tests.utils.insertion import insert_declarations


def test_crud_avantages_returns_schemas(db):
    avantages_to_insert = [
        models.Declaration(
            declaration_id="a1",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.avantage,
        ),
        models.Declaration(
            declaration_id="a2",
            entreprise_identifiant="e2",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.avantage,
        ),
    ]
    expected_avantages = [
        schemas.Avantage(
            declaration_id="a1",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.avantage,
        ),
        schemas.Avantage(
            declaration_id="a2",
            entreprise_identifiant="e2",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.avantage,
        ),
    ]

    insert_declarations(avantages_to_insert, db)

    fetched_avantages = crud_ts_declaration.get_avantages_by_hashes(["h1"], db)
    assert all(
        isinstance(avantage, schemas.Avantage)
        for avantage in fetched_avantages
    )
    assert expected_avantages == fetched_avantages


def test_crud_remunerations_returns_schemas(db):
    remunerations_to_insert = [
        models.Declaration(
            declaration_id="r1",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.remuneration,
        ),
        models.Declaration(
            declaration_id="r2",
            entreprise_identifiant="e2",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.remuneration,
        ),
    ]
    expected_remunerations = [
        schemas.Remuneration(
            declaration_id="r1",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.remuneration,
        ),
        schemas.Remuneration(
            declaration_id="r2",
            entreprise_identifiant="e2",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.remuneration,
        ),
    ]
    insert_declarations(remunerations_to_insert, db)
    fetched_remunerations = crud_ts_declaration.get_remunerations_by_hashes(
        ["h1"], db
    )
    assert all(
        isinstance(remuneration, schemas.Remuneration)
        for remuneration in fetched_remunerations
    )
    assert expected_remunerations == fetched_remunerations


def test_crud_conventions_returns_schemas(db):
    conventions_to_insert = [
        models.Declaration(
            declaration_id="c1",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.convention,
        ),
        models.Declaration(
            declaration_id="c2",
            entreprise_identifiant="e2",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.convention,
        ),
    ]
    expected_conventions = [
        schemas.Convention(
            declaration_id="c1",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.convention,
        ),
        schemas.Convention(
            declaration_id="c2",
            entreprise_identifiant="e2",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.convention,
        ),
    ]
    insert_declarations(conventions_to_insert, db)
    fetched_conventions = crud_ts_declaration.get_conventions_by_hashes(
        ["h1"], db
    )
    assert all(
        isinstance(convention, schemas.Convention)
        for convention in fetched_conventions
    )
    assert expected_conventions == fetched_conventions


def test_crud_avantages_multi_hashes(db):
    avantages_to_insert = [
        models.Declaration(
            declaration_id="a11",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.avantage,
        ),
        models.Declaration(
            declaration_id="a12",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.avantage,
        ),
        models.Declaration(
            declaration_id="a21",
            entreprise_identifiant="e1",
            benef_hash="h2",
            lien_interet=schemas.LienInteret.avantage,
        ),
        models.Declaration(
            declaration_id="a31",
            entreprise_identifiant="e1",
            benef_hash="h3",
            lien_interet=schemas.LienInteret.avantage,
        ),
        models.Declaration(
            declaration_id="a32",
            entreprise_identifiant="e1",
            benef_hash="h3",
            lien_interet=schemas.LienInteret.avantage,
        ),
    ]
    insert_declarations(avantages_to_insert, db)

    avantages_h1 = crud_ts_declaration.get_avantages_by_hashes(
        hashes=["h1"], db=db
    )
    assert ["a11", "a12"] == [
        avantage.declaration_id for avantage in avantages_h1
    ]
    avantages_h2_h3 = crud_ts_declaration.get_avantages_by_hashes(
        hashes=["h2", "h3"], db=db
    )
    assert ["a21", "a31", "a32"] == [
        avantage.declaration_id for avantage in avantages_h2_h3
    ]


def test_crud_remunerations_multi_hashes(db):
    remunerations_to_insert = [
        models.Declaration(
            declaration_id="r11",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.remuneration,
        ),
        models.Declaration(
            declaration_id="r12",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.remuneration,
        ),
        models.Declaration(
            declaration_id="r21",
            entreprise_identifiant="e1",
            benef_hash="h2",
            lien_interet=schemas.LienInteret.remuneration,
        ),
        models.Declaration(
            declaration_id="r31",
            entreprise_identifiant="e1",
            benef_hash="h3",
            lien_interet=schemas.LienInteret.remuneration,
        ),
        models.Declaration(
            declaration_id="r32",
            entreprise_identifiant="e1",
            benef_hash="h3",
            lien_interet=schemas.LienInteret.remuneration,
        ),
    ]
    insert_declarations(remunerations_to_insert, db)

    remunerations_h1 = crud_ts_declaration.get_remunerations_by_hashes(
        hashes=["h1"], db=db
    )
    assert ["r11", "r12"] == [
        remuneration.declaration_id for remuneration in remunerations_h1
    ]
    remunerations_h2_h3 = crud_ts_declaration.get_remunerations_by_hashes(
        hashes=["h2", "h3"], db=db
    )
    assert ["r21", "r31", "r32"] == [
        remuneration.declaration_id for remuneration in remunerations_h2_h3
    ]


def test_crud_conventions_multi_hashes(db):
    conventions_to_insert = [
        models.Declaration(
            declaration_id="c11",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.convention,
        ),
        models.Declaration(
            declaration_id="c12",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.convention,
        ),
        models.Declaration(
            declaration_id="c21",
            entreprise_identifiant="e1",
            benef_hash="h2",
            lien_interet=schemas.LienInteret.convention,
        ),
        models.Declaration(
            declaration_id="c31",
            entreprise_identifiant="e1",
            benef_hash="h3",
            lien_interet=schemas.LienInteret.convention,
        ),
        models.Declaration(
            declaration_id="c32",
            entreprise_identifiant="e1",
            benef_hash="h3",
            lien_interet=schemas.LienInteret.convention,
        ),
    ]
    insert_declarations(conventions_to_insert, db)

    conventions_h1 = crud_ts_declaration.get_conventions_by_hashes(
        hashes=["h1"], db=db
    )
    assert ["c11", "c12"] == [
        convention.declaration_id for convention in conventions_h1
    ]
    conventions_h2_h3 = crud_ts_declaration.get_conventions_by_hashes(
        hashes=["h2", "h3"], db=db
    )
    assert ["c21", "c31", "c32"] == [
        convention.declaration_id for convention in conventions_h2_h3
    ]


def test_crud_different_declaration_types(db):
    conventions_to_insert = [
        models.Declaration(
            declaration_id="c1",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.convention,
        ),
        models.Declaration(
            declaration_id="r1",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.remuneration,
        ),
        models.Declaration(
            declaration_id="a1",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.avantage,
        ),
    ]
    insert_declarations(conventions_to_insert, db)

    assert crud_ts_declaration.get_conventions_by_hashes(
        hashes=["h1"], db=db
    ) == [
        schemas.Convention(
            declaration_id="c1",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.convention,
        )
    ]
    assert crud_ts_declaration.get_remunerations_by_hashes(
        hashes=["h1"], db=db
    ) == [
        schemas.Remuneration(
            declaration_id="r1",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.remuneration,
        )
    ]
    assert crud_ts_declaration.get_avantages_by_hashes(
        hashes=["h1"], db=db
    ) == [
        schemas.Avantage(
            declaration_id="a1",
            entreprise_identifiant="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.avantage,
        )
    ]
