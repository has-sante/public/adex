from sqlalchemy.orm import Session

from app import models


def insert_declarations(
    declarations: models.Declaration,
    db: Session,
) -> None:
    for declaration in declarations:
        db.add(declaration)
    db.commit()
