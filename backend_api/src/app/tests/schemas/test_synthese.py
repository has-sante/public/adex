import datetime

from app import schemas
from app.core.summary.synthese_to_rows import all_declarations_to_synthese_rows


def create_row_convention(
    motif: str,
    autre_motif: str,
    information_convention: str,
    date_debut: datetime.date,
    date_fin: datetime.date = None,
) -> schemas.RowSyntheseDeclarationsConvention:
    convention = schemas.Convention(
        benef_hash="h1",
        denomination_sociale="e1",
        montant=100,
        lien_interet=schemas.LienInteret.convention,
        motif=motif,
        autre_motif=autre_motif,
        information_convention=information_convention,
        date=datetime.date(2020, 1, 1),
        date_debut=date_debut,
        date_fin=date_fin,
    )
    all_declarations = schemas.AllDeclarationAggregate(
        conventions=[convention], remunerations=[], avantages=[]
    )
    rows = all_declarations_to_synthese_rows(all_declarations)
    return rows[0]


def test_synthese_convention_full_objet():
    row = create_row_convention(
        motif="motif",
        autre_motif="autre motif",
        information_convention="info convention",
        date_debut=datetime.date(2020, 1, 1),
        date_fin=datetime.date(2020, 2, 1),
    )
    expected = (
        "motif / autre motif\n"
        "(info convention)\n"
        "Date de début : 2020-01-01 - Date de fin : 2020-02-01"
    )

    assert row.full_convention_objet == expected


def test_synthese_convention_full_objet_date_fin_none():
    row = create_row_convention(
        motif="motif",
        autre_motif="autre motif",
        information_convention="info convention",
        date_debut=datetime.date(2020, 1, 1),
        date_fin=None,
    )
    expected = (
        "motif / autre motif\n"
        "(info convention)\n"
        "Date de début : 2020-01-01"
    )

    assert row.full_convention_objet == expected


def test_synthese_convention_full_objet_motif_none():
    row = create_row_convention(
        motif=None,
        autre_motif="autre motif",
        information_convention="info convention",
        date_debut=datetime.date(2020, 1, 1),
    )
    expected = (
        "Vide / autre motif\n"
        "(info convention)\n"
        "Date de début : 2020-01-01"
    )

    assert row.full_convention_objet == expected


def test_synthese_convention_full_objet_autre_motif_none():
    row = create_row_convention(
        motif="motif",
        autre_motif=None,
        information_convention="info convention",
        date_debut=datetime.date(2020, 1, 1),
    )
    expected = "motif\n(info convention)\nDate de début : 2020-01-01"

    assert row.full_convention_objet == expected


def test_synthese_convention_full_objet_info_convention_none():
    row = create_row_convention(
        motif="motif",
        autre_motif="autre motif",
        information_convention=None,
        date_debut=datetime.date(2020, 1, 1),
    )
    expected = "motif / autre motif\nDate de début : 2020-01-01"

    assert row.full_convention_objet == expected
