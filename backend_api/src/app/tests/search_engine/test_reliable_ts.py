import json

import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient

from app.crud import (
    crud_ts_declaration_avantage,
    crud_ts_declaration_convention,
    crud_ts_declaration_remuneration,
)
from app.crud.crud_beneficiaires import get_reliable_from_directory
from app.db.session import SessionLocal
from app.main import app

client = TestClient(app)


def test_reliable_declarations(ts_reliable_declarations):
    rpps, declaration_ids = ts_reliable_declarations
    data = {
        "query": rpps,
        "page_size": 1,
        "highlight": False,
        "page_current": 0,
    }

    # Get directory entry elastic id
    response = client.post("/backend/api/v1/directory/search", json=data)
    identification_nationale_pp = response.json()["data"][0][
        "identification_nationale_pp"
    ]

    # Get reliable recipients
    results = get_reliable_from_directory(identification_nationale_pp)
    hashes = []
    for r in results.data:
        hashes.extend([c.hash for c in r.children])

    # Get all declarations from the reliable ts recipients
    db = SessionLocal()
    conventions = crud_ts_declaration_convention.get_multi_by_hashes(
        hashes, db
    )
    remunerations = crud_ts_declaration_remuneration.get_multi_by_hashes(
        hashes, db
    )
    avantages = crud_ts_declaration_avantage.get_multi_by_hashes(hashes, db)
    found_declaration_ids = (
        [convention.declaration_id for convention in conventions]
        + [remuneration.declaration_id for remuneration in remunerations]
        + [avantage.declaration_id for avantage in avantages]
    )
    assert set(found_declaration_ids) == set(declaration_ids)
