import json

import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient

from app.main import app

client = TestClient(app)


def test_autosuggest_results(ans_search_query):
    search_query, ps_id, max_allow = ans_search_query
    data = {
        "query": search_query,
        "page_size": max_allow,
        "highlight": True,
        "page_current": 0,
    }
    response = client.post("/backend/api/v1/directory/search", json=data)
    assert response.status_code == 200
    ps_found = False
    for i, ps in enumerate(response.json()["data"]):
        if ps["identification"]["identification_nationale_pp"] == ps_id:
            ps_found = True
            break
    assert ps_found
