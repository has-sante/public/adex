# Can't be found
# "LIESSE Brigitte","",1
# "ROSSI PACINI Florence","",1
# "VASSON Marie Paule","",1
# "JOLY Francisca","",1
# "ING Olivia","",1
# "PETITPREZ Karine","",1
# "ZANETTI Laura","",1
# "PIEL Clément","",1 -> Peut être Pinel clément
# "BAGUELIN Marc","",1

# TODO add this TYPO and use n-grams
# "MICHALET Mauricette","",1 -> est en fait écrit avec deux L (MICHALLET)
# TODO a c was added to the firstname (should be Annaik)
# TODO is now called PETITHOMME-FEVE
# "FEVE Annaick","810000462993",1
# TODO lot of homonymes
# ("BENOIT Michel", "810002011582", 1),
# "VINCENT Isabelle","",1
# TODO on a trouvé Pecault Rémi, mais pas :
# "PECAULT-CHARBY Rémi","",1
# TODO not done
# "BELEC Laurent","",1

# The mighty BRUNO Bénédicte
case_ascii_tests = [
    ("BRUNO Benedicte", "810002580818", 1),
    ("Bruno bénédicte", "810002580818", 1),
    ("BruNo bEnediCTE", "810002580818", 1),
    ("BenEdicte BruNo", "810002580818", 1),
    ("810002580818", "810002580818", 1),
]

homonyms = [
    # The multiple BRUNO Martin
    ("Bruno martin", "810003197380", 15),
    ("Bruno martin", "0856387899", 15),
    ("Bruno martin", "810002951092", 15),
    ("Bruno martin", "0676114184", 15),
    ("Bruno martin", "0419301593", 15),
    ("Bruno martin", "0646956169", 15),
    ("Bruno martin", "810002165412", 15),
    ("Bruno martin", "0136032703", 15),
    ("Bruno martin", "810003627402", 15),
    ("Bruno martin", "0886165711", 15),
    ("Bruno martin", "810001880151", 15),
    ("Bruno martin", "810005283808", 15),
    ("Bruno martin", "810002535226", 15),
    ("Bruno martin", "810003945366", 15),
    ("Bruno martin", "0136104726", 15),
]

# With place
homonyms_with_place = [
    ("Bruno martin pau", "0646956169", 1),
    ("Bruno martin nimes", "810003197380", 1),
]

cvdi_examples = [
    # Names from the CVDI examples
    ("bay jacques olivier", "810003167912", 1),
    ("Bay jacques-olivier", "810003167912", 1),
    ("DE BOTTON Marie Laure", "0596414797", 1),
    ("BOTTON Marie Laure", "0596414797", 1),
    ("Marie Laure Botton", "0596414797", 1),
    ("GALINIER Anne", "810003360897", 4),
    ("GALINIER Anne", "810002870912", 4),
    ("GALINIER Anne", "0346456825", 4),
    ("GALINIER Anne", "0756530762", 4),
    ("HANKARD Régis", "810000572080", 1),
    ("MOREL Yves", "810003113072", 4),
    ("MOREL Yves", "810003037008", 4),
    ("MOREL Yves", "0896666575", 4),
    ("MOREL Yves", "810100736387", 4),
    ("SOUDANI Martine", "810001451482", 1),
    ("ZAZZO Jean Fabien", "810000932045", 1),
    ("COSTES Frédéric", "810003010419", 1),
    ("DELARUE Jacques", "810002031473", 3),
    ("DELARUE Jacques", "810002634599", 3),
    ("DELARUE Jacques", "0350002382", 3),
    ("DESPORT Jean-Claude", "810002934072", 1),
    ("DUBERN Béatrice", "810001383628", 1),
    ("FONTAINE Eric", "810003723524", 6),
    ("FONTAINE Eric", "0166094391", 6),
    ("FONTAINE Eric", "810000846864", 6),
    ("FONTAINE Eric", "810003130357", 6),
    ("FONTAINE Eric", "810002323011", 6),
    ("FONTAINE Eric", "810001687077", 6),
    ("RAYNAUD SIMON Agathe", "810001459550", 1),
    ("GOLDWASSER François", "810003734091", 1),
    ("GRUNBERG Bernard", "810003351623", 1),
    ("MAS Emmanuel", "810100071116", 3),
    ("MAS Emmanuel", "0316815372", 3),
    ("MAS Emmanuel", "810002915469", 3),
    ("RUETSCH Marcel", "810002445541", 1),
    ("SEGUY David", "810002291127", 1),
    ("DE PARSCAU DU PLESSIX Loïc", "810002621307", 1),
    ("FREDERIC Joel", "810002033735", 1),
    ("SCHNITZLER Alexis", "810001607810", 1),
    ("CAMIER LONGEPIERRE Aurélie", "0056154982", 1),
    ("LECHOWSKI Laurent", "810001435170", 1),
    ("LOWINSKI Deborah", "0779601871", 1),
    ("LOWINSKI-LETINOIS Deborah", "0779601871", 1),
    ("BENHAMOU Dan", "810001617108", 3),
    ("BENHAMOU Dan", "810101297561", 3),
    ("BENHAMOU Dan", "810000392687", 3),
    ("GODEAU Bertrand", "810001238921", 1),
    ("MICHALLET Mauricette", "810003065389", 1),
    ("CAZENAVE Jean Pierre", "810002414711", 1),
    ("MILPIED Noël", "810002517463", 1),
    ("MION Georges", "810004983754", 1),
    ("SCHVED Jean François", "810003228805", 1),
    ("FEVE Annaik", "810000462993", 1),
    ("PETITHOMME FEVE Annaik", "810000462993", 1),
    ("TCHAKAMIAN Sophie", "810001455699", 1),
    ("DOUSSIN Anne", "810004915004", 1),
    ("MORIN-SURROCA Michèle", "810001152999", 1),
    ("PECAULT Rémi", "810003954665", 1),
    ("ABITEBOUL Dominique", "810000976307", 1),
    ("BELIAH-NAPPEZ Muriel", "810001964641", 1),
    # With typos
    ("Petithomme Annaick", "810000462993", 4),  # First name is actuall annaik
    ("feve Annaick", "810000462993", 1),  # Same
    ("CAMIER Aurélie", "0056154982", 1),  # Has a second name (Camier Aurélie)
    ("MICHALET Mauricette", "810003065389", 1),  # Michalet with 2 L
    # RPPS
    ("810003065389", "810003065389", 1),
    ("10003065389", "810003065389", 1),
    ("810002414711", "810002414711", 1),
    ("10002414711", "810002414711", 1),
    ("0316815372", "0316815372", 1),
    ("316815372", "0316815372", 1),
]
