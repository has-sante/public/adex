import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.db.base_class import Base


@pytest.fixture()
def in_memory_db():
    engine = create_engine("sqlite:///:memory:")
    Base.metadata.create_all(bind=engine)
    return engine


@pytest.fixture
def session_factory(in_memory_db):
    yield sessionmaker(autocommit=False, autoflush=False, bind=in_memory_db)


@pytest.fixture
def db(session_factory):
    return session_factory()
