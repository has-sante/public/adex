from datetime import date

import pytest

from app import schemas
from app.core.summary import synthese_to_rows
from app.tests.core.fixtures import avantage, convention, remuneration


@pytest.fixture
def synthese_multiple_avantages():
    return [
        schemas.SyntheseAvantage(
            date=date(2020, 1, 1),
            montant=107,
            source=schemas.Avantage(lien_interet=schemas.LienInteret.avantage),
        ),
        schemas.SyntheseAvantage(
            date=date(2020, 1, 1),
            montant=231,
            source=schemas.Avantage(lien_interet=schemas.LienInteret.avantage),
        ),
    ]


@pytest.fixture
def synthese_multiple_remunerations():
    return [
        schemas.SyntheseRemuneration(
            date=date(2020, 1, 1),
            montant=123,
            source=schemas.Remuneration(
                lien_interet=schemas.LienInteret.remuneration
            ),
        ),
        schemas.SyntheseRemuneration(
            date=date(2020, 1, 1),
            montant=349,
            source=schemas.Remuneration(
                lien_interet=schemas.LienInteret.remuneration
            ),
        ),
    ]


def test_avantage_to_row(avantage):
    synthese_avantage_row = synthese_to_rows.avantage_to_synthese_row(avantage)
    expected_avantage_row = schemas.RowSyntheseDeclarationsAvantage(
        benef_hash="h1",
        entreprise="denom_e1",
        annee="2020",
        annee_debut="2020",
        annee_fin="2020",
        montants=schemas.Montants(
            montants_avantages=300.0, min_montant=300.0, max_montant=300.0
        ),
        declaration_type=schemas.LienInteret.avantage,
        declaration=schemas.SyntheseAvantage(
            date=date(2020, 1, 1),
            montant=300.0,
            source=avantage.copy(deep=True),
        ),
    )
    assert synthese_avantage_row == expected_avantage_row


def test_avantage_to_synthese(avantage):
    synthese_avantage = synthese_to_rows.avantage_to_synthese(avantage)
    assert synthese_avantage == schemas.SyntheseAvantage(
        date=date(2020, 1, 1),
        montant=300.0,
        source=avantage.copy(deep=True),
    )


def test_avantage_to_synthese_with_convention_description(avantage):
    """
    On teste si la description de la convention est bien ajouté à
    la nature de l'avantage.
    """
    avantage.autre_motif = "nature"
    avantage.convention_description = "description_test"
    avantage_copy = avantage.copy(deep=True)
    avantage_copy.autre_motif = (
        "nature (Description convention liée : description_test)"
    )
    synthese_avantage = synthese_to_rows.avantage_to_synthese(avantage)
    assert synthese_avantage == schemas.SyntheseAvantage(
        date=date(2020, 1, 1),
        montant=300.0,
        source=avantage_copy.copy(deep=True),
    )


def test_remuneration_to_synthese(remuneration):
    synthese_remuneration = synthese_to_rows.remuneration_to_synthese(
        remuneration
    )
    assert synthese_remuneration == schemas.SyntheseRemuneration(
        date=date(2020, 1, 1),
        montant=200.0,
        source=remuneration.copy(deep=True),
    )


def test_remuneration_to_row(remuneration):
    synthese_remuneration_row = synthese_to_rows.remuneration_to_synthese_row(
        remuneration
    )
    expected_remuneration_row = schemas.RowSyntheseDeclarationsRemuneration(
        benef_hash="h1",
        entreprise="denom_e1",
        annee="2020",
        annee_debut="2020",
        annee_fin="2020",
        montants=schemas.Montants(
            montants_remunerations=200.0, min_montant=200.0, max_montant=200.0
        ),
        declaration_type=schemas.LienInteret.remuneration,
        declaration=schemas.SyntheseRemuneration(
            date=date(2020, 1, 1),
            montant=200.0,
            source=remuneration.copy(deep=True),
        ),
    )
    assert synthese_remuneration_row == expected_remuneration_row


def test_convention_row(convention, remuneration, avantage):
    avantage.convention_id = convention.declaration_id
    remuneration.convention_id = convention.declaration_id

    all_declarations = schemas.AllDeclarationAggregate(
        conventions=[convention],
        remunerations=[remuneration],
        avantages=[avantage],
    )

    synthese_convention_row = (
        synthese_to_rows.all_declarations_to_synthese_rows(all_declarations)
    )
    expected_synthese_row = schemas.RowSyntheseDeclarationsConvention(
        benef_hash="h1",
        entreprise="denom_e1",
        annee="2020 - 2021",
        annee_debut="2020",
        annee_fin="2021",
        montants=schemas.Montants(
            montant_convention=100.0,
            montants_remunerations=200.0,
            montants_avantages=300.0,
            min_montant=500.0,
            max_montant=500.0,
        ),
        declaration_type=schemas.LienInteret.convention,
        declaration=schemas.SyntheseConvention(
            source=convention.copy(deep=True),
            montant=100.0,
            date=date(2020, 1, 1),
            avantages=[
                synthese_to_rows.avantage_to_synthese(avantage.copy(deep=True))
            ],
            remunerations=[
                synthese_to_rows.remuneration_to_synthese(
                    remuneration.copy(deep=True)
                )
            ],
        ),
    )
    assert synthese_convention_row == [expected_synthese_row]


def test_get_montants_conventions_alone():
    # Montant minimum est sum(as, rs) et donc 0
    synthese_convention = schemas.SyntheseConvention(
        source=schemas.Convention(lien_interet=schemas.LienInteret.convention),
        montant=100.0,
        date=date(2020, 1, 1),
        avantages=[],
        remunerations=[],
    )
    assert synthese_to_rows.get_montants_convention(
        synthese_convention
    ) == schemas.Montants(
        montants_avantages=0,
        montants_remunerations=0,
        montant_convention=100.0,
        min_montant=0,
        max_montant=100.0,
    )


def test_get_montants_conventions_with_avantages_smaller_convention(
    synthese_multiple_avantages,
):
    synthese_convention = schemas.SyntheseConvention(
        montant=1000.0,
        source=schemas.Convention(lien_interet=schemas.LienInteret.convention),
        date=date(2020, 1, 1),
        avantages=synthese_multiple_avantages,
        remunerations=[],
    )
    assert synthese_to_rows.get_montants_convention(
        synthese_convention
    ) == schemas.Montants(
        montants_avantages=338,
        montants_remunerations=0,
        montant_convention=1000.0,
        min_montant=338,
        max_montant=1000.0,
    )


def test_get_montants_conventions_with_avantages_bigger_convention(
    synthese_multiple_avantages,
):
    synthese_convention = schemas.SyntheseConvention(
        montant=100.0,
        source=schemas.Convention(lien_interet=schemas.LienInteret.convention),
        date=date(2020, 1, 1),
        avantages=synthese_multiple_avantages,
        remunerations=[],
    )
    assert synthese_to_rows.get_montants_convention(
        synthese_convention
    ) == schemas.Montants(
        montants_avantages=338,
        montants_remunerations=0,
        montant_convention=100.0,
        min_montant=338,
        max_montant=338,
    )


def test_get_montants_conventions_with_remunerations_smaller_convention(
    synthese_multiple_remunerations,
):
    synthese_convention = schemas.SyntheseConvention(
        montant=1000.0,
        source=schemas.Convention(lien_interet=schemas.LienInteret.convention),
        date=date(2020, 1, 1),
        remunerations=synthese_multiple_remunerations,
        avantages=[],
    )
    assert synthese_to_rows.get_montants_convention(
        synthese_convention
    ) == schemas.Montants(
        montants_avantages=0,
        montants_remunerations=472,
        montant_convention=1000.0,
        min_montant=472,
        max_montant=1000.0,
    )


def test_get_montants_conventions_with_remunerations_bigger_convention(
    synthese_multiple_remunerations,
):
    synthese_convention = schemas.SyntheseConvention(
        montant=100.0,
        source=schemas.Convention(lien_interet=schemas.LienInteret.convention),
        date=date(2020, 1, 1),
        remunerations=synthese_multiple_remunerations,
        avantages=[],
    )
    assert synthese_to_rows.get_montants_convention(
        synthese_convention
    ) == schemas.Montants(
        montants_avantages=0,
        montants_remunerations=472,
        montant_convention=100.0,
        min_montant=472,
        max_montant=472,
    )


def test_get_montants_conventions_with_linked_bigger_convention(
    synthese_multiple_remunerations,
    synthese_multiple_avantages,
):
    synthese_convention = schemas.SyntheseConvention(
        montant=10000.0,
        source=schemas.Convention(lien_interet=schemas.LienInteret.convention),
        date=date(2020, 1, 1),
        remunerations=synthese_multiple_remunerations,
        avantages=synthese_multiple_avantages,
    )
    assert synthese_to_rows.get_montants_convention(
        synthese_convention
    ) == schemas.Montants(
        montants_avantages=338,
        montants_remunerations=472,
        montant_convention=10000.0,
        min_montant=810,
        max_montant=10000,
    )


def test_get_montants_conventions_with_linked_smaller_convention(
    synthese_multiple_remunerations,
    synthese_multiple_avantages,
):
    synthese_convention = schemas.SyntheseConvention(
        montant=100.0,
        source=schemas.Convention(lien_interet=schemas.LienInteret.convention),
        date=date(2020, 1, 1),
        remunerations=synthese_multiple_remunerations,
        avantages=synthese_multiple_avantages,
    )
    assert synthese_to_rows.get_montants_convention(
        synthese_convention
    ) == schemas.Montants(
        montants_avantages=338,
        montants_remunerations=472,
        montant_convention=100.0,
        min_montant=810,
        max_montant=810,
    )
