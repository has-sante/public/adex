import datetime

import pandas as pd
import pytest
from openpyxl import Workbook, load_workbook

from app import schemas
from app.core.summary.export import DATA_TAB_COLUMNS, ExcelSummaryCreator
from app.core.summary.synthese_to_rows import all_declarations_to_synthese_rows
from app.schemas.annuaire_ans import (
    ProfessionalActivityPlace,
    ProfessionalElasticResult,
    ProfessionalIdentification,
    ProfessionalName,
)


@pytest.fixture
def professional():
    return ProfessionalElasticResult(
        score=10,
        identification_nationale_pp="1298391",
        identification=ProfessionalIdentification(),
        lieux_activite=[
            ProfessionalActivityPlace(
                adresse_complete="2 avenue Philipe Hubert Paris"
            )
        ],
        noms_complets_exercice=[
            ProfessionalName(nom_exercice="Test", prenom_exercice="Nicolas")
        ],
        professions=["Ostéopathe", "Chirurgien"],
        specialites=["Orthopédie"],
        children=None,
    )


@pytest.fixture
def declarations():
    df = pd.DataFrame(
        data=[
            [
                "Nom1",
                "Prénom1",
                "categorie1",
                "profession1",
                "type_id1",
                "id1",
                "ville1",
            ],
            [
                "Nom2",
                "Prénom1",
                "categorie1",
                "profession1",
                "type_id1",
                "id1",
                "ville1",
            ],
            [
                "Nom3",
                "Prénom3",
                "categorie2",
                None,
                "type_id2",
                "id2",
                "ville2",
            ],
        ],
        columns=[
            "identite_nom_normalized",
            "prenom_normalized",
            "categorie_beneficiaire_libelle",
            "profession",
            "type_identifiant",
            "numero_identifiant",
            "ville",
        ],
    )
    cols_to_add = list(set(DATA_TAB_COLUMNS).difference(df.columns))

    df[cols_to_add] = None

    return df


@pytest.fixture
def avantages():
    return [
        schemas.Avantage(
            declaration_id="a1",
            denomination_sociale="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.avantage,
            date=datetime.date(2011, 1, 1),
            montant=100,
            autre_motif="Repas",
            prenom="hop",
            nom="hop",
            convention_description="Pas de convention",
            convention_id=None,
        ),
        schemas.Avantage(
            declaration_id="a2",
            denomination_sociale="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.avantage,
            date=datetime.date(2020, 10, 10),
            montant=200,
            autre_motif="Repas",
            prenom="hop",
            nom="hop",
            convention_id="c2",
        ),
        schemas.Avantage(
            declaration_id="a3",
            denomination_sociale="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.avantage,
            date=datetime.date(2020, 10, 10),
            montant=300,
            autre_motif="Repas",
            prenom="hop",
            nom="hop",
            convention_id="c4",
        ),
        schemas.Avantage(
            declaration_id="a4",
            denomination_sociale="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.avantage,
            date=datetime.date(2020, 10, 10),
            montant=300,
            autre_motif="Repas",
            prenom="hop",
            nom="hop",
            convention_id="c4",
        ),
    ]


@pytest.fixture
def remunerations():
    return [
        # Rémunération solo
        schemas.Remuneration(
            declaration_id="r1",
            denomination_sociale="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.remuneration,
            date=datetime.date(2010, 1, 1),
            montant=100,
            prenom="hop",
            nom="hop",
            convention_id=None,
        ),
        schemas.Remuneration(
            declaration_id="r2",
            denomination_sociale="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.remuneration,
            date=datetime.date(2020, 10, 10),
            montant=200,
            prenom="hop",
            nom="hop",
            convention_id="c3",
        ),
        schemas.Remuneration(
            declaration_id="r3",
            denomination_sociale="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.remuneration,
            date=datetime.date(2020, 10, 10),
            montant=300,
            prenom="hop",
            nom="hop",
            convention_id="c4",
        ),
        schemas.Remuneration(
            declaration_id="r4",
            denomination_sociale="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.remuneration,
            date=datetime.date(2020, 10, 10),
            montant=300,
            prenom="hop",
            nom="hop",
            convention_id="c4",
        ),
    ]


@pytest.fixture
def conventions():
    return [
        schemas.Convention(
            declaration_id="c1",
            denomination_sociale="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.convention,
            date=datetime.date(2012, 1, 1),
            date_debut=datetime.date(2012, 2, 1),
            date_fin=datetime.date(2012, 3, 1),
            motif=None,
            autre_motif="Autre motif",
            information_convention="Convention seule avec date début / date fin",
            montant=100,
            prenom="hop",
            nom="hop",
        ),
        schemas.Convention(
            declaration_id="c2",
            denomination_sociale="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.convention,
            date=datetime.date(2013, 4, 1),
            date_debut=datetime.date(2013, 5, 1),
            montant=200,
            autre_motif=None,
            motif="Motif",
            information_convention="Convention sans date fin et un avantage en 2020",
            prenom="hop",
            nom="hop",
            convention_id="c3",
        ),
        schemas.Convention(
            declaration_id="c3",
            denomination_sociale="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.convention,
            date=datetime.date(2014, 6, 1),
            date_debut=datetime.date(2014, 7, 1),
            date_fin=datetime.date(2014, 8, 1),
            information_convention="Convention avec une rémunération",
            montant=300,
            motif="Motif",
            prenom="hop",
            nom="hop",
        ),
        schemas.Convention(
            declaration_id="c4",
            denomination_sociale="e1",
            benef_hash="h1",
            lien_interet=schemas.LienInteret.convention,
            date=datetime.date(2020, 9, 11),
            date_debut=datetime.date(2020, 10, 11),
            date_fin=datetime.date(2020, 11, 11),
            information_convention="Convention avec 2 avants et 2 remus",
            montant=300,
            motif="Motif",
            prenom="hop",
            nom="hop",
        ),
    ]


@pytest.fixture
def summary_rows(avantages, remunerations, conventions):
    # We use:
    # - a convention with no links
    # - a convention with one avantage
    # - a convention with one remunerations
    # - a convention with multiples remunerations / avantages
    # - an isolated remuneration
    # - an isolated avantage
    all_declarations = schemas.AllDeclarationAggregate(
        conventions=conventions,
        remunerations=remunerations,
        avantages=avantages,
    )
    return all_declarations_to_synthese_rows(all_declarations)


@pytest.fixture
def metadata():
    return {"date_maj_donnees": "2021-09-24T12:43:37Z"}


def test_export(professional, declarations, summary_rows, metadata):
    """
    L'export est fait avec des données synthétiques.

    L'export généré par ce test sera disponible à ce chemin :
    `backend_api/src/app/tests/core/test_export_new.xlsx`

    L'export avec lequel il sera comparé est disponible à ce chemin :
    `backend_api/src/app/tests/core/test_export.xlsx`

    Lorsque le test fail, il faut alors vérifier si l'export `test_export_new.xlsx`
    est bien celui attendu.

    Si c'est le cas, il est nécessaire de modifier le fichier `test_export.xlsx` localement
    en lancant les tests puis en renommant et commit le fichier généré.

    À la racine du projet :
        make tests-backend-api-dev
        # Vérifier le contenu du ficher `backend_api/src/app/tests/core/test_export_new.py`
        sudo chown ${USER}:${USER} backend_api/src/app/tests/core/test_export_new.py
        mv backend_api/src/app/tests/core/test_export_new.xlsx backend_api/src/app/tests/core/test_export.xlsx
        git add backend_api/src/app/tests/core/test_export.xlsx
    """
    exporter = ExcelSummaryCreator(
        professional=professional,
        declarations=declarations,
        summary_rows=summary_rows,
        metadata=metadata,
    )
    exported = exporter.create()
    exported.save("app/tests/core/test_export_new.xlsx")
    assert_equal_to_template(exported)


def assert_equal_to_template(exported: Workbook) -> None:
    template = load_workbook("app/tests/core/test_export.xlsx")
    compare_excel_workbook(exported, template)
    compare_excel_workbook(template, exported)


def compare_excel_workbook(workbook_to_compare, workbook_compared):
    for sheetname in workbook_to_compare.sheetnames:
        workbook_to_compare_sheet = workbook_to_compare[sheetname]
        for row in workbook_to_compare_sheet.iter_rows():
            for cell in row:
                # Ignoring cell with export time
                if (
                    sheetname == "Synthèse"
                    and (cell.row == 1 or cell.row == 2)
                    and cell.column == 9
                ):
                    continue
                cell_compared = workbook_compared[sheetname].cell(
                    row=cell.row, column=cell.column
                )
                # TODO: Write a stronger asset method checking for font, alignment...
                assert_cells_equal(cell, cell_compared)


def assert_cells_equal(cell, cell_compared):
    if isinstance(cell.value, datetime.date) or isinstance(
        cell.value, datetime.datetime
    ):
        assert cell.value.year == cell_compared.value.year
        assert cell.value.month == cell_compared.value.month
        assert cell.value.day == cell_compared.value.day
    else:
        assert cell.value == cell_compared.value
