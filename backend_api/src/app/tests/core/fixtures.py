from datetime import date

import pytest

from app import schemas


@pytest.fixture
def convention():
    return schemas.Convention(
        declaration_id="c1",
        benef_hash="h1",
        lien_interet=schemas.LienInteret.convention,
        entreprise_identifiant="e1",
        denomination_sociale="denom_e1",
        date_debut=date(2020, 2, 1),
        date_fin=date(2021, 1, 1),
        date=date(2020, 1, 1),
        montant=100,
    )


@pytest.fixture
def remuneration():
    return schemas.Remuneration(
        declaration_id="r1",
        benef_hash="h1",
        lien_interet=schemas.LienInteret.remuneration,
        entreprise_identifiant="e1",
        denomination_sociale="denom_e1",
        date=date(2020, 1, 1),
        montant=200.0,
    )


@pytest.fixture
def avantage():
    return schemas.Avantage(
        declaration_id="a1",
        benef_hash="h1",
        lien_interet=schemas.LienInteret.avantage,
        entreprise_identifiant="e1",
        denomination_sociale="denom_e1",
        date=date(2020, 1, 1),
        montant=300.0,
    )
