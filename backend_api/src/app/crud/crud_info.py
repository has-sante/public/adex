from app.es.utils import get_elasticsearch
from app.schemas import Metadata


def get_info_from_es() -> Metadata:
    es = get_elasticsearch()
    # add try catch ?
    try:
        results = es.get(index="info", id=1)
        return results["_source"]
    except:
        return
