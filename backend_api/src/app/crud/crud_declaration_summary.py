import os
import tempfile
from typing import List

from sqlalchemy.orm import Session

from app.core.summary.export import ExcelSummaryCreator
from app.crud import (
    crud_all_declarations,
    crud_declaration_summary_rows,
    crud_info,
)
from app.schemas import ProfessionalElasticResult

DATE_FORMAT = "%d/%m/%Y"


def generate_summary(
    hashes: List[str],
    professional: ProfessionalElasticResult,
    db: Session,
) -> str:

    p_temp_file = os.path.join(
        "/", "tmp", next(tempfile._get_candidate_names()) + ".xlsx"
    )
    declarations = crud_all_declarations.get_dataframe_by_hashes(
        hashes=hashes, db=db
    )
    summary_rows = crud_declaration_summary_rows.get_synthese_by_hashes(
        hashes=hashes, db=db
    )
    metadata = crud_info.get_info_from_es()

    excel_summary_creator = ExcelSummaryCreator(
        professional=professional,
        declarations=declarations,
        summary_rows=summary_rows,
        metadata=metadata,
    )
    summary = excel_summary_creator.create()
    summary.save(p_temp_file)
    summary.close()
    return p_temp_file
