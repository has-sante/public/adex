from typing import Any, Dict

from app.es.utils import get_elasticsearch, unpack_es_results
from app.schemas import ProfessionalElasticResult, ProfessionalSearchResult

HIGHLIGHT_PRE_TAG = "<mark>"
HIGHLIGHT_POST_TAG = "</mark>"


def get_query_full_name(search: str, highlight: bool) -> Dict[str, Any]:
    query_match_full_names = {
        "match": {
            "noms_complets_exercice.full_name": {
                "query": search,
                "boost": 1.2,
            }
        }
    }
    query_nested_full_names = {
        "nested": {
            "path": "noms_complets_exercice",
            "query": query_match_full_names,
        }
    }
    if highlight:
        query_nested_full_names["nested"]["inner_hits"] = {
            "name": "full_name",
            "highlight": {
                "require_field_match": "false",
                "pre_tags": HIGHLIGHT_PRE_TAG,
                "post_tags": HIGHLIGHT_POST_TAG,
                "fields": {
                    "noms_complets_exercice.prenom_exercice": {"type": "fvh"},
                    "noms_complets_exercice.nom_exercice": {"type": "fvh"},
                },
            },
        }
    return query_nested_full_names


def get_query_rpps(search: str) -> Dict[str, Any]:
    wildcarded = search + "*"
    query = {
        "bool": {
            "should": [
                {
                    "wildcard": {
                        "identification.identifiant_pp": {
                            "value": wildcarded,
                        }
                    }
                },
                {
                    "wildcard": {
                        "identification.identification_nationale_pp": {
                            "value": wildcarded,
                        }
                    }
                },
            ]
        }
    }
    return query


def get_query_full_name_and_town(
    search: str, highlight: bool
) -> Dict[str, Any]:
    query_match_full_names = {
        "match": {
            "noms_complets_exercice.full_name": {
                "query": search,
                "boost": 0.8,
                "fuzziness": "AUTO",
            }
        }
    }
    query_nested_full_names = {
        "nested": {
            "path": "noms_complets_exercice",
            "query": query_match_full_names,
        }
    }
    query_match_activity_places = {
        "match": {
            "lieux_activite.libelle_commune_coord_structure": {
                "query": search,
                "boost": 0.5,
                "fuzziness": "AUTO",
            }
        }
    }
    query_nested_activity_place = {
        "nested": {
            "path": "lieux_activite",
            "query": query_match_activity_places,
        }
    }
    if highlight:
        query_nested_full_names["nested"]["inner_hits"] = {
            "name": "full_name_2",
            "highlight": {
                "require_field_match": "false",
                "pre_tags": HIGHLIGHT_PRE_TAG,
                "post_tags": HIGHLIGHT_POST_TAG,
                "fields": {
                    "noms_complets_exercice.prenom_exercice": {"type": "fvh"},
                    "noms_complets_exercice.nom_exercice": {"type": "fvh"},
                },
            },
        }
        query_nested_activity_place["nested"]["inner_hits"] = {
            "name": "activity_place",
            "highlight": {
                "require_field_match": "false",
                "pre_tags": HIGHLIGHT_PRE_TAG,
                "post_tags": HIGHLIGHT_POST_TAG,
                "fields": {
                    "lieux_activite.adresse_complete": {"type": "plain"}
                },
            },
        }

    query = {
        "bool": {
            "must": query_nested_full_names,
            "should": [query_nested_activity_place],
        }
    }
    return query


def handle_highlights(elasticsearch_results):
    for hit in elasticsearch_results["hits"]["hits"]:
        hit = handle_hit_highlights(hit)
        hit = handle_hit_inner_hits(hit)
    return elasticsearch_results


def handle_hit_inner_hits(hit):
    """
    Replace the highlights in inner_hits.
    Will probably need some refactoring...
    """
    for inner_hit in hit["inner_hits"].values():
        inner_hit_hits = inner_hit["hits"]["hits"]
        for inner_hit_to_hightlight in inner_hit_hits:
            key_field_to_update = inner_hit_to_hightlight["_nested"]["field"]
            offset = int(inner_hit_to_hightlight["_nested"]["offset"])
            if "highlight" in inner_hit_to_hightlight:
                for key, replacement in inner_hit_to_hightlight[
                    "highlight"
                ].items():
                    key_to_replace = key.split(".")[1]
                    replacement = replacement[0]
                    hit["_source"][key_field_to_update][offset][
                        key_to_replace
                    ] = replacement
    return hit


def handle_hit_highlights(hit):
    if "highlight" in hit:
        hit["_source"]["identification"]["identifiant_pp"] = hit["highlight"][
            "identification.identifiant_pp"
        ][0]
    return hit


def get_multi_by_search(
    search: str,
    page_current: int = 0,
    page_size: int = 10,
    highlight: bool = False,
    raw_data: bool = False,
) -> ProfessionalSearchResult:
    es = get_elasticsearch()
    search = search.strip()

    _source_fields = [
        "identification",
        "noms_complets_exercice",
        "professions",
        "lieux_activite",
        "specialites",
    ]
    if raw_data:
        _source_fields.append("children")

    query = {
        "size": page_size,
        "from": page_size * page_current,
        "query": {
            "bool": {
                "should": [
                    get_query_full_name(search, highlight),
                    get_query_full_name_and_town(search, highlight),
                    get_query_rpps(search),
                ]
            }
        },
        "_source": _source_fields,
    }

    if highlight:
        query["highlight"] = {
            "require_field_match": "false",
            "pre_tags": HIGHLIGHT_PRE_TAG,
            "post_tags": HIGHLIGHT_POST_TAG,
            "fields": {
                "identification.identifiant_pp": {"type": "plain"},
            },
        }

    results = es.search(index="professionals", body=query)

    if highlight:
        results = handle_highlights(results)

    return unpack_es_results(results)


def get_by_id(identification_nationale_pp: str) -> ProfessionalElasticResult:
    es = get_elasticsearch()

    query = {"query": {"terms": {"_id": [identification_nationale_pp]}}}
    results = es.search(index="professionals", body=query)
    r = unpack_es_results(results)

    return ProfessionalElasticResult.parse_obj(r["data"][0])
