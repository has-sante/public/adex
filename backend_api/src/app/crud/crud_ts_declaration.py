from typing import List

from sqlalchemy.orm import Session

from app import models, schemas


def get_conventions_by_hashes(
    hashes: List[str], db: Session, *, skip: int = 0, limit: int = 1000
) -> List[schemas.Convention]:
    return [
        schemas.Convention.from_orm(convention)
        for convention in get_declarations_by_hashes_and_lien_interet(
            hashes, schemas.LienInteret.convention, db, skip, limit
        )
    ]


def get_remunerations_by_hashes(
    hashes: List[str], db: Session, *, skip: int = 0, limit: int = 1000
) -> List[schemas.Remuneration]:
    return [
        schemas.Remuneration.from_orm(remuneration)
        for remuneration in get_declarations_by_hashes_and_lien_interet(
            hashes, schemas.LienInteret.remuneration, db, skip, limit
        )
    ]


def get_avantages_by_hashes(
    hashes: List[str], db: Session, *, skip: int = 0, limit: int = 1000
) -> List[schemas.Avantage]:
    return [
        schemas.Avantage.from_orm(avantage)
        for avantage in get_declarations_by_hashes_and_lien_interet(
            hashes, schemas.LienInteret.avantage, db, skip, limit
        )
    ]


def get_declarations_by_hashes_and_lien_interet(
    hashes: List[str],
    lien_interet: schemas.LienInteret,
    db: Session,
    skip: int = 0,
    limit: int = 1000,
):
    return (
        db.query(models.Declaration)
        .filter(models.Declaration.lien_interet == lien_interet)
        .filter(models.Declaration.benef_hash.in_(hashes))
        .offset(skip)
        .limit(limit)
        .all()
    )
