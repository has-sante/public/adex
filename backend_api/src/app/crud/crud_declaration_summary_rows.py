from typing import List

import pandas as pd
from sqlalchemy.orm import Session

from app import schemas
from app.core.summary.synthese_to_rows import all_declarations_to_synthese_rows
from app.crud import crud_all_declarations


def get_synthese_by_hashes(
    hashes: List[str], db: Session
) -> List[schemas.RowSyntheseDeclarations]:

    all_declarations = crud_all_declarations.get_all_by_hashes(hashes, db)
    return all_declarations_to_synthese_rows(all_declarations)
