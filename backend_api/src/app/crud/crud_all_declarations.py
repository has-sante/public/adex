from typing import List

import pandas as pd
from sqlalchemy.orm import Session

from app import schemas
from app.crud import crud_ts_declaration


def get_dataframe_by_hashes(hashes: List[str], db: Session) -> pd.DataFrame:

    str_hashes = "('" + "', '".join(hashes) + "')"
    sql_get_declarations = """
    SELECT *
    FROM ts_declarations
    WHERE benef_hash IN {str_hashes}
    """
    declarations = pd.read_sql(
        sql_get_declarations.format(str_hashes=str_hashes), db.get_bind()
    )

    return declarations


def get_all_by_hashes(
    hashes: List[str], db: Session
) -> schemas.AllDeclarationAggregate:

    conventions = crud_ts_declaration.get_conventions_by_hashes(hashes, db)
    remunerations = crud_ts_declaration.get_remunerations_by_hashes(hashes, db)
    avantages = crud_ts_declaration.get_avantages_by_hashes(hashes, db)

    return schemas.AllDeclarationAggregate(
        conventions=conventions,
        remunerations=remunerations,
        avantages=avantages,
    )
