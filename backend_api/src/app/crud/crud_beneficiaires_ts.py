from app.crud.crud_annuaire_ans import get_by_id
from app.es.utils import get_elasticsearch, unpack_es_results
from app.schemas import RecipientSearchResults

HIGHLIGHT_PRE_TAG = "<mark>"
HIGHLIGHT_POST_TAG = "</mark>"


def handle_hit_highlights(es_results):
    for hit in es_results["hits"]["hits"]:
        if "highlight" in hit:
            for key, value in hit["highlight"].items():
                hit["_source"][key] = value[0]
    return es_results


def get_multi_by_search(
    search: str,
    page_current: int = 0,
    page_size: int = 50,
    highlight: bool = True,
) -> RecipientSearchResults:
    es = get_elasticsearch()

    query = {
        "size": page_size,
        "from": page_size * page_current,
        "query": {
            "match": {"full_name": {"query": search, "fuzziness": "AUTO"}}
        },
    }

    if highlight:
        query["highlight"] = {
            "require_field_match": "false",
            "pre_tags": HIGHLIGHT_PRE_TAG,
            "post_tags": HIGHLIGHT_POST_TAG,
            "fields": {
                "prenom": {"type": "plain"},
                "identite_nom": {"type": "plain"},
            },
        }

    results = es.search(index="recipients-groups", body=query)
    if highlight:
        results = handle_hit_highlights(results)

    return unpack_es_results(results)


def get_from_directory(
    _id: str,
    page_current: int = 0,
    page_size: int = 10,
    highlight: bool = True,
) -> RecipientSearchResults:
    es = get_elasticsearch()

    # First get the professional from the directory
    professional = get_by_id(_id)

    # Construct query to fetch from the recipient-groups
    # This is a hack for now, we will have to implement more complex aggregations
    # over the children of the professional
    first_full_name = professional.noms_complets_exercice[0]
    search_name = (
        first_full_name.nom_exercice + " " + first_full_name.prenom_exercice
    )
    search_id = professional.identification.identifiant_pp

    query = {
        "size": page_size,
        "from": page_size * page_current,
        "query": {
            "bool": {
                "should": [
                    {
                        "bool": {
                            "should": [
                                {
                                    "match": {"numero_identifiant": search_id},
                                },
                                {"match": {"full_name": search_name}},
                            ]
                        }
                    }
                ]
            }
        },
    }

    if highlight:
        query["highlight"] = {
            "require_field_match": "false",
            "pre_tags": HIGHLIGHT_PRE_TAG,
            "post_tags": HIGHLIGHT_POST_TAG,
            "fields": {
                "identite_nom": {"type": "plain"},
                "prenom": {"type": "plain"},
                "numero_identifiant": {"type": "plain"},
            },
        }

    results = es.search(index="recipients-groups", body=query)
    if highlight:
        results = handle_hit_highlights(results)
    return unpack_es_results(results)


def get_reliable_from_directory(
    _id: str,
    page_current: int = 0,
    page_size: int = 50,
    highlight: bool = True,
) -> RecipientSearchResults:
    professional = get_by_id(_id)
    identifiant_pp = professional.identification.identifiant_pp
    identification_nationale_pp = (
        professional.identification.identification_nationale_pp
    )
    names = [
        " ".join((name.prenom_exercice, name.nom_exercice))
        for name in professional.noms_complets_exercice
    ]
    query = {
        "size": page_size,
        "from": page_size * page_current,
        "query": {
            "bool": {
                "must": [
                    {
                        "bool": {
                            "should": [
                                {
                                    "match": {
                                        "numero_identifiant": identifiant_pp
                                    }
                                },
                                {
                                    "match": {
                                        "numero_identifiant": identification_nationale_pp
                                    }
                                },
                            ]
                        }
                    },
                    {
                        "match": {"full_name": names[0]},
                    },
                ]
            }
        },
    }
    if highlight:
        query["highlight"] = {
            "require_field_match": "false",
            "pre_tags": HIGHLIGHT_PRE_TAG,
            "post_tags": HIGHLIGHT_POST_TAG,
            "fields": {
                "numero_identifiant": {"type": "plain"},
            },
        }
    es = get_elasticsearch()

    results = es.search(index="recipients-groups", body=query)

    return RecipientSearchResults.parse_obj(unpack_es_results(results))


def get_not_reliable_from_directory(
    _id: str,
    page_current: int = 0,
    page_size: int = 50,
) -> RecipientSearchResults:
    professional = get_by_id(_id)

    identifiant_pp = professional.identification.identifiant_pp
    identification_nationale_pp = (
        professional.identification.identification_nationale_pp
    )
    names = [
        " ".join((name.prenom_exercice, name.nom_exercice))
        for name in professional.noms_complets_exercice
    ]

    reliable_recipients = get_reliable_from_directory(_id, highlight=False)
    reliable_hashes = []
    for reliable_recipient in reliable_recipients.data:
        reliable_hashes.extend([r.hash for r in reliable_recipient.children])

    query = {
        "size": page_size,
        "from": page_size * page_current,
        "min_score": 0.1,
        "query": {
            "bool": {
                "should": [
                    {
                        "bool": {
                            "should": [
                                {
                                    "match": {
                                        "numero_identifiant": identifiant_pp
                                    }
                                },
                                {
                                    "match": {
                                        "numero_identifiant": identification_nationale_pp
                                    }
                                },
                            ]
                        }
                    },
                    {
                        "match": {
                            "full_name": {
                                "query": names[0],
                                "fuzziness": "AUTO",
                            }
                        }
                    },
                ],
                "must_not": {
                    "terms": {"children.benef_hash": reliable_hashes}
                },
            }
        },
    }
    es = get_elasticsearch()

    results = es.search(index="recipients-groups", body=query)

    return RecipientSearchResults.parse_obj(unpack_es_results(results))
