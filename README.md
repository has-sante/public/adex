# À propos du projet

Le Projet [ADEX](https://eig.etalab.gouv.fr/defis/adex/) s'inscrit dans le cadre du programme [Entrepreneur d'intérêt général](https://eig.etalab.gouv.fr/) mis en place au sein de la [Haute Autorité de Santé](https://www.has-sante.fr/).

Adex permet l'analyse des liens d'intérêts à partir des données issues de Transparence Santé.

Adex utilise les données de [Transparence Santé](https://www.transparence.sante.gouv.fr/) [(conditions d'utilisations)](https://www.data.gouv.fr/fr/datasets/transparence-sante-1/) et [l'annuaire RPPS de l'ANS](https://annuaire.sante.fr/web/site-pro/extractions-publiques). Assurez-vous d'avoir pris connaissance des licences d'utilisation des données, notamment que vous avez pris les mesures nécessaires pour interdire l'indexation des données par les moteurs de recherche.

Le code de ce projet est [sous licence EUPL](LICENCE).

La documentation en ligne du projet est disponible [ici](https://has-sante.pages.has-sante.fr/public/adex).

# Table des matières

- [À propos du projet](#à-propos-du-projet)
- [Table des matières](#table-des-matières)
- [Installation](#installation)
  - [Pré-requis](#pré-requis)
  - [Configuration du repository](#configuration-du-repository)
    - [Récupération du code](#récupération-du-code)
    - [Configuration des variables d'environment](#configuration-des-variables-denvironment)
    - [Création de l'arborescence pour les données des services](#création-de-larborescence-pour-les-données-des-services)
    - [Construction des images docker](#construction-des-images-docker)
    - [Test de la configuration](#test-de-la-configuration)
- [Usages](#usages)
  - [Makefile](#makefile)
  - [Lancement du préprocessing](#lancement-du-préprocessing)
  - [Development](#development)
    - [Pre commit](#pre-commit)
    - [Virtualenvs pour le dev local](#virtualenvs-pour-le-dev-local)
    - [Backend-api](#backend-api)
    - [Preprocessing](#preprocessing)
    - [Frontend](#frontend)
      - [NPM](#npm)
      - [Environement de dev](#environement-de-dev)
    - [Données](#données)
    - [Docker](#docker)
    - [Troubleshoot](#troubleshoot)
      - [Problèmes de certificats](#problèmes-de-certificats)
- [Contribution](#contribution)

# Installation

L'installation s'appuie sur docker et devrait fonctionner sur un system UNIX (pour windows, utiliser wsl).

## Pré-requis

- docker
- docker-compose
- python 3.8
- poetry
- libpq-dev (pour la communication avec postgres)

## Configuration du repository

### Récupération du code

Récupération du code depuis gitlab :

    git clone https://gitlab.has-sante.fr/has-sante/public/adex.git
    cd adex

### Configuration des variables d'environment

Copie du template vers `.env`:

    cp template.env .env  # Crée un fichier de configuration

Remplacer les variables dans le `.env`.

Pour les variables `$PATH_TO_APPLICATIONS_DATA` et `$PATH_TO_DATA`, une bonne pratique est d'utiliser un dossier du type `$HOME/docker-volumes` afin de stocker les volumes de manière persistente pour tous les projets utilisant docker.

### Création de l'arborescence pour les données des services

En fonction des variables `$PATH_TO_DATA` et `$PATH_TO_APPLICATIONS_DATA` configurées dans le fichier `.env`, il faut créer une arborescence de dossier avec les bons droits afin que les différents services puissent écrire dedans.

Création des dossiers racines (remplacer `PATH_TO_YOUR_DATA` et `PATH_TO_YOUR_APPLICATIONS_DATA` par le chemin renseigné dans le `.env` :

    mkdir PATH_TO_YOUR_DATA
    mkdir PATH_TO_YOUR_APPLICATIONS_DATA
    cd PATH_TO_YOUR_APPLICATIONS_DATA

Elasticsearch (cf [docs](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#_configuration_files_must_be_readable_by_the_elasticsearch_user)):

    mkdir esdatadir
    sudo chmod g+rwx esdatadir
    sudo chgrp 0 esdatadir

Pgadmin (cf [docs](https://www.pgadmin.org/docs/pgadmin4/development/container_deployment.html#mapped-files-and-directories)):

    mkdir pgadmindatadir
    sudo chown -R 5050:5050 pgadmindatadir

### Construction des images docker

Afin de lancer le build des images docker, utiliser :

    make build-dev
    make build-prod # Optionnel

### Test de la configuration

Pour pouvoir lancer en mode développement, il est nécessaire d'installer les paquets npm, cf [section installation frontend](#frontend).

On peut tester la configuration en lancant les services sans données :

    make up-dev # Après l'installation des paquest NPM
    make up-prod # Ne nécessite pas l'installation des paquets npm, mais d'avoir lancé `make build-prod`

On a alors accès (selon les ports définis dans le `.env`) :

- [ADEX : http://localhost](http://localhost)
- [ADEX API docs : http://localhost/docs](http://localhost/docs)
- [Frontend port dev : http://localhost:8081](http://localhost:8081)
- [Backend-api docs port dev : http://localhost:8082/docs](http://localhost:8082/docs)
- [Backend-api redoc port dev : http://localhost:8082/redoc](http://localhost:8082/redoc)
- [Pgadmin : http://localhost:6544](http://localhost:6544) (le user et pwd sont dans la section `pgadmin` du `.env`
- [Kibana : http://localhost:5601](http://localhost:5601)

# Usages

## Makefile

Le `Makefile` permet de lancer la plupart des commandes nécessaire pour développer sur le projet.

Les commandes sont commentées dans le fichier [Makefile](Makefile).

Les plus utilisées sont :

- `make build-dev` : constructions des images docker
- `make processing-dev` : lance le preprocessing sur un petit ensemble de dev
- `make up-dev` : lance tous les services
- `make tests-dev` : lance les tests du backend, du frontend et du preprocessing

## Lancement du préprocessing

Afin de faire les différentes étapes de préprocessing sur un sous ensemble de données, il suffit de lancer :

    make processing-dev

On peut alors relancer les services et chercher `Bruno Francis` dans le l'[interface](http://localhost) après avoir lancé les services :

    docker-compose up -d

## Development

### Pre commit

Pre commit doit être installé localement

    pip install --user pre-commit

### Virtualenvs pour le dev local

Installation des dépendances pour le backend et le preprocessing

    cd backend_api/src/
    poetry install
    cd preprocessing/src/
    poetry install

Pour preprocessing, il faudra installer les dépendances système suivantes:

```
sudo apt install libpq-dev
```

### Backend-api

Par défaut, le backend sera lancé avec le volume de code et un auto reload lorsqu'on modifie le code :

    # Lancement du service
    docker-compose up -d backend-api
    # Lancement de vscode
    workon backend_api
    code backend-api/src
    # Affichage des logs
    docker-compose logs -f backend-api # Pour voir les logs du backend

Pour qu'il ne soit pas lancé par défaut, on peu commenter dans le docker-compose.override.yml la command `/start-reload.sh`, et décommenter la ligne `command: bash -c "while true; do echo hello; sleep 2; done"`.

Ainsi le container est lancé, mais ne fait rien. On peut alors lancer un bash dans celui-ci et lancer le `start-reload.sh`

    make backend-api-bash
    > /start-reload.sh # Depuis le container

### Preprocessing

Même setup que pour le backend-api (virtualenv en local). Mais il n'y a pas de service lancé.

On peut lancer un shell, et lancer la commande `process.py` qui lance les traitement :

    make preprocessing-bash
    > process --help
    > ingest --help

### Frontend

#### NPM

Le projet utilise node 12 (LTS). Pour gêrer plusieurs versions de npm et installer une version compatible il est conseillé d'utiliser [nvm](https://github.com/nvm-sh/nvm)

#### Environement de dev

Lancer depuis le dossier `frontend/src` l'installation :

    npm install

On peut alors lancer le serveur de dev pour le développement en local :

    npm run dev

L'application est alors disponible sur https://localhost:3000

Un storybook est disponible pour un certain nombre de composants. Pour développer avec storybook :

    npm run storybook

Le storybook est alors disponible sur http://localhost:6006

### Données

Il peut être nécessaire de modifier le jeu de données de test pour rajouter des cas d'usages précis, pour ça la procédure est la suivante :

1. Aller sur ADEX en production
2. Rechercher la personne à ajouter
3. Selectionner les personnes d'intérêts à garder
4. Faire un export excel
5. Ouvrir l'excel, dans l'onglet "Données Brutes", copier le contenu de la colonne `ligne_identifiant` puis ajouter le au fichier `preprocessing/src/scripts/resources/ts_ligne_ids.txt`
6. Copier l'id RPPS de la personne et l'ajouter au fichier `preprocessing/src/scripts/resources/ans_identifiant_rpps.txt`
7. Lancer:
   ```shell
   make preprocessing-bash
   ```
8. Dans le shell du container executer:
   ```shell
   ./scripts/create_dev_dumps.py
   ```
   Cela va générer des fichiers zip dans `preprocessing/src/scripts/resources`
9. Commiter le tout

### Docker

Le projet utilise docker et docker-compose pour encapsuler le maximum de logique de déploiement.

Il peut être nécessaire de rebuilder qu'une seule image des services listés dans le docker-compose.

Pour cela il est possible de faire par exemple:

```shell
docker-compose build preprocessing
```

### Troubleshoot

#### Problèmes de certificats

Lorsqu'on execute les traitements sur l'infrastructure de la HAS, il est possible de rencontrer des problèmes liés au certificats.
Pour les résoudre voir le [tutoriel du wiki data](https://gitlab.has-sante.fr/has-sante/wiki-data/-/wikis/Tutoriels/Certificat-HAS)

# Contribution

Lors du développement d'une PR, veuillez mettre à jour la section "A Venir" du fichier CHANGELOG.md. Cela facilitera sa mise à jour lors des déploiements de nouvelles versions.
