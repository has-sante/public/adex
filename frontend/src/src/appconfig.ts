const isTrue = (envVar: String | null | undefined) => {
  if (!envVar) {
    return false;
  }
  return envVar.toLowerCase() === "true";
};

export const features = {
  FEATURE_DISPLAY_KV: isTrue(process.env.REACT_APP_FEATURE_DISPLAY_KV),
  DISABLE_TRACKING: isTrue(process.env.REACT_APP_DISABLE_TRACKING),
  DISPLAY_SCORE: isTrue(process.env.REACT_APP_DISPLAY_SCORE),
};

console.log(features);
