import React from "react";
import { Layout } from "antd";
import { BrowserRouter as Router } from "react-router-dom";

import { basename } from "Adapters/primary/apis/config";

import "./App.less";

import TransparenceSantePage from "../transparenceSante-page";
import { AdexHeader } from "../../Components/Header";

const App = () => (
  <Layout className={"App"}>
    <Router basename={basename}>
      <AdexHeader />
      <TransparenceSantePage />
    </Router>
  </Layout>
);

export default App;
