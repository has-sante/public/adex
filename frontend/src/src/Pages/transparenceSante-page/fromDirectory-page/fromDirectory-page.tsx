import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { Grid, Layout, Row, Col } from "antd";

import type { IRoutesParams } from "../index";
import { useAppDispatch } from "Core/store/hooks";
import { getBeneficiaryDetailsFromDirectoryThunk } from "Core/useCases/getBeneficiaryDetailsFromDirectory/thunks";

import { useTrackPage } from "Components/ATTracker/useTrackPage";

import GoToLinksOfInterests from "Components/GoToLinksOfInterests/GoToLinksOfInterests";
import { EnhancedBeneficiaryCard } from "Components/EnhancedBeneficiaryCard";
import { HeaderResultsTS } from "Components/HeaderResultsTS";
import { BeneficiariesFromTS } from "Components/BeneficiariesFromTS/BeneficiariesFromTS";

const { useBreakpoint } = Grid;

export const FromDirectoryPage = () => {
  useTrackPage({ name: "recherche_via_ans" });
  const { identification_nationale_pp } = useParams<IRoutesParams>();
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(
      getBeneficiaryDetailsFromDirectoryThunk({ identification_nationale_pp })
    );
  }, [identification_nationale_pp]);

  const screens = useBreakpoint();

  const mobileLayout = (
    <Layout>
      <Layout.Content style={{ display: "flex", flexDirection: "column" }}>
        <div style={{ flex: "1", overflowY: "auto" }}>
          <Row>
            <Col xs={24} className={"containerBlue-7"}>
              <EnhancedBeneficiaryCard />
            </Col>
          </Row>
          <HeaderResultsTS />
          <BeneficiariesFromTS
            identification_nationale_pp={identification_nationale_pp}
          />
        </div>
        <Row>
          <Col>
            <GoToLinksOfInterests />
          </Col>
        </Row>
      </Layout.Content>
    </Layout>
  );

  const standardLayout = (
    <Layout>
      <Layout.Content style={{ display: "flex" }}>
        <Row style={{ flex: "1" }}>
          <Col sm={5} lg={4} className={"containerBlue-7"}>
            <EnhancedBeneficiaryCard />
          </Col>
          <Col
            sm={19}
            lg={20}
            style={{
              display: "flex",
              flexDirection: "column",
              overflow: "auto",
            }}
          >
            <HeaderResultsTS />
            <BeneficiariesFromTS
              identification_nationale_pp={identification_nationale_pp}
            />
            <GoToLinksOfInterests />
          </Col>
        </Row>
      </Layout.Content>
    </Layout>
  );
  return screens.xs ? mobileLayout : standardLayout;
};
