import React from "react";
import { Row, Col } from "antd";

import { ScrollToTopOnMount } from "Components/ScrollToTopOnMount";
import {
  ReactMarkdownWithAnchors,
  ReactMarkdownTOC,
} from "Components/ReactMarkdowWithLinks";
import { AdexFooter } from "Components/Footer";
import { PageHeader } from "Components/PageHeader";

import faq from "./faq.md";
import faqTOC from "./faq-TOC.md";

export const FAQPage = () => {
  return (
    <>
      <ScrollToTopOnMount />
      <PageHeader
        title={"Questions Fréquentes"}
        illustration_url={"./images/Illustration-FAQ.svg"}
      />
      <Row className={"content"}>
        <Col
          xs={{ offset: 1, span: 23 }}
          md={{ offset: 2, span: 22 }}
          lg={{ offset: 2, span: 6 }}
          xl={{ offset: 4, span: 4 }}
        >
          <ReactMarkdownTOC>{faqTOC}</ReactMarkdownTOC>
        </Col>
        <Col
          xs={{ offset: 1, span: 23 }}
          md={{ offset: 2, span: 22 }}
          lg={{ span: 16 }}
          xl={{ offset: 1, span: 12 }}
        >
          <ReactMarkdownWithAnchors>{faq}</ReactMarkdownWithAnchors>
        </Col>
      </Row>
      <AdexFooter></AdexFooter>
    </>
  );
};
