Questions générales
===================

Qu’est-ce qu’ADEX ?
-------------------

ADEX est un outil qui permet de retrouver facilement un professionnel de
santé ou une personne morale (association de patients, établissement de
santé, organisme professionnel, etc.) de la base Transparence Santé et
d’afficher ses liens d’intérêts financiers avec les industries de
santé.

D’où viennent les informations d’ADEX ?
---------------------------------------

ADEX utilise des données ouvertes issues de l’*Annuaire Santé* de
l’Agence du Numérique en Santé (ANS) ainsi que de la base de données
publique [*Transparence Santé*](https://transparence.sante.gouv.fr/).

Quelles informations vais-je trouver sur ADEX ?
-----------------------------------------------

Vous trouverez sur ADEX des informations relatives aux professionnels de
santé (nom, prénom, identifiant professionnel et lieu(x) d’exercice) et
personnes morales (Nom de l’établissement, Siren, adresse), ainsi que les
conventions, avantages et rémunérations octroyées par des industriels à des
professionnels de santé ou des personnes morales.

ADEX ne contient pas d’informations issues de Déclarations Publiques
d’Intérêt (DPI).

Les données sont-elles à jour ?
-------------------------------

Les données d’ADEX sont synchronisées toutes les nuits avec celles de la
base Transparence Santé. Le décalage entre les deux sera donc de un jour
maximum. La date de mise à jour des données est indiquée en pied de page
ainsi que dans les [mentions légales](https://adex.has-sante.fr/mentions-legales).

Les déclarations des industriels sur Transparence Santé sont
semestrielles, avec une mise en ligne en mars et en septembre.
C’est pourquoi vous ne trouverez généralement pas de
liens d’intérêts financiers datant de moins de 6 mois. La base
Transparence Santé est cependant alimentée en continu, car des corrections
sur les déclarations sont régulièrement apportées.

Quelle est la différence entre ADEX et le site Transparence Santé ?
-------------------------------------------------------------------

ADEX croise les données de [*Transparence
Santé*](https://transparence.sante.gouv.fr/) avec celles de l’*Annuaire
Santé* de l’Agence du Numérique en Santé (ANS) afin de pouvoir retrouver
plus facilement un professionnel de santé et afficher ses liens
d’intérêt financiers.

Le croisement de ces deux sources de données permet plusieurs choses :

-   Rechercher une personne par nom, prénom, identifiant professionnel
    et/ou lieu d’activité, et non plus seulement avec son nom de famille
    comme c’est le cas sur le site Transparence Santé. Cela permet de
    gagner du temps et d’éviter de rater des bénéficiaires à cause
    d’erreurs dans les déclarations (un prénom et un nom de famille
    inversés par exemple).

-   Regrouper plusieurs bénéficiaires référencés sur le site
    Transparence Santé dont on est certains qu’ils correspondent à une
    seule et même personne (nom, prénom et identifiant professionnel
    identiques) pour avoir moins de lignes à sélectionner manuellement.

-   Ordonner la liste des bénéficiaires de manière à faire ressortir en
    premier les résultats les plus pertinents et ainsi éviter de
    parcourir de nombreuses pages de résultats.

Par ailleurs, les liens d’intérêts financiers issus de Transparence
Santé ont été traités de manière à être plus facilement exploitables :

-   Les données relatives aux montants perçus sont regroupées par
    industrie, par année et par type pour plus de lisibilité.

-   Lorsque c’est possible, les avantages et rémunérations sont
    regroupés avec la convention liée, pour vous permettre de mieux
    apprécier le contexte global de la convention.

-   Les totaux des montants perçus sont automatiquement calculés.

Enfin, l’ensemble de la synthèse peut être téléchargé sous forme de
tableur.

Quelle est la différence entre ADEX et le site DPI Santé ?
----------------------------------------------------------

ADEX référencie les informations déclarées par les industries de santé
sur la base Transparence Santé, tandis que DPI Santé référencie les
Déclarations Publiques d’Intérêts remplies par les déclarants eux-mêmes.
DPI Santé concerne uniquement les personnes physiques mais n’est pas limité aux seuls
professionnels de santé.

Je suis un professionnel de santé, et je ne comprends pas certaines des informations me concernant ou bien celles-ci sont erronées. Que dois-je faire ?
-------------------------------------------------------------------------------------------------------------------------------------------------------

ADEX exploite des informations provenant de l’*Annuaire Santé* de
l’Agence du Numérique en Santé (ANS) ainsi que de la base de données
publique [*Transparence Santé*](https://transparence.sante.gouv.fr/),
mais ne les édite pas. Pour demander une rectification des données vous
concernant, nous vous conseillons de contacter directement le producteur
de ces données :

-   L’*Annuaire Santé* de l’ANS si l’erreur se situe au niveau des
    suggestions du moteur de recherche ou dans la barre latérale bleue
    de la synthèse.

    ![](./images/faq/FAQ-1.png)

    ![](./images/faq/FAQ-2.png)

-   [*Transparence Santé*](https://transparence.sante.gouv.fr/) si
    l’erreur se situe au niveau de la sélection des bénéficiaires ou de
    la synthèse des liens d’intérêts financiers. Vous aurez probablement
    besoin de l’identifiant de la déclaration erronée.

    ![](./images/faq/FAQ-3.png)

Comment en savoir plus sur le fonctionnement précis d’ADEX ?
------------------------------------------------------------

ADEX est édité en open source et le code ainsi que toute la
documentation technique et de conception sont consultables sur [le dépôt
Gitlab](https://gitlab.has-sante.fr/has-sante/public/adex/) du projet.

Rechercher un bénéficiaire
==========================

Qu’est-ce qu’un bénéficiaire ?
------------------------------

Un bénéficiaire de Transparence Santé est une personne physique ou
morale ayant signé une convention avec un industriel et/ou reçu de
l’argent ou un avantage en nature de celui-ci.

Un bénéficiaire est-il forcément un professionnel de santé ?
------------------------------------------------------------

Non, un bénéficiaire peut également être une personne morale comme par
exemple une association de patients, un établissement de santé ou encore
un organisme professionnel.

Comment trouver un bénéficiaire ?
---------------------------------

**Pour trouver un professionnel de santé**, il suffit de chercher son
nom, son prénom et/ou son identifiant professionnel dans l’onglet
« Personne physique » du champ de recherche et de cliquer sur la bonne
personne dans le menu déroulant. Si aucun résultat ne correspond à la
personne que vous recherchez, cliquez sur « Rechercher une personne qui
n’est pas dans la liste ».

![](./images/faq/FAQ-4.png)

**Pour trouver une personne morale**, entrez sa dénomination dans
l’onglet « Personne morale » du champ de recherche et cliquez sur
« Rechercher des bénéficiaires ».

![](./images/faq/FAQ-5.png)

Dans les deux cas, vous aurez alors accès à une liste de bénéficiaires
de Transparence Santé susceptibles de correspondre, triés par ordre de
pertinence. Sélectionnez *toutes* les lignes correspondant à votre
recherche pour afficher les liens d’intérêts financiers. Si aucune ligne
ne semble correspondre, cela signifie qu’aucune déclaration n’a pu être
reliée à votre recherche sur Transparence Santé.

![](./images/faq/FAQ-6.png)

Pourquoi dois-je sélectionner plusieurs fois un même bénéficiaire ?
-------------------------------------------------------------------

Il n’existe pas d’identifiant unique pour un bénéficiaire dans
Transparence Santé, et il est de la responsabilité de l’industriel de
renseigner les informations nécessaires à l’identification du
bénéficiaire d’une déclaration. Ainsi, selon l’entreprise qui a soumis
une déclaration, le nom, l’adresse, la profession ou même l’identifiant
(RPPS, Ordre, Adeli, Siren) renseigné peut varier pour un même bénéficiaire,
entraînant autant d‘occurrences de ce bénéficiaire que de différences de
saisies.

Pourquoi y-a-t-il parfois plusieurs identifiants professionnels pour un même bénéficiaire ?
-------------------------------------------------------------------------------------------

Il est possible que l’industriel de santé ait mal renseigné
l’identifiant professionnel d’un bénéficiaire, ce qui explique ces
différences. Les autres informations affichées (comme l’adresse
d’exercice ou la spécialité) pourront alors vous aider à apprécier s’il
s’agit de la même personne ou non.

Pourquoi des bénéficiaires qui n’ont rien à voir avec la personne que j’ai sélectionnée s’affichent-ils ?
---------------------------------------------------------------------------------------------------------

ADEX classe la totalité des bénéficiaires de la base Transparence Santé
par ordre de pertinence. Si plus aucune ligne ne semble correspondre à
votre recherche, vous pouvez arrêter de faire défiler les résultats et
passer à l’étape suivante.

Pourquoi plusieurs bénéficiaires sont-ils regroupés en un seul ?
----------------------------------------------------------------

Si le nom, le prénom et l’identifiant professionnel de plusieurs
bénéficiaires de Transparence Santé correspondent parfaitement, ADEX
considère qu’il s’agit du même bénéficiaire et les regroupe donc
automatiquement pour vous faire gagner du temps. Il est possible de
dérouler le groupe pour vérifier qu’il n’y a pas d’erreurs.

Comment puis-je être sûr(e) de sélectionner le bon bénéficiaire ?
-----------------------------------------------------------------

Il existe malheureusement de nombreuses erreurs de saisies sur la base
Transparence Santé, rendant difficile de sélectionner avec certitude les
bons bénéficiaires. Pour vous aider à y voir plus clair, ADEX surligne
automatiquement les informations les plus pertinentes comme le nom, le
prénom et l’identifiant professionnel.

Afficher les liens d’intérêts financiers
========================================

Comment lire le tableau de synthèse ?
-------------------------------------

Tous les liens d’intérêts financiers correspondant au bénéficiaire
sélectionné sont classés par entreprise puis par année. Vous pouvez
dérouler uniquement les entreprises et années qui vous intéressent, ou
bien tout déplier en un seul clic.

![](./images/faq/FAQ-7.png)

Toutes les déclarations sont regroupées dans la convention qui les lie.
Il est à noter qu’une convention peut s’étaler sur une ou plusieurs
années. Dans ce cas, l’année de la déclaration liée la plus récente et
l’année de la déclaration liée la plus ancienne sont retenues pour trier
chronologiquement les déclarations.

![](./images/faq/FAQ-8.png)

Sur la droite du tableau, les montants perçus sont indiqués dans trois
colonnes, tels qu’ils ont été déclarés par l’industriel. Théoriquement,
chaque avantage et chaque rémunération est liée à une convention (au moins
à partir de 2018). Le montant indiqué pour la convention peut être soit
l’enveloppe globale prévue pour la durée de la convention, soit le montant
total réel versé. Les montants des avantages et rémunérations correspondent
aux montants réellement dépensés par les entreprises.
Ainsi, lorsqu’une convention est échue, le montant déclaré dans la convention
est censé correspondre au total des rémunérations et avantages. Cependant,
il peut arriver que cela ne soit pas le cas. Plusieurs raisons sont possibles:
- tous les avantages et rémunérations n’ont peut être pas encore été déclarés
- le montant initial de la convention peut avoir été mal estimé et l’information n’a pas été corrigée
- certains avantages ou rémunérations n’ont pas correctement été associés à la convention
- aucun montant n’a été déclaré dans la convention

ADEX affiche les différents montants pour vous permettre d’apprécier la
cohérence des données. *Il est important de ne pas sommer les montants des
conventions avec les montants des avantages et rémunérations au risque de
comptabiliser plusieurs fois un même flux financier.*

![](./images/faq/FAQ-9.png)

Pour tenir compte de ces potentiels doublons de flux financiers, le montant
total des liens d’intérêt est présenté sous forme de fourchette
([Comment cette fourchette est-elle
calculée ?](/questionsFrequentes/#comment-est-calcule-le-montant-total-des-liens-dinterets-financiers)).

![](./images/faq/FAQ-10.png)

D’où viennent les données figurant sur le tableau de synthèse ?
---------------------------------------------------------------

Les données du tableau de synthèse ont été renseignées par les
industries de santé eux-mêmes sur le site Transparence Santé. Depuis la
loi Bertrand de 2011, ces entreprises ont l’obligation légale de
renseigner certains types d’accords qu’ils concluent avec des
professionnels de santé ou des personnes morales (institutions,
associations, etc.). Il existe 3 types de déclarations : les
conventions, les rémunérations et les avantages.

Qu’est-ce qu’une convention ?
-----------------------------

D’après Transparence Santé, « *les conventions entre les entreprises et
les acteurs de la santé sont des accords impliquant des obligations de
part et d’autre. Il s’agit, par exemple, de la participation à un
congrès en tant qu’orateur (obligation remplie par le professionnel),
avec prise en charge du transport et de l’hébergement (obligation
remplie par l’entreprise). Les conventions peuvent aussi avoir pour
objet une activité de recherche ou des essais cliniques sur un produit
de santé, la participation à un congrès scientifique, une action de
formation, etc.* »

Il existe plus de 5,5 millions de conventions dans la base Transparence
Santé, généralement reliées à des avantages et des rémunérations
correspondant à la contrepartie perçue par le professionnel de santé.

Qu’est-ce qu’une rémunération ?
-------------------------------

D’après Transparence Santé, « *les rémunérations sont les sommes versées
par les entreprises à un acteur de la santé (professionnel de santé ou
personne morale) en contrepartie de la réalisation d’un travail ou d’une
prestation.* »

Ainsi, les rémunérations sont versées dans le cadre de
[conventions](/questionsFrequentes/#quest-ce-quune-convention) qui établissent les
contreparties de ces rémunérations, c’est-à-dire ce à quoi s’engage le
professionnel de santé.

Qu’est-ce qu’un avantage ?
--------------------------

D’après Transparence Santé, « *les avantages pris en compte dans la base
de données Transparence Santé recouvrent tout ce qui est alloué ou versé
sans contrepartie par une entreprise à un acteur de la santé (don de
matériel, repas, transport, hébergement, etc.).* »

Il peut s’agir d’avantages en nature ou en espèce, directs ou indirects.

Pourquoi des déclarations de plus de 5 ans apparaissent-elles dans le tableau de synthèse ?
-------------------------------------------------------------------------------------------

Il existe malheureusement beaucoup d’erreurs dans la base Transparence
Santé : un certain nombre de dates ne sont pas saisies de manière
standard et il y a parfois des incohérences entre les dates des
conventions et les avantages et rémunérations liés.

Les données exposées par Transparence Santé sur le site
[data.gouv](https://www.data.gouv.fr/en/datasets/transparence-sante-1/)
contiennent des déclarations qui respectent cette contrainte de 5 ans maximum
d’ancienneté. Mais, à cause des erreurs de saisies, il y a aussi des déclarations
dont il n’a pas été possible de déterminer si elles étaient antérieures à
la période de 5 ans (pas de date de fin sur une convention, date mal formatée...).

L’équipe ADEX a fait le choix d’exposer toutes les données proposées par l’export de
[data.gouv](https://www.data.gouv.fr/en/datasets/transparence-sante-1/) après avoir consulté
l’équipe de Transparence Santé.

Pourquoi le total des montants des conventions, rémunérations et avantages sont-ils calculés séparément ?
--------------------------------------------------------------------------------------------------------

Dans le cadre d’une convention, un industriel de santé va verser une
contrepartie financière ou en nature au bénéficiaire. Or, selon
l’entreprise qui a soumis la déclaration, le montant de cette
contrepartie peut être indiqué au niveau de la convention (de manière
anticipé ou non), au niveau de la rémunération ou de l’avantage lié, ou
les deux (générant ainsi des doublons). Qui plus est, les montants au
niveau de la convention et des avantages et rémunération liés peuvent ne
pas être identiques, par exemple si toutes les rémunérations relatives à
une convention n’ont pas été encore versées ou déclarées.

Afin d’éviter les erreurs, les totaux des conventions et ceux des
rémunérations et avantages sont donc calculés et présentés séparément,
afin de vous aider à apprécier les éventuels doublons et adapter votre
calcul.

Comment est calculé le montant total des liens d’intérêts financiers ?
----------------------------------------------------------------------

Comme il existe de nombreux doublons de montants dus au fait que les
industriels de santé [ne renseignent pas tous les montants de
conventions de la même
manière](/questionsFrequentes/#pourquoi-le-total-des-conventions-remunerations-et-avantages-sont-ils-calcules-separment),
ADEX propose une fourchette calculée comme ceci :

-   **Montant minimum =** total des avantages + total des rémunérations

-   **Montant maximum =** total des avantages et rémunérations non liées
à des conventions + pour chaque convention, le montant le plus élevé entre
soit le montant de la convention, soit le montant total des avantages et
rémunérations associées.

Est-il possible de filtrer les données du tableau de synthèse ?
---------------------------------------------------------------

Non, il n’est pour le moment pas possible de filtrer les données du
tableau de synthèse. Néanmoins, il est possible d’exporter ce tableau
sous forme de tableur dans lequel vous pourrez sans problème filtrer les
résultats par année, entreprise ou type de déclaration.

Exporter les liens d’intérêts financiers
========================================

Comment exporter le tableau de synthèse ?
-----------------------------------------

Une fois que vous avez sélectionné votre bénéficiaire et affiché le
tableau de ses liens d’intérêts financiers, il suffit de cliquer sur le
bouton « Exporter la synthèse » situé en bas de l’écran pour l’exporter
sous forme de tableur.

Quel type de fichier est-il possible d’exporter ?
-------------------------------------------------

Le fichier est exporté au format XLS et peut être utilisé avec
LibreOffice, OpenOffice, Excel, Google Sheets, Numbers, etc.

Est-il possible de filtrer les données exportées ?
--------------------------------------------------

Il n’est pas possible de filtrer en amont les données qui seront
exportées.

Une fois exportées, il est possible de filtrer certaines données de la
synthèse depuis les en-têtes du tableau.

![](./images/faq/FAQ-11.png)

D’où viennent les informations sur l’en-tête du fichier exporté ?
-----------------------------------------------------------------

Les informations relatives au professionnel de santé (nom, prénom, RPPS
et lieu(x) d’exercice) sont issues de l’*Annuaire Santé* de l’Agence du
Numérique en Santé (ANS).

Pourquoi l’en-tête du fichier exporté est-il vide ?
---------------------------------------------------

Si vous n’avez pas sélectionné un professionnel de santé dans les
suggestions du moteur de recherche ou si votre synthèse concerne une
personne morale, alors aucune donnée ne pourra être affichée dans
l’en-tête. Néanmoins, vous pouvez si vous le souhaitez remplir ces
informations vous-même dans le fichier exporté.

Pourquoi l’export affiche-t-il une fourchette pour le montant total des liens d’intérêts financiers ?
------------------------------------------------------------------------------------------

Comme il existe de nombreux doublons de montants dus au fait que les
industriels de santé [ne renseignent pas tous les montants de
conventions de la même manière](/questionsFrequentes/#pourquoi-le-total-des-conventions-remunerations-et-avantages-sont-ils-calcules-separment),
ADEX propose une fourchette calculée comme ceci :

-   **Montant minimum =** total des avantages + total des rémunérations

-   **Montant maximum =** total des avantages et rémunérations non liées
à des conventions + pour chaque convention, le montant le plus élevé entre
soit le montant de la convention, soit le montant total des avantages et
rémunérations associées.

Cette fourchette est automatiquement mise-à-jour si vous filtrez les
données.

Pourquoi la fourchette de montant des liens d’intérêts financiers ne s’affiche-t-elle pas dans le fichier exporté ?
-------------------------------------------------------------------------------------------------------------------

Une fois que vous avez ouvert le fichier exporté avec Excel ou tout
autre logiciel compatible, assurez-vous de bien avoir autorisé la
modification du fichier. Les sommes automatiques des formules seront
alors calculées.

Est-il possible de télécharger les données brutes relatives à un bénéficiaire de Transparence Santé ?
-----------------------------------------------------------------------------------------------------

Oui, les données brutes sont automatiquement exportées avec la synthèse,
dans l’onglet « Données brutes » du fichier généré.
