Questions générales
===================

Qu’est-ce qu’ADEX ?
-------------------

D’où viennent les informations d’ADEX ?
---------------------------------------

Quelles informations vais-je trouver sur ADEX ?
-----------------------------------------------

Les données sont-elles à jour ?
-------------------------------

Quelle est la différence entre ADEX et le site Transparence Santé ?
-------------------------------------------------------------------

Quelle est la différence entre ADEX et le site DPI Santé ?
----------------------------------------------------------

Je suis un professionnel de santé, et je ne comprends pas certaines des informations me concernant ou bien celles-ci sont erronées. Que dois-je faire ?
-------------------------------------------------------------------------------------------------------------------------------------------------------

Comment en savoir plus sur le fonctionnement précis d’ADEX ?
------------------------------------------------------------

Rechercher un bénéficiaire
==========================

Qu’est-ce qu’un bénéficiaire ?
------------------------------

Un bénéficiaire est-il forcément un professionnel de santé ?
------------------------------------------------------------

Comment trouver un bénéficiaire ?
---------------------------------

Pourquoi dois-je sélectionner plusieurs fois un même bénéficiaire ?
-------------------------------------------------------------------

Pourquoi y-a-t-il parfois plusieurs identifiants professionnels pour un même bénéficiaire ?
-------------------------------------------------------------------------------------------

Pourquoi des bénéficiaires qui n’ont rien à voir avec la personne que j’ai sélectionnée s’affichent-ils ?
---------------------------------------------------------------------------------------------------------

Pourquoi plusieurs bénéficiaires sont-ils regroupés en un seul ?
----------------------------------------------------------------

Comment puis-je être sûr(e) de sélectionner le bon bénéficiaire ?
-----------------------------------------------------------------

Afficher les liens d’intérêts financiers
========================================

Comment lire le tableau de synthèse ?
-------------------------------------

D’où viennent les données figurant sur le tableau de synthèse ?
---------------------------------------------------------------

Qu’est-ce qu’une convention ?
-----------------------------

Qu’est-ce qu’une rémunération ?
-------------------------------

Qu’est-ce qu’un avantage ?
--------------------------

De quand datent les données du tableau de synthèse ?
----------------------------------------------------

Pourquoi des déclarations de plus de 5 ans apparaissent-elles dans le tableau de synthèse ?
-------------------------------------------------------------------------------------------

Pourquoi le total des conventions et celui des rémunérations et avantages sont-ils calculés séparément ?
--------------------------------------------------------------------------------------------------------

Comment est calculé le montant total des liens d’intérêts financiers ?
----------------------------------------------------------------------

Est-il possible de filtrer les données du tableau de synthèse ?
---------------------------------------------------------------

Exporter les liens d’intérêts financiers
========================================

Comment exporter le tableau de synthèse ?
-----------------------------------------

Comment exporter le graphique ?
-------------------------------

Quel type de fichier est-il possible d’exporter ?
-------------------------------------------------

Est-il possible de filtrer les données exportées ?
--------------------------------------------------

D’où viennent les informations sur l’en-tête du fichier exporté ?
-----------------------------------------------------------------

Pourquoi l’en-tête du fichier exporté est-il vide ?
---------------------------------------------------

Pourquoi l’export affiche-t-il une fourchette de montant des liens d’intérêts financiers ?
------------------------------------------------------------------------------------------

Pourquoi la fourchette de montant des liens d’intérêts financiers ne s’affiche-t-elle pas dans le fichier exporté ?
-------------------------------------------------------------------------------------------------------------------

Est-il possible de télécharger les données brutes relatives à un bénéficiaire de Transparence Santé ?
-----------------------------------------------------------------------------------------------------
