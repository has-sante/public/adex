import React from "react";
import { Grid, Layout, Row, Col } from "antd";
import { useParams } from "react-router-dom";

import { useTrackPage } from "Components/ATTracker/useTrackPage";

import { HeaderResultsTS } from "Components/HeaderResultsTS";
import { BeneficiariesFromTS } from "Components/BeneficiariesFromTS/BeneficiariesFromTS";
import GoToLinksOfInterests from "Components/GoToLinksOfInterests/GoToLinksOfInterests";

const { useBreakpoint } = Grid;

import type { IRoutesParams } from "../index";

export const BrowseTSPage = () => {
  useTrackPage({ name: "recherche_libre" });
  const { searchString } = useParams<IRoutesParams>();

  const screens = useBreakpoint();
  const mobileLayout = (
    <Layout>
      <Layout.Content style={{ display: "flex", flexDirection: "column" }}>
        <div style={{ flex: "1", overflowY: "auto" }}>
          <Row>
            <Col xs={24}>
              <HeaderResultsTS />
            </Col>
          </Row>
          <BeneficiariesFromTS search_query={searchString} />
        </div>
        <Row>
          <Col xs={24}>
            <GoToLinksOfInterests />
          </Col>
        </Row>
      </Layout.Content>
    </Layout>
  );
  const standardLayout = (
    <Layout>
      <Layout.Content style={{ display: "flex", flexDirection: "column" }}>
        <Row style={{ flex: "1" }}>
          <Col xs={24}>
            <HeaderResultsTS />
            <BeneficiariesFromTS search_query={searchString} />
          </Col>
        </Row>
        <Row>
          <Col xs={24}>
            <GoToLinksOfInterests />
          </Col>
        </Row>
      </Layout.Content>
    </Layout>
  );
  return screens.xs ? mobileLayout : standardLayout;
};
