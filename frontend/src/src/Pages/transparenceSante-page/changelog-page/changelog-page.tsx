import React from "react";
import ReactMarkdown from "react-markdown";
import { Row, Col } from "antd";

import { ScrollToTopOnMount } from "Components/ScrollToTopOnMount";
import { PageHeader } from "Components/PageHeader";
import { AdexFooter } from "Components/Footer";

import changelog from "Assets/CHANGELOG.md";

export const ChangelogPage = () => {
  return (
    <>
      <ScrollToTopOnMount />
      <PageHeader
        title={"Mises à jour"}
        illustration_url={"./images/Illustration-MentionsLegales.svg"}
      />
      <Row className={"content"}>
        <Col
          xs={{ offset: 1, span: 23 }}
          md={{ offset: 2, span: 22 }}
          lg={{ offset: 8, span: 16 }}
          xl={{ offset: 4, span: 12 }}
        >
          <ReactMarkdown>{changelog}</ReactMarkdown>
        </Col>
      </Row>
      <AdexFooter></AdexFooter>
    </>
  );
};
