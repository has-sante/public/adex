import React, { useEffect } from "react";
import { Layout, Row, Col } from "antd";

import { EnhancedBeneficiaryCard } from "Components/EnhancedBeneficiaryCard";
import { GoBackToSelectionPage } from "Components/GoBackToSelectionPage";
import ProfessionalSummary from "Components/ProfessionalSummary/ProfessionalSummary";
import { SyntheseFooter } from "Components/SyntheseFooter/SyntheseFooter";

import { useParsedQuery } from "Hooks/useQueryParamsInURL";
import { useAppDispatch } from "Core/store/hooks";

import { getSyntheseThunk } from "Core/useCases/synthese/thunks";
import { getBeneficiaryDetailsFromDirectoryThunk } from "Core/useCases/getBeneficiaryDetailsFromDirectory/thunks";

export const LinksOfInterestsPage = () => {
  const queryParams = useParsedQuery();
  const hashes = queryParams["hashes"];
  const identification_nationale_pp =
    queryParams["identification_nationale_pp"];
  const hashesArray = hashes?.split(",");

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getSyntheseThunk({ hashes: hashesArray }));
  }, [hashes]);
  useEffect(() => {
    dispatch(
      getBeneficiaryDetailsFromDirectoryThunk({ identification_nationale_pp })
    );
  }, [identification_nationale_pp]);

  return (
    <Layout>
      <Layout.Content className={"linkOfInterestContainer"}>
        <Row className={"linkOfInterestRow"}>
          {identification_nationale_pp && (
            <Col xs={24} lg={6} className={"containerBlue-7"}>
              <EnhancedBeneficiaryCard />
            </Col>
          )}

          <Col
            xs={24}
            lg={identification_nationale_pp ? 18 : 24}
            className={"rightPanel"}
          >
            <Row className={"rightPanelTopRow"}>
              <Col xs={24} className={"rightPanelTopRowCol"}>
                <GoBackToSelectionPage />
                <h2 className={"headerResultsTS"}>
                  <span className={"headerLinksOfInterestIconRed"}>€</span>{" "}
                  Liens d’intérêts financiers correspondant à votre sélection
                </h2>
                <ProfessionalSummary />
              </Col>
            </Row>
            <Row>
              <Col xs={24}>
                <SyntheseFooter />
              </Col>
            </Row>
          </Col>
        </Row>
      </Layout.Content>
    </Layout>
  );
};
