import React, { useEffect, useState, useRef } from "react";
import { Layout, Col, Button, Grid } from "antd";
import { CaretRightOutlined } from "@ant-design/icons";

import { SearchBeneficiaries } from "Components/SearchBeneficiaries";
import { LandingCard } from "Components/LandingCard";
import { AdexFooter } from "Components/Footer";
import { useTrackPage } from "Components/ATTracker/useTrackPage";
import { ROUTES } from "Pages/routes";
import { ContainedRow } from "Components/ContainedRow/ContainedRow";

const { useBreakpoint } = Grid;

export const LandingPage = () => {
  useTrackPage({ name: "searchpage" });
  const videoRef = useRef<HTMLVideoElement>(null);
  const [isPlaying, setIsPlaying] = useState(false);
  const handlePlayVideo = () => {
    if (videoRef && videoRef.current) {
      videoRef.current.play();
    }
  };
  useEffect(() => {
    if (videoRef && videoRef.current) {
      videoRef.current.addEventListener("play", () => {
        setIsPlaying(true);
      });
      videoRef.current.addEventListener("pause", () => {
        setIsPlaying(false);
      });
    }
    return;
  });

  const screens = useBreakpoint();

  return (
    <Layout>
      <Layout.Content className={"landingPage"}>
        <ContainedRow className={"jumbo"} align="middle">
          <Col
            sm={{ span: 14 }}
            md={{ span: 10, offset: 2 }}
            xl={{ span: 14, offset: 0 }}
          >
            <h1>Assurer l’indépendance de l’expertise sanitaire.</h1>
            <p>
              ADEX permet de rechercher facilement les liens d’intérêts
              financiers entre l’industrie de la santé et les professionnels,
              associations et établissements de santé.
            </p>
          </Col>
          <Col sm={{ span: 10 }} md={{ span: 8 }} xl={{ span: 10 }}>
            <img
              src={"./images/Illustration-Header.svg"}
              style={{ marginLeft: "8px" }}
            />
          </Col>
        </ContainedRow>
        <ContainedRow className={"searchBar"} justify="space-around">
          <Col xs={{ span: 24 }} md={{ span: 20 }} xl={{ span: 24 }}>
            <h1>
              Rechercher une personne physique ou morale pour voir ses liens
              d’intérêts financiers
            </h1>
            <SearchBeneficiaries />
          </Col>
        </ContainedRow>
        <ContainedRow justify="space-around">
          <Col md={{ span: 20 }} xl={{ span: 16 }}>
            <h1 className={"title centered"}>
              Pour une transparence des liens entre l’industrie et le secteur de
              la santé
            </h1>
            <hr className={"has-separator"} />
            <p className={"statement centered"}>
              En 2011, la loi relative au renforcement de la sécurité sanitaire
              du médicament et des produits de santé vise à rendre transparents
              les liens d’intérêt entre les industries et les autres acteurs du
              champ de la santé (professionnels de santé, étudiants, sociétés
              savantes, associations, médias, etc.)
            </p>
            <p className={"statement centered enAvant"}>
              En 2014, la base de données publique{" "}
              <a
                target="_blank"
                rel="noreferrer noopener"
                href="https://www.transparence.sante.gouv.fr/"
              >
                Transparence Santé
              </a>
              , sur laquelle les entreprises doivent déclarer ces liens, est
              officiellement lancée.
            </p>
          </Col>
        </ContainedRow>
        <ContainedRow className={"infographie"} justify="space-around">
          <Col xs={{ span: 24 }} md={{ span: 20 }} xl={{ span: 24 }}>
            <img
              src={
                screens.xs
                  ? "./images/Infographie-Fonctionnement-TS.png"
                  : "./images/Infographie-Fonctionnement-TS@2x.png"
              }
              width={"100%"}
              alt={
                "Infographie : Les industries de santé (pharmaceutiques, dispositifs médicaux, cosmétiques, prestataires associés, etc.) versent des avantages et rémunérations dans le cadre de conventions à des bénéficiaires physiques ou moraux (professionnels de santé, étudiants, sociétés savantes, associations de patients, établissements, recherche, médias, sociétés de conseil, éditeurs de logiciels, etc.). Tous ces liens d’intérêts financiers sont déclarés sur la base Transparence Santé par les industriels eux-mêmes et consultables par tous. "
              }
            />
          </Col>
        </ContainedRow>
        <ContainedRow justify="space-around" className={"lightGradient"}>
          <Col md={{ span: 20 }}>
            <p className={"statement centered"}>
              ADEX facilite l’accès à ces informations pour tous en traitant les
              déclarations présentes sur la base Transparence Santé et en les
              restituant dans un format lisible et compréhensible.
            </p>
          </Col>
        </ContainedRow>
        <h1 className={"title centered"}>Comment ça marche ?</h1>
        <hr className={"has-separator"} />
        <ContainedRow align="top" justify="space-around">
          <LandingCard
            illustration_url={"./images/Illustration-Recherche.svg"}
            title={"Trouvez un bénéficiaire facilement"}
            description={
              "en tapant simplement son nom ou sa raison sociale dans le moteur de recherche."
            }
          />
          <LandingCard
            illustration_url={"./images/Illustration-Selection.svg"}
            title={"Sélectionnez les déclarations correspondantes"}
            description={"retrouvées par ADEX sur la base Transparence Santé."}
          />
          <LandingCard
            illustration_url={"./images/Illustration-Synthese.svg"}
            title={"Consultez les liens d’intérêts financiers"}
            description={
              "et découvrez les montants perçus par le bénéficiaire et les entreprises qui les ont versés."
            }
          />
          <LandingCard
            illustration_url={"./images/Illustration-Export.svg"}
            title={"Paramétrez et partagez les information"}
            description={"en les téléchargeant dans un format exploitable."}
          />
        </ContainedRow>

        <h1 className={"title centered"}>Démonstration d’ADEX en vidéo</h1>
        <hr className={"has-separator"} />
        <ContainedRow justify="space-around">
          <Col>
            <button
              onClick={handlePlayVideo}
              className={`videoButton ${isPlaying ? "isPlaying" : ""}`}
            >
              <CaretRightOutlined />
              Présentation de la plateforme ADEX
            </button>
            <video ref={videoRef} controls width={"100%"}>
              <source
                src={"./assets/demo_adex_juin_2021.mp4"}
                type="video/mp4"
              />
              Sorry, your browser does not support embedded videos.
            </video>
          </Col>
        </ContainedRow>

        <h1 className={"title centered"}>Des questions, besoin d’aide ?</h1>
        <hr className={"has-separator"} />
        <ContainedRow align="top" justify="space-around">
          <LandingCard
            illustration_url={"./images/Illustration-CommentCaMarche.svg"}
            title={"Une question ?"}
            description={
              "Nous avons sûrement la réponse dans notre rubrique d’aide."
            }
            cta={"Voir les questions fréquentes"}
            cta_url={ROUTES.questionsFrequentes}
            internalLink={true}
          />
          <LandingCard
            illustration_url={"./images/Illustration-UnProbleme.svg"}
            title={"Un problème ?"}
            description={
              "Racontez-nous ce qui s’est passé et nous tâcherons d'intervenir aussi rapidement que possible."
            }
            cta={"Signaler un problème"}
            cta_email={"mailto:adex@has-sante.fr"}
          />
        </ContainedRow>
        <ContainedRow className={"jumboPrimary"} justify="space-around">
          <Col xs={{ span: 22 }} md={{ span: 20 }}>
            <h1 className={"title"}>
              Les compétences de la Haute Autorité de Santé au service des
              personnes
            </h1>
            <p>
              Autorité publique indépendante à caractère scientifique, la{" "}
              <a
                href={"https://has-sante.fr"}
                target="_blank"
                rel="noreferrer noopener"
              >
                Haute Autorité de santé
              </a>{" "}
              (HAS) travaille à garantir la qualité dans le champ sanitaire,
              social et médico-social, au bénéfice des personnes.
            </p>
            <p>
              Pour garantir la santé publique, l’institution s’assure de
              l’indépendance et de l’impartialité des experts avec lesquels elle
              collabore. L’analyse de leurs liens d’intérêts est donc
              primordiale.
            </p>
            <p className={"enAvant"}>
              Afin de faciliter cette analyse, la HAS a développé l’outil ADEX
              (Aide à la Décision pour l’EXpertise en santé) dans le cadre du
              programme{" "}
              <a
                href={"https://has-sante.fr"}
                target="_blank"
                rel="noreferrer noopener"
              >
                Entrepreneurs d’Intérêt Général
              </a>
              . Dans un souci à la fois d’efficience globale et de transparence,
              l’institution a choisi de mettre à disposition cet outil
              publiquement.
            </p>
            <Button ghost size={"large"} href="mailto:adex@has-sante.fr">
              Contactez-nous
            </Button>
          </Col>
        </ContainedRow>
        <AdexFooter />
      </Layout.Content>
    </Layout>
  );
};
