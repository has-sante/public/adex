import React from "react";
import { Route, Switch } from "react-router-dom";

import { ROUTES } from "Pages/routes";
import { MetaNoIndex } from "Components/MetaNoIndex";
import LandingPage from "./landing-page";
import FromDirectoryPage from "./fromDirectory-page";
import BrowseTSPage from "./browseTS-page";
import LinksOfInterestPage from "./linksOfInterest-page";
import FAQPage from "./faq-page";
import MentionsLegales from "./mentionsLegales-page";
import Changelog from "./changelog-page";
import { NoRouteMatch } from "Components/NoRouteMatch";

export interface IRoutesParams {
  identification_nationale_pp?: string;
  searchString?: string;
}

const TransparenceSantePage = () => {
  return (
    <Switch>
      <Route exact path={`/`}>
        <LandingPage />
      </Route>
      <Route path={ROUTES.changelog}>
        <Changelog />
      </Route>
      <Route path={ROUTES.questionsFrequentes}>
        <FAQPage />
      </Route>
      <Route path={ROUTES.mentionsLegales}>
        <MentionsLegales />
      </Route>
      <Route path={`${ROUTES.fromDirectory}/:identification_nationale_pp`}>
        <MetaNoIndex />
        <FromDirectoryPage />
      </Route>
      <Route path={`${ROUTES.browseTS}/:searchString`}>
        <MetaNoIndex />
        <BrowseTSPage />
      </Route>
      <Route
        path={[
          `${ROUTES.linksOfInterests}/`,
          `${ROUTES.linksOfInterests}/:identification_nationale_pp`,
        ]}
      >
        <MetaNoIndex />
        <LinksOfInterestPage />
      </Route>
      <Route>
        <NoRouteMatch />
      </Route>
    </Switch>
  );
};
export default TransparenceSantePage;
