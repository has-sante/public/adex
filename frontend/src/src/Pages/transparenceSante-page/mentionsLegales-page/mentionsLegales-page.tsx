import React, { useEffect } from "react";
import { Row, Col, Anchor } from "antd";

import { useAppSelector, useAppDispatch } from "Core/store/hooks";
import { getVersionThunk } from "Core/useCases/getVersion/thunks";

import { apiDateParser } from "Utils/dateUtils";
import { PageHeader } from "Components/PageHeader";
import { AdexFooter } from "Components/Footer";
import { ScrollToTopOnMount } from "Components/ScrollToTopOnMount";

import { ROUTES } from "Pages/routes";

// types
import type { RootState } from "Core/store/rootReducer";

const { Link } = Anchor;

const MentionsLegalesPage = () => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(getVersionThunk());
  });
  const versionning = useAppSelector((state: RootState) => {
    return state.version;
  });

  const parsedDate = apiDateParser(versionning.date_maj_donnees);
  return (
    <>
      <ScrollToTopOnMount />
      <PageHeader
        title={"Mentions légales"}
        illustration_url={"./images/Illustration-MentionsLegales.svg"}
      />
      <Row className={"content"}>
        <Col
          xs={{ offset: 1, span: 23 }}
          md={{ offset: 2, span: 22 }}
          lg={{ offset: 2, span: 6 }}
          xl={{ offset: 4, span: 4 }}
        >
          <Anchor>
            <Link
              href={`${ROUTES.mentionsLegales}#editeur`}
              title="Éditeur du site"
            />
            <Link
              href={`${ROUTES.mentionsLegales}#traitement`}
              title="Données personnelles"
            />
            <Link href={`${ROUTES.mentionsLegales}#cookies`} title="Cookies" />
            <Link
              href={`${ROUTES.mentionsLegales}#liens`}
              title="Liens hypertextes"
            />
            <Link
              href={`${ROUTES.mentionsLegales}#hebergement`}
              title="Hébergement"
            />
          </Anchor>
        </Col>
        <Col
          xs={{ offset: 1, span: 23 }}
          md={{ offset: 2, span: 22 }}
          lg={{ span: 16 }}
          xl={{ offset: 1, span: 12 }}
        >
          <h2 id={"editeur"}>Éditeur du site</h2>
          <p>
            Haute Autorité de santé, 5 avenue du Stade de France 93218
            Saint-Denis La Plaine Cedex Directeur de la publication&nbsp;:
            Pr.&nbsp; Lionel&nbsp;Collet
          </p>
          <h2 id={"traitement"}>
            Traitement des données personnelles / Loi informatique et libertés
          </h2>
          <p>
            Conformément à l’art.5 de l’arrêté relatif aux droits des personnes
            (&nbsp;
            <a
              target="_blank"
              rel="noreferrer noopener"
              href={
                "https://www.legifrance.gouv.fr/loda/id/JORFTEXT000028339198/2020-09-28/"
              }
            >
              https://www.legifrance.gouv.fr/loda/id/JORFTEXT000028339198/2020-09-28/
            </a>
            &nbsp; ), le droit d’opposition prévu à l’article 38 de la loi n°
            78-17 du 6 janvier 1978 susvisée par rapport aux données affichées
            sur le site ne s’applique pas directement auprès de la HAS mais
            directement auprès des fournisseurs de données. Les données
            présentées étant en OpenData et également présentées sur d’autres
            sites internet, cette mesure est jugée suffisante et proportionnée.
            Les données présentées sur ce site sont issues&nbsp;:
            <ul>
              <li>
                de la base de données Transparence Santé, dans le cadre de sa
                licence d’utilisation (
                <a
                  target="_blank"
                  rel="noreferrer noopener"
                  href={
                    "https://www.data.gouv.fr/fr/datasets/transparence-sante-1/"
                  }
                >
                  données et licence disponibles ici
                </a>
                ),
              </li>
              <li>
                de l’annuaire ANS, dans le cadre de sa licence d’utilisation (
                <a
                  target="_blank"
                  rel="noreferrer noopener"
                  href={
                    "https://annuaire.sante.fr/web/site-pro/extractions-publiques"
                  }
                >
                  données et licence disponibles ici
                </a>
                ).
              </li>
            </ul>
            Les données sont mises à jour tous les jours (date de la dernière
            mise à jour: {parsedDate && parsedDate.toLocaleString()}). Ainsi,
            toute donnée présente sur ce site n’est que le reflet de données
            présentes sur d’autres sites internet et jeux de données disponibles
            en OpenData. Ainsi, nous recommandons aux utilisateurs d’adresser
            leurs demandes de correction ou rectification directement auprès du
            producteur de données.
            <br />
            Conformément aux licences de réutilisation des données, tout
            référencement automatisé ou manuel des pages de ce site contenant
            des données personnelles est interdit.
            <br />
            Les informations recueillies à l’occasion notamment de la réception
            d’un courriel servent uniquement au traitement du support et ne sont
            jamais transmises à des tiers. La loi informatique et libertés du 6
            janvier 1978 vous donne un droit d’accès, d’opposition, de
            rectification et de suppression des données vous concernant. Vous
            pouvez exercer ce droit en vous adressant à l’un des webmasters
            (adex[at]has-sante.fr) ou par courrier à l’adresse suivante&nbsp;:
            Haute Autorité de santé, 5 avenue du Stade de France - 93218
            Saint-Denis La Plaine Cedex.
          </p>

          <h2 id={"cookies"}>Cookies</h2>
          <p>
            Afin de mieux vous servir et d’améliorer l’expérience utilisateur
            sur notre site, nous mesurons son audience grâce à une solution
            utilisant la technologie des cookies. Les données collectées
            permettent de fournir uniquement des données statistiques anonymes
            de fréquentation (le nombre de pages vues, le nombre de visites,
            leur fréquence de retour, etc.). Une fois que vous quittez le site
            internet de la HAS nous ne suivons pas votre navigation. Dans ce
            cadre, la HAS est exemptée du recueil du consentement, conformément
            aux recommandations de la CNIL&nbsp;:&nbsp;
            <a
              target="_blank"
              rel="noreferrer noopener"
              href={
                "https://www.cnil.fr/fr/cookies-solutions-pour-les-outils-de-mesure-daudience"
              }
            >
              https://www.cnil.fr/fr/cookies-solutions-pour-les-outils-de-mesure-daudience
            </a>
            . <br />
            <strong>
              Pour rappel, ce site n’accepte aucune forme de publicité, ni ne
              reçoit de fonds publicitaires.
            </strong>
          </p>
          <h2 id={"liens"}>Liens hypertextes</h2>
          <p>
            La création de liens hypertextes pointant vers le contenu du site
            has-sante.fr est autorisée sous réserve d’être accessible par
            l’ouverture d’une nouvelle fenêtre. En aucun cas, les pages du site
            has-sante.fr ne doivent être incluses à l’intérieur d’un autre site.
            La responsabilité de la HAS ne saurait être engagée pour les
            informations contenues sur les sites vers lesquels renvoie le site
            has-sante.fr.
          </p>
          <h2 id={"hebergement"}>Hébergement</h2>
          <p>
            L’application ADEX est hébergée par la Haute Autorité de Santé, 5
            avenue du Stade de France 93218 Saint-Denis La Plaine Cedex
          </p>
        </Col>
      </Row>
      <AdexFooter></AdexFooter>
    </>
  );
};

export default MentionsLegalesPage;
