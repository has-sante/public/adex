// Une route commence par un "/" et n'a pas de "/" final

export const ROUTES = {
  landingPage: "/",
  changelog: "/changelog",
  questionsFrequentes: "/questionsFrequentes",
  mentionsLegales: "/mentions-legales",
  fromDirectory: "/d",
  browseTS: "/b",
  linksOfInterests: "/l",
};
