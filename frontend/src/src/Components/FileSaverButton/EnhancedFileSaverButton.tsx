import React from "react";
import { Button } from "antd";

import { tag } from "Components/ATTracker/ATInternetService";
import { syntheseExportAPI } from "Adapters/primary/apis/syntheseExportApi";
import { useParsedQuery } from "Hooks/useQueryParamsInURL";

export const EnhancedFileSaverButton = () => {
  const searchQuery = useParsedQuery();
  const hashesStringInURL = searchQuery["hashes"];
  const identification_nationale_pp =
    searchQuery["identification_nationale_pp"];
  const exportApiURL = syntheseExportAPI({
    hashesStringInURL,
    identification_nationale_pp,
  });

  const trackClick = () => {
    tag.sendClick({
      type: "download",
      name: "download synthese",
    });
  };
  return (
    <Button
      size="large"
      ghost
      type="default"
      href={exportApiURL}
      target="_blank"
      disabled={!hashesStringInURL}
      onClick={trackClick}
    >
      Télécharger la synthèse
    </Button>
  );
};
