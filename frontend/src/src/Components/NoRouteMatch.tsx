import React from "react";
import { Link } from "react-router-dom";
import { ROUTES } from "Pages/routes";

export const NoRouteMatch = () => {
  return (
    <>
      <h1>Erreur, la page recherchée n&apos;existe pas. </h1>
      <Link to={ROUTES.landingPage}>Revenir à la page d&apos;accueil</Link>
    </>
  );
};
