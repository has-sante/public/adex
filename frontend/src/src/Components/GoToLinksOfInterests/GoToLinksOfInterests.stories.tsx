import React from "react";
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from "@storybook/react/types-6-0";

import StoryRouter from "storybook-react-router";

import "Styles/index.less";

import GoToLinksOfInterests from "./GoToLinksOfInterests";

export default {
  title: "GoToLinksOfInterests",
  component: GoToLinksOfInterests,
  parameters: { actions: { argTypesRegex: "^on.*" } },
} as Meta;

const Template: Story = (args) => <GoToLinksOfInterests {...args} />;

export const Default = Template.bind({});
Default.decorators = [StoryRouter()];
export const WithSelection = Template.bind({});
WithSelection.decorators = [
  StoryRouter(
    {},
    {
      initialEntries: [
        {
          pathname: "/d/s5nf83sBT756zupABI1S",
          search:
            "?identification_nationale_pp&hashes=a7f699464408%2C65d7a8bcfbf5&s=bruno",
        },
      ],
      initialIndex: 0,
    }
  ),
];
