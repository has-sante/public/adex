/* eslint-disable react/prop-types */
import React from "react";
import { Button, Row, Col } from "antd";
import { useHistory, useParams } from "react-router-dom";
import queryString from "query-string";

import { useParsedQuery } from "Hooks/useQueryParamsInURL";

import type { IRoutesParams } from "Pages/transparenceSante-page";
import { ROUTES } from "Pages/routes";

import styles from "./GoToLinksOfInterests.module.less";

const selectedBeneficiariesText = (selectedBeneficiaries: any) => {
  if (selectedBeneficiaries.length === 1) {
    return <h3>1 Bénéficiaire sélectionné</h3>;
  } else if (selectedBeneficiaries.length > 1) {
    return (
      <h3>{`${selectedBeneficiaries.length} Bénéficiaires sélectionnés`}</h3>
    );
  }
  return <h3>Sélectionnez au moins un Bénéficiaire</h3>;
};

const GoToLinksOfInterests = () => {
  const searchQuery = useParsedQuery();
  const hashesStringInURL = searchQuery["hashes"];
  const selectedHashesInURL =
    hashesStringInURL && !(hashesStringInURL === "")
      ? hashesStringInURL.split(",")
      : [];

  const { identification_nationale_pp } = useParams<IRoutesParams>();

  const history = useHistory();

  const handleOnClick = () => {
    if (identification_nationale_pp) {
      searchQuery["identification_nationale_pp"] = identification_nationale_pp;
    }
    history.push({
      pathname: `${ROUTES.linksOfInterests}/`,
      search: queryString.stringify(searchQuery),
    });
  };

  return (
    <Row className={styles.GoToLinksOfInterests} align="middle">
      <Col flex={"1 1 250px"}>
        {selectedBeneficiariesText(selectedHashesInURL)}
      </Col>
      <Col>
        <Button
          type="default"
          size={"large"}
          className={styles.button}
          disabled={selectedHashesInURL.length === 0}
          onClick={handleOnClick}
        >
          Voir les liens d’intérêts
        </Button>
      </Col>
    </Row>
  );
};

export default GoToLinksOfInterests;
