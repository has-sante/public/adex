import React from "react";

export const NbResultsHeader = ({ nbResults, searchString }) => {
  if (searchString) {
    return (
      <h1>
        {nbResults
          ? `${nbResults} résultat${
              nbResults === 1 ? "" : "s"
            } pour "${searchString}"`
          : `Aucun résultat pour "${searchString}"`}
      </h1>
    );
  }
  return (
    <h1>
      {nbResults
        ? `${nbResults} résultat${nbResults === 1 ? "" : "s"}`
        : `Aucun résultat`}
    </h1>
  );
};
