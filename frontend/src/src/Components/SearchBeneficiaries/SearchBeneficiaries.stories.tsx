import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import StoryRouter from "storybook-react-router";
import { Row, Col } from "antd";

import { withProvider } from "../../../.storybook/utils/redux/provider";
import { SearchBeneficiaries } from "./SearchBeneficiaries";

import "../../Styles/index.less";

export default {
  title: "SearchBeneficiaries/SearchBeneficiaries",
  component: SearchBeneficiaries,
  parameters: { actions: { argTypesRegex: "^on.*" } },
  decorators: [StoryRouter(), withProvider],
} as Meta;

const Template: Story = (args) => (
  <div
    style={{
      height: "100%",
      display: "flex",
      flexDirection: "column",
      backgroundColor: "#f2f2f2",
    }}
  >
    <div style={{ flex: "1", backgroundColor: "#f2f2f2" }}>
      <Row>
        <Col
          xs={{ span: 24 }}
          sm={{ span: 22, offset: 1 }}
          lg={{ span: 16, offset: 4 }}
        >
          <h1>Rechercher un bénéficiaire</h1>
          <SearchBeneficiaries {...args} />
        </Col>
      </Row>
    </div>
    <div style={{ flex: "2", backgroundColor: "#e2e2e2" }}>Adex</div>
  </div>
);

export const Default = Template.bind({});
