import React from "react";
import { Tabs } from "antd";
import useDimensions from "react-cool-dimensions";

import { AutoCompleteFooter } from "./AutoCompleteFooter";
import { AutoCompleteRow, entityDetails } from "./AutoCompleteRow";

import styles from "./SearchBeneficiaries.module.less";

export type AutoCompleteTabProps = {
  isOpen: Boolean;
  search: string;
  results: any[];
  highlightedIndex: number | null;
  getItemProps: Function;
  getToggleButtonProps: Function;
};
export const AutoCompleteTab = ({
  isOpen,
  search,
  results,
  highlightedIndex,
  getItemProps,
  getToggleButtonProps,
}: AutoCompleteTabProps) => {
  const { observe, width } = useDimensions({
    // The "currentBreakpoint" will be the object key based on the target's width
    // for instance, 0px - 319px (currentBreakpoint = XS), 320px - 479px (currentBreakpoint = SM) and so on
    breakpoints: { XS: 0, SM: 320, MD: 480, LG: 640 },
    // Will only update the state on breakpoint changed, default is false
    updateOnBreakpointChange: true,
    // onResize: ({ currentBreakpoint }) => {
    //   // Now the event callback will be triggered when breakpoint is changed
    //   // we can also access the "currentBreakpoint" here
    // },
  });
  return (
    <Tabs size={"small"}>
      <Tabs.TabPane tab={"Via l'annuaire RPPS"} key="1">
        <ul className={styles.DirectoryList} ref={observe}>
          {isOpen &&
            results.map((item, index) => (
              <li key={`${index}`} {...getItemProps({ item, index })}>
                <AutoCompleteRow
                  {...entityDetails(item)}
                  selected={highlightedIndex === index}
                  compact={width < 500}
                ></AutoCompleteRow>
              </li>
            ))}
        </ul>
      </Tabs.TabPane>
      <Tabs.TabPane tab={"Recherche directe"} key="2">
        <AutoCompleteFooter {...getToggleButtonProps()} placeholder={search} />
      </Tabs.TabPane>
    </Tabs>
  );
};
