import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import StoryRouter from "storybook-react-router";

import "../../Styles/index.less";
import { AutoCompleteTab, AutoCompleteTabProps } from "./AutoCompleteTab";
import { withProvider } from "../../../.storybook/utils/redux/provider";

import { AnnuaireAnsRecherche } from "../../Adapters/primary/mocks/Fixtures/annuaireAnsRechercheFixture";

export default {
  title: "SearchBeneficiaries/AutoCompleteTab",
  component: AutoCompleteTab,
  parameters: { actions: { argTypesRegex: "^on.*" } },
  decorators: [StoryRouter(), withProvider],
} as Meta;

const fixture = AnnuaireAnsRecherche.data;

const Template: Story<AutoCompleteTabProps> = (args) => (
  <div style={{ flex: "1" }}>
    <AutoCompleteTab results={fixture.slice(0, 5)} {...args} />
  </div>
);

export const Default = Template.bind({});
Default.args = {
  isOpen: true,
  search: "Bruno",
  highlightedIndex: 2,
  getItemProps: () => {
    console.log("getItemProps");
  },
  getToggleButtonProps: () => {
    console.log("getToggleButtonProps");
  },
};
