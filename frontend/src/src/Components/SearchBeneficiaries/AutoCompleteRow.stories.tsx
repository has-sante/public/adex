import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import StoryRouter from "storybook-react-router";

import "../../Styles/index.less";
import {
  AutoCompleteRow,
  AutoCompleteRowProps,
  entityDetails,
} from "./AutoCompleteRow";
import { withProvider } from "../../../.storybook/utils/redux/provider";

import { AnnuaireAnsRecherche } from "../../Adapters/primary/mocks/Fixtures/annuaireAnsRechercheFixture";

export default {
  title: "SearchBeneficiaries/AutoCompleteRow",
  component: AutoCompleteRow,
  parameters: { actions: { argTypesRegex: "^on.*" } },
  decorators: [StoryRouter(), withProvider],
} as Meta;

const fixture1 = entityDetails(AnnuaireAnsRecherche.data[0]);
const fixture2 = entityDetails(AnnuaireAnsRecherche.data[1]);

const Template: Story<AutoCompleteRowProps> = (args) => (
  <div style={{ flex: "1" }}>
    <AutoCompleteRow {...fixture1} {...args} selected={true} />
    <AutoCompleteRow {...fixture2} {...args} />
  </div>
);

export const AutoCompleteRowLarge = Template.bind({});
AutoCompleteRowLarge.args = { compact: false };
export const AutoCompleteRowCompact = Template.bind({});
AutoCompleteRowCompact.args = { compact: true };
