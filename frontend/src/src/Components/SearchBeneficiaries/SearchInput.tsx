import React from "react";
import { Input } from "antd";
import { SearchOutlined } from "@ant-design/icons";

const PLACEHOLDER =
  "Saisir un prénom et un nom, une profession, un établissement, etc.";

export const SearchInput = (inputProps: any) => {
  return (
    <Input
      size={"large"}
      placeholder={PLACEHOLDER}
      prefix={<SearchOutlined />}
      {...inputProps}
    />
  );
};
