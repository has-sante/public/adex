import React, { useState, useEffect, useCallback } from "react";
import ReactDOM from "react-dom";

import { useHistory } from "react-router-dom";
import _ from "lodash";
import { usePopper } from "react-popper";
import Downshift from "downshift";
import queryString from "query-string";
import { Input } from "antd";
import { SearchOutlined } from "@ant-design/icons";

import type { RootState } from "Core/store/rootReducer";
import { useAppDispatch, useAppSelector } from "Core/store/hooks";
import { searchDirectoryThunk } from "Core/useCases/searchInDirectory/thunks";
import { useParsedQuery } from "Hooks/useQueryParamsInURL";
import { ROUTES } from "Pages/routes";

import { sameWidth } from "Utils/popperModifier";

import { AutoCompleteTab } from "./AutoCompleteTab";

import stylesModule from "./SearchBeneficiaries.module.less";

const PLACEHOLDER =
  "Saisir un prénom et un nom, une profession, un établissement, etc.";
const ROW_HEIGHT = 56;
const NUM_DISPLAYED_ROWS = 8;

export const SearchBeneficiaries = () => {
  const inputRef = React.useRef<any>(null);
  // positionning dropdown
  const [referenceElement, setReferenceElement] =
    useState<HTMLDivElement | null>(null);
  const [popperElement, setPopperElement] = useState<HTMLDivElement | null>(
    null
  );
  const { styles, attributes } = usePopper<"sameWidth">(
    referenceElement,
    popperElement,
    {
      modifiers: [sameWidth],
    }
  );
  // end positionning dropdown

  // handling loading results
  const queryParams = useParsedQuery();
  const search = queryParams["s"] || "";
  const dispatch = useAppDispatch();
  const debouncedSearchDispatch = useCallback(
    _.debounce((q) => dispatch(searchDirectoryThunk(q)), 400, {
      leading: true,
      maxWait: 1000,
    }),
    []
  );

  useEffect(() => {
    debouncedSearchDispatch({ query: search });
  }, [search]);

  const { directoryResults, requestState } = useAppSelector(
    (state: RootState) => state.searchInDirectory
  );

  const results = directoryResults || [];

  const History = useHistory();

  // end handling loading results

  // handling Navigation / routing / urls

  const handleSearch = (e: string | undefined) => {
    const newQueryParams = { ...queryParams, s: e };
    const searchParams = queryString.stringify(newQueryParams);
    History.replace({ search: searchParams });
  };

  const handleSelectBeneficiary = (id: string) => {
    if (requestState !== "loading") {
      const searchParams = queryString.stringify({
        ...queryParams,
        hashes: null,
      });
      History.push({
        pathname: `${ROUTES.fromDirectory}/${id}`,
        search: searchParams,
      });
      if (inputRef.current) {
        inputRef.current.blur();
      }
    } else {
      console.log("isloading");
    }
  };

  const handleBrowseTS = () => {
    const { identification_nationale_pp, ...params } = queryParams;
    const searchParams = queryString.stringify({
      ...params,
      hashes: null,
    });
    History.push({
      pathname: `${ROUTES.browseTS}/${search}`,
      search: searchParams,
    });
  };
  // end handling Navigation

  const numLinesToDisplay = Math.min(results?.length + 1, NUM_DISPLAYED_ROWS);
  const dropdownMaxHeight = (numLinesToDisplay + 0.5) * ROW_HEIGHT;
  return (
    <div style={{ width: "100%" }}>
      <Downshift
        inputValue={search}
        itemToString={(item) => {
          return item?.identification_nationale_pp;
        }}
        defaultHighlightedIndex={0}
        onInputValueChange={(changes, st: any) => {
          if (st.type === Downshift.stateChangeTypes.changeInput) {
            handleSearch(st.inputValue);
          }
        }}
        stateReducer={(st, changes) => {
          switch (changes.type) {
            case Downshift.stateChangeTypes.keyDownEnter:
            case Downshift.stateChangeTypes.clickItem:
              handleSelectBeneficiary(
                changes.selectedItem?.identification_nationale_pp
              );
              return {};
            default:
              return changes;
          }
        }}
      >
        {({
          isOpen,
          getMenuProps,
          getInputProps,
          highlightedIndex,
          getItemProps,
          getToggleButtonProps,
          openMenu,
        }) => {
          const enhancedGetToggleButtonPropsBrowseTS = () => {
            return getToggleButtonProps({
              onClick: () => {
                handleBrowseTS();
              },
            });
          };
          return (
            <div>
              <div ref={setReferenceElement}>
                <Input
                  className={stylesModule.searchInput}
                  placeholder={PLACEHOLDER}
                  prefix={<SearchOutlined />}
                  {...getInputProps({
                    size: "large",
                    ref: inputRef,
                    onFocus: () => {
                      if (!isOpen && search.length > 0) {
                        openMenu();
                      }
                    },
                  })}
                />
              </div>
              {ReactDOM.createPortal(
                <div
                  ref={setPopperElement}
                  style={styles.popper}
                  {...attributes.popper}
                >
                  <div
                    {...getMenuProps()}
                    className={`${stylesModule.AutoCompleteDropdown} ${
                      isOpen ? "" : stylesModule.AutoCompleteDropdownHidden
                    }`}
                    style={{
                      height: dropdownMaxHeight,
                      maxHeight: "50vh",
                    }}
                  >
                    <AutoCompleteTab
                      isOpen={isOpen}
                      search={search}
                      results={results}
                      highlightedIndex={highlightedIndex}
                      getItemProps={getItemProps}
                      getToggleButtonProps={
                        enhancedGetToggleButtonPropsBrowseTS
                      }
                    />
                  </div>
                </div>,
                document.body
              )}
            </div>
          );
        }}
      </Downshift>
    </div>
  );
};
