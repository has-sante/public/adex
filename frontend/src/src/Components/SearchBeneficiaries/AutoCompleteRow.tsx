import React from "react";
import { Row, Col } from "antd";
import { EnterOutlined } from "@ant-design/icons";

import styles from "./SearchBeneficiaries.module.less";

import type { DirectoryEntityGroupType } from "Core/ports/secondary/entities/directoryEntity";

export type AutoCompleteRowProps = {
  name: string;
  professions: string;
  specialites: string;
  identifiant: string;
  first_address: string;
  selected?: boolean;
  rowHeight?: number;
  compact?: boolean;
};

export const AutoCompleteRow = ({
  name,
  professions,
  specialites,
  identifiant,
  first_address,
  selected,
  compact,
}: AutoCompleteRowProps) => {
  const professionSpecialite = `${professions}&nbsp;${
    specialites && `-&nbsp;${specialites} `
  }-&nbsp;${identifiant}`;
  return (
    <Row
      align={"middle"}
      className={`${styles.AutoCompleteRowOption} ${
        selected ? styles.AutoCompleteRowOptionSelected : ""
      }`}
    >
      <Col xs={{ span: 24 }} sm={{ span: compact ? 24 : 12 }}>
        <h1 dangerouslySetInnerHTML={{ __html: name }}></h1>
        <p
          className={styles.profession}
          dangerouslySetInnerHTML={{
            __html: professionSpecialite,
          }}
        ></p>
      </Col>

      <Col xs={{ span: 24 }} sm={{ span: compact ? 24 : 8 }}>
        <p
          className={styles.adresse}
          dangerouslySetInnerHTML={{ __html: first_address }}
        ></p>
      </Col>

      <Col
        xs={{ span: 0 }}
        sm={{ span: compact ? 0 : 4 }}
        className={styles.AutoCompleteRowSelectButton}
      >
        {selected && (
          <>
            <EnterOutlined />
            <div>Taper sur ENTR&Eacute;E</div>
          </>
        )}
      </Col>
    </Row>
  );
};

export const entityDetails = (entity: DirectoryEntityGroupType) => {
  const name = `${entity.noms_complets_exercice[0].nom_exercice}
  ${entity.noms_complets_exercice[0].prenom_exercice}`;
  const professions = entity.professions.join(" / ");
  const specialites = entity.specialites.join(" / ");
  const identifiant = `${entity.identification.identifiant_pp} ${
    entity.identification.libelle_type_identifiant_pp
      ? `(${entity.identification.libelle_type_identifiant_pp})`
      : ""
  }`;
  const first_address = `${entity.lieux_activite[0].adresse_complete}`;
  return { name, professions, specialites, identifiant, first_address };
};
