import React, { MouseEventHandler } from "react";
import { Row } from "antd";
import { SearchOutlined } from "@ant-design/icons";

import styles from "./SearchBeneficiaries.module.less";

type AutoCompleteFooterProps = {
  onClick: MouseEventHandler;
  placeholder?: string;
};

export const AutoCompleteFooter: React.FC<AutoCompleteFooterProps> = ({
  onClick,
  placeholder,
}) => {
  return (
    <Row>
      <div onClick={onClick} className={styles.AutoCompleteFooter}>
        <span className={styles.icon}>
          <SearchOutlined />
        </span>
        {placeholder ? (
          <>
            Rechercher des bénéficiaires pour <i>&quot;{placeholder}&quot;</i>
          </>
        ) : (
          "Rechercher une personne qui n’est pas dans la liste"
        )}
      </div>
    </Row>
  );
};
