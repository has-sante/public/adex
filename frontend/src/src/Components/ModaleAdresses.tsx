import React, { useState } from "react";
import { Modal } from "antd";

interface ModaleAdressesProps {
  adresses: Array<React.ReactNode>;
  children: React.ReactNode;
}

export const ModaleAdresses = ({ adresses, children }: ModaleAdressesProps) => {
  const [visible, toggleModal] = useState(false);
  const handleToggleModal = () => {
    return toggleModal(!visible);
  };
  return (
    <>
      <div onClick={handleToggleModal}>{children}</div>
      <Modal
        title="Lieu(x) d'exercice du bénéficiaire"
        visible={visible}
        footer={null}
        destroyOnClose
        onCancel={handleToggleModal}
      >
        <ul>{adresses}</ul>
      </Modal>
    </>
  );
};
