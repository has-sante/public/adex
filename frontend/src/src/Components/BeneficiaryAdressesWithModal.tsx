import React from "react";
import _ from "lodash";

// types
import type {
  BeneficiaryType,
  BeneficiaryGroupType,
} from "Core/ports/secondary/entities/beneficiary";

// utils
import { capitalize } from "Utils/dataFormatter";
// components
import { ModaleAdresses } from "./ModaleAdresses";

export const BeneficiaryAdressToString = (a: BeneficiaryType): string => {
  if (typeof a === "undefined") return "Erreur pas d'adresse";
  return capitalize(`${a.structure_exercice ?? ""} ${a.adresse}`);
};
export const BeneficiaryPostalCodeCityToString = (
  a: BeneficiaryType
): string => {
  if (typeof a === "undefined") return "Erreur pas d'adresse";
  return capitalize(`${a.ville} ${a.code_postal ? ` (${a.code_postal})` : ""}`);
};

export const NumberOfDistinctAdresses = (
  BeneficiaryFromTS: BeneficiaryGroupType
) => {
  return _.uniqWith(BeneficiaryFromTS.children, (b1, b2) => {
    return (
      b1.structure_exercice === b2.structure_exercice &&
      b1.adresse === b2.adresse &&
      b1.code_postal === b2.code_postal &&
      b1.ville === b2.ville
    );
  }).length;
};

export const buildAdressFromBeneficiaryFromTS = (
  c: BeneficiaryType,
  i: number
) => {
  const address = BeneficiaryAdressToString(c);

  return React.cloneElement(<li>{address}</li>, {
    key: `${(c && c.benef_hash) || ""}-${i}`,
  });
};

export const BeneficiaryAdresses = (
  BeneficiaryFromTS: BeneficiaryGroupType,
  nbAdresses = 2
) => {
  const adresses =
    (BeneficiaryFromTS &&
      BeneficiaryFromTS.children &&
      BeneficiaryFromTS.children.slice(0, nbAdresses).map((c, i) => {
        return buildAdressFromBeneficiaryFromTS(c, i);
      })) ||
    buildAdressFromBeneficiaryFromTS(BeneficiaryFromTS, 0);
  const nbAdressesSup = Math.max(
    ((BeneficiaryFromTS &&
      BeneficiaryFromTS.children &&
      BeneficiaryFromTS.children.length) ||
      0) - nbAdresses,
    0
  );

  const allAddresses =
    BeneficiaryFromTS?.children?.map((c, i) => {
      return buildAdressFromBeneficiaryFromTS(c, i);
    }) || [];

  return [adresses, nbAdressesSup, allAddresses] as const;
};

export const BeneficiaryAdressesWithModal = (
  BeneficiaryFromTS: BeneficiaryGroupType,
  nbAdresses = 2
) => {
  const [adresses, nbAdressesSup, allAddresses] = BeneficiaryAdresses(
    BeneficiaryFromTS,
    nbAdresses
  );

  if (nbAdressesSup === 0) {
    return (
      <ul style={{ listStyle: "None", padding: 0, margin: 0 }}>{adresses}</ul>
    );
  }

  return (
    <>
      <ul style={{ listStyle: "None", padding: 0, margin: 0 }}>{adresses}</ul>
      <ModaleAdresses adresses={allAddresses}>
        <h4>{`${nbAdressesSup} adresse${
          nbAdressesSup === 1 ? "" : "s"
        } supplémentaire${nbAdressesSup === 1 ? "" : "s"}`}</h4>
      </ModaleAdresses>
    </>
  );
};
