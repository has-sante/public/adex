import React from "react";
import { Row, Col } from "antd";
import { EnvironmentOutlined } from "@ant-design/icons";

import { features } from "appconfig";
import { capitalize, capitalizeWithSuffix } from "Utils/dataFormatter";
import { DisplayKVToNotification } from "./../DisplayKVToNotification";
import { ModaleAdresses } from "./../ModaleAdresses";

import styles from "./BeneficiaryCard.module.less";

import type {
  DirectoryEntityGroupType,
  ProfessionalActivityPlace,
} from "Core/ports/secondary/entities/directoryEntity";
import type { BeneficiaryFromDirectoryStates } from "Core/useCases/getBeneficiaryDetailsFromDirectory";

const buildAdressFromDirectoryEntity = (
  identification: string,
  activity_place: ProfessionalActivityPlace,
  i: number
) => {
  return React.cloneElement(
    <li>
      {capitalizeWithSuffix(activity_place.raison_sociale_site)}
      {capitalizeWithSuffix(
        activity_place.indice_repetition_voie_coord_structure
      )}
      <br />
      {capitalizeWithSuffix(activity_place.numero_voie_coord_structure)}
      {capitalizeWithSuffix(activity_place.libelle_type_voie_coord_structure)}
      {capitalizeWithSuffix(activity_place.libelle_voie_coord_structure)}
      <br />
      {capitalizeWithSuffix(
        `${activity_place.libelle_commune_coord_structure ?? ""}`
      )}
      {` ${
        activity_place.code_postal_coord_structure
          ? `(${capitalize(activity_place.code_postal_coord_structure)})`
          : ""
      }`}
    </li>,
    { key: `${identification}-${i}` }
  );
};

const getAdresses = (
  directoryEntity: DirectoryEntityGroupType,
  nbAdresses = 2
) => {
  const adresses = directoryEntity.lieux_activite
    .slice(0, nbAdresses)
    .map((c, i) => {
      return buildAdressFromDirectoryEntity(
        directoryEntity.identification.identification_nationale_pp,
        c,
        i
      );
    });
  const nbAdressesSup = Math.max(
    directoryEntity.children.length - nbAdresses,
    0
  );
  if (nbAdressesSup === 0) {
    return <ul>{adresses}</ul>;
  }

  const allAddresses = directoryEntity.lieux_activite.map((c, i) => {
    return buildAdressFromDirectoryEntity(
      directoryEntity.identification.identification_nationale_pp,
      c,
      i
    );
  });
  return (
    <>
      <ul>{adresses}</ul>
      <ModaleAdresses adresses={allAddresses}>
        <h4>{`${nbAdressesSup} adresse${
          nbAdressesSup === 1 ? "" : "s"
        } supplémentaire${nbAdressesSup === 1 ? "" : "s"}`}</h4>
      </ModaleAdresses>
    </>
  );
};

export type BeneficiaryCardPropsType = {
  beneficiaryDetailsState: BeneficiaryFromDirectoryStates;
};

export const BeneficiaryCard: React.FC<BeneficiaryCardPropsType> = ({
  beneficiaryDetailsState,
}) => {
  switch (beneficiaryDetailsState.requestState) {
    case "idle":
    case "loading":
      return <div>Chargement</div>;
    case "error":
      return <div>Erreur au chargement</div>;
    case "loaded": {
      const { beneficiaryDetails } = beneficiaryDetailsState;
      const identification =
        `${beneficiaryDetails.identification.libelle_type_identifiant_pp} : ` +
        ` ${beneficiaryDetails.identification.identifiant_pp}`;

      const name =
        `${
          beneficiaryDetails?.noms_complets_exercice[0]
            .libelle_civilite_exercice ||
          beneficiaryDetails?.noms_complets_exercice[0].libelle_civilite
        } ` +
        `${beneficiaryDetails?.noms_complets_exercice[0].nom_exercice} ` +
        `${beneficiaryDetails?.noms_complets_exercice[0].prenom_exercice} `;

      const libelle_profession = `${beneficiaryDetails.professions.join(
        " / "
      )}`;
      const libelle_specialite = `${beneficiaryDetails.specialites.join(
        " / "
      )}`;

      const handleDoubleClick = () => {
        if (features.FEATURE_DISPLAY_KV) {
          DisplayKVToNotification(beneficiaryDetails)();
        }
      };

      const renderedAdresses = getAdresses(beneficiaryDetails);

      return (
        <Row className={styles.directoryCard} onDoubleClick={handleDoubleClick}>
          <Col xs={{ span: 22, offset: 1 }}>
            <div className={styles.vousavezchoisi}>Professionnel recherché</div>
          </Col>
          <Col xs={{ span: 22, offset: 1 }}>
            <div className={styles.directoryDetails}>
              Informations issues de l&apos;annuaire RPPS
            </div>
          </Col>

          <Col className={styles.directoryDetails} xs={{ span: 22, offset: 1 }}>
            <Row>
              <Col>
                <h2>{name}</h2>
              </Col>
            </Row>
            <Row>
              <Col>
                <span>{libelle_profession}</span>
                {libelle_specialite && (
                  <>
                    <br></br>
                    <span>{libelle_specialite}</span>
                  </>
                )}
                <br></br>
                <span>{identification}</span>
              </Col>
            </Row>
          </Col>
          <Col className={styles.directoryDetails} xs={{ span: 22, offset: 1 }}>
            <div className={styles.localisationIcon}>
              <EnvironmentOutlined />
            </div>
            <div>
              <h3>Lieu(x) d’exercice</h3>
              {renderedAdresses}
            </div>
          </Col>
        </Row>
      );
    }
    default:
      const _exhaustivityCheck: never = beneficiaryDetailsState;
      return _exhaustivityCheck;
  }
};
