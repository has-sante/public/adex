import React from "react";
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from "@storybook/react/types-6-0";

import "Styles/index.less";

import { BeneficiaryCard, BeneficiaryCardPropsType } from "./BeneficiaryCard";

import {
  AnnuaireAnsElasticId,
  AnnuaireAnsElasticIdMultipleAdresses,
} from "Adapters/primary/mocks/Fixtures/annuaireAnsElasticIdFixture";

export default {
  title: "BeneficiaryCard",
  component: BeneficiaryCard,
  parameters: { actions: { argTypesRegex: "^on.*" } },
} as Meta;

const Template: Story<BeneficiaryCardPropsType> = (args) => (
  <BeneficiaryCard {...args} />
);

export const Default = Template.bind({});
Default.args = {
  beneficiaryDetailsState: {
    requestState: "loaded",
    beneficiaryDetails: AnnuaireAnsElasticId,
  },
};
export const MultipleAdresses = Template.bind({});
MultipleAdresses.args = {
  beneficiaryDetailsState: {
    requestState: "loaded",
    beneficiaryDetails: AnnuaireAnsElasticIdMultipleAdresses,
  },
};

export const LoadingBeneficiary = Template.bind({});
LoadingBeneficiary.args = {
  beneficiaryDetailsState: {
    requestState: "idle",
    beneficiaryDetails: AnnuaireAnsElasticId,
  },
};
