import React from "react";
import { useHistory } from "react-router-dom";
import { Button } from "antd";
import { ArrowLeftOutlined } from "@ant-design/icons";

const BackToPreviousPage = ({ children }: { children: React.ReactNode }) => {
  const history = useHistory();
  const handleOnClick = () => {
    history.goBack();
  };
  return (
    <div className={"backToPreviousPage"}>
      <Button icon={<ArrowLeftOutlined />} onClick={handleOnClick}>
        {children}
      </Button>
    </div>
  );
};

export default BackToPreviousPage;
