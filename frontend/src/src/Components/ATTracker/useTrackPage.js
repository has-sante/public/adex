import { useEffect } from "react";
import { tag } from "./ATInternetService";

export const useTrackPage = (info) => {
  return useEffect(() => {
    tag.sendPage(info);
  }, []);
};
