import React from "react";

import { tag } from "./ATInternetService";

export const TrackOnClick = ({ info, children }) => {
  handleTrackOnClick = () => {
    tag.sendClick(info);
  };

  return <div onClick={handleTrackOnClick}>{children}</div>;
};
