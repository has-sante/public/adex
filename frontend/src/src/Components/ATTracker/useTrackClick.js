import { useEffect } from "react";
import { tag } from "./ATInternetService";

export const useTrackClick = (info) => {
  return useEffect(() => {
    tag.sendClick(info);
  }, []);
};
