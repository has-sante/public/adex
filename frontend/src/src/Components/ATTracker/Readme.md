Configuration et Mise en place d'un tracker AT Internet

# Création d'un nouveau tag
Aller sur Tag Composer : https://collection.atinternet-solutions.com/

Plugins :
- clics
- identifiat visiteur sur domaine
- informations contextuelles
- sales insights
- evenements
- moteur de recherche interne
- pages
- privacy


Paramètres:
[x] Sécuriser les cookies // Adex est en HTTPS
[x] ajout automatique d'une propriété contenant l'URL de la page // Permet de tracker les recherches et les hash d'ADEX
[ ] Lier les données de redirection au domaine du site // Pour être sûr de gérer correctement les données du point de vue RGPD (pas de consentement demandé)

Créer le tag

# Déployer le tag
## Sites
Sélection du site à déployer : ADEX
## Paramètres
pixel.path : /hit.xiti // inchangé
Domaine des cookies: .has-sante.fr
Mode de récupération: téléchargement
