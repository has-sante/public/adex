import { features } from "appconfig";
import { mockATInternet } from "Adapters/primary/mocks/mockATInternet";

class ATInternetService {
  constructor() {
    this.tag = this.newTag({ secure: true });
  }

  newTag(options) {
    if (features.DISABLE_TRACKING) {
      console.log("tracking is disabled");
      return mockATInternet;
    }
    try {
      //tag = new window.ATInternet.Tracker.Tag(options);
      // tag.privacy.setVisitorMode("cnil", "exempt");
      return new window.ATInternet.Tracker.Tag(options);
    } catch (ex) {
      console.log("error loading AT Internet Tracker", ex);
      return mockATInternet;
    }
  }

  //@param info: {name: string, level2?: string, chapter1?: string, chapter2?: string, chapter3?: string, customObject?: any}
  sendPage(info) {
    this.tag.page.set(info);
    this.tag.dispatch();
  }
  //@param info: {elem: any, name: string, level2?: string, chapter1?: string, chapter2?: string, chapter3?: string, type: string, customObject?: any}
  sendClick(info) {
    this.tag.click.send(info);
  }
}

export let tag = new ATInternetService();
