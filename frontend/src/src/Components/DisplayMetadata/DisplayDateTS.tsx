import React, { useEffect } from "react";

import { useAppSelector, useAppDispatch } from "Core/store/hooks";
import { getVersionThunk } from "Core/useCases/getVersion/thunks";

import { apiDateParser } from "Utils/dateUtils";

// types
import type { RootState } from "Core/store/rootReducer";

export const DisplayDateTS = () => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(getVersionThunk());
  });
  const versionning = useAppSelector((state: RootState) => {
    return state.version;
  });

  const parsedDate = apiDateParser(versionning.date_maj_donnees);

  return <>{parsedDate && parsedDate.toLocaleString()}</>;
};
