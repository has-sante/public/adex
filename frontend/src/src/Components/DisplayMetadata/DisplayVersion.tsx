import React from "react";
import {
  adexFrontendVersion,
  ciCommitShortSha,
} from "Adapters/primary/apis/config";

export const DisplayVersion = () => {
  return (
    <div>
      Version {adexFrontendVersion}
      {ciCommitShortSha}
    </div>
  );
};
