import React from "react";

import { useAppSelector } from "Core/store/hooks";
import { BeneficiaryCard } from "./BeneficiaryCard/BeneficiaryCard";

import type { RootState } from "Core/store/rootReducer";

export const EnhancedBeneficiaryCard = () => {
  const beneficiaryDetailsState = useAppSelector((state: RootState) => {
    return state.getBeneficiaryDetailsFromDirectory;
  });

  return (
    <BeneficiaryCard
      beneficiaryDetailsState={beneficiaryDetailsState}
    ></BeneficiaryCard>
  );
};
