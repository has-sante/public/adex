import React from "react";
import { Grid, Row, Col } from "antd";

type PageHeaderProps = {
  title: string;
  illustration_url: string;
};

export const PageHeader = ({ title, illustration_url }: PageHeaderProps) => {
  const screens = Grid.useBreakpoint();
  return (
    <Row className={"pageHeader"}>
      <Col
        xs={{ span: 24 }}
        md={{ offset: 2, span: 20 }}
        xl={{ span: 16, offset: 4 }}
      >
        <h1>{title}</h1>
        {!screens.xs && <img src={illustration_url} />}
      </Col>
    </Row>
  );
};
