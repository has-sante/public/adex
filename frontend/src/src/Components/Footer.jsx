import React from "react";
import { Link } from "react-router-dom";
import { Layout, Row, Col } from "antd";

import { DisplayVersion } from "Components/DisplayMetadata/DisplayVersion";
import { DisplayDateTS } from "Components/DisplayMetadata/DisplayDateTS";

import { ROUTES } from "Pages/routes";
import { ContainedRow } from "./ContainedRow/ContainedRow";

const { Footer } = Layout;
4 + 1 + 4 + 1 + 4 + 1 + 4 + 1 + 4;
export const AdexFooter = () => {
  return (
    <Footer className="footer">
      <ContainedRow justify={"space-around"}>
        <Col xs={{ offset: 1, span: 23 }} md={{ offset: 0, span: 24 }}>
          <h1 className="brand">ADEX</h1>
        </Col>
      </ContainedRow>
      <ContainedRow justify={"space-around"}>
        <Col xs={{ offset: 1, span: 23 }} md={{ offset: 0, span: 9 }}>
          <p>
            Un projet porté par la Haute Autorité de Santé et le programme
            Entrepreneurs d’Intérêt Général
          </p>
          <Row justify={"space-around"}>
            <Col>
              <a
                target="_blank"
                rel="noreferrer noopener"
                href={"https://has-sante.fr"}
              >
                <img className={"logo"} src={"./images/Logo - HAS.svg"}></img>
              </a>
            </Col>
            <Col>
              <a
                target="_blank"
                rel="noreferrer noopener"
                href={
                  "https://entrepreneur-interet-general.etalab.gouv.fr/defis/2020/adex.html"
                }
              >
                <img className={"logo"} src={"./images/Logo - EIG.png"}></img>
              </a>
            </Col>
          </Row>
        </Col>
        <Col xs={{ offset: 1, span: 23 }} md={{ offset: 1, span: 4 }}>
          <ul>
            <li>
              <a
                target="_blank"
                rel="noreferrer noopener"
                href="./assets/demo_adex_juin_2021.mp4"
              >
                Regarder la démo
              </a>
            </li>
            <li>
              <Link to={ROUTES.questionsFrequentes}>Questions fréquentes</Link>
            </li>
          </ul>
        </Col>
        <Col xs={{ offset: 1, span: 23 }} md={{ offset: 1, span: 4 }}>
          <ul>
            <li>
              <a
                target="_blank"
                rel="noreferrer noopener"
                href="mailto:adex@has-sante.fr"
              >
                Nous contacter
              </a>
            </li>
            <li>
              <Link to={ROUTES.mentionsLegales}>Mentions légales</Link>
            </li>
          </ul>
        </Col>
        <Col xs={{ offset: 1, span: 23 }} md={{ offset: 1, span: 4 }}>
          <ul>
            <li>
              <a
                target="_blank"
                rel="noreferrer noopener"
                href="https://www.transparence.sante.gouv.fr/"
              >
                Transparence Santé
              </a>
            </li>

            <li>
              <a
                href="https://gitlab.has-sante.fr/has-sante/public/adex/"
                target="_blank"
                rel="noreferrer noopener"
              >
                Code source
              </a>
            </li>
            <li>
              <a href={`/docs`} target="_blank" rel="noreferrer noopener">
                Documentation des api ADEX
              </a>
            </li>
            <li>
              <DisplayVersion />
              <Link to={ROUTES.changelog}>(Voir le Changelog)</Link>
              <br />
              Mise à jour des données: <DisplayDateTS />
            </li>
          </ul>
        </Col>
      </ContainedRow>
    </Footer>
  );
};
