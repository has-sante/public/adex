import React from "react";
import { Col, Row } from "antd";
import { CheckOutlined } from "@ant-design/icons";

export const HeaderResultsTS = () => {
  return (
    <Row>
      <Col>
        <h2 className={"headerResultsTS"}>
          <CheckOutlined className={"headerResultsTSIconGreen"} /> Sélectionner
          tous les résultats issus de Transparence-Santé correspondant au
          professionnel recherché
        </h2>
        <p className={"headerResultsTSSubtext"}>
          Il est possible qu&apos;aucune déclaration dans Transparence Santé ne
          corresponde à cette recherche
        </p>
      </Col>
    </Row>
  );
};
