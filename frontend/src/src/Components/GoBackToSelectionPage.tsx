import React from "react";
import { useHistory } from "react-router-dom";
import queryString from "query-string";
import { Button } from "antd";
import { ArrowLeftOutlined } from "@ant-design/icons";

import { useParsedQuery } from "Hooks/useQueryParamsInURL";
import { ROUTES } from "Pages/routes";

export const GoBackToSelectionPage = () => {
  const queryParams = useParsedQuery();
  const { identification_nationale_pp, s } = queryParams;
  const history = useHistory();
  const searchParams = queryString.stringify({
    ...queryParams,
  });
  const route = identification_nationale_pp
    ? `${ROUTES.fromDirectory}/${identification_nationale_pp}`
    : `${ROUTES.browseTS}/${s}`;
  const handleOnClick = () => {
    history.push({
      pathname: route,
      search: searchParams,
    });
  };
  return (
    <div className={"backToPreviousPage"}>
      <Button icon={<ArrowLeftOutlined />} onClick={handleOnClick}>
        Modifier la liste des bénéficiaires
      </Button>
    </div>
  );
};
