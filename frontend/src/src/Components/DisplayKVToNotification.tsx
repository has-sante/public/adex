import React from "react";
import { notification } from "antd";

const mapObjToString = (o: object) => {
  if (o) {
    const kv = Object.entries(o);
    const content = [];
    for (let index = 0; index < kv.length; index++) {
      if (typeof kv[index][1] === "object" && !(kv[index][1] === null)) {
        content.push(
          <div>
            {kv[index][0]}
            &gt;
            {mapObjToString(kv[index][1])}
          </div>
        );
      } else {
        content.push(
          <div>
            {`${kv[index][0]}:`}
            <b>{`${kv[index][1]}`}</b>
          </div>
        );
      }
    }
    return (
      <div
        style={{ overflowY: "scroll", maxHeight: "80vh", paddingLeft: "8px" }}
        // dangerouslySetInnerHTML={{ __html: content.join("<br />") }}
      >
        {content}
      </div>
    );
  }
  return "";
};

export const DisplayKVToNotification = (r: object) => {
  return () => {
    notification.open({
      duration: 10,
      message: "Contenu",
      description: mapObjToString(r),
      placement: "topLeft",
      style: {
        width: 600,
      },
    });
  };
};
