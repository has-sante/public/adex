import React from "react";
import { Row, Col } from "antd";

import { useAppSelector } from "Core/store/hooks";

import { EnhancedFileSaverButton } from "Components/FileSaverButton/EnhancedFileSaverButton";
import { DisplayDateTS } from "Components/DisplayMetadata/DisplayDateTS";
import { ROUTES } from "Pages/routes";
// types
import type { RootState } from "Core/store/rootReducer";
import { SyntheseListType } from "Core/ports/secondary/entities/synthese";

import styles from "./SyntheseFooter.module.less";

const SumsOfInterests = (data: SyntheseListType | undefined) => {
  if (!data) {
    return ["-", "-"];
  }
  const sumsArray = data.reduce(
    (sums, declaration) => {
      const montant_max = declaration.montants.max_montant || 0;

      const montant_avant_remu =
        (declaration.montants.montants_avantages || 0) +
        (declaration.montants.montants_remunerations || 0);
      return [sums[0] + montant_avant_remu, sums[1] + montant_max];
    },
    [0, 0]
  );

  return sumsArray;
};
export const SyntheseFooter = () => {
  const syntheseState = useAppSelector((state: RootState) => {
    return state.synthese;
  });

  const [min, max] = SumsOfInterests(syntheseState?.synthese);
  return (
    <Row className={styles.SyntheseFooter}>
      <Col flex={"auto"}>
        <div className={styles.Synthese}>
          Entre <span className={styles.Montant}>{min} €</span>et{" "}
          <span className={styles.Montant}>{max} €</span> de liens d’intérêts
          ont été identifiés.
        </div>
        <div className={styles.Metadata}>
          Source : Transparence santé <DisplayDateTS /> -{" "}
          <a
            href={`${ROUTES.questionsFrequentes}#comment-est-calcule-le-montant-total-des-liens-dinterets-financiers`}
          >
            Comment est calculé ce montant ?
          </a>
        </div>
      </Col>
      <Col flex={"0 0 250px"}>
        <EnhancedFileSaverButton />
      </Col>
    </Row>
  );
};
