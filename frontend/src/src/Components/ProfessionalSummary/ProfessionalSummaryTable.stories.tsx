import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import StoryRouter from "storybook-react-router";

import "../../Styles/index.less";

import { ProfessionalSummaryTable } from "./ProfessionalSummaryTable";
import { ProfessionalSummaryColumns } from "./ProfessionalSummaryColumns";

import { withProvider } from "../../../.storybook/utils/redux/provider";

import { synthese } from "../../Adapters/primary/mocks/Fixtures/syntheseFixture";

export default {
  title: "ProfessionalSummary/ProfessionalSummary",
  component: ProfessionalSummaryTable,
  parameters: { actions: { argTypesRegex: "^on.*" } },
  decorators: [StoryRouter(), withProvider],
} as Meta;

const Template: Story = () => (
  <ProfessionalSummaryTable
    columns={ProfessionalSummaryColumns}
    data={synthese}
  />
);

export const Default = Template.bind({});
