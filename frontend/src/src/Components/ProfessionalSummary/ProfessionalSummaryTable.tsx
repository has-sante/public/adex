import React from "react";
import _ from "lodash";

import {
  useTable,
  useFilters,
  useGroupBy,
  useExpanded,
  useFlexLayout,
  useSortBy,
  ColumnInstance,
} from "react-table";

import { DownOutlined, UpOutlined } from "@ant-design/icons";

// Components

import { expanderColumnBuilder } from "./expanderColumnBuilder";
import { LinkedRow } from "./LinkedRow";

import { getRowClassName, cellClassName } from "./styleUtils";
import styles from "./ProfessionalSummary.module.less";

// types
import type { TableOptions, TableState, HeaderGroup } from "react-table";

import {
  RowSyntheseDeclarations,
  SyntheseRemuneration,
  SyntheseAvantage,
} from "Core/ports/secondary/entities/synthese";

function useControlledState(state: TableState<any> /*,{ instance }*/) {
  return React.useMemo(() => {
    if (state.groupBy.length) {
      const { hiddenColumns = [], groupBy } = state;
      return {
        ...state,
        hiddenColumns: [...hiddenColumns, ...groupBy].filter(
          (d, i, all) => all.indexOf(d) === i
        ),
      };
    }
    return state;
  }, [state]);
}

const SortArrow = ({ column }: { column: HeaderGroup<any> }) => {
  return (
    <span>
      {column.isSorted ? (
        column.isSortedDesc ? (
          <>
            &nbsp;
            <DownOutlined />
          </>
        ) : (
          <>
            &nbsp;
            <UpOutlined />
          </>
        )
      ) : (
        ""
      )}
    </span>
  );
};

export const ProfessionalSummaryTable = ({
  columns,
  data,
}: TableOptions<RowSyntheseDeclarations>) => {
  const defaultColumn = React.useMemo(
    () => ({
      minWidth: 30, // minWidth is only used as a limit for resizing
      width: 150, // width is used for both the flex-basis and flex-grow
      maxWidth: 200, // maxWidth is only used as a limit for resizing
      Filter: () => {
        return <></>;
      },
    }),
    []
  );

  const initialState = React.useMemo(
    () => ({
      groupBy: ["entreprise", "annee"],
      sortBy: [{ id: "entreprise" }, { id: "annee" }],
    }),
    []
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    footerGroups,
    rows,
    prepareRow,
  } = useTable<RowSyntheseDeclarations>(
    {
      columns,
      data,
      defaultColumn,
      initialState,
    },
    useFlexLayout,
    useFilters,
    useGroupBy,
    useSortBy,
    useExpanded,
    (hooks) => {
      hooks.useControlledState.push(useControlledState);
      hooks.visibleColumns.push(
        (columns: ColumnInstance<RowSyntheseDeclarations>[], { instance }) => {
          if (!instance.state.groupBy.length) {
            return columns;
          }
          return expanderColumnBuilder({
            columns,
          });
        }
      );
    }
  );
  return (
    <>
      <table {...getTableProps()} className={styles.labTable}>
        <thead>
          {headerGroups.map((headerGroup) => {
            const { key, ...restHeaderGroupProps } =
              headerGroup.getHeaderGroupProps();
            return (
              <tr key={key} {...restHeaderGroupProps}>
                {headerGroup.headers.map((column) => {
                  return (
                    <th
                      {...column.getHeaderProps(column.getSortByToggleProps())}
                      className={styles.header}
                      key={column.id}
                    >
                      {column.render("Header")}
                      {column.canFilter ? column.render("Filter") : null}
                      <SortArrow column={column} />
                    </th>
                  );
                })}
              </tr>
            );
          })}
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.map((row) => {
            prepareRow(row);
            const { key, ...restRowProps } = row.getRowProps();
            return (
              <React.Fragment key={key}>
                <tr className={getRowClassName(row)} {...restRowProps}>
                  {row.cells.map((cell) => {
                    const { key, ...restCellProps } = cell.getCellProps();
                    return (
                      <td
                        key={key}
                        {...restCellProps}
                        className={cellClassName(cell)}
                      >
                        {cell.isPlaceholder ? null : cell.render("Cell")}
                      </td>
                    );
                  })}
                </tr>
                {
                  // Ajout des lignes des avantages et rémunérations liées à une convention
                }
                {!row.isGrouped &&
                  row.original.declaration_type === "convention" &&
                  !_.isEmpty(row.original.declaration.remunerations) &&
                  _.map(
                    row.original.declaration.remunerations,
                    (rem: SyntheseRemuneration) => {
                      return (
                        <LinkedRow
                          row={row}
                          decl={rem}
                          key={rem.source.declaration_id}
                        />
                      );
                    }
                  )}
                {!row.isGrouped &&
                  row.original.declaration_type === "convention" &&
                  !_.isEmpty(row.original.declaration.avantages) &&
                  _.map(
                    row.original.declaration.avantages,
                    (avant: SyntheseAvantage) => {
                      return (
                        <LinkedRow
                          row={row}
                          decl={avant}
                          key={avant.source.declaration_id}
                        />
                      );
                    }
                  )}
              </React.Fragment>
            );
          })}
        </tbody>
        <tfoot>
          {footerGroups.map((group) => {
            const { key, ...restFooterProps } = group.getFooterGroupProps();
            return (
              <tr key={key} {...restFooterProps}>
                {group.headers.map((column) => {
                  const { key, ...restFooterColumnProps } =
                    column.getFooterProps();
                  return (
                    <td key={key} {...restFooterColumnProps}>
                      {column.render("Footer")}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tfoot>
      </table>
    </>
  );
};
