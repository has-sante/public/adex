import React from "react";

// components

import { useAppSelector } from "Core/store/hooks";
import { useTrackPage } from "Components/ATTracker/useTrackPage";

import { ProfessionalSummaryTable } from "./ProfessionalSummaryTable";
import { ProfessionalSummaryColumns } from "./ProfessionalSummaryColumns";
// import { NumberRangeColumnFilter } from "./NumberRangeColumnFilter";

// types
import type { RootState } from "Core/store/rootReducer";

// styles
import styles from "./ProfessionalSummary.module.less";

const ProfessionalSummary = () => {
  useTrackPage({ name: "synthese" });

  const syntheseState = useAppSelector((state: RootState) => {
    return state.synthese;
  });

  const data = React.useMemo(() => {
    return syntheseState.synthese || [];
  }, [syntheseState.synthese]);

  const columns = React.useMemo(() => ProfessionalSummaryColumns, []);

  if (syntheseState.requestState === "error") {
    return <div>Erreur au chargement des données.</div>;
  }
  if (syntheseState.requestState === "loading") {
    return <div>Chargement des données en cours...</div>;
  }
  if (syntheseState.synthese?.length === 0) {
    // Normalement la requête doit toujours retourner des liens d'intérêts
    return (
      <div>
        Pas de liens d’intérêts financiers trouvés au sein de Transparence Santé
        pour la sélection.
      </div>
    );
  }
  return (
    <div className={styles.tableContainer}>
      <ProfessionalSummaryTable columns={columns} data={data} />
    </div>
  );
};

export default ProfessionalSummary;
