import styles from "./ProfessionalSummary.module.less";

import type { Cell, Row } from "react-table";

import type { RowSyntheseDeclarations } from "Core/ports/secondary/entities/synthese";

export const getRowClassName = (r: Row<RowSyntheseDeclarations>): string => {
  return [
    r.index % 2 === 0 ? styles.even : styles.odd,
    styles["RowDepth" + r.depth],
    r?.original?.declaration_type ? styles[r.original.declaration_type] : "",
    r.isGrouped ? styles.isGrouped : "",
    r.isExpanded ? styles.isExpanded : "",
  ].join(" ");
};

export const cellClassName = (cell: Cell<RowSyntheseDeclarations>) => {
  return [
    cell.isPlaceholder ? styles.isPlaceholder : styles.cellContent,
    cell.column.id === "expander"
      ? cell.row.depth === 0
        ? styles.headerCell
        : styles.subHeaderCell
      : "",

    cell.column.id === "montant_conv" ||
    cell.column.id === "montant_dec_avantages" ||
    cell.column.id === "montant_dec_remunerations"
      ? styles.cellContentCurrency
      : "",
  ].join(" ");
};
