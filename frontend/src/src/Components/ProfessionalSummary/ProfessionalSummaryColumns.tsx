import React from "react";

import { formatNumberInEuros } from "Utils/dataFormatter";
import styles from "./ProfessionalSummary.module.less";

// types
import type { TableInstance, CellProps, Column } from "react-table";
import type { RowSyntheseDeclarations } from "Core/ports/secondary/entities/synthese";

const TotalConventionsFooter = ({
  rows,
}: TableInstance<RowSyntheseDeclarations>) => {
  const total = React.useMemo(
    () =>
      rows.reduce(
        (sum: number, row) =>
          (row.isGrouped && row.depth === 0
            ? row.values.montant_conv || 0
            : 0) + sum,
        0
      ),
    [rows]
  );

  return (
    <>
      Total Conventions : <br />
      {total}&nbsp;€
    </>
  );
};

const TotalMontantRemunerationsFooter = ({
  rows,
}: TableInstance<RowSyntheseDeclarations>) => {
  const total = React.useMemo(() => {
    return rows.reduce((sum: number, row) => {
      return (
        (row.isGrouped &&
        row.depth === 0 &&
        row.values.montant_dec_remunerations > 0
          ? row.values.montant_dec_remunerations
          : 0) + sum
      );
    }, 0);
  }, [rows]);

  return (
    <div>
      Total rémunérations:
      <br />
      {formatNumberInEuros({ value: total })}
    </div>
  );
};
const TotalMontantAvantagesFooter = ({
  rows,
}: TableInstance<RowSyntheseDeclarations>) => {
  const total = React.useMemo(() => {
    return rows.reduce((sum: number, row) => {
      return (
        (row.isGrouped &&
        row.depth === 0 &&
        row.values.montant_dec_avantages > 0
          ? row.values.montant_dec_avantages
          : 0) + sum
      );
    }, 0);
  }, [rows]);

  return (
    <div className={styles.tableFooter}>
      Total avantages:
      <br />
      {formatNumberInEuros({ value: total })}
    </div>
  );
};

// Exported content
// Columns
export const ProfessionalSummaryColumns: Column<RowSyntheseDeclarations>[] = [
  {
    id: "entreprise",
    Header: () => {
      return <span className={styles.expanderHeader}>Entreprise</span>;
    },
    accessor: "entreprise",
    width: 100,
    minWidth: 100,
    maxWidth: 200,
  },
  {
    id: "annee",
    accessor: "annee",
  },
  {
    Header: "Date",
    id: "date",
    accessor: (row: RowSyntheseDeclarations) => {
      return row.declaration.date;
    },
    disableSortBy: true,
    width: 50,
    minWidth: 40,
    maxWidth: 50,
  },
  {
    Header: "Type",
    id: "type",
    accessor: "declaration_type",
    disableSortBy: true,
    width: 60,
    minWidth: 40,
    maxWidth: 80,
  },
  {
    Header: "Objet",
    disableSortBy: true,
    width: 120,
    minWidth: 40,
    maxWidth: 250,
    Cell: ({ row: { original } }: CellProps<RowSyntheseDeclarations>) => {
      if (!original) {
        return "";
      }
      let objet = original.declaration.source.motif;
      if (objet && objet.toLowerCase() === "autre") {
        return original.declaration.source.autre_motif;
      }
      if (original.declaration.source.autre_motif) {
        objet += ` ${original.declaration.source.autre_motif}`;
      }
      if (original.declaration.source.information_convention) {
        objet += ` (${original.declaration.source.information_convention})`;
      }
      if (original.declaration.source.date_debut) {
        objet += ` - Date de début : ${original.declaration.source.date_debut}`;
      }
      if (original.declaration.source.date_fin) {
        objet += ` - Date de fin : ${original.declaration.source.date_fin}`;
      }

      return objet;
    },
  },
  {
    Header: "Montant déclaré dans la convention",
    id: "montant_conv",
    width: 60,
    minWidth: 60,
    maxWidth: 60,
    accessor: (row: RowSyntheseDeclarations) => {
      return row.montants.montant_convention;
    },
    Cell: formatNumberInEuros,
    aggregate: "sum",
    Aggregated: formatNumberInEuros,
    Footer: TotalConventionsFooter,
  },
  {
    Header: "Montant déclaré dans les avantages",
    id: "montant_dec_avantages",
    width: 60,
    minWidth: 60,
    maxWidth: 60,
    accessor: (row: RowSyntheseDeclarations) => {
      return row.montants.montants_avantages;
    },
    Cell: formatNumberInEuros,
    aggregate: "sum",
    Aggregated: formatNumberInEuros,
    Footer: TotalMontantAvantagesFooter,
  },
  {
    Header: "Montant déclaré dans les rémunérations",
    id: "montant_dec_remunerations",
    width: 60,
    minWidth: 60,
    maxWidth: 80,
    accessor: (row: RowSyntheseDeclarations) => {
      return row.montants.montants_remunerations;
    },
    Cell: formatNumberInEuros,
    aggregate: "sum",
    Aggregated: formatNumberInEuros,
    Footer: TotalMontantRemunerationsFooter,
  },
];
