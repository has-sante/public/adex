import React from "react";
import { ExpandAltOutlined } from "@ant-design/icons";

import { DisplayKVToNotification } from "../DisplayKVToNotification";
import { formatNumberInEuros } from "Utils/dataFormatter";

import { cellClassName } from "./styleUtils";
import styles from "./ProfessionalSummary.module.less";

import { features } from "appconfig";

import type { Row } from "react-table";
import type {
  SyntheseRemuneration,
  SyntheseAvantage,
  RowSyntheseDeclarations,
} from "Core/ports/secondary/entities/synthese";

export const LinkedRow = ({
  row,
  decl,
}: {
  row: Row<RowSyntheseDeclarations>;
  decl: SyntheseRemuneration | SyntheseAvantage;
}) => {
  return (
    <tr
      key={decl.source.declaration_id}
      role="row"
      className={`${styles.linkedRow} ${
        row.index % 2 === 0 ? styles.even : styles.odd
      }`}
    >
      {row.cells.map((cell: any) => {
        const { key, ...restCellProps } = cell.getCellProps();

        switch (cell.column.id) {
          case "expander":
            return (
              <td key={key} {...restCellProps} className={cellClassName(cell)}>
                {features.FEATURE_DISPLAY_KV && (
                  <span
                    className={styles.showDetails}
                    onClick={DisplayKVToNotification(decl.source)}
                  >
                    <ExpandAltOutlined /> montrer les détails
                  </span>
                )}
              </td>
            );
          case "date":
            return (
              <td key={key} {...restCellProps} className={cellClassName(cell)}>
                {decl.date}
              </td>
            );
          case "type":
            return (
              <td key={key} {...restCellProps} className={cellClassName(cell)}>
                {decl.source.lien_interet}
              </td>
            );
          case "Objet":
            return (
              <td key={key} {...restCellProps} className={cellClassName(cell)}>
                {decl?.source?.autre_motif}
              </td>
            );
          case "montant_dec_avantages":
            return (
              <td key={key} {...restCellProps} className={cellClassName(cell)}>
                {decl.source.lien_interet === "avantage" &&
                  formatNumberInEuros({ value: decl.montant })}
              </td>
            );
          case "montant_dec_remunerations":
            return (
              <td key={key} {...restCellProps} className={cellClassName(cell)}>
                {decl.source.lien_interet === "remuneration" &&
                  formatNumberInEuros({ value: decl.montant })}
              </td>
            );
          default:
            return (
              <td
                key={key}
                {...restCellProps}
                className={cellClassName(cell)}
              ></td>
            );
        }
      })}
    </tr>
  );
};
