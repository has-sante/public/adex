import React from "react";

import { Cell, Column } from "react-table";
import {
  RightOutlined,
  DownOutlined,
  ExpandAltOutlined,
} from "@ant-design/icons";

import { DisplayKVToNotification } from "../DisplayKVToNotification";

import { features } from "appconfig";

import styles from "./ProfessionalSummary.module.less";
// types
import type { TableInstance } from "react-table";

import type { RowSyntheseDeclarations } from "Core/ports/secondary/entities/synthese";

interface IRowExpanderProps {
  toggleAllRowsExpanded: Function;
  isAllRowsExpanded?: boolean;
}
const RowExpander = ({
  toggleAllRowsExpanded,
  isAllRowsExpanded,
}: IRowExpanderProps) => {
  const text = isAllRowsExpanded ? "Replier tout" : "Déplier tout";
  const handleClickToggle = () => {
    return toggleAllRowsExpanded(!isAllRowsExpanded);
  };
  return (
    <>
      -{" "}
      <span
        onClick={handleClickToggle}
        className={styles.expandAllRows}
        title={text}
      >
        {text}
      </span>
    </>
  );
};

// expanderColumn allows us to have a more compact layout
// with grouped by columns in the same tableColumn
export const expanderColumnBuilder = ({
  columns,
}: {
  columns: Column<RowSyntheseDeclarations>[];
}) => {
  return [
    {
      id: "expander",
      width: 100,
      Header: ({
        allColumns,
        state: { groupBy },
        toggleAllRowsExpanded,
        isAllRowsExpanded,
      }: TableInstance) => {
        return (
          <>
            {groupBy.map((columnId) => {
              const column = allColumns.find((d) => d.id === columnId);
              if (!column) {
                return null;
              }

              const { key, ...restHeaderProps } = column.getHeaderProps();
              return (
                <span key={key} {...restHeaderProps}>
                  {column.render("Header")}
                </span>
              );
            })}
            <RowExpander
              toggleAllRowsExpanded={toggleAllRowsExpanded}
              isAllRowsExpanded={isAllRowsExpanded}
            />
          </>
        );
      },
      Cell: ({ row }: Cell<RowSyntheseDeclarations>) => {
        if (row.canExpand) {
          const groupedCell = row.allCells.find((d) => d.isGrouped);
          if (!groupedCell) {
            return null;
          }
          return (
            <span
              {...row.getToggleRowExpandedProps({
                style: {
                  paddingLeft: `${row.depth * 2}rem`,
                },
              })}
            >
              {row.isExpanded ? (
                <DownOutlined className={styles.expanderArrow} />
              ) : (
                <RightOutlined className={styles.expanderArrow} />
              )}
              {groupedCell.render("Cell")} ({row.leafRows.length})
            </span>
          );
        }
        return (
          (features.FEATURE_DISPLAY_KV && (
            <span
              className={styles.showDetails}
              onClick={DisplayKVToNotification(row.original.declaration.source)}
            >
              <ExpandAltOutlined /> montrer les détails
            </span>
          )) ||
          null
        );
      },
    },
    ...columns,
  ];
};
