// internalLink makes next rule not applicable
/* eslint-disable react/jsx-no-target-blank */
import React from "react";
import { Link } from "react-router-dom";
import { Col, Button } from "antd";

type LandingCardProps = {
  illustration_url: string;
  title: string;
  description: string;
  cta?: string;
  cta_url?: string;
  cta_email?: string;
  internalLink?: boolean;
};

export const LandingCard = ({
  illustration_url,
  cta_url,
  cta_email,
  internalLink = false,
  title,
  description,
  cta,
}: LandingCardProps) => {
  const CtaButton = () => {
    if (cta && cta_url) {
      if (internalLink) {
        return (
          <Link to={cta_url}>
            <Button size={"large"} type={"primary"}>
              {cta}
            </Button>
          </Link>
        );
      }
      return (
        <a
          href={cta_url}
          target={internalLink ? "_self" : "_blank"}
          rel={internalLink ? "" : "noreferrer noopener"}
        >
          <Button size={"large"} type={"primary"}>
            {cta}
          </Button>
        </a>
      );
    }
    if (cta_email) {
      return (
        <Button size={"large"} type={"primary"} href={cta_email}>
          {cta}
        </Button>
      );
    }
    return null;
  };

  const colProps = () => {
    return {
      xs: { span: 24 },
      md: { span: 12 },
      xl: { span: 5 },
    };
  };

  return (
    <Col {...colProps()} className={"card"}>
      <div>
        <img src={illustration_url} />
      </div>
      <h2>{title}</h2>
      <p>{description}</p>
      {CtaButton()}
    </Col>
  );
};
