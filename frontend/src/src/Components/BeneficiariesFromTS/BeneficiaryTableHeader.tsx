import React from "react";

import { Row, Col } from "antd";

import styles from "./BeneficiariesFromTS.module.less";

export const BeneficiaryTableHeader = () => {
  return (
    <Row className={styles.beneficiaryTableHeader}>
      <Col flex={"60px"}></Col>
      <Col flex={"1"}>
        <Row>
          <Col xs={{ span: 0 }} md={{ span: 4 }}></Col>
          <Col xs={{ span: 0 }} md={{ span: 5 }}>
            Profession
          </Col>
          <Col xs={{ span: 0 }} md={{ span: 4 }}>
            Identifiant
          </Col>
          <Col xs={{ span: 0 }} md={{ span: 4 }}>
            Ville
          </Col>
          <Col xs={{ span: 0 }} md={{ span: 6 }}>
            Adresses
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
