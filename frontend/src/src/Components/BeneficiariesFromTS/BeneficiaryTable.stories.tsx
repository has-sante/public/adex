import React, { useState } from "react";
import { Story, Meta } from "@storybook/react";
import StoryRouter from "storybook-react-router";

import { withProvider } from "../../../.storybook/utils/redux/provider";

import {
  EnhancedBeneficiaryTable,
  BeneficiaryTableConnectedProps,
} from "./BeneficiaryTable";
import { toggleSelectionInHashes } from "../../Utils/hashesSelector";

export default {
  title: "Beneficiaries/BeneficiaryTable",
  component: EnhancedBeneficiaryTable,
  decorators: [StoryRouter(), withProvider],
} as Meta;

const Template: Story<BeneficiaryTableConnectedProps> = ({
  selectedHashes,
  ...args
}) => {
  const [localHashes, setSelectedHashes] = useState(selectedHashes);
  const onSelectHashes = (selection: string[], shouldSelect: boolean) => {
    setSelectedHashes(
      toggleSelectionInHashes(localHashes, selection, shouldSelect)
    );
  };
  return (
    <EnhancedBeneficiaryTable
      {...args}
      selectedHashes={localHashes}
      onSelectHashes={onSelectHashes}
    />
  );
};

export const WithData = Template.bind({});
WithData.args = {};
