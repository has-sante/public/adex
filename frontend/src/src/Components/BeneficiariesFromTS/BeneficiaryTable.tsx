import React from "react";
import _ from "lodash";
import { connect, ConnectedProps } from "react-redux";
import { Virtuoso, VirtuosoHandle } from "react-virtuoso";
import { Button } from "antd";

// components
import { BeneficiaryTableHeader } from "./BeneficiaryTableHeader";
import { BeneficiaryTableRow } from "./BeneficiaryTableRow";
import { Actions } from "Core/useCases/listBeneficiaryFromTS";
import { listBeneficiariesFromTSThunk } from "Core/useCases/listBeneficiaryFromTS/thunks";
import { RootState } from "Core/store/rootReducer";

// connect to Redux & get connector types
const mapStateToProps = (state: RootState) => {
  return {
    hasNextPage: state.beneficiariesFromTS.hasNextPage,
    isError: state.beneficiariesFromTS.isError,
    isNextPageLoading: state.beneficiariesFromTS.isNextPageLoading,
    items: state.beneficiariesFromTS.beneficiaryItems,
  };
};

const mapDispatchToProps = {
  listBeneficiariesFromTSThunk,
  resetState: Actions.resetState,
};
const connector = connect(mapStateToProps, mapDispatchToProps);

// types
interface BeneficiaryTableProps {
  identification_nationale_pp?: string;
  search_query?: string;
  onSelectHashes: Function;
  selectedHashes: string[];
}
interface BeneficiaryTableState {
  expandedItems: Array<string>;
}

export type BeneficiaryTableConnectedProps = ConnectedProps<typeof connector> &
  BeneficiaryTableProps;

// component
class BeneficiaryTable extends React.PureComponent<
  BeneficiaryTableConnectedProps,
  BeneficiaryTableState
> {
  private virtuosoRef: React.RefObject<VirtuosoHandle>;
  constructor(props: BeneficiaryTableConnectedProps) {
    super(props);
    this.virtuosoRef = React.createRef();
  }

  state = {
    expandedItems: [],
  };

  componentDidMount() {
    this._loadNextPage();
  }
  componentDidUpdate(prevProps: BeneficiaryTableConnectedProps) {
    // reset & reload only when query has changed
    if (
      !(
        prevProps.identification_nationale_pp ===
        this.props.identification_nationale_pp
      ) ||
      !(prevProps.search_query === this.props.search_query)
    ) {
      this.props.resetState();
      this.virtuosoRef.current?.scrollToIndex({ index: 0 });
      this._loadNextPage();
    }
  }

  _loadNextPage = () => {
    const { identification_nationale_pp, search_query } = this.props;
    this.props.listBeneficiariesFromTSThunk({
      identification_nationale_pp,
      search_query,
    });
  };

  _toggleExpandedItem = (hash: string) => {
    const { expandedItems } = this.state;
    this.setState({ expandedItems: _.xor(expandedItems, [hash]) });
  };

  render() {
    const { expandedItems } = this.state;
    const {
      onSelectHashes,
      selectedHashes,
      hasNextPage,
      isNextPageLoading,
      items,
      isError,
    } = this.props;
    const Footer = () => {
      if (isError) {
        return (
          <div
            style={{
              padding: "2rem",
              display: "flex",
              justifyContent: "center",
            }}
          >
            <Button onClick={this._loadNextPage}>
              Erreur au chargement des données. Réessayer ?
            </Button>
          </div>
        );
      }
      if (isNextPageLoading) {
        return (
          <div
            style={{
              padding: "2rem",
              display: "flex",
              justifyContent: "center",
            }}
          >
            Chargement en cours...
          </div>
        );
      }
      if (hasNextPage) {
        return (
          <div
            style={{
              padding: "2rem",
              display: "flex",
              justifyContent: "center",
            }}
          >
            Autres résultats disponibles...
          </div>
        );
      }
      return (
        <div
          style={{
            padding: "2rem",
            display: "flex",
            justifyContent: "center",
          }}
        >
          Fin des résultats disponibles.
        </div>
      );
    };
    return (
      <>
        <BeneficiaryTableHeader />
        <Virtuoso
          ref={this.virtuosoRef}
          data={items}
          endReached={this._loadNextPage}
          overscan={200}
          itemContent={(index, data) => {
            return (
              <BeneficiaryTableRow
                index={index}
                record={data}
                onSelectHashes={onSelectHashes}
                hashes={selectedHashes}
                expandedItems={expandedItems}
                toggleExpandedItem={this._toggleExpandedItem}
              />
            );
          }}
          components={{
            Footer: Footer,
          }}
        />
      </>
    );
  }
}

export const EnhancedBeneficiaryTable = connector(BeneficiaryTable);
