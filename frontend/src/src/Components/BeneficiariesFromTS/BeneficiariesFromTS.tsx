import React from "react";
import { Row, Col } from "antd";

import { useHistory } from "react-router-dom";
import queryString from "query-string";

import { useParsedQuery } from "Hooks/useQueryParamsInURL";
import { toggleSelectionInHashes } from "Utils/hashesSelector";

import { EnhancedBeneficiaryTable } from "./BeneficiaryTable";

type BeneficiariesFromTSProps = {
  identification_nationale_pp?: string;
  search_query?: string;
};

export const BeneficiariesFromTS = ({
  identification_nationale_pp,
  search_query,
}: BeneficiariesFromTSProps) => {
  const searchQuery = useParsedQuery();
  const hashesStringInURL = searchQuery["hashes"];
  const selectedHashesInURL =
    hashesStringInURL &&
    !(hashesStringInURL === "") &&
    typeof hashesStringInURL === "string"
      ? hashesStringInURL.split(",")
      : [];

  const history = useHistory();

  const setSelectedHashesInURL = (hashes: string[]) => {
    const listOfHashes = hashes.join(",");
    searchQuery["hashes"] = listOfHashes;
    history.replace({ search: queryString.stringify(searchQuery) });
  };

  const _onSelectHashes = (selection: Array<string>, shouldSelect: boolean) => {
    setSelectedHashesInURL(
      toggleSelectionInHashes(selectedHashesInURL, selection, shouldSelect)
    );
  };

  return (
    <>
      <Row style={{ height: "100%", overflow: "visible" }}>
        <Col style={{ flex: "auto", overflow: "auto" }}>
          <div
            style={{ height: "100%", display: "flex", flexDirection: "column" }}
          >
            <EnhancedBeneficiaryTable
              identification_nationale_pp={identification_nationale_pp}
              search_query={search_query}
              onSelectHashes={_onSelectHashes}
              selectedHashes={selectedHashesInURL}
            ></EnhancedBeneficiaryTable>
          </div>
        </Col>
      </Row>
    </>
  );
};
