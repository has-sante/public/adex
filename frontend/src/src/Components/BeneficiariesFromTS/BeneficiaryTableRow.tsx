import React, { MouseEventHandler } from "react";
import _ from "lodash";
import { Row, Col, Checkbox } from "antd";
import { RightOutlined, DownOutlined } from "@ant-design/icons";
// types
import type {
  BeneficiaryGroupType,
  BeneficiaryType,
} from "Core/ports/secondary/entities/beneficiary";

// Components
import {
  BeneficiaryAdressToString,
  BeneficiaryPostalCodeCityToString,
  NumberOfDistinctAdresses,
} from "../BeneficiaryAdressesWithModal";

import { DisplayKVToNotification } from "../DisplayKVToNotification";
// utils
import { formatBeneficiaryName } from "Utils/formatBeneficiaryDetails";
import { capitalize } from "Utils/dataFormatter";

import { features } from "appconfig";
// Styles

import styles from "./BeneficiariesFromTS.module.less";
// --- Component --- //

export interface BeneficiaryTableRowPropsType {
  record: BeneficiaryGroupType;
  onSelectHashes: Function;
  toggleExpandedItem: Function;
  hashes?: string[];
  expandedItems: string[];
  index: number;
}

const displayScore = (record: BeneficiaryGroupType) => {
  if (features.DISPLAY_SCORE) {
    return (
      <Col flex={"60px"}>
        <span className={styles.score}>{Math.round(record.score)}</span>
      </Col>
    );
  }
};

const displayNameCategory = (record: BeneficiaryGroupType) => {
  if (record.children) {
    return (
      <div>
        <h3
          dangerouslySetInnerHTML={{
            __html: `${formatBeneficiaryName(record)}`,
          }}
        ></h3>
        <p className={styles.beneficiaryId}>
          {record.categorie_beneficiaire_libelle}
        </p>
      </div>
    );
  }
  return "";
};

const displayProfession = (record: BeneficiaryType) => {
  return (
    <p className={styles.beneficiaryId}>
      {record.profession ? `${record.profession}` : ""}
    </p>
  );
};

const displayIdentifiant = (record: BeneficiaryType) => {
  return (
    <p className={styles.beneficiaryId}>
      <span
        dangerouslySetInnerHTML={{
          __html: record.numero_identifiant || "",
        }}
      ></span>
      {` (${capitalize(record.type_identifiant)})`}
    </p>
  );
};

const displayCity = (
  record: BeneficiaryGroupType,
  handleToggleExpandGroup: MouseEventHandler<HTMLSpanElement>,
  isExpanded = false
) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const nbAdresses = NumberOfDistinctAdresses(record);
  const firstBeneficiaryDetails = record?.children && record.children[0];
  const firstAdress =
    firstBeneficiaryDetails &&
    BeneficiaryPostalCodeCityToString(firstBeneficiaryDetails);

  if (nbAdresses === 1) {
    return <span>{firstAdress}</span>;
  }
  if (isExpanded) {
    return (
      <span onClick={handleToggleExpandGroup}>
        <h4 className={styles.addressesToggleRow}>{`${nbAdresses} adresse${
          nbAdresses === 1 ? "" : "s"
        }`}</h4>
      </span>
    );
  }
  return (
    <>
      <span>{firstAdress}</span>
      <span onClick={handleToggleExpandGroup}>
        <h4 className={styles.addressesToggleRow}>{`${nbAdresses - 1} adresse${
          nbAdresses - 1 === 1 ? "" : "s"
        } supplémentaire${nbAdresses - 1 === 1 ? "" : "s"}`}</h4>
      </span>
    </>
  );
};

const ExpandedRow = ({
  hashes,
  onSelectHashes,
  children,
}: {
  hashes?: string[];
  onSelectHashes: Function;
  children: BeneficiaryType;
}) => {
  const isChecked = hashes && hashes.includes(children.benef_hash);
  const onClick = () => {
    onSelectHashes && onSelectHashes([children.benef_hash], !isChecked);
  };
  return (
    <Row
      key={children.benef_hash}
      className={`${styles.BeneficiaryExpandedRow} ${
        isChecked && styles.BeneficiaryTableRowSelected
      }`}
    >
      <Col flex={"60px"}>
        <span className={styles.checkboxExpanded}>
          <Checkbox checked={isChecked} onClick={onClick} />
        </span>
      </Col>
      <Col flex={1} style={{ maxWidth: "calc(100% - 60px)" }}>
        <Row>
          <Col xs={24} md={4}></Col>
          <Col xs={24} md={5}></Col>
          <Col xs={24} md={4}></Col>
          <Col xs={24} md={4}>
            {BeneficiaryPostalCodeCityToString(children)}
          </Col>
          <Col xs={24} md={6}>
            {BeneficiaryAdressToString(children)}
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

interface HeaderRowProps {
  record: BeneficiaryGroupType;
  isGroupSelected?: boolean;
  isGroupExpanded?: boolean;
  isGroupSelectedIntermediate?: boolean;
  handleToggleExpandGroup: MouseEventHandler;
  handleToggleGroup: MouseEventHandler;
  firstBeneficiaryDetails?: BeneficiaryType;
}

const HeaderRow: React.FC<HeaderRowProps> = ({
  record,
  isGroupSelected,
  isGroupExpanded,
  isGroupSelectedIntermediate,
  handleToggleExpandGroup,
  handleToggleGroup,
  firstBeneficiaryDetails,
}) => (
  <Row className={styles.BeneficiaryHeaderRow}>
    <Col flex={"60px"}>
      <span className={styles.checkboxHeader}>
        {record.group_size > 1 && (
          <span onClick={handleToggleExpandGroup}>
            {isGroupExpanded ? <DownOutlined /> : <RightOutlined />}
          </span>
        )}
      </span>
      <Checkbox
        checked={isGroupSelected}
        indeterminate={isGroupSelectedIntermediate}
        onClick={handleToggleGroup}
      />
    </Col>

    <Col flex={"1"}>
      <Row>
        <Col xs={20} md={4}>
          {displayNameCategory(record)}
        </Col>
        <Col xs={20} md={5}>
          {firstBeneficiaryDetails && displayProfession(record)}
        </Col>
        <Col xs={20} md={4}>
          {firstBeneficiaryDetails && displayIdentifiant(record)}
        </Col>
        <Col xs={0} md={4}>
          {!isGroupExpanded &&
            firstBeneficiaryDetails &&
            displayCity(record, handleToggleExpandGroup, isGroupExpanded)}
        </Col>
        <Col xs={20} md={6}>
          {!isGroupExpanded &&
            firstBeneficiaryDetails &&
            BeneficiaryAdressToString(firstBeneficiaryDetails)}
        </Col>
      </Row>
    </Col>
    {displayScore(record)}
  </Row>
);

export class BeneficiaryTableRow extends React.PureComponent<BeneficiaryTableRowPropsType> {
  state = {
    isGroupExpanded: false,
  };

  _expandedRows = (record: BeneficiaryGroupType) => {
    const { hashes, onSelectHashes } = this.props;
    if (record.children) {
      return record.children.map((beneficiaryVariant) => {
        return (
          <ExpandedRow
            key={beneficiaryVariant.benef_hash}
            hashes={hashes}
            onSelectHashes={onSelectHashes}
          >
            {beneficiaryVariant}
          </ExpandedRow>
        );
      });
    }
  };

  _numberOfSelectedChildren = () => {
    const { record, hashes } = this.props;
    return (
      (record.children &&
        record.children.filter((c) => {
          return hashes && hashes.includes(c.benef_hash);
        }).length) ||
      0
    );
  };

  _isGroupSelected = () => {
    const { record } = this.props;
    return (
      this._numberOfSelectedChildren() ===
      (record.children && record.children.length)
    );
  };

  handleToggleGroup = () => {
    const { record, onSelectHashes } = this.props;
    const selectAll = !this._isGroupSelected();
    const selectedHashes =
      record.children &&
      record.children.map((c) => {
        return c.benef_hash;
      });
    onSelectHashes && onSelectHashes(selectedHashes, selectAll);
  };

  handleToggleExpandGroup = () => {
    const { record, toggleExpandedItem } = this.props;
    toggleExpandedItem(record.benef_hash);
  };

  handleDoubleClick = () => {
    if (features.FEATURE_DISPLAY_KV) {
      DisplayKVToNotification(this.props.record)();
    }
  };

  render() {
    const { record, expandedItems, index } = this.props;
    if (_.isEmpty(record)) {
      return (
        <Row>
          <Col>Chargement...</Col>
        </Row>
      );
    }

    const isGroupExpanded = !!expandedItems.includes(record.benef_hash);
    const isGroupSelected = this._isGroupSelected();

    const numberOfSelectedChildren = this._numberOfSelectedChildren();
    const isGroupSelectedIntermediate =
      numberOfSelectedChildren > 0 &&
      numberOfSelectedChildren < record.group_size;

    const firstBeneficiaryDetails = record?.children && record.children[0];

    return (
      <Row
        onDoubleClick={this.handleDoubleClick}
        className={`${
          index % 2
            ? styles.BeneficiaryTableRowOdd
            : styles.BeneficiaryTableRowEven
        } ${isGroupSelected ? styles.BeneficiaryTableRowSelected : ""}`}
      >
        <Col flex={1}>
          <HeaderRow
            record={record}
            firstBeneficiaryDetails={firstBeneficiaryDetails}
            isGroupSelected={isGroupSelected}
            isGroupExpanded={isGroupExpanded}
            isGroupSelectedIntermediate={isGroupSelectedIntermediate}
            handleToggleExpandGroup={this.handleToggleExpandGroup}
            handleToggleGroup={this.handleToggleGroup}
          />
          {isGroupExpanded && this._expandedRows(record)}
        </Col>
      </Row>
    );
  }
}
