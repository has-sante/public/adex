import React, { useState } from "react";
import { Story, Meta } from "@storybook/react";
import StoryRouter from "storybook-react-router";
import _ from "lodash";

import {
  BeneficiaryTableRow,
  BeneficiaryTableRowPropsType,
} from "./BeneficiaryTableRow";
import { BeneficiaryFromTSFixture } from "../../Adapters/primary/mocks/Fixtures/beneficiaryFromTSFixture";
import { toggleSelectionInHashes } from "../../Utils/hashesSelector";

const data = BeneficiaryFromTSFixture.data;

export default {
  title: "Beneficiaries/BeneficiaryTableRow",
  component: BeneficiaryTableRow,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story<BeneficiaryTableRowPropsType> = ({ hashes, ...args }) => {
  const [expandedItems, setExpandedItems] = useState([]);
  const toggleExpandedItem = (hash: string) => {
    setExpandedItems(_.xor(expandedItems, [hash]));
  };
  const [selectedHashes, setSelectedHashes] = useState(hashes);
  const onSelectHashes = (selection: string[], shouldSelect: boolean) => {
    setSelectedHashes(
      toggleSelectionInHashes(selectedHashes, selection, shouldSelect)
    );
  };

  return (
    <BeneficiaryTableRow
      {...args}
      hashes={selectedHashes}
      onSelectHashes={onSelectHashes}
      expandedItems={expandedItems}
      toggleExpandedItem={toggleExpandedItem}
    />
  );
};

export const WithOneSingleRow = Template.bind({});
WithOneSingleRow.args = {
  record: data[0],
};

export const WithData = Template.bind({});
WithData.args = {
  record: data[1],
};
export const SelectedRow = Template.bind({});
SelectedRow.args = {
  record: data[0],
  hashes: [data[0].children && data[0].children[0].benef_hash],
};
export const AllSelected = Template.bind({});
const hashes = _.map(data[1].children, "benef_hash");
AllSelected.args = {
  record: data[1],
  hashes: hashes,
};

export const Loading = Template.bind({});
