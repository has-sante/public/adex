import React from "react";
import { Col, Row, Grid } from "antd";
import { Link, Route } from "react-router-dom";

import { SearchBeneficiaries } from "Components/SearchBeneficiaries";
import { ROUTES } from "Pages/routes";

export const AdexHeader = () => {
  const screens = Grid.useBreakpoint();
  return (
    <div className={"adexHeader"}>
      <Row>
        <Col
          xs={{ offset: 1, span: 3 }}
          md={{ offset: 0, span: 12 }}
          xl={{ offset: 0, span: 9 }}
        >
          {screens.md ? (
            <span className={"logo"}>
              <img src="./images/logo-has-simple.svg"></img>
            </span>
          ) : null}

          <Link to={ROUTES.landingPage} className={"brand"}>
            ADEX
          </Link>
          {screens.md ? (
            <Link to={ROUTES.landingPage} className={"home"}>
              Accueil
            </Link>
          ) : null}
        </Col>
        <Route
          path={[
            ROUTES.fromDirectory,
            ROUTES.browseTS,
            ROUTES.linksOfInterests,
          ]}
        >
          <Col xs={{ span: 19 }} md={{ span: 10 }} xl={{ span: 10 }}>
            <SearchBeneficiaries className={"searchBenef"} />
          </Col>
        </Route>
      </Row>
    </div>
  );
};
