import React, { ReactChild } from "react";
import { Row } from "antd";

import styles from "./ContainedRow.module.less";

export type ContainedRowPropTypes = {
  children: ReactChild[] | ReactChild;
  className?: string;
  align?: "top" | "middle" | "bottom";
  justify?: "start" | "end" | "center" | "space-around" | "space-between";
};

export const ContainedRow = ({
  children,
  className,
  align,
  justify,
}: ContainedRowPropTypes) => {
  return (
    <div className={`${styles.containedRow} ${className ?? ""}`}>
      <div className={styles.container}>
        <Row align={align} justify={justify}>
          {children}
        </Row>
      </div>
    </div>
  );
};
