import React from "react";
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from "@storybook/react/types-6-0";

import { Col } from "antd";

import "Styles/index.less";

import { ContainedRow, ContainedRowPropTypes } from "./ContainedRow";

export default {
  title: "Generic/ContainedRow",
  component: ContainedRow,
  parameters: { actions: { argTypesRegex: "^on.*" } },
} as Meta;

const Template: Story<ContainedRowPropTypes & { breakpoint: number }> = (
  args
) => {
  const { children, breakpoint, ...rest } = args;
  return (
    <div style={{ width: `${breakpoint}px` }}>
      <ContainedRow {...rest}>{children}</ContainedRow>
    </div>
  );
};

export const Default = Template.bind({});
Default.args = {
  breakpoint: 576,
  className: "jumboPrimary",
  children: (
    <Col span={24} style={{ border: "3px solid red", color: "white" }}>
      ContainedRow retourne un Composant &quot;Row&quot; (antd) qui fera maximum
      1232px de large. Il convient de lui donner 1 ou plusieurs composants
      &quot;Col&quot; (antd).
    </Col>
  ),
};

export const Large = Template.bind({});
Large.args = {
  breakpoint: 992,
  className: "jumboPrimary",
  children: (
    <Col span={24} style={{ border: "3px solid red", color: "white" }}>
      ContainedRow retourne un Composant &quot;Row&quot; (antd) qui fera maximum
      1232px de large. Il convient de lui donner 1 ou plusieurs composants
      &quot;Col&quot; (antd).
    </Col>
  ),
};

export const XXL = Template.bind({});
XXL.args = {
  breakpoint: 1600,
  className: "jumboPrimary",
  children: (
    <Col span={24} style={{ border: "3px solid red", color: "white" }}>
      ContainedRow retourne un Composant &quot;Row&quot; (antd) qui fera maximum
      1232px de large. Il convient de lui donner 1 ou plusieurs composants
      &quot;Col&quot; (antd).
    </Col>
  ),
};
