import React, { ReactNode } from "react";
import _ from "lodash";
import { useLocation } from "react-router";
import { Anchor, message } from "antd";
import ReactMarkdown from "react-markdown";
import { LinkOutlined } from "@ant-design/icons";

const CopyLocationToClipboard = (title: string) => {
  return () => {
    // setTimeout est nécessaire pour permettre de copier l'url après l'accès au lien
    // window.location.href est utilisé pour éviter de devoir reconstruire l'url manuellement
    setTimeout(() => {
      navigator.clipboard.writeText(window.location.href);
      message.success(
        `Le lien pour "${title}" a été copié dans le presse-papier"`
      );
    }, 50);
  };
};

// Pour créer des liens entre TOC et contenu, un id est généré à partir de la
// chaine de caractère des titres de niveau h1 et h2.
// Ainsi le titre utilisé pour l'entrée de la TOC et le titre du contenu
// doivent être identiques.

const stringToAttributeId = (text: string) => {
  const from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
  const to = "aaaaaeeeeeiiiiooooouuuunc------";

  _.each(from, function (character, i) {
    text = text.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
  });

  return text
    .toLowerCase() // Convert the string to lowercase letters
    .replace(/&/g, "-et-") // Replace & with 'and'
    .trim() // Remove whitespace from both sides of a string
    .replace(/\s+/g, "-") // Replace spaces with -
    .replace(/[^\w\-]+/g, "") // Remove all non-word chars
    .replace(/\-\-+/g, "-") // Replace multiple - with single -
    .replace(/\-$/, ""); // remove trailing -
};

const AnchorLinks = ({ children }: { children: ReactNode[] }) => {
  const location = useLocation();

  if (children instanceof Array && typeof children[0] === "string") {
    const title = children[0];
    const slug = stringToAttributeId(title);
    return <Anchor.Link href={`${location.pathname}#${slug}`} title={title} />;
  }
  return <div>error</div>;
};

const H1WithAnchor = ({ children }: { children: ReactNode[] }) => {
  const location = useLocation();
  if (children instanceof Array && typeof children[0] === "string") {
    const title = children[0];
    const slug = stringToAttributeId(title);
    const link = `${location.pathname}#${slug}`;
    return (
      <h1 title={title} id={`${slug}`} className={"anchorable"}>
        {title}
        <a href={link} onClick={CopyLocationToClipboard(title)}>
          <LinkOutlined />
        </a>
      </h1>
    );
  }
  return null;
};
const H2WithAnchor = ({ children }: { children: ReactNode[] }) => {
  if (children instanceof Array && typeof children[0] === "string") {
    const title = children[0];
    const slug = stringToAttributeId(title);
    const link = `${location.pathname}#${slug}`;
    return (
      <h2 title={title} id={`${slug}`} className={"anchorable"}>
        {title}
        <a href={link} onClick={CopyLocationToClipboard(title)}>
          <LinkOutlined />
        </a>
      </h2>
    );
  }
  return null;
};

export const ReactMarkdownWithAnchors = ({
  children,
}: {
  children: string;
}) => {
  return (
    <ReactMarkdown
      components={{
        h1: ({ node, ...props }) => <H1WithAnchor {...props} />,
        h2: ({ node, ...props }) => <H2WithAnchor {...props} />,
      }}
    >
      {children}
    </ReactMarkdown>
  );
};

export const ReactMarkdownTOC = ({ children }: { children: string }) => {
  return (
    <Anchor>
      <ReactMarkdown
        components={{
          h1: ({ node, ...props }) => <AnchorLinks {...props} />,
          h2: ({ node, ...props }) => <AnchorLinks {...props} />,
        }}
      >
        {children}
      </ReactMarkdown>
    </Anchor>
  );
};
