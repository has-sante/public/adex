import React from "react";
import { Helmet } from "react-helmet";

export const MetaNoIndex = () => {
  return (
    <Helmet>
      <meta name="robots" content="noindex, nofollow" />
    </Helmet>
  );
};
