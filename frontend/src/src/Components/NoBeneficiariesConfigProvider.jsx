import React from "react";
import { ConfigProvider, Empty } from "antd";

export const NoBeneficiariesConfigProvider = ({ description, children }) => {
  const empty = () => (
    <Empty description={description} image={Empty.PRESENTED_IMAGE_SIMPLE} />
  );
  return <ConfigProvider renderEmpty={empty}>{children}</ConfigProvider>;
};
