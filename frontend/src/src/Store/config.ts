import { AxiosCallGetDetailsFromDirectory } from "Adapters/primary/apis/AxiosCallGetDetailsFromDirectory";
import { AxiosDirectorySearch } from "Adapters/primary/apis/AxiosDirectorySearch";
import { AxiosGetSynthese } from "Adapters/primary/apis/AxiosGetSynthese";
import { AxiosGetVersion } from "Adapters/primary/apis/AxiosGetVersion";
import { AxiosListBeneficiariesFromTSById } from "Adapters/primary/apis/AxiosListBeneficiariesFromTSById";
import { AxiosListBeneficiariesFromTSBySearch } from "Adapters/primary/apis/AxiosListBeneficiariesFromTSBySearch";

import type { GatewaysType } from "Core/ports/primary";

export const gatewaysConfig: GatewaysType = {
  searchDirectory: AxiosDirectorySearch,
  getDetailsFromDirectory: AxiosCallGetDetailsFromDirectory,
  getSynthese: AxiosGetSynthese,
  getVersion: AxiosGetVersion,
  listBeneficiariesFromTSById: AxiosListBeneficiariesFromTSById,
  listBeneficiariesFromTSBySearch: AxiosListBeneficiariesFromTSBySearch,
};
