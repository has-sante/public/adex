import { configureReduxStore } from "Core/store/store.config";
// Pour utiliser les fixtures au lieu des appels aux apis,
// utiliser config.with_fixtures au lieu de config
// import { gatewaysConfig } from "./config.with_fixtures";
import { gatewaysConfig } from "./config";

export const appStore = configureReduxStore(gatewaysConfig);
