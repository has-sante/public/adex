import { AxiosCallGetDetailsFromDirectory } from "Adapters/primary/apis/AxiosCallGetDetailsFromDirectory";
import { AxiosDirectorySearch } from "Adapters/primary/apis/AxiosDirectorySearch";

import { mockListBeneficiariesFromTS } from "Adapters/primary/mocks/mockListBeneficiariesFromTS";
import { mockGetSynthese } from "Adapters/primary/mocks/mockGetSynthese";
import { mockGetVersion } from "Adapters/primary/mocks/mockGetVersion";

import type { GatewaysType } from "Core/ports/primary";

export const gatewaysConfig: GatewaysType = {
  searchDirectory: AxiosDirectorySearch,
  getDetailsFromDirectory: AxiosCallGetDetailsFromDirectory,
  getSynthese: mockGetSynthese,
  getVersion: mockGetVersion,
  listBeneficiariesFromTSById: mockListBeneficiariesFromTS,
  listBeneficiariesFromTSBySearch: mockListBeneficiariesFromTS,
};
