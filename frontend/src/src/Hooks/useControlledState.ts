import { useState } from "react";

export type controlledStateType<T> = [T | undefined, Function];
export const useControlledState = <T extends unknown>(
  value?: T,
  setValue?: Function
): controlledStateType<T> => {
  if (typeof setValue === "function") {
    console.log("controlled");
    return [value, setValue];
  }

  return useState(value);
};
