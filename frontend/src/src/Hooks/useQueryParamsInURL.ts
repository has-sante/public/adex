import { useLocation } from "react-router-dom";
import queryString from "query-string";

export function useQuery() {
  return new URLSearchParams(useLocation().search);
}

export function useParsedQuery() {
  const location = useLocation();
  const parsedQuery = queryString.parse(location.search);
  return {
    s: typeof parsedQuery.s === "string" ? parsedQuery.s : null,
    hashes: typeof parsedQuery.hashes === "string" ? parsedQuery.hashes : null,
    identification_nationale_pp:
      typeof parsedQuery.identification_nationale_pp === "string"
        ? parsedQuery.identification_nationale_pp
        : null,
  };
}
