import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export type requestStateType = "loading" | "error" | "loaded" | "idle";

export type state = {
  requestState: requestStateType;
  lastRequest?: number;
  commit_sha?: string;
  date_maj_donnees?: string;
  version?: string;
};

interface SetResponseDataType {
  lastRequest?: number;
  commit_sha?: string;
  date_maj_donnees?: string;
  version?: string;
}

export const reducerName = "version";

const initialState: state = {
  requestState: "idle",
};
const slice = createSlice({
  name: reducerName,
  initialState,
  reducers: {
    resetState: () => {
      return initialState;
    },
    setIsLoading: (state) => {
      state.requestState = "loading";
    },
    setIsError: (state, { payload }: PayloadAction<SetResponseDataType>) => {
      state.requestState = "error";
      state.lastRequest = payload.lastRequest;
    },
    setResponseData: (
      state,
      { payload }: PayloadAction<SetResponseDataType>
    ) => {
      state.commit_sha = payload.commit_sha;
      state.date_maj_donnees = payload.date_maj_donnees;
      state.version = payload.version;
      state.lastRequest = payload.lastRequest;
      state.requestState = "loaded";
    },
  },
});

export const Actions = slice.actions;
export const Reducer = slice.reducer;

export default { reducerName, Reducer };
