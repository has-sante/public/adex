import type { AppThunk } from "Core/store/store.config";

import { Actions, reducerName } from "./index";

export const getVersionThunk = (): AppThunk => {
  return async (dispatch, getState, { getVersion }) => {
    const currentState = getState();
    // don't do anything if waiting for response
    if (currentState[reducerName].requestState === "loading") {
      return;
    }
    // wait a bit if there is an error
    if (currentState[reducerName].requestState === "loading") {
      return;
    }

    //  call the api only if query is new, otherwise do nothing
    const lastRequest = currentState[reducerName]?.lastRequest;
    const aDayInMilliseconds = 24 * 3600 * 1000;
    if (
      !lastRequest ||
      (lastRequest && Date.now() - lastRequest > aDayInMilliseconds)
    ) {
      dispatch(Actions.setIsLoading());
      try {
        const result = await getVersion();
        dispatch(
          Actions.setResponseData({
            commit_sha: result.commit_sha,
            version: result.version,
            date_maj_donnees: result.date_maj_donnees,
            lastRequest: Date.now(),
          })
        );
      } catch (error: any) {
        dispatch(Actions.setIsError({ lastRequest: Date.now() }));
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          // The request was made but no response was received
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log("Error", error.message);
        }
        console.log(error.config);
      }
    }
  };
};
