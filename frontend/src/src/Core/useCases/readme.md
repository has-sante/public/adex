# Adding a useCase

## structure
`index.ts` holds the reducer and actions created by `createSlice`
`thunks.ts` holds thunks chaining actions, useful for async/unpure operations like api calls

## other files to modify

- Add the reducer in `Core/store/rootReducer.ts`
