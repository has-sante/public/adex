import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import type { DirectoryEntityGroupType } from "Core/ports/secondary/entities/directoryEntity";

export type requestStateType = "loading" | "error" | "loaded" | "idle";

export type state = {
  requestState: requestStateType;
  lastRequest?: string | null;
  directoryResults?: DirectoryEntityGroupType[];
};

interface SetResponseDataType {
  lastRequest?: string | null;
  directoryResults: DirectoryEntityGroupType[];
}

export const reducerName = "searchInDirectory";

const initialState: state = {
  requestState: "idle",
};
const slice = createSlice({
  name: reducerName,
  initialState,
  reducers: {
    resetState: () => {
      return initialState;
    },
    setIsLoading: (state) => {
      state.requestState = "loading";
    },
    setIsError: (state) => {
      state.requestState = "error";
    },
    setResponseData: (
      state,
      { payload }: PayloadAction<SetResponseDataType>
    ) => {
      state.directoryResults = payload.directoryResults;
      state.lastRequest = payload.lastRequest;
      state.requestState = "loaded";
    },
  },
});

export const Actions = slice.actions;
export const Reducer = slice.reducer;

export default { reducerName, Reducer };
