import type { AppThunk } from "Core/store/store.config";
import type { DirectorySearchQueryType } from "Core/ports/primary/apis/directorySearch";
import { Actions, reducerName } from "./index";

export const searchDirectoryThunk = (
  query: DirectorySearchQueryType
): AppThunk => {
  return async (dispatch, getState, { searchDirectory }) => {
    const currentState = getState();
    // don't do anything if waiting for response
    if (currentState[reducerName].requestState === "loading") {
      return;
    }
    // don't do anything if empty request && state is idle
    if (
      query.query === "" &&
      currentState[reducerName].requestState === "idle"
    ) {
      return;
    }
    // reset state if query is empty whatever the state
    if (
      query.query === "" &&
      !(currentState[reducerName].requestState === "idle")
    ) {
      dispatch(Actions.resetState());
      return;
    }
    //  call the api only if query is new, otherwise do nothing
    if (currentState[reducerName].lastRequest !== query.query) {
      dispatch(Actions.setIsLoading());
      try {
        const result = await searchDirectory(query);
        dispatch(
          Actions.setResponseData({
            directoryResults: result,
            lastRequest: query.query,
          })
        );
      } catch (error: any) {
        dispatch(Actions.setIsError());
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          // The request was made but no response was received
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log("Error", error.message);
        }
        console.log(error.config);
      }
    }
  };
};
