import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { SyntheseListType } from "Core/ports/secondary/entities/synthese";

export interface GetDetailsFromDirectoryQueryType {
  identification_nationale_pp?: string;
}

export interface ISyntheseIsLoaded {
  synthese: SyntheseListType;
  requestState: "loaded";
}
export interface ILoadingStates {
  requestState: "loading" | "error" | "idle";
  synthese: undefined;
}

export type SyntheseStates = ISyntheseIsLoaded | ILoadingStates;

export const reducerName = "synthese";

const initialState: ILoadingStates = {
  requestState: "idle",
  synthese: undefined,
};
const slice = createSlice({
  name: reducerName,
  initialState: initialState as SyntheseStates,
  reducers: {
    setIsLoading: (state) => {
      state.requestState = "loading";
      state.synthese = undefined;
    },
    setIsError: (state) => {
      state.requestState = "error";
    },
    setResponseData: (state, { payload }: PayloadAction<SyntheseListType>) => {
      state.requestState = "loaded";
      state.synthese = payload;
    },
  },
});

export const Actions = slice.actions;
export const Reducer = slice.reducer;

export default { reducerName, Actions, Reducer };
