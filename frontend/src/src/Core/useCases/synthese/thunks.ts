import { SyntheseQueryType } from "Core/ports/primary/apis/getSynthese";
import type { AppThunk } from "Core/store/store.config";
import { Actions } from "./index";

export const getSyntheseThunk = ({ hashes }: SyntheseQueryType): AppThunk => {
  return async (dispatch, getState, { getSynthese }) => {
    if (!hashes) {
      return;
    } else {
      dispatch(Actions.setIsLoading());

      try {
        const result = await getSynthese({ hashes });
        dispatch(Actions.setResponseData(result));
      } catch (error: any) {
        dispatch(Actions.setIsError());
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          // The request was made but no response was received
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log("Error", error.message);
        }
        console.log(error.config);
      }
    }
  };
};
