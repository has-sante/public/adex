import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import type { BeneficiaryGroupType } from "Core/ports/secondary/entities/beneficiary";
import type { IBeneficiariesFromTSResults } from "Core/ports/primary/apis/listBeneficiariesFromTSById";
export interface BeneficiarySearchQueryType {
  query?: string | null;
  page_current?: number;
  page_size?: number;
}

export type state = {
  currentQuery: "";
  nextPageNumber: number;
  pageSize: number;
  hasNextPage: boolean;
  isNextPageLoading: boolean;
  isError: boolean;
  beneficiaryItems?: BeneficiaryGroupType[];
  lastQuery?: string | null;
};

interface ISetResponseData {
  response: IBeneficiariesFromTSResults;
}

interface ISetIsLoading {
  lastQuery?: string | null;
  resetState: boolean;
}

export const reducerName = "beneficiariesFromTS";

const initialState: state = {
  currentQuery: "",
  nextPageNumber: 0,
  pageSize: 30,
  hasNextPage: true,
  isNextPageLoading: false,
  isError: false,
};
const slice = createSlice({
  name: reducerName,
  initialState,
  reducers: {
    resetState: () => initialState,
    setIsLoading: (state, { payload }: PayloadAction<ISetIsLoading>) => {
      state.isError = false;
      state.isNextPageLoading = true;
      state.lastQuery = payload.lastQuery;
      if (payload.resetState) {
        state.beneficiaryItems = undefined;
        state.nextPageNumber = 0;
      }
    },
    setIsError: (state) => {
      state.isError = true;
      state.isNextPageLoading = false;
    },
    setResponseData: (state, { payload }: PayloadAction<ISetResponseData>) => {
      const newItems = state.beneficiaryItems
        ? state.beneficiaryItems.concat(payload.response.data)
        : payload.response.data;
      if (newItems.length >= payload.response.total) {
        state.hasNextPage = false;
      }
      state.beneficiaryItems = newItems;
      state.nextPageNumber += 1;
      state.isNextPageLoading = false;
    },
  },
});

export const Actions = slice.actions;
export const Reducer = slice.reducer;

export default { reducerName, Reducer };
