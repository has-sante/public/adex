import type { AppThunk } from "Core/store/store.config";
import { Actions, reducerName } from "./index";

const QUERY_BY_ID = "QUERY_BY_ID";
const QUERY_BY_SEARCH = "QUERY_BY_SEARCH";

export type listBeneficiaryFromTSQueryType = {
  identification_nationale_pp?: string;
  search_query?: string;
};
export const listBeneficiariesFromTSThunk = (
  query: listBeneficiaryFromTSQueryType
): AppThunk => {
  return async (
    dispatch,
    getState,
    { listBeneficiariesFromTSById, listBeneficiariesFromTSBySearch }
  ) => {
    const currentState = getState();
    if (currentState[reducerName].isNextPageLoading) {
      return;
    }

    const queryType = query.identification_nationale_pp
      ? QUERY_BY_ID
      : QUERY_BY_SEARCH;
    const queryPayload =
      query.identification_nationale_pp || query.search_query;

    let resetState = true;
    let page_current = 0;
    if (currentState[reducerName].lastQuery === queryPayload) {
      if (!currentState[reducerName].hasNextPage) {
        return;
      }
      resetState = false;
      page_current = currentState.beneficiariesFromTS.nextPageNumber;
    }
    dispatch(Actions.setIsLoading({ resetState, lastQuery: queryPayload }));
    try {
      const apiQuery = {
        query: queryPayload,
        page_current: page_current,
        page_size: currentState.beneficiariesFromTS.pageSize,
      };
      let result;
      switch (queryType) {
        case QUERY_BY_ID:
          result = await listBeneficiariesFromTSById(apiQuery);
          break;
        case QUERY_BY_SEARCH:
          result = await listBeneficiariesFromTSBySearch(apiQuery);
          break;
        default:
          throw new Error("unable to define query type");
      }
      dispatch(
        Actions.setResponseData({
          response: result,
        })
      );
    } catch (error: any) {
      dispatch(Actions.setIsError());
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      } else if (error.request) {
        // The request was made but no response was received
        console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log("Error", error.message);
      }
      console.log(error.config);
    }
  };
};
