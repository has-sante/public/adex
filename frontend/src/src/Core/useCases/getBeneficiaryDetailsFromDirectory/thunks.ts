import type { AppThunk } from "Core/store/store.config";
import type { GetDetailsFromDirectoryQueryType } from "Core/ports/primary/apis/getDetailsFromDirectory";

import { Actions } from "./index";

export const getBeneficiaryDetailsFromDirectoryThunk = (
  query: GetDetailsFromDirectoryQueryType
): AppThunk => {
  return async (dispatch, getState, { getDetailsFromDirectory }) => {
    if (!query.identification_nationale_pp) {
      dispatch(Actions.resetState());
    } else {
      dispatch(Actions.setIsLoading());
      try {
        const result = await getDetailsFromDirectory(query);
        dispatch(
          Actions.setResponseData({
            lastRequest: query.identification_nationale_pp,
            beneficiaryDetails: result,
          })
        );
      } catch (error: any) {
        dispatch(Actions.setIsError());
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          // The request was made but no response was received
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log("Error", error.message);
        }
        console.log(error.config);
      }
    }
  };
};
