import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import type { DirectoryEntityGroupType } from "Core/ports/secondary/entities/directoryEntity";

interface BeneficiaryDetailsLoaded {
  beneficiaryDetails: DirectoryEntityGroupType;
  requestState: "loaded";
  lastRequest?: string | null;
}
interface BeneficiaryOtherStates {
  requestState: "loading" | "error" | "idle";
  beneficiaryDetails: undefined;
  lastRequest?: string | null;
}

interface ISetResponseData {
  lastRequest: string | null;
  beneficiaryDetails: DirectoryEntityGroupType;
}

export type BeneficiaryFromDirectoryStates =
  | BeneficiaryDetailsLoaded
  | BeneficiaryOtherStates;

export const reducerName = "BeneficiaryFromDirectory";

const initialState: BeneficiaryFromDirectoryStates = {
  requestState: "idle",
  beneficiaryDetails: undefined,
  lastRequest: undefined,
};
const slice = createSlice({
  name: reducerName,
  initialState: initialState as BeneficiaryFromDirectoryStates,
  reducers: {
    resetState: () => {
      return initialState;
    },
    setIsLoading: (state) => {
      state.requestState = "loading";
    },
    setIsError: (state) => {
      state.requestState = "error";
    },
    setResponseData: (state, { payload }: PayloadAction<ISetResponseData>) => {
      state.requestState = "loaded";
      state.beneficiaryDetails = payload.beneficiaryDetails;
      state.lastRequest = payload.lastRequest;
    },
  },
});

export const Actions = slice.actions;
export const Reducer = slice.reducer;

export default { reducerName, Actions, Reducer };
