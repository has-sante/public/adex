export interface DeclarationSummary {
  entreprise: string;
  annee: number;
  avantages_count: number;
  avantages_montant: number;
  remunerations_count: number;
  remunerations_montant: number;
  conventions_count: number;
  conventions_montant: number;
}
