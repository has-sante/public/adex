export interface IVersion {
  commit_sha: string;
  date_maj_donnees: string;
  version: string;
}
