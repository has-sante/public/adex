type nullableString = string | null;

export type BeneficiaryType = {
  categorie_beneficiaire_libelle: string;
  prenom: nullableString;
  profession: nullableString;
  pays_code: nullableString;
  pays: nullableString;
  type_identifiant: string;
  numero_identifiant: nullableString;
  structure_exercice: nullableString;
  identite_nom: string;
  code_postal: nullableString;
  adresse: nullableString;
  ville: nullableString;
  identite_nom_normalized: string;
  prenom_normalized: nullableString;
  benef_hash: string;
};

export type BeneficiaryGroupType = BeneficiaryType & {
  group_size: number;
  score: number;
  identification_nationale_pp: string;
  children?: Array<BeneficiaryType>;
};

export type BeneficiariesArrayType = Array<BeneficiaryGroupType>;
