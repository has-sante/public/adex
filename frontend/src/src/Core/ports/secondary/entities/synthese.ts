import type {
  RowConvention,
  RowRemuneration,
  RowAvantage,
} from "Core/ports/secondary/entities/declarations";

export interface SyntheseDeclaration {
  date: string;
  montant?: number | null;
}

export interface SyntheseRemuneration extends SyntheseDeclaration {
  source: RowRemuneration;
}
export interface SyntheseAvantage extends SyntheseDeclaration {
  source: RowAvantage;
}

export interface SyntheseConvention extends SyntheseDeclaration {
  source: RowConvention;
  remunerations: Array<SyntheseRemuneration> | [];
  avantages: Array<SyntheseAvantage> | [];
}

export interface Montants {
  montants_avantages: number | null;
  montants_remunerations: number | null;
  montant_convention: number | null;
  max_montant: number | null;
  min_montant: number | null;
}

export type RowSyntheseDeclarationsBase = {
  benef_hash: string;
  entreprise: string;
  annee: string;
  annee_debut: string;
  annee_fin: string;
  montants: Montants;
};

export interface RowSyntheseDeclarationsConvention
  extends RowSyntheseDeclarationsBase {
  declaration_type: "convention";
  declaration: SyntheseConvention;
}

export interface RowSyntheseDeclarationsRemuneration
  extends RowSyntheseDeclarationsBase {
  declaration_type: "remuneration";
  declaration: SyntheseRemuneration;
}
export interface RowSyntheseDeclarationsAvantage
  extends RowSyntheseDeclarationsBase {
  declaration_type: "avantage";
  declaration: SyntheseAvantage;
}

export type RowSyntheseDeclarations =
  | RowSyntheseDeclarationsConvention
  | RowSyntheseDeclarationsAvantage
  | RowSyntheseDeclarationsRemuneration;

export type SyntheseListType = Array<RowSyntheseDeclarations>;
