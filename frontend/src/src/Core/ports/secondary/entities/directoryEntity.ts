export type DirectoryEntityGroupType = {
  score: number;
  identification_nationale_pp: string;
  identification: ProfessionalIdentification;
  lieux_activite: Array<ProfessionalActivityPlace>;
  noms_complets_exercice: Array<ProfessionalName>;
  professions: Array<string>;
  specialites: Array<string>;
  children: Array<DirectoryEntityType>;
};

export type ProfessionalIdentification = {
  code_type_identifiant_pp: string;
  libelle_type_identifiant_pp: string;
  identifiant_pp: string;
  identification_nationale_pp: string;
};

export type ProfessionalActivityPlace = {
  adresse_complete: string;
  numero_voie_coord_structure: string;
  indice_repetition_voie_coord_structure: string;
  libelle_type_voie_coord_structure: string;
  libelle_voie_coord_structure: string;
  code_postal_coord_structure: string;
  libelle_commune_coord_structure: string;
  libelle_pays_coord_structure: string;
  libelle_department_structure: string;
  raison_sociale_site: string;
};

export type ProfessionalName = {
  libelle_civilite: string;
  libelle_civilite_exercice: string;
  nom_exercice: string;
  prenom_exercice: string;
};

export type DirectoryEntityType = {
  address: string;
  type_identifiant_pp: string;
  identifiant_pp: string;
  identification_nationale_pp: string;
  code_civilite_exercice: string;
  libelle_civilite_exercice: string;
  code_civilité: string;
  libelle_civilite: string;
  nom_exercice: string;
  prenom_exercice: string;
  code_profession: string;
  libelle_profession: string;
  code_categorie_professionnelle: string;
  libelle_categorie_professionnelle: string;
  code_type_savoir_faire: string;
  libelle_type_savoir_faire: string;
  code_savoir_faire: string;
  libelle_savoir_faire: string;
  code_mode_exercice: string;
  libelle_mode_exercice: string;
  numero_siret_site: string;
  numero_siren_site: string;
  numero_finess_site: string;
  numero_finess_etablissement_juridique: string;
  raison_sociale_site: string;
  enseigne_commerciale_site: string;
  numero_voie_coord_structure: string;
  indice_repetition_voie_coord_structure: string;
  code_type_voie_coord_structure: string;
  libelle_type_voie_coord_structure: string;
  libelle_voie_coord_structure: string;
  bureau_cedex_coord_structure: string;
  code_postal_coord_structure: string;
  code_commune_coord_structure: string;
  libelle_commune_coord_structure: string;
  code_pays_coord_structure: string;
  libelle_pays_coord_structure: string;
  telephone_coord_structure: string;
  telephone_2_coord_structure: string;
  telecopie_coord_structure: string;
  adresse_email_coord_structure: string;
  code_departement_structure: string;
  libelle_department_structure: string;
  complement_destinataire_coord_structure: string;
  complement_point_geographique_coord_structure: string;
  mention_distribution_coord_structure: string;
  identifiant_technique_structure: string;
  ancien_identifiant_structure: string;
  autorite_enregistrement: string;
  code_secteur_activite: string;
  libelle_secteur_activite: string;
  code_section_tableau_pharmaciens: string;
  libelle_section_tableau_pharmaciens: string;
  score: number;
  libelle_type_identifiant: string;
};
