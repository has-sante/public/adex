type nullableString = string | null;

export const LIEN_INTERET = {
  CONVENTION: "convention",
  REMUNERATION: "remuneration",
  AVANTAGE: "avantage",
};

export type DeclarationType = {
  declaration_id: nullableString;
  entreprise_identifiant: nullableString;
  denomination_sociale: nullableString;
  identifiant_unique: nullableString;
  categorie_beneficiaire_code: nullableString;
  categorie_beneficiaire_libelle: nullableString;
  prenom: nullableString;
  profession: nullableString;
  pays_code: nullableString;
  pays: nullableString;
  type_identifiant: nullableString;
  numero_identifiant: nullableString;
  structure_exercice: nullableString;
  date: nullableString;
  autre_motif: nullableString;
  date_debut: nullableString;
  date_fin: nullableString;
  montant: number | null;
  identite_nom: nullableString;
  code_postal: nullableString;
  adresse: nullableString;
  ville: nullableString;
  information_convention: nullableString;
  identite_nom_normalized: nullableString;
  prenom_normalized: nullableString;
  benef_hash: nullableString;
  convention_description: nullableString;
  convention_id: nullableString;
};

export interface RowConvention extends DeclarationType {
  lien_interet: "convention";
  motif: nullableString;
}
export interface RowRemuneration extends DeclarationType {
  lien_interet: "remuneration";
  motif: null;
}
export interface RowAvantage extends DeclarationType {
  lien_interet: "avantage";
  motif: nullableString;
}
