import type { IVersion } from "Core/ports/secondary/entities/version";

export type GetVersionQueryType = {};

export interface CallGetVersion {
  (): Promise<IVersion>;
}
