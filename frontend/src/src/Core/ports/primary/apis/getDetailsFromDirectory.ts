import type { DirectoryEntityGroupType } from "../../secondary/entities/directoryEntity";

export interface GetDetailsFromDirectoryQueryType {
  identification_nationale_pp?: string | null;
}

export interface CallGetDetailsFromDirectory {
  (query: GetDetailsFromDirectoryQueryType): Promise<DirectoryEntityGroupType>;
}
