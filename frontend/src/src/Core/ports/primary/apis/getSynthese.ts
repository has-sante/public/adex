import { SyntheseListType } from "../../secondary/entities/synthese";

export interface SyntheseQueryType {
  hashes?: string[];
}

export interface CallGetSynthese {
  (query: SyntheseQueryType): Promise<SyntheseListType>;
}
