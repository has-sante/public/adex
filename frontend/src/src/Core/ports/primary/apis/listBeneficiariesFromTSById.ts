import type { BeneficiaryGroupType } from "Core/ports/secondary/entities/beneficiary";
export interface BeneficiarySearchQueryType {
  query?: string | null;
  page_current?: number;
  page_size?: number;
}

export interface IBeneficiariesFromTSResults {
  total: number;
  data: BeneficiaryGroupType[];
}
export interface CallListBeneficiariesFromTSById {
  (query: BeneficiarySearchQueryType): Promise<IBeneficiariesFromTSResults>;
}
