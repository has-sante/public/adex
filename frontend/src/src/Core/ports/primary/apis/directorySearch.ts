import type { DirectoryEntityGroupType } from "../../secondary/entities/directoryEntity";
export interface DirectorySearchQueryType {
  query?: string;
  page_current?: number;
  page_size?: number;
}

export interface CallDirectorySearch {
  (query: DirectorySearchQueryType): Promise<DirectoryEntityGroupType[]>;
}
