import type { CallGetDetailsFromDirectory } from "./apis/getDetailsFromDirectory";
import type { CallGetSynthese } from "./apis/getSynthese";
import type { CallGetVersion } from "./apis/getVersion";
import type { CallListBeneficiariesFromTSById } from "./apis/listBeneficiariesFromTSById";
import type { CallListBeneficiariesFromTSBySearch } from "./apis/listBeneficiariesFromTSBySearch";
import type { CallDirectorySearch } from "./apis/directorySearch";

export type GatewaysType = {
  getDetailsFromDirectory: CallGetDetailsFromDirectory;
  getSynthese: CallGetSynthese;
  getVersion: CallGetVersion;
  listBeneficiariesFromTSById: CallListBeneficiariesFromTSById;
  listBeneficiariesFromTSBySearch: CallListBeneficiariesFromTSBySearch;
  searchDirectory: CallDirectorySearch;
};
