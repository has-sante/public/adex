import { combineReducers } from "@reduxjs/toolkit";

import getBeneficiaryDetailsFromDirectory from "../useCases/getBeneficiaryDetailsFromDirectory";
import searchInDirectory from "../useCases/searchInDirectory";
import listBeneficiaryFromTS from "../useCases/listBeneficiaryFromTS";
import synthese from "../useCases/synthese";
import getVersion from "../useCases/getVersion";

export const rootReducer = combineReducers({
  getBeneficiaryDetailsFromDirectory:
    getBeneficiaryDetailsFromDirectory.Reducer,
  searchInDirectory: searchInDirectory.Reducer,
  beneficiariesFromTS: listBeneficiaryFromTS.Reducer,
  synthese: synthese.Reducer,
  version: getVersion.Reducer,
});

export type RootState = ReturnType<typeof rootReducer>;
