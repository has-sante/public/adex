import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { rootReducer } from "./rootReducer";

import type { Action, ThunkAction } from "@reduxjs/toolkit";
import type { RootState } from "./rootReducer";

import type { GatewaysType } from "../ports/primary";

export type Dependencies = GatewaysType;

export const configureReduxStore = (dependencies: Dependencies) =>
  configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware({
      thunk: {
        extraArgument: dependencies,
      },
    }),
  });

export type ReduxStore = ReturnType<typeof configureReduxStore>;
export type AppDispatch = ReduxStore["dispatch"];

export type AppThunk = ThunkAction<
  Promise<void>,
  RootState,
  Dependencies,
  Action<string>
>;
