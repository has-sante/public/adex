import type { StrictModifiers, Modifier } from "@popperjs/core";

type CustomModifier = Modifier<"sameWidth", {}>;
export type ExtendedModifiers = StrictModifiers | Partial<CustomModifier>;

export const sameWidth: CustomModifier = {
  name: "sameWidth",
  enabled: true,
  phase: "beforeWrite",
  requires: ["computeStyles"],
  fn: ({ state }) => {
    state.styles.popper.width = `${state.rects.reference.width}px`;
  },
  effect: ({ state }) => {
    if (state.elements.reference instanceof HTMLElement) {
      state.elements.popper.style.width = `${state.elements.reference.offsetWidth}px`;
    }
  },
};
