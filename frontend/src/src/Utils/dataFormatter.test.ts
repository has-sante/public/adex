import { capitalize } from "./dataFormatter";

describe("Capitalize string", () => {
  it("returns empty if string is undefined or emtpy", () => {
    expect(capitalize("")).toEqual("");
    expect(capitalize()).toEqual("");
    expect(capitalize(null)).toEqual("");
  });
  it("Properly capitalize strings", () => {
    expect(capitalize("Paul")).toEqual("Paul");
    expect(capitalize("PAUL")).toEqual("Paul");
    expect(capitalize("pAUL")).toEqual("Paul");
    expect(capitalize("paul")).toEqual("Paul");
  });
  it("doesn't break with non alpha chars", () => {
    expect(capitalize("0Paul")).toEqual("0paul");
    expect(capitalize("P0AUL")).toEqual("P0aul");
    expect(capitalize(":pAUL")).toEqual(":paul");
    expect(capitalize(" paul")).toEqual(" Paul");
  });
  it("works with multi words", () => {
    expect(capitalize("PAUL AUSTER")).toEqual("Paul Auster");
    expect(capitalize("PAUL    AUSTER")).toEqual("Paul    Auster");
    expect(capitalize("paul auster")).toEqual("Paul Auster");
  });
  it("works with multiline", () => {
    const source = `paul
      auster
      moon palace`;
    const expected = `Paul
      Auster
      Moon Palace`;

    expect(capitalize(source)).toEqual(expected);
  });
});
