import _ from "lodash";

export const toggleSelectionInHashes = (
  hashes: string[],
  selection: string[],
  shouldSelect: boolean
) => {
  return shouldSelect
    ? _.union(hashes, selection)
    : _.without(hashes, ...selection);
};
