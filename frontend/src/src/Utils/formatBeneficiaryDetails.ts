import type { BeneficiaryType } from "Core/ports/secondary/entities/beneficiary";

import { capitalize } from "./dataFormatter";

export const formatBeneficiaryName = (beneficiary: BeneficiaryType) => {
  if (beneficiary.identite_nom || beneficiary.prenom) {
    // we assume that if name is
    return `${beneficiary.identite_nom.toUpperCase() ?? ""} ${
      capitalize(beneficiary.prenom) ?? ""
    }`;
  }
  return `${beneficiary.identite_nom}`;
};

export const formatBeneficiaryAddress = (beneficiary: BeneficiaryType) => {
  return capitalize(
    `${beneficiary.adresse ?? ""} ${beneficiary.code_postal ?? ""} ${
      beneficiary.ville ?? ""
    }`
  );
};
