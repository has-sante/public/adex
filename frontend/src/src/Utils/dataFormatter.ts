export const capitalize = (str: null | string = "") => {
  return str
    ? str
        .split(" ")
        .map((s: string) => {
          return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase();
        })
        .join(" ")
    : "";
};

export const capitalizeWithSuffix = (str = "", suffix = " ") => {
  return capitalize(str) + suffix;
};
export const formatNumberInEuros = ({
  value,
}: {
  value: number | null | undefined;
}) => {
  if (!value) {
    return "-";
  }
  return `${new Intl.NumberFormat("fr-FR", {
    style: "currency",
    currency: "EUR",
    minimumFractionDigits: 0,
  }).format(value)}`;
};
