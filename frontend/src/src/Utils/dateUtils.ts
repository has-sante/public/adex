const dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/;

export function apiDateParser(dateString: string | undefined) {
  if (typeof dateString === "string" && dateFormat.test(dateString)) {
    return new Date(dateString);
  }

  return null;
}

export const parseDate = (dateString: string | null): null | Date => {
  if (!dateString) {
    return null;
  }
  const [day, month, year] = dateString.split("/");
  if (
    isNaN(parseInt(year)) ||
    !(parseInt(month) <= 12) ||
    !(parseInt(day) <= 31)
  ) {
    return null;
  }

  return new Date(parseInt(year), parseInt(month) - 1, parseInt(day));
};

/**
 * Return interval between dates in ms
 * @param a string - date DD/MM/YYYY
 * @param b string - date DD/MM/YYYY
 */
export const dateInterval = (a: string | null, b: string | null): number => {
  const aDate = parseDate(a);
  const bDate = parseDate(b);

  if (!aDate && !bDate) {
    return 0;
  }
  if (!aDate) {
    return Infinity;
  }
  if (!bDate) {
    return -Infinity;
  }
  if (!aDate || !bDate) return 0;
  return aDate.valueOf() - bDate.valueOf();
};
