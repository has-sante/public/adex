import { dateInterval, parseDate } from "./dateUtils";

describe("Parse date", () => {
  it("Parses a date written as DD/MM/YYYY", () => {
    const stringDate = "25/12/2020";
    const parsedDate = new Date("2020-12-25T00:00");
    expect(parseDate(stringDate)).toEqual(parsedDate);
  });
  it("return null if param is null, empty or undefined", () => {
    const emptyString = "";
    const nullString = null;
    expect(parseDate(emptyString)).toEqual(null);
    expect(parseDate(nullString)).toEqual(null);
  });
});

describe("compare dates", () => {
  it("dateInterval returns value < 0 if first param is before second param", () => {
    const dateA = "15/01/2015";
    const dateB = "18/01/2015";
    expect(dateInterval(dateA, dateB)).toBeLessThan(0);
  });
  it("dateInterval returns -1 if first param is after second param", () => {
    const dateA = "15/01/2015";
    const dateB = "18/01/2015";
    expect(dateInterval(dateB, dateA)).toBeGreaterThanOrEqual(0);
  });
  it("dateInterval returns 0 if dates are equal", () => {
    const dateA = "15/01/2015";
    expect(dateInterval(dateA, dateA)).toEqual(0);
  });
  it("returns -infinity when second param is null or not parseable", () => {
    const dateA = "15/01/2015";
    const dateB = "notparseable";
    expect(dateInterval(dateA, dateB)).toBeLessThan(0);
    expect(dateInterval(dateA, dateB)).toEqual(-Infinity);

    expect(dateInterval(dateA, null)).toBeLessThan(0);
    expect(dateInterval(dateA, null)).toEqual(-Infinity);
  });
  it("returns infinity when first param is null/not parseable and second param is a date", () => {
    const dateA = "notparseable";
    const dateB = "15/01/2015";
    expect(dateInterval(dateA, dateB)).toBeGreaterThanOrEqual(0);
    expect(dateInterval(dateA, dateB)).toEqual(Infinity);

    expect(dateInterval(null, dateB)).toBeGreaterThanOrEqual(0);
    expect(dateInterval(null, dateB)).toEqual(Infinity);
  });
  it("returns 0 when params are null or not parseable", () => {
    const dateNotParseable = "notparseable";
    const dateNull = null;
    expect(dateInterval(dateNotParseable, dateNotParseable)).toEqual(0);
    expect(dateInterval(dateNull, dateNotParseable)).toEqual(0);
    expect(dateInterval(dateNotParseable, dateNull)).toEqual(0);
    expect(dateInterval(dateNull, dateNull)).toEqual(0);
  });
});
