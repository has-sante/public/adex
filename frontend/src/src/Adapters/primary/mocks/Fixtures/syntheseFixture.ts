import type { SyntheseListType } from "Core/ports/secondary/entities/synthese";

export const synthese: SyntheseListType = [
  {
    benef_hash: "436c4bed8392",
    entreprise: "AMGEN SAS",
    annee: "2015",
    annee_debut: "2015",
    annee_fin: "2015",
    montants: {
      montants_avantages: 359,
      montants_remunerations: null,
      montant_convention: null,
      max_montant: 359,
      min_montant: 359,
    },
    declaration_type: "avantage",
    declaration: {
      date: "2015-06-12",
      montant: 359,
      source: {
        declaration_id: "avantage | INJMXEAE | AMG-S22015-10976",
        entreprise_identifiant: "INJMXEAE",
        denomination_sociale: "AMGEN SAS",
        identifiant_unique: "AMG-S22015-10976",
        categorie_beneficiaire_code: "PRS",
        categorie_beneficiaire_libelle: "Professionnel de santé",
        prenom: "JEAN MICHEL",
        profession: "Médecin",
        pays_code: "[FR]",
        pays: "FRANCE",
        type_identifiant: "RPPS",
        numero_identifiant: "10003169033",
        structure_exercice: null,
        date: "2015-06-12",
        motif: "AUTRE",
        autre_motif: "TRANSPORT",
        date_debut: null,
        date_fin: null,
        montant: 359,
        identite_nom: "BONS",
        code_postal: "03100",
        adresse:
          "CTRE RADIOTHERAPIE JOSEPH RELOT CTRE RADIOTHERAPIE JOSEPH RELOT 7 AVENUE PIERRE TROUBAT",
        ville: "MONTLUCON",
        information_convention: null,
        identite_nom_normalized: "BONS",
        prenom_normalized: "Jean Michel",
        benef_hash: "436c4bed8392",
        convention_description: null,
        convention_id: null,
        lien_interet: "avantage",
      },
    },
  },
  {
    benef_hash: "01ab266ebad9",
    entreprise: "BRACCO IMAGING FRANCE",
    annee: "2014",
    annee_debut: "2014",
    annee_fin: "2014",
    montants: {
      montants_avantages: 359,
      montants_remunerations: null,
      montant_convention: null,
      max_montant: 359,
      min_montant: 359,
    },
    declaration_type: "avantage",
    declaration: {
      date: "2014-04-28",
      montant: 359,
      source: {
        declaration_id: "avantage | QCUOYUXO | 5100000949",
        entreprise_identifiant: "QCUOYUXO",
        denomination_sociale: "BRACCO IMAGING FRANCE",
        identifiant_unique: "5100000949",
        categorie_beneficiaire_code: "PRS",
        categorie_beneficiaire_libelle: "Professionnel de santé",
        prenom: "DOROTHEE",
        profession: "Manipulateur d’électroradiologie médicale",
        pays_code: "[FR]",
        pays: "FRANCE",
        type_identifiant: "AUTRE",
        numero_identifiant: "",
        structure_exercice: null,
        date: "2014-04-28",
        motif: "AUTRE",
        autre_motif:
          "HOSPITALITE (Description convention liée : AFPPE 28EME JOURNEE FRANCOPHONE IRM)",
        date_debut: null,
        date_fin: null,
        montant: 359,
        identite_nom: "FIGUET",
        code_postal: "27207",
        adresse: "HOPITAL EURE/SEINE VERNON",
        ville: "VERNON",
        information_convention: null,
        identite_nom_normalized: "FIGUET",
        prenom_normalized: "Dorothee",
        benef_hash: "01ab266ebad9",
        convention_description: "AFPPE 28EME JOURNEE FRANCOPHONE IRM",
        convention_id: null,
        lien_interet: "avantage",
      },
    },
  },
  {
    benef_hash: "bdbf4b2b8b5a",
    entreprise: "Bayer HealthCare SAS",
    annee: "2017",
    annee_debut: "2017",
    annee_fin: "2017",
    montants: {
      montants_avantages: 359,
      montants_remunerations: null,
      montant_convention: null,
      max_montant: 359,
      min_montant: 359,
    },
    declaration_type: "avantage",
    declaration: {
      date: "2017-11-14",
      montant: 359,
      source: {
        declaration_id:
          "avantage | SOWWIAEU | A_BHCFRA48240000000DNVAAQP_02111710381009_HE",
        entreprise_identifiant: "SOWWIAEU",
        denomination_sociale: "Bayer HealthCare SAS",
        identifiant_unique: "A_BHCFRA48240000000DNVAAQP_02111710381009_HE",
        categorie_beneficiaire_code: "PRS",
        categorie_beneficiaire_libelle: "Professionnel de santé",
        prenom: "HERVE",
        profession: "Médecin",
        pays_code: "[FR]",
        pays: "FRANCE",
        type_identifiant: "RPPS",
        numero_identifiant: "10002558079",
        structure_exercice: null,
        date: "2017-11-14",
        motif: "AUTRE",
        autre_motif:
          "HEBERGEMENT (Description convention liée : C_BHCFRA48240000000DNVAAQ_HOS)",
        date_debut: null,
        date_fin: null,
        montant: 359,
        identite_nom: "JOUAN",
        code_postal: "72000",
        adresse: "RADIOLOGIE ETOILE DES JACOBINS 7 AVENUE PIERRE MENDES FRANCE",
        ville: "LE MANS",
        information_convention: null,
        identite_nom_normalized: "JOUAN",
        prenom_normalized: "Herve",
        benef_hash: "bdbf4b2b8b5a",
        convention_description: "C_BHCFRA48240000000DNVAAQ_HOS",
        convention_id: null,
        lien_interet: "avantage",
      },
    },
  },
  {
    benef_hash: "5f9a9c9d6c4f",
    entreprise: "FCI SAS",
    annee: "2017",
    annee_debut: "2017",
    annee_fin: "2017",
    montants: {
      montants_avantages: 39,
      montants_remunerations: null,
      montant_convention: null,
      max_montant: 39,
      min_montant: 39,
    },
    declaration_type: "avantage",
    declaration: {
      date: "2017-03-21",
      montant: 39,
      source: {
        declaration_id: "avantage | OWOIVPOC | 5222",
        entreprise_identifiant: "OWOIVPOC",
        denomination_sociale: "FCI SAS",
        identifiant_unique: "5222",
        categorie_beneficiaire_code: "PRS",
        categorie_beneficiaire_libelle: "Professionnel de santé",
        prenom: "STEPHAN",
        profession: "Médecin",
        pays_code: "[FR]",
        pays: "FRANCE",
        type_identifiant: "RPPS",
        numero_identifiant: "10003948352",
        structure_exercice: null,
        date: "2017-03-21",
        motif: "AUTRE",
        autre_motif:
          "REPAS (Description convention liée : FCI SCHOOL MARSEILLE)",
        date_debut: null,
        date_fin: null,
        montant: 39,
        identite_nom: "FAUQUIER",
        code_postal: "13008",
        adresse: "CLINIQUE JUGE 116 RUE JEAN MERMOZ",
        ville: "MARSEILLE--8E--ARRONDISSEMENT",
        information_convention: null,
        identite_nom_normalized: "FAUQUIER",
        prenom_normalized: "Stephan",
        benef_hash: "5f9a9c9d6c4f",
        convention_description: "FCI SCHOOL MARSEILLE",
        convention_id: null,
        lien_interet: "avantage",
      },
    },
  },
  {
    benef_hash: "33923652c49b",
    entreprise: "FERRING SAS",
    annee: "2015",
    annee_debut: "2015",
    annee_fin: "2015",
    montants: {
      montants_avantages: 359,
      montants_remunerations: null,
      montant_convention: null,
      max_montant: 359,
      min_montant: 359,
    },
    declaration_type: "avantage",
    declaration: {
      date: "2015-05-31",
      montant: 359,
      source: {
        declaration_id: "avantage | WUPXLSRM | CO-2015-0129_PS-0193028_1685276",
        entreprise_identifiant: "WUPXLSRM",
        denomination_sociale: "FERRING SAS",
        identifiant_unique: "CO-2015-0129_PS-0193028_1685276",
        categorie_beneficiaire_code: "PRS",
        categorie_beneficiaire_libelle: "Professionnel de santé",
        prenom: "PAUL",
        profession: "Médecin",
        pays_code: "[FR]",
        pays: "FRANCE",
        type_identifiant: "RPPS",
        numero_identifiant: "10100284586",
        structure_exercice: null,
        date: "2015-05-31",
        motif: "AUTRE",
        autre_motif:
          "Transport (Description convention liée : CO-2015-0129_PS-0193028)",
        date_debut: null,
        date_fin: null,
        montant: 359,
        identite_nom: "SARGOS",
        code_postal: "33076",
        adresse: "229 COURS DE L ARGONNE",
        ville: "BORDEAUX CEDEX",
        information_convention: null,
        identite_nom_normalized: "SARGOS",
        prenom_normalized: "Paul",
        benef_hash: "33923652c49b",
        convention_description: "CO-2015-0129_PS-0193028",
        convention_id: null,
        lien_interet: "avantage",
      },
    },
  },
  {
    benef_hash: "7c9568142728",
    entreprise: "GILEAD SCIENCES",
    annee: "2017",
    annee_debut: "2017",
    annee_fin: "2017",
    montants: {
      montants_avantages: 359,
      montants_remunerations: null,
      montant_convention: null,
      max_montant: 359,
      min_montant: 359,
    },
    declaration_type: "avantage",
    declaration: {
      date: "2017-06-21",
      montant: 359,
      source: {
        declaration_id: "avantage | MYXUTQWV | BHO201706538_A_478793",
        entreprise_identifiant: "MYXUTQWV",
        denomination_sociale: "GILEAD SCIENCES",
        identifiant_unique: "BHO201706538_A_478793",
        categorie_beneficiaire_code: "PRS",
        categorie_beneficiaire_libelle: "Professionnel de santé",
        prenom: "THIERRY",
        profession: "Médecin",
        pays_code: "[FR]",
        pays: "FRANCE",
        type_identifiant: "RPPS",
        numero_identifiant: "10002818580",
        structure_exercice: null,
        date: "2017-06-21",
        motif: "AUTRE",
        autre_motif: "HEBERGEMENT",
        date_debut: null,
        date_fin: null,
        montant: 359,
        identite_nom: "PISTONE",
        code_postal: "33075",
        adresse:
          "1 RUE JEAN BURGUET UNITES 20 21 ET 28 HOPITAL SAINT ANDRE - MEDECINE INTERNE INFECTIOLOGIE",
        ville: "BORDEAUX CEDEX",
        information_convention: null,
        identite_nom_normalized: "PISTONE",
        prenom_normalized: "Thierry",
        benef_hash: "7c9568142728",
        convention_description: null,
        convention_id: null,
        lien_interet: "avantage",
      },
    },
  },
  {
    benef_hash: "4672525dced4",
    entreprise: "GILEAD SCIENCES",
    annee: "2019",
    annee_debut: "2019",
    annee_fin: "2019",
    montants: {
      montants_avantages: 0,
      montants_remunerations: 0,
      montant_convention: 359,
      max_montant: 359,
      min_montant: 0,
    },
    declaration_type: "convention",
    declaration: {
      date: "2019-06-18",
      montant: 359,
      remunerations: [],
      avantages: [],
      source: {
        declaration_id: "convention | MYXUTQWV | HCP201900630_C_1961688",
        entreprise_identifiant: "MYXUTQWV",
        denomination_sociale: "GILEAD SCIENCES",
        identifiant_unique: "HCP201900630_C_1961688",
        categorie_beneficiaire_code: "PRS",
        categorie_beneficiaire_libelle: "Professionnel de santé",
        prenom: "EMMANUEL",
        profession: "Médecin",
        pays_code: "[FR]",
        pays: "FRANCE",
        type_identifiant: "RPPS",
        numero_identifiant: "10001970481",
        structure_exercice: null,
        date: "2019-06-18",
        motif: "Contrat d'intervenant à une manifestation / orateur",
        autre_motif: null,
        date_debut: "2019-06-18",
        date_fin: null,
        montant: 359,
        identite_nom: "CHEVALIER",
        code_postal: "83056",
        adresse:
          "54 RUE HENRI SAINTE CLAIRE DEVILLE HOPITAL SAINTE MUSSE - CEGIDD",
        ville: "TOULON CEDEX",
        information_convention:
          "18/06/2019 TOULON (83) Reunion Professionnelle GILEAD SCIENCES S.A.S.",
        identite_nom_normalized: "CHEVALIER",
        prenom_normalized: "Emmanuel",
        benef_hash: "4672525dced4",
        convention_description: null,
        convention_id: "convention | MYXUTQWV | HCP201900630_C_1961688",
        lien_interet: "convention",
      },
    },
  },
  {
    benef_hash: "256b856bd1da",
    entreprise: "INTUITIVE SURGICAL SARL",
    annee: "2012",
    annee_debut: "2012",
    annee_fin: "2012",
    montants: {
      montants_avantages: 359,
      montants_remunerations: null,
      montant_convention: null,
      max_montant: 359,
      min_montant: 359,
    },
    declaration_type: "avantage",
    declaration: {
      date: "2012-05-21",
      montant: 359,
      source: {
        declaration_id: "avantage | POCLSRGW | IS0102-2022",
        entreprise_identifiant: "POCLSRGW",
        denomination_sociale: "INTUITIVE SURGICAL SARL",
        identifiant_unique: "IS0102-2022",
        categorie_beneficiaire_code: "PRS",
        categorie_beneficiaire_libelle: "Professionnel de santé",
        prenom: "ERIC",
        profession: "Médecin",
        pays_code: "[FR]",
        pays: "FRANCE",
        type_identifiant: "RPPS",
        numero_identifiant: "10003937116",
        structure_exercice: null,
        date: "2012-05-21",
        motif: "AUTRE",
        autre_motif: "TRANSPORT",
        date_debut: null,
        date_fin: null,
        montant: 359,
        identite_nom: "BARRET",
        code_postal: "75674",
        adresse: "42 BOULEVARD JOURDAN",
        ville: "PARIS",
        information_convention: null,
        identite_nom_normalized: "BARRET",
        prenom_normalized: "Eric",
        benef_hash: "256b856bd1da",
        convention_description: null,
        convention_id: null,
        lien_interet: "avantage",
      },
    },
  },
  {
    benef_hash: "d184ce2e1316",
    entreprise: "IPSEN PHARMA",
    annee: "2019",
    annee_debut: "2019",
    annee_fin: "2019",
    montants: {
      montants_avantages: 359,
      montants_remunerations: null,
      montant_convention: null,
      max_montant: 359,
      min_montant: 359,
    },
    declaration_type: "avantage",
    declaration: {
      date: "2019-06-06",
      montant: 359,
      source: {
        declaration_id:
          "avantage | WMQRNJKV | RPC-2019-0026_PS-0249116_557335_557336",
        entreprise_identifiant: "WMQRNJKV",
        denomination_sociale: "IPSEN PHARMA",
        identifiant_unique: "RPC-2019-0026_PS-0249116_557335_557336",
        categorie_beneficiaire_code: "PRS",
        categorie_beneficiaire_libelle: "Professionnel de santé",
        prenom: "VINCENT",
        profession: "Médecin",
        pays_code: "[FR]",
        pays: "FRANCE",
        type_identifiant: "RPPS",
        numero_identifiant: "10003051256",
        structure_exercice: null,
        date: "2019-06-06",
        motif: "AUTRE",
        autre_motif:
          "Transport (Description convention liée : RPC-2019-0026_PS-0249116)",
        date_debut: null,
        date_fin: null,
        montant: 359,
        identite_nom: "SERVAJEAN",
        code_postal: "69100",
        adresse: "35 RUE DU TONKIN",
        ville: "VILLEURBANNE",
        information_convention: null,
        identite_nom_normalized: "SERVAJEAN",
        prenom_normalized: "Vincent",
        benef_hash: "d184ce2e1316",
        convention_description: "RPC-2019-0026_PS-0249116",
        convention_id: null,
        lien_interet: "avantage",
      },
    },
  },
  {
    benef_hash: "5d4b434559c6",
    entreprise: "LABORATOIRES MAYOLY SPINDLER",
    annee: "2017",
    annee_debut: "2017",
    annee_fin: "2017",
    montants: {
      montants_avantages: 359,
      montants_remunerations: null,
      montant_convention: null,
      max_montant: 359,
      min_montant: 359,
    },
    declaration_type: "avantage",
    declaration: {
      date: "2017-07-01",
      montant: 359,
      source: {
        declaration_id: "avantage | QLBGWJTQ | LMSP_2017_1_F_PREST_426",
        entreprise_identifiant: "QLBGWJTQ",
        denomination_sociale: "LABORATOIRES MAYOLY SPINDLER",
        identifiant_unique: "LMSP_2017_1_F_PREST_426",
        categorie_beneficiaire_code: "PRS",
        categorie_beneficiaire_libelle: "Professionnel de santé",
        prenom: "Hakim",
        profession: "Médecin",
        pays_code: "[FR]",
        pays: "FRANCE",
        type_identifiant: "RPPS",
        numero_identifiant: "10001374270",
        structure_exercice: null,
        date: "2017-07-01",
        motif: "AUTRE",
        autre_motif: "TRANSPORT",
        date_debut: null,
        date_fin: null,
        montant: 359,
        identite_nom: "BECHEUR",
        code_postal: "75877",
        adresse: "46 rue Henri Huchard",
        ville: "PARIS cedex 18",
        information_convention: null,
        identite_nom_normalized: "BECHEUR",
        prenom_normalized: "Hakim",
        benef_hash: "5d4b434559c6",
        convention_description: null,
        convention_id: null,
        lien_interet: "avantage",
      },
    },
  },
];
