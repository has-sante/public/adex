import { synthese } from "./Fixtures/syntheseFixture";

import type { CallGetSynthese } from "Core/ports/primary/apis/getSynthese";

export const mockGetSynthese: CallGetSynthese = (hashes) => {
  console.log(hashes);
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(synthese);
    }, 1000);
  });
};
