import { BeneficiaryFromTSFixture } from "./Fixtures/beneficiaryFromTSFixture";

import type { CallListBeneficiariesFromTSById } from "Core/ports/primary/apis/listBeneficiariesFromTSById";
import type { CallListBeneficiariesFromTSBySearch } from "Core/ports/primary/apis/listBeneficiariesFromTSBySearch";

export const mockListBeneficiariesFromTS:
  | CallListBeneficiariesFromTSById
  | CallListBeneficiariesFromTSBySearch = ({
  query,
  page_current,
  page_size,
}) => {
  const offset = page_size && page_current ? page_size * page_current : 0;
  console.log("query", query, "page_current", page_current, "offset", offset);
  const response = {
    data: {
      total: BeneficiaryFromTSFixture.total,
      data: BeneficiaryFromTSFixture.data.slice(
        offset,
        offset + (page_size || 0)
      ),
    },
  };
  console.log(response);
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(response.data);
    }, 1000);
  });
};
