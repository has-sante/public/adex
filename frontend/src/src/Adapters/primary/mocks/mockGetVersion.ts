import type { CallGetVersion } from "Core/ports/primary/apis/getVersion";

export const mockGetVersion: CallGetVersion = () => {
  const response = {
    data: {
      commit_sha: "commitsha012",
      date_maj_donnees: "2016-04-26T18:09:16Z",
      version: "0.0.1",
    },
  };
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(response.data);
    }, 1000);
  });
};
