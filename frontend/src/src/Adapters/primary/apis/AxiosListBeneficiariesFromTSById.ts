import axios from "axios";
import { API_URL } from "./config";

import type {
  CallListBeneficiariesFromTSById,
  IBeneficiariesFromTSResults,
} from "Core/ports/primary/apis/listBeneficiariesFromTSById";

export const AxiosListBeneficiariesFromTSById: CallListBeneficiariesFromTSById =
  async ({ query = "", page_current = 0, page_size = 30 }) => {
    const result = await axios.post<IBeneficiariesFromTSResults>(
      `${API_URL}/api/v1/beneficiaires_ts/depuis_annuaire_ans`,
      {
        identification_nationale_pp: query,
        page_current,
        page_size,
      }
    );
    return result.data;
  };
