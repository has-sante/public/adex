import axios from "axios";

import type { CallGetVersion } from "Core/ports/primary/apis/getVersion";
import { IVersion } from "Core/ports/secondary/entities/version";

import { API_URL } from "./config";

export const AxiosGetVersion: CallGetVersion = async () => {
  const result = await axios.get<IVersion>(`${API_URL}/api/v1/info/info`);
  return result.data;
};
