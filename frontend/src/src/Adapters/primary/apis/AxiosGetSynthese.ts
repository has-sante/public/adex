import axios from "axios";

import type { CallGetSynthese } from "Core/ports/primary/apis/getSynthese";
import { SyntheseListType } from "Core/ports/secondary/entities/synthese";

import { API_URL } from "./config";

export const AxiosGetSynthese: CallGetSynthese = async ({ hashes }) => {
  const result = await axios.post<SyntheseListType>(
    `${API_URL}/api/v1/declaration_summaries/synthese/`,
    {
      hashes,
    }
  );
  return result.data;
};
