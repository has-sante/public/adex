import axios from "axios";

import type { CallGetDetailsFromDirectory } from "Core/ports/primary/apis/getDetailsFromDirectory";
import type { DirectoryEntityGroupType } from "Core/ports/secondary/entities/directoryEntity";

import { API_URL } from "./config";

export const AxiosCallGetDetailsFromDirectory: CallGetDetailsFromDirectory =
  async ({ identification_nationale_pp }) => {
    const result = await axios.get<DirectoryEntityGroupType>(
      `${API_URL}/api/v1/annuaire_ans/${identification_nationale_pp}`
    );
    return result.data;
  };
