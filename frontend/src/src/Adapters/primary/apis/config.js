export const API_URL = process.env.REACT_APP_API_URL;
export const basename = process.env.REACT_APP_BASENAME;

export const adexFrontendVersion = process.env.REACT_APP_ADEX_FRONTEND_VERSION;
export const ciCommitShortSha = process.env.REACT_APP_CI_COMMIT_SHORT_SHA;
