import axios from "axios";
import { CallDirectorySearch } from "Core/ports/primary/apis/directorySearch";
import { DirectoryEntityGroupType } from "Core/ports/secondary/entities/directoryEntity";
import { API_URL } from "./config";

interface IAxiosDirectorySearch {
  total: number;
  data: DirectoryEntityGroupType[];
}

export const AxiosDirectorySearch: CallDirectorySearch = async ({
  query = "",
  page_current = 0,
  page_size = 30,
}) => {
  const result = await axios.post<IAxiosDirectorySearch>(
    `${API_URL}/api/v1/annuaire_ans/recherche`,
    {
      query,
      page_current,
      page_size,
    }
  );
  return result.data.data;
};
