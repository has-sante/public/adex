import axios from "axios";
import { IBeneficiariesFromTSResults } from "Core/ports/primary/apis/listBeneficiariesFromTSBySearch";
import { CallListBeneficiariesFromTSBySearch } from "Core/ports/primary/apis/listBeneficiariesFromTSBySearch";
import { API_URL } from "./config";

export const AxiosListBeneficiariesFromTSBySearch: CallListBeneficiariesFromTSBySearch =
  async ({ query = "", page_current = 0, page_size = 30 }) => {
    const result = await axios.post<IBeneficiariesFromTSResults>(
      `${API_URL}/api/v1/beneficiaires_ts/recherche`,
      {
        query,
        page_current,
        page_size,
      }
    );
    return result.data;
  };
