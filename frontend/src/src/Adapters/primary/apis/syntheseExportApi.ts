import { API_URL } from "./config";

export interface ISyntheseExportAPI {
  hashesStringInURL: string | null;
  identification_nationale_pp: string | null;
}
export const syntheseExportAPI = ({
  hashesStringInURL,
  identification_nationale_pp,
}: ISyntheseExportAPI) => {
  if (!hashesStringInURL) {
    return "";
  }
  if (identification_nationale_pp) {
    return `${API_URL}/api/v1/declaration_summaries/synthese_export/?hashes=${hashesStringInURL}&identification_nationale_pp=${identification_nationale_pp}`;
  } else {
    return `${API_URL}/api/v1/declaration_summaries/synthese_export/?hashes=${hashesStringInURL}`;
  }
};
