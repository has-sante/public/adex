Toutes les évolutions notables de l'application ADEX sont listées ici.

Le code source du projet est accessible sur le dépôt gitlab: [https://gitlab.has-sante.fr/has-sante/public/adex](https://gitlab.has-sante.fr/has-sante/public/adex)

NB: Le format est inspiré de [Keep a Changelog](https://keepachangelog.com/fr/1.0.0/)
et ce projet adhère à la [Gestion Sémantique de Version](https://semver.org/lang/fr/spec/v2.0.0.html).

# [1.7.1] - 2025-01-17

## Changements

- Mise à jour de spark [#236](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/236)

# [1.7.0] - 2024-01-12

## Améliorations

- Unpatch du DNS suite à discussion avec open data soft
- Changements de wording front et export excel pour mieux identifiers les erreurs de sélection [#230](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/230)

# [1.6.6] - 2023-06-02

## Corrections

- Patch DNS pour le preprocessing [#231](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/231)

# [1.6.5] - 2023-06-02

## Améliorations

- texte sur les résultats de recherche plus explicite [#227](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/227)

## Retiré

- Remplacement des formulaires par des email [#224](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/224)

# [1.6.4] - 2023-02-23

## Ajouts

## Changements

- Amélioration du preprocessing [#220](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/220)

## Fin de vie

## Retiré

## Corrections

- Correction de typo `remuneration` [#225](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/225)

## Sécurité

# [1.6.3] - 2022-11-04

Cette section liste les changements à venir.

## Changements

- Amélioration du preprocessing [#220](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/220)

# [1.6.2] - 2022-10-20

## Ajouts

- Export du gzip de transparence santé sur le stockage objet [#219](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/219)

# [1.6.1] - 2022-10-12

## Changements

- Changement de l'instance de gitlab
- Adex est maintenant public ! 🍾🥳🎉

# [1.6.0] - 2022-08-31

## Ajouts

- Preprocessing: Support d'un stockage objet distant (optionnel) [#215](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/215)
- Support de podman et podman-compose pour l'execution de la stack [#216](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/216)

## Changements

- Changement de la configuration de traeffik [#216](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/216)
- Preprocessing: maj de la version de spark [#217](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/217)

## Sécurité

- Upgrade d'elasticsearch [#212](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/212)
- Upgrade des dépendances Python

# [1.5.2] - 2022-04-28

## Ajouts

- Timeout configurable pour l'upload sur data-gouv. [#213](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/213)

## Corrections

- Correction d'un crash dû a une donnée manquante. [#214](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/214)

# [1.5.1] - 2022-04-27

## Ajouts

- Upload automatique du dump transparence santé vers data-gouv. [#207](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/207)

## Changements

- Amélioration des dumps de dev. [#208](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/208)

## Retiré

- Retrait du bandeau indiquant que le produit est en beta. [#206](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/206)

# [1.5.0] - 2022-04-04

## Ajouts

- Support de la nouvelle version de transparence santé. [#204](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/204)

## Retiré

- retrait du bandeau d'alerte sur la nouvelle version de transparence santé. [#204](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/204)

# [1.4.1] - 2022-03-18

## Changements

- Ajout du bandeau d'alerte sur la nouvelle version de transparence santé [#200](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/200)
- Amélioration de la CI [47d404a10](https://gitlab.has-sante.fr/has-sante/public/adex/-/commit/47d404a109af8e8ae92834b04abc76b8291e10d0) [#201](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/201)

# [1.4.0] - 2022-01-12

## Ajouts

- Ajout de la spécialité de la personne physique dans la recherche et les résultat [#193](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/193)

## Changements

- Modification de l'ordre et du contenu de colonnes de la synthèse [#193](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/193)

# [1.3.0] - 2021-10-27

## Ajouts

- Affichage des dates de début et de fin des conventions [#62](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/62)
- Ajout de la date de traitement des données dans l'export excel [#135](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/135) [#162](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/162) [#188](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/188)

## Changements

- Amélioration de la documentation du projet [#186](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/186)
- Amélioration du calcul du montant total des liens d'intérêts [#187](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/187) [#44](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/44)
- Amélioration diverses du front et de l'export: [#116](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/116), [#117](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/117), [#126](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/126), , [#29](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/29), et [#39](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/39)
- Amélioration de l'image docker de dev du frontend: [#166](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/166)

## Corrections

- Correction d'un bug affichant toujours des informations issues de l'annuaire lorsque l'on fait une recherche libre après une recherche via l'annuaire [#189](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/189)

# [1.2.0] - 2021-10-14

## Ajouts

- Il est possible de récupérer le lien d'une entrée de la FAQ sur simple clic. [#185](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/185)
- Documentation basique du projet dans gitlab-pages. [#155](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/155)

## Changements

- Les URLs d'ADEX liés à l'utilisation de l'annuaire ANS sont maintenant fixes grâce à l'utilisation du champ "identification_nationale_pp" comme clé pour l'interrogation des API à la place de l'utilisation d'un identifiant généré à chaque ingestion des données par ElasticSearch. [#4](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/4)
- Utilisation de poetry pour la gestion des paquets et des dépendances pour le preprocessing et le backend. [#157](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/157), [#156](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/156)

## Retiré

- Dans la page de recherche des bénéficiaires de TS, le score indiquant la pertinence pour chaque ligne est enlevé. [#184](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/184)

## Corrections

- Ajout d'une restart policy aux containers [#176](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/176)
- Ajout des directives meta "noindex, nofollow" dans le header des pages présentant des résultats de recherche pour empêcher leur indexation. (Mesure complémentaire au fichier robots.txt) [#54](https://gitlab.has-sante.fr/has-sante/public/adex/-/issues/54)
- Correction de bugs sur l'affichage de déclarations sans motifs et sur l'export de rémunérations non liées à des conventions

# [1.1.0] - 2021-09-29

## Ajouts

- Dans la page de recherche des bénéficiaires de TS, un score indique la pertinence pour chaque ligne.
- Le changelog est maintenant accessible.

## Changements

- La FAQ a été entièrement réecrite.
- La notice d'utilisation de l'export excel a été corrigé.

## Retiré

- Le filtrage des données de Transparence santé a été supprimé et toutes les données
  disponibles dans les données exposées sur data.gouv sont affichées.

## Corrections

- Lien vers le code dans le footer de l'application fonctionnel.
- Certains caractères spéciaux non supportés lors de la création de l'export ont été supprimés des données.
- Amélioration de la réactivité de l'application.
- Amélioration de problèmes d'affichages.
