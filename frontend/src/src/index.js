import React from "react";
import ReactDOM from "react-dom";

import { ErrorBoundary } from "react-error-boundary";
import { Provider } from "react-redux";

import App from "./Pages/app/App";
import { appStore } from "Store";

import { useTrackPage } from "Components/ATTracker/useTrackPage";

const ErrorFallback = ({ error }) => {
  useTrackPage({ name: "Erreur React JS" });
  return (
    <div role="alert">
      <p>Une erreur est survenue. Veuillez nous en excuser. </p>
      <p>
        Vous pouvez signaler le bug en nous écrivant à l&apos;adresse
        <a
          target="_blank"
          rel="noreferrer noopener"
          href="mailto:adex@has-sante.fr"
        >
          adex@has-sante.fr
        </a>
      </p>
      <p>Merci de recharger la page</p>
      <pre>{error.message}</pre>
      <a href="/">Revenir à l&apos;accueil</a>
    </div>
  );
};

ReactDOM.render(
  <React.StrictMode>
    <ErrorBoundary FallbackComponent={ErrorFallback}>
      <Provider store={appStore}>
        <App />
      </Provider>
    </ErrorBoundary>
  </React.StrictMode>,
  document.getElementById("root")
);
