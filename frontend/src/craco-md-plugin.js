const { addBeforeLoader, loaderByName } = require("@craco/craco");

// Inspired by https://github.com/gsoft-inc/craco/blob/master/recipes/use-markdown-loader/craco.config.js
// Additional configuration for Typescript added  in `react-app-env.d.ts` file : `declare module '*.md'`.

module.exports = {
  overrideWebpackConfig: ({
    webpackConfig,
    cracoConfig,
    pluginOptions,
    context: { env, paths },
  }) => {
    webpackConfig.resolve.extensions.push(".md");

    const markdownLoader = {
      test: /\.md$/,
      exclude: /node_modules/,
      use: [
        {
          loader: require.resolve("raw-loader"),
        },
      ],
    };

    addBeforeLoader(webpackConfig, loaderByName("file-loader"), markdownLoader);

    return webpackConfig;
  },
};
