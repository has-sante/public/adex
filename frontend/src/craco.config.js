const path = require("path");
const CracoAntDesignPlugin = require("craco-antd");
const CracoLessPlugin = require("craco-less");
const getCSSModuleLocalIdent = require("react-dev-utils/getCSSModuleLocalIdent");

const WebpackMarkdownLoaderConfigPlugin = require("./craco-md-plugin");
const WebpackTSLoaderConfigPlugin = require("./craco-ts-plugin");

module.exports = {
  plugins: [
    { plugin: WebpackTSLoaderConfigPlugin },
    { plugin: WebpackMarkdownLoaderConfigPlugin },
    {
      plugin: CracoAntDesignPlugin,
      options: {
        customizeThemeLessPath: path.join(__dirname, "antd.theme.config.less"),
      },
    },
    {
      plugin: CracoLessPlugin,
      options: {
        cssLoaderOptions: {
          modules: {
            getLocalIdent: (context, localIdentName, localName, options) => {
              if (context.resourcePath.includes("node_modules")) {
                return localName;
              }
              return getCSSModuleLocalIdent(
                context,
                localIdentName,
                localName,
                options
              );
            },
          },
        },
        lessLoaderOptions: {
          lessOptions: {
            noIeCompat: true,
            modules: { localIdentName: "[local]_[hash:base64:5]" },
            javascriptEnabled: true,
          },
        },
        modifyLessRule: function (lessRule, _context) {
          lessRule.test = /\.(module)\.(less)$/;
          lessRule.exclude = path.join(__dirname, "node_modules");
          return lessRule;
        },
      },
    },
  ],
};
