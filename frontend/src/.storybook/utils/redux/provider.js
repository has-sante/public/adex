import React from 'react'
import { Provider } from 'react-redux'
import { configureReduxStore } from "Core/store/store.config";
import { gatewaysConfig } from "Store/config.with_fixtures";

export const appStore = configureReduxStore(gatewaysConfig);

export const ProviderWrapper = ({ children, store }) => (
  <Provider store={store}>
      { children }
  </Provider>
)


export const withProvider = (story) => <Provider store={appStore}>{story()}</Provider>;