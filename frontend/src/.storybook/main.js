const path = require("path");

module.exports = {
  stories: [
    "../src/Components/**/*.stories.@(js|jsx|ts|tsx)",
  ],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    '@storybook/addon-a11y',
    {
      name: "storybook-preset-craco",
      options: {
        cracoConfigFile: path.resolve(__dirname, "./../craco.config.js"),
      },
    },
  ],
  webpackFinal: async (config) => {
    config.resolve.modules = [
      ...(config.resolve.modules || []),
      path.resolve(__dirname, "../src"),
    ];

    return config;
  },
};
