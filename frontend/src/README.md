# Frontend ADEX

## intro

Stack (principaux composants):

- TypeScript
- React (via create-react-app)
- craco (Create React App Configuration Override, to correctly use antd)
- antd

## Paramétrage de l'environnement

Créer un fichier `.env` depuis `.env.template`.

## Scripts

npm et Node v12.18.4 ont été utilisés pour le développement de l'application.

### Install

`npm install`

NB: dans les dépendances, `"babel-loader": "8.1.0"` a été ajouté bien qu'il soit une dépendance de react-script et de storybook. 
Cet ajout est lié au fait qu'il y a un conflit entre les 2 dépendances demandées qui n'est pas correctement résolu.
cf. https://github.com/storybookjs/storybook/issues/5183

### Dev

`npm run dev`:

L'app est alors dispo sur `http://localhost:3000`
Utilise les appels aux api: Ne pas oublier de lancer le backend pour que les appels aux apis fonctionnent.

`npm run storybook`: 
Le storybook est alors dispo sur `http://localhost:6006`
Storybook utilise les fixtures au lien des appels aux apis. Il n'est pas nécessaire de lancer le backend.

Pour utiliser les fixtures au lieu des appels aux api, modifier la configuration du store dans `Store/index.ts`

## Organisation du code

- `Adapters/` : contient les implémentations non directement liées au domaine (ie. les appels api ou les fixtures)
- `Assets/`: contient les fichiers statiques copiés vers public et pouvant être importés par React
- `Components/`: contient les composants 
- `Core/`: contient les objets du domaine (ports) et les useCase (contenant notamment les actions/reducers)
- `Hooks`: contient des Hooks génériques
- `Pages`: arborescence des pages en lien avec react-router
- `Store`: configuration du store Redux
- `Styles`: contient les fichiers less globaux
- `Utils`: fonctions utilitaires (formattage, dé/normalisation)
- index.js : point d'entrée

Ne pas oublier la configuration du `.env`

## Configuration

### Craco

Craco permet d'ajouter des configurations spécifiques pour webpack utilisé par create-react-app (CRA) sans avoir à "éjecter" la configuration créée par CRA.
2 configurations spécifiques sont nécessaires :

- configuration de less avec ant Design
- configuration de baseUrl de typescript

### Plugins Craco

#### tsconfig-paths-webpack-plugin

Permet déviter de créer des alias pour la résolution des chemins des modules depuis `baseURL` (ici: `./src/`)
