ifeq ($(USE_PODMAN_COMPOSE),true)
COMPOSE_COMMAND := podman-compose
else
COMPOSE_COMMAND := docker-compose
endif

##########
# Config #
##########

# Affiche la configuration docker-compose pour le dev
config-dev:
	$(COMPOSE_COMMAND) config

# Affiche la configuration docker-compose pour la prod
config-prod:
	$(COMPOSE_COMMAND) -f ./docker-compose.yml -f ./docker-compose.prod.yml config

# Affiche la configuration docker-compose pour les tests
config-test:
	$(COMPOSE_COMMAND) -f ./docker-compose.yml -f ./docker-compose.test.yml config

# Affiche la configuration docker-compose pour les tests en mode dev
config-test-dev:
	$(COMPOSE_COMMAND) -f ./docker-compose.yml -f ./docker-compose.override.yml -f ./docker-compose.test.yml config

#########
# Build #
#########

# Construction des images docker pour le développement
build-dev:
	$(COMPOSE_COMMAND) build

# Construction des images docker pour la production
build-prod:
	$(COMPOSE_COMMAND) -f ./docker-compose.yml -f ./docker-compose.prod.yml build

######
# Up #
######

# Lance les services en mode développement
up-dev:
	$(COMPOSE_COMMAND) up -d --force-recreate

# Lance les services en mode production
up-prod:
	$(COMPOSE_COMMAND) -f ./docker-compose.yml -f ./docker-compose.prod.yml up -d

##############
# Processing #
##############

processing-dev:
	$(COMPOSE_COMMAND) up --force-recreate -d elasticsearch postgres
	$(COMPOSE_COMMAND) run --rm preprocessing poetry run process --no-full-dataset --force-download --force-extract --extract-ts
	$(COMPOSE_COMMAND) run --rm preprocessing poetry run ingest

processing-dev-no-download-only-ts:
	$(COMPOSE_COMMAND) up -d elasticsearch postgres
	$(COMPOSE_COMMAND) run --rm preprocessing poetry run process --no-full-dataset --no-force-download --no-force-extract --no-process-professionals --extract-ts
	$(COMPOSE_COMMAND) run --rm preprocessing poetry run ingest --no-es-ans-ingestion

# Lance le processing sur l'ensemble des données de TS
processing-prod:
	$(COMPOSE_COMMAND) run --rm preprocessing poetry run process --full-dataset --force-download --force-extract --no-push-to-data-gouv
	$(COMPOSE_COMMAND) run --rm preprocessing poetry run ingest

processing-setmetadata:
	$(COMPOSE_COMMAND) run --rm preprocessing poetry run ingest --no-elasticsearch-ts-ingestion --no-elasticsearch-ans-ingestion --no-postgres-ingestion

#############
# Dev tools #
#############

# Lance le processing sur un petit subset de données avec les professionnels de santé suivant :
# - Bruno Francis
# - Violaine Natal Rel Dakir
create-dev-dumps:
	$(COMPOSE_COMMAND) run --rm preprocessing poetry run python scripts/create_dev_dumps.py

# Lance un shell dans le container du backend avec les ports disponibles
backend-api-bash:
	$(COMPOSE_COMMAND) run --rm --service-ports backend-api /bin/bash

# Lance un shell dans le container du backend
backend-api-bash-no-ports:
	$(COMPOSE_COMMAND) run --rm backend-api /bin/bash

# Lance un shell dans le container du preprocessing
preprocessing-bash:
	$(COMPOSE_COMMAND) run --rm --service-ports preprocessing /bin/bash

# Lance le service du frontend et affiche les logs
start-frontend:
	$(COMPOSE_COMMAND) up -d frontend
	$(COMPOSE_COMMAND) logs -f frontend

# Lance le service du backend et ses dépendances
start-backend:
	$(COMPOSE_COMMAND) up -d reverse-proxy backend-api postgres elasticsearch
	$(COMPOSE_COMMAND) logs -f backend-api

# Ouvre une invit de commande postgres
psql:
	$(COMPOSE_COMMAND) exec postgres psql -U postgres

# Lance Kibana
kibana:
	$(COMPOSE_COMMAND) up -d kibana


###########
# Linting #
###########

# Linting pour le preprocessing
pylint-preprocessing:
	$(COMPOSE_COMMAND) run --rm preprocessing sh -c 'find . -type f -name "*.py" | xargs pylint --disable=missing-docstring,C0103 --ignore=build'

# Linting pour le backend
pylint-backend-api:
	$(COMPOSE_COMMAND) run --rm backend-api sh -c 'find . -type f -name "*.py" | xargs pylint --disable=missing-docstring,C0103'

#########
# Tests #
#########

# Dans les commandes suivantes, le suffixe -dev permet de lancer
# les tests avec les volumes de code montés dans les containers.
#
# Sans ce suffixe, il est nécessaire de rebuild les images.

# Tests pour le backend
test-backend-api:
	$(COMPOSE_COMMAND) -f docker-compose.yml -f docker-compose.test.yml run --rm backend-api
test-backend-api-dev:
	$(COMPOSE_COMMAND) -f docker-compose.yml -f docker-compose.override.yml -f docker-compose.test.yml run --rm backend-api
coverage-backend-api-dev:
	$(COMPOSE_COMMAND) run --rm backend-api poetry run pytest --ignore=app/tests/search_engine app/tests --cov=app app/tests

# Tests pour le frontend
test-frontend:
	$(COMPOSE_COMMAND) -f docker-compose.yml -f docker-compose.test.yml run --rm frontend
test-frontend-dev:
	$(COMPOSE_COMMAND) -f docker-compose.yml -f docker-compose.override.yml -f docker-compose.test.yml run --rm frontend

# Tests pour le preprocessing
test-preprocessing:
	$(COMPOSE_COMMAND) -f docker-compose.yml -f docker-compose.test.yml run --rm preprocessing
test-preprocessing-dev:
	$(COMPOSE_COMMAND) -f docker-compose.yml -f docker-compose.override.yml -f docker-compose.test.yml run --rm preprocessing
coverage-preprocessing-api-dev:
	$(COMPOSE_COMMAND) run --rm preprocessing poetry run pytest --cov=functions --cov=flows --cov=tasks --cov=utils tests/

# Lance les tests pour le frontend, le backend et le preprocessing
tests-dev:
	$(COMPOSE_COMMAND) -f docker-compose.yml -f docker-compose.override.yml -f docker-compose.test.yml run --rm frontend
	$(COMPOSE_COMMAND) -f docker-compose.yml -f docker-compose.override.yml -f docker-compose.test.yml run --rm backend-api
	$(COMPOSE_COMMAND) -f docker-compose.yml -f docker-compose.override.yml -f docker-compose.test.yml run --rm preprocessing

docs-build:
	cd docs; make html

docs-serve:
	python -m http.server --directory ./docs/_build/html
