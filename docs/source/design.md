# Processus de conception

ADEX est mené avec une approche Design Thinking, une méthode de conception collaborative qui intègre les utilisateurs et les parties prenantes du projet à toutes les étapes du développement de la solution numérique, que ce soit au moment du cadrage, de la conception ou des tests du prototype.


## Note de cadrage

Au début du projet, un atelier a été organisé afin de clarifier l'orientation du projet.

```{note}
{download}`Télécharger la note de cadrage <./design/1-cadrage-du-projet.pdf>`
```

## Synthèse de la recherche utilisateur

L'objectif de la recherche utilisateur est de se mettre en empathie avec les utilisateurs d’ADEX en observant et recueillant les problématiques du terrain, afin d’être au plus proche des besoins réels et non de ceux que nous imaginons.

À noter que le périmètre de la recherche utilisateur a vocation à être plus large que le périmètre d’ADEX, la finalité étant d’obtenir des informations sans a priori sur les problématiques globales rencontrées à la HAS en matière d’expertise. 

```{note}
{download}`Télécharger la synthèse de la recherche utilisateur <./design/2-synthese-de-la-recherche-utilisateur.pdf>`
```
## Synthèse des premiers tests utilisateurs

Un premier prototype d'ADEX a été présenté à 5 agents de la haute autorité de santé dès décembre 2020.

```{note}
{download}`Télécharger la synthèse des premiers tests utilisateurs <./design/3-synthese-du-test-1.pdf>`
```
## Synthèse des seconds tests utilisateurs

Une fois ce prototype amélioré, une deuxième phase de tests à été lancée en mars 2021 avec de nouveau 5 agents.

```{note}
{download}`Télécharger la synthèse des seconds tests utilisateurs <./design/4-synthese-du-test-2.pdf>`
```
## Synthèse des beta tests

En mai 2021, une version beta d'ADEX a été mise en ligne et 9 agents ont pu tester l'outil et nous faire leurs retours.

```{note}
{download}`Télécharger la synthèse des béta tests <./design/5-synthese-des-beta-tests.pdf>`
```