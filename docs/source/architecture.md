# Architecture du projet

Cette page présente l'architecture du projet ADEX:

- Présentation des données exploitées
- Organisation des différentes briques techniques de la solution

## Données exploitées

Deux bases de données sont utilisées par la solution ADEX:

- la base Transparence Santé
- l'annuaire ANS des professionnels de santé

### Transparence Santé

La base de données [Transparence Santé](https://transparence.sante.gouv.fr/) contient
les déclarations déposées par les entreprises de l’industrie de santé.
On y trouve les conventions, avantages et rémunérations octroyées par les industriels aux acteurs de la santé: professionnels de santé, étudiants, sociétés savantes, associations, médias, etc.

Un export de la base de données est accessible via le portail [data.gouv](https://www.data.gouv.fr/en/datasets/transparence-sante-1/).
Les données sont téléchargées depuis l'[api opendatasoft](https://www.transparence.sante.gouv.fr/explore/dataset/declarations/api/)
L'utilisation des données est notamment soumise aux conditions suivante :

> La réutilisation des données issues de la base Transparence - Santé est soumise au respect de la licence de réutilisation de l’information.
>
> **La réutilisation des données est restreinte**, elle doit être faite dans le respect de la finalité
> de la transparence des liens d’intérêts, ce qui exclut notamment une réutilisation à des fins strictement commerciales.
>
> Par ailleurs, en cas de réutilisation des données donnant lieu à un traitement de données,
> **le réutilisateur doit se conformer aux dispositions de la loi n° 78-17 du 6 janvier 1978** > **relative à l’informatique, aux fichiers et aux libertés**. Le réutilisateur doit aussi assurer la sécurité
> et la protection des seules données directement identifiantes contre l'indexation par des moteurs de recherche externes."

En cas de déploiement de la solution Adex, il peut être nécessaire d'ajouter une fiche de traitement au registre de traitement de données de l'organisme et d'obtenir une validation du DPO le cas échéant afin d'être en respect de la RGPD.

### Annuaire Santé de l’ANS

L’[Annuaire Santé](https://annuaire.sante.fr/) édité par l’Agence du Numérique en Santé (ANS) propose un référentiel
des diplômes et activités des professionnels de santé Français.

[Les conditions d’utilisations](https://annuaire.sante.fr/web/site-pro/public-conditions-utilisation)
ne semblent pas contraignantes par rapport à l’usage prévu qui est uniquement d’afficher les données présentent dans l’export public.

## Architecture technique

La solution ADEX traite dans un premier temps deux bases de données ouvertes en récupérant les exports aux liens suivants :

- L’Annuaire Santé de l’ANS : https://service.annuaire.sante.fr/annuaire-sante-webservices/V300/services/extraction/PS_LibreAcces
- Les données Transparence Santé : https://www.transparence.sante.gouv.fr/exports-etalab/exports-etalab.zip
  Un ensemble de traitements sont réalisés, principalement pour nettoyer les données.
  Une description détaillée des traitements est accessible dans
  [la documentation du preprocessing d'ADEX](https://has-sante.pages.has-sante.fr/public/adex/traitement_donnees.html)

Puis une étape d’ingestion dans des bases de données est lancée.

Ensuite, une application web, composée d’un frontend communiquant avec un backend via une API REST
expose ces données dans des interfaces utilisateurs.

```{mermaid}


flowchart LR
subgraph Preprocessing
    ans(Annuaire ANS)
    ts(Transparence Santé)

    traitement(Traitement)
    ingestion(Ingestion)
end
    pg[(Base de données)]
    es[(Elasticsearch)]

subgraph "Application Web"
    direction TB
    backend(Backend) --> frontend(frontend)
end
    users(Utilisateurs)

    ans & ts --> traitement
    traitement --> ingestion
    ingestion --> pg & es --> backend
    frontend --/--> users
    backend --/docs--> users


```

### Architecture applicative

La solution Adex est composée des briques suivantes :

- La brique de **préprocessing** implémentée en python avec le framework [prefect](https://www.prefect.io/).
  Certains calculs sont faits en utilisant une [instance spark installée localement](http://spark.apache.org/docs/latest/spark-standalone.html)
  commandée via l’api python [pyspark](http://spark.apache.org/docs/latest/api/python/). Elle possède deux points d’entrée :
  - Process.py : qui télécharge, nettoie, prépare les données des datasets (TS, Annuaire ANS) et les écrit
    sur un disque dans des formats divers (.zip, .csv, .parquet).
  - Ingest.py : qui récupère les données préparées au format [parquet](https://parquet.apache.org/), et les
    insère dans les bases de données [Elasticsearch](https://www.elastic.co/fr/) et [Postgres](https://www.postgresql.org/)
- Les fichiers issus de la première étape de processing sont déposés sur un **filer**.
  C’est actuellement simplement un espace sur un disque. Après l’ensemble des traitements, il contient environ 12G de données
- Les **bases de données** :
  - Elasticsearch : Trois index sont créés. L’un rassemble les bénéficiaires de transparence santé.
    Le second les entrées de l’annuaire de santé de l’ANS.
    Le troisième des données liées à la date de traitement des données.
  - PostgreSQL : Une table est créé avec l'ensemble des déclarations.
- Le **backend** de l’application web est implémenté en python via le framework [fastapi](https://fastapi.tiangolo.com/).
  Il permet d’avoir naturellement une documentation et une vérification de types à la lecture / requête.
  Il ne possède pas de base de données dédiée pour l’instant, et communiquera en lecture seule avec
  les bases de données ElasticSearch et PostgreSQL afin de restituer l’information demandée via une API Rest.
- Le **frontend** est implémenté en [ReactJS](https://reactjs.org/), avec la bibliothèque de composants [Ant Design](https://ant.design/).
  Pour la production, il est compilé, puis servi statiquement via un serveur [NGINX](https://www.nginx.com/).
- Un reverse proxy [Traefik](https://doc.traefik.io/traefik/) reçoit les requêtes sur le port 80, puis redirige vers les différents services.

L'ensemble des services est encapsulée dans des images [docker](https://www.docker.com/)
et l'applicatif est aujourd'hui déployé grâce à [docker-compose](https://docs.docker.com/compose/).

```{mermaid}
flowchart TB
    subgraph open_data [Open data]
        ans(Annuaire ANS)
        ts(Transparence Santé)
    end
    subgraph processing [Traitement de la donnée]
        subgraph prefect [Prefect]
            preprocessing(Preprocessing)
            ingestion(Ingestion)
        end
        subgraph storage [Stockage]
            filer[(Filer)]
            pg[(Postgres)]
            es[(Elasticsearch)]
        end
        preprocessing --> filer
        filer --> ingestion --> es & pg
    end
    subgraph adex [ADEX]
        backend(Backend / fastapi)
        proxy(Reverse proxy / Traefik)
        frontend(Frontent / reactjs + NGINX)
        pg & es <--> backend
        backend <--> frontend
        proxy <--> backend & frontend
    end
    ans & ts --> preprocessing
    users(Utilisateurs)
    application(Autre Applications)
    users <--> proxy
    application <--api--> proxy

    style open_data stroke:#40BFC1,fill:none,stroke-width:5px
    style processing stroke:#40BFC1,fill:none,stroke-width:5px
    style prefect stroke:#034eca,fill:none,stroke-width:5px
    style storage stroke:#034eca,fill:none,stroke-width:5px
    style adex stroke:#40BFC1,fill:none,stroke-width:5px

    classDef node fill:#7989AA,color:#FFFFFF,stroke:none

```
