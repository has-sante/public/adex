# Traitements des données

## Transparence Santé

Un traitement est réalisé sur les données de [transparence santé](https://transparence.sante.gouv.fr) (TS).

Ces données contiennent les déclarations soumises par les entreprises des
industries de santé dans lesquelles elles mentionnent les conventions
signées avec des professionnels de santé ou des établissements de santé, ainsi que
des rémunérations et avantages versés.

La [FAQ de TS pour les entreprises](https://www.entreprises-transparence.sante.gouv.fr/content/pdf/DocumentFAQ.pdf)
permet de bien comprendre comment les données sont renseignées dans la base de données.

### Graphe simplifié du traitement

```{graphviz}
digraph {
    dl[label="Téléchargement des fichiers"]
    extract[label="Extraction des fichiers"]
    csv_2_parquet[label="Réecriture format parquet"]
    migration[label="Migration du schéma de données"]
    nettoyage[label="Nettoyage des données"]
    link[label="Création des liens entre déclarations"]
    extract_recipients[label="Extraction des bénéficiaires"]
    group_recipients[label="Regroupement des bénéficiaires"]

    dl -> extract -> csv_2_parquet
    csv_2_parquet -> migration -> nettoyage -> link
    link -> extract_recipients -> group_recipients

}
```

### Téléchargement et extractions

Les données sources sont récupérées depuis l'[api opendatasoft](https://www.transparence.sante.gouv.fr/explore/dataset/declarations/api/) avec des checks de vérification d'intégrité des données.

Les fichiers csv des conventions, rémunérations et avantages sont ensuite concaténés
puis réécrits au format parquet afin de faciliter les traitements suivants.

### Migration

La base de données transparence santé a été restructurée (octobre 2021-mars 2022).

Le principal changement est la création d'un format commun pour toutes les déclarations.

L'étape de migration permet d'adapter ce nouveau format a celui supporté par adex.
Cela permet d'obtenir un unique fichier parquet `parquet/ts/declarations.parquet` avec un schéma unique.

### Nettoyage

Plusieurs traitements sont réalisés afin de nettoyer les déclarations :

- Les noms / prénoms / identités sont normalisés
- Un hash unique est créé à partir des champs liés au bénéficiaire afin de pouvoir identifier de manière
  unique la variation des champs liés au bénéficiaire dans une ligne de TS
- Les montants vides sont mis à 0
- Les dates sont parsées afin d'être mises dans un format date
- Le identifiants mal formatés (`000000000`, `88888888`, ...) présents dans la base sont mis à null
- Un identifiant complet est créé pour chaque déclaration en concaténant :
  - L'identifiant de l'entreprise déclarante
  - Le type de la déclaration (`convention`, `remuneration`, `avantage`)
  - L'identifiant unique de la ligne
- Un identifiant similaire est généré sur la colonne `convention_id` pour les avantages et les rémunérations

### Liens entre déclarations

Les rémunérations de TS sont toutes liées à une convention. Pour les avantages, il est possible qu'il y ait
une convention liée mentionnée.

Dans la [FAQ de TS pour les entreprises](https://www.entreprises-transparence.sante.gouv.fr/content/pdf/DocumentFAQ.pdf)
on peut lire :

    2. A quoi correspond le champ « AVANT_CONVENTION_LIE » ?
    Cette information doit permettre de retrouver facilement la convention liée.
    Ce champ n'est pas contrôlé, vous pouvez donc choisir ce qui semble le plus pertinent pour retrouver la convention.

Étant un champ libre, il est fréquent que l'identifiant de la convention liée mentionnée au niveau
de l'avantage n'existe pas.

Si la convention existe, le champ `convention_id` est conservé tel quel.

Si la convention n'existe pas, on considère que l'identifiant de la convention fourni est en fait
une description de la convention liée, et le champ `convention_description` est rempli avec ces informations.
Le champ `convention_id` est alors `null`.

### Extractions et regroupement des bénéficiaires

Les variations des informations relatives aux bénéficiaires sont extraites grâce à une déduplication
sur le champ créé `benef_hash`.

Ensuite, une opération permet de réunir dans un même groupe toutes les variations d'un bénéficiaire
dont le prénom, le nom, le type d'identifiant et la valeur de l'identifiant sont identiques.

Cela permettra dans l'interface d'afficher des groupes réunissants des variations d'un même bénéficiare
physique.

## L'annuaire de l'Agence du Numérique en Santé (ANS)

Un traitement est réalisé afin de permettre l'indexation efficace de la base de données de l'annuaire
de l'Agence du Numérique en Santé (ANS).

Celui-ci contient l'ensemble des lieux d'activités des professionnels de santé enregistrés.

Plus d'informations sont accessibles sur [le site de l'annuaire de l'ANS](https://annuaire.sante.fr/).

### Graphe simplifié du traitement

```{graphviz}
digraph {
    dl[label="Téléchargement des fichiers"]
    extract[label="Extraction des fichiers"]
    csv_2_parquet[label="Réecriture format parquet"]
    traitement[label="Traitement des données"]


    dl -> extract -> csv_2_parquet
    csv_2_parquet -> traitement
}
```

### Téléchargement et extractions

Le fichier `PS_libreacces` est récupéré à partir du lien mis à disposition par [l'ANS](https://annuaire.sante.fr/web/site-pro/extractions-publiques).

Comme pour transparence santé, les données sont alors extraites de l'archive, puis réecrites au format parquet.

### Traitements

Les traitements réalisés sont très simples.

Tout d'abord une déduplication est faite sur l'identifiant national pp afin de réunir dans un même groupe tous les lieux d'activité d'un professionnel de santé.

Puis les données sont restructurées afin de faciliter l'exploitation des données.

## L'ingestion des données

Une fois les traitements réalisés sur les bases de données Transparence Santé et de l'annuaire ANS,
les données sont ingérées dans les deux bases de données du projet.

### Graphe simplifié du traitement

```{graphviz}
digraph {

    subgraph cluster_es {
        label="Ingestion Elasticsearch"
        ingest_ans[label="Ingestion de l'annuaire ANS"]
        ingest_ts[label="Ingestion de TS"]
        ingest_metadata[label="Ingestion des metadata"]
        ingest_ans -> ingest_ts -> ingest_metadata
    }
    ingest_metadata -> ingest_pg
    subgraph cluster_pg {
        label="Ingestion PostgreSQL"
        ingest_pg[label="Ingestion des déclarations"]
        creation_index[label="Création d'index"]
        ingest_pg -> creation_index
    }
    creation_index -> roll_over_es_aliases
    subgraph cluster_clean_up {
        label="Roll over et nettoyage"

        roll_over_es_aliases[label="Mise à jour des index Elasticsearch"]
        roll_over_postgres_schemas[label="Mise à jour des schémas PostgreSQL"]
        clean_aliases[label="Nettoyage des index Elasticsearch"]
        roll_over_es_aliases -> roll_over_postgres_schemas
        roll_over_postgres_schemas -> clean_aliases
    }
}
```

### Ingestion dans elasticsearch

Les groupes de variantes de bénéficiaires ainsi que les données de l'annuaire de santé de l'ANS sont
ingérés dans elasticsearch afin de permettre la recherche dans les données via l'interface ADEX.

Un index est aussi créé contenant les metadatas liés à la date du traitement des données.

De nouveaux index sont créés avec un timestamp lié à la date de création de l'ingestion des données :

- Pour l'ANS : `professionals-2021-09-24t12.43.37`
- Pour TS : `recipients-groups-2021-09-24t12.43.37`
- Pour les metadonnées : `info-2021-09-24t12.43.37`

### Ingestion dans PostgreSQL

Les déclarations sont insérées dans la base de données PostgreSQL.

Le schéma PostgreSQL `new` est utilisé pour cette insertion.

Des index sont créés sur la table nouvellement créée `new.ts_declarations` sur les champs :

- `declaration_id`
- `benef_hash`
- `lien_interet`

### Roll over et nettoyage

Afin d'utiliser les données nouvellement générées, des alias sont générés côté Elasticsearch
afin de pointer vers les index nouvellement créés :

- `professionals` -> `professionals-2021-09-24t12.43.37`
- `recipients-groups` -> `recipients-groups-2021-09-24t12.43.37`
- `info` -> `info-2021-09-24t12.43.37`

De plus, le schéma PostgreSQL `public` est renommé `old` et le schéma `new` est renommé `public` afin
de rendre disponbile les informations.

Enfin, les anciens index elasticsearch, c'est à dire ceux avec des timestamps anciens, sont supprimés.

# Données générées

## TS

### Déclarations de TS

`/data/clean/ts/declarations.parquet`

```
|-- entreprise_identifiant: string (nullable = true)
|-- denomination_sociale: string (nullable = true)
|-- identifiant_unique: string (nullable = true)
|-- categorie_beneficiaire_code: string (nullable = true)
|-- categorie_beneficiaire_libelle: string (nullable = true)
|-- prenom: string (nullable = true)
|-- profession: string (nullable = true)
|-- pays_code: string (nullable = true)
|-- pays: string (nullable = true)
|-- type_identifiant: string (nullable = true)
|-- numero_identifiant: string (nullable = true)
|-- structure_exercice: string (nullable = true)
|-- date: date (nullable = true)
|-- motif: string (nullable = true)
|-- autre_motif: string (nullable = true)
|-- date_debut: date (nullable = true)
|-- date_fin: date (nullable = true)
|-- montant: double (nullable = true)
|-- identite_nom: string (nullable = true)
|-- code_postal: string (nullable = true)
|-- adresse: string (nullable = true)
|-- ville: string (nullable = true)
|-- information_convention: string (nullable = true)
|-- lien_interet: string (nullable = true)
|-- identite_nom_normalized: string (nullable = true)
|-- prenom_normalized: string (nullable = true)
|-- benef_hash: string (nullable = true)
|-- declaration_id: string (nullable = true)
|-- convention_description: string (nullable = true)
|-- convention_id: string (nullable = true)
```

### Les bénéficiares de TS

`/data/prepared/recipients/recipients`

```
|-- identite_nom: string (nullable = true)
|-- prenom: string (nullable = true)
|-- adresse: string (nullable = true)
|-- structure_exercice: string (nullable = true)
|-- pays_code: string (nullable = true)
|-- pays: string (nullable = true)
|-- numero_identifiant: string (nullable = true)
|-- type_identifiant: string (nullable = true)
|-- profession: string (nullable = true)
|-- code_postal: string (nullable = true)
|-- ville: string (nullable = true)
|-- categorie_beneficiaire_code: string (nullable = true)
|-- categorie_beneficiaire_libelle: string (nullable = true)
|-- benef_hash: string (nullable = true)
|-- identite_nom_normalized: string (nullable = true)
|-- prenom_normalized: string (nullable = true)
```

### Les groupes de bénéficiares de TS

`/data/prepared/recipients/recipients_groups`

```
|-- type_identifiant: string (nullable = true)
|-- numero_identifiant: string (nullable = true)
|-- identite_nom_normalized: string (nullable = true)
|-- prenom_normalized: string (nullable = true)
|-- children: array (nullable = true)
|    |-- element: struct (containsNull = true)
|    |    |-- identite_nom: string (nullable = true)
|    |    |-- prenom: string (nullable = true)
|    |    |-- adresse: string (nullable = true)
|    |    |-- structure_exercice: string (nullable = true)
|    |    |-- pays_code: string (nullable = true)
|    |    |-- pays: string (nullable = true)
|    |    |-- numero_identifiant: string (nullable = true)
|    |    |-- type_identifiant: string (nullable = true)
|    |    |-- profession: string (nullable = true)
|    |    |-- code_postal: string (nullable = true)
|    |    |-- ville: string (nullable = true)
|    |    |-- categorie_beneficiaire_code: string (nullable = true)
|    |    |-- categorie_beneficiaire_libelle: string (nullable = true)
|    |    |-- benef_hash: string (nullable = true)
|    |    |-- identite_nom_normalized: string (nullable = true)
|    |    |-- prenom_normalized: string (nullable = true)
|-- group_size: long (nullable = true)
|-- identite_nom: string (nullable = true)
|-- prenom: string (nullable = true)
|-- adresse: string (nullable = true)
|-- structure_exercice: string (nullable = true)
|-- pays_code: string (nullable = true)
|-- pays: string (nullable = true)
|-- profession: string (nullable = true)
|-- code_postal: string (nullable = true)
|-- ville: string (nullable = true)
|-- categorie_beneficiaire_code: string (nullable = true)
|-- categorie_beneficiaire_libelle: string (nullable = true)
|-- benef_hash: string (nullable = true)
```

## Annuaire de l'ANS

`/data/clean/professionals/professionals.parquet`

```
|-- identification: struct (nullable = true)
|    |-- code_type_identifiant_pp: long (nullable = true)
|    |-- libelle_type_identifiant_pp: string (nullable = true)
|    |-- identifiant_pp: string (nullable = true)
|    |-- identification_nationale_pp: string (nullable = true)
|-- noms_complets_exercice: array (nullable = true)
|    |-- element: struct (containsNull = true)
|    |    |-- libelle_civilite: string (nullable = true)
|    |    |-- libelle_civilite_exercice: string (nullable = true)
|    |    |-- nom_exercice: string (nullable = true)
|    |    |-- prenom_exercice: string (nullable = true)
|-- lieux_activite: array (nullable = true)
|    |-- element: struct (containsNull = true)
|    |    |-- adresse_complete: string (nullable = true)
|    |    |-- numero_voie_coord_structure: string (nullable = true)
|    |    |-- indice_repetition_voie_coord_structure: string (nullable = true)
|    |    |-- libelle_type_voie_coord_structure: string (nullable = true)
|    |    |-- libelle_voie_coord_structure: string (nullable = true)
|    |    |-- code_postal_coord_structure: string (nullable = true)
|    |    |-- libelle_commune_coord_structure: string (nullable = true)
|    |    |-- libelle_pays_coord_structure: string (nullable = true)
|    |    |-- libelle_department_structure: string (nullable = true)
|    |    |-- raison_sociale_site: string (nullable = true)
|-- professions: array (nullable = true)
|    |-- element: string (containsNull = true)
|-- children: array (nullable = true)
|    |-- element: struct (containsNull = true)
|    |    |-- type_identifiant_pp: long (nullable = true)
|    |    |-- identifiant_pp: string (nullable = true)
|    |    |-- identification_nationale_pp: string (nullable = true)
|    |    |-- code_civilite_exercice: string (nullable = true)
|    |    |-- libelle_civilite_exercice: string (nullable = true)
|    |    |-- code_civilité: string (nullable = true)
|    |    |-- libelle_civilite: string (nullable = true)
|    |    |-- nom_exercice: string (nullable = true)
|    |    |-- prenom_exercice: string (nullable = true)
|    |    |-- code_profession: long (nullable = true)
|    |    |-- libelle_profession: string (nullable = true)
|    |    |-- code_categorie_professionnelle: string (nullable = true)
|    |    |-- libelle_categorie_professionnelle: string (nullable = true)
|    |    |-- code_type_savoir_faire: string (nullable = true)
|    |    |-- libelle_type_savoir_faire: string (nullable = true)
|    |    |-- code_savoir_faire: string (nullable = true)
|    |    |-- libelle_savoir_faire: string (nullable = true)
|    |    |-- code_mode_exercice: string (nullable = true)
|    |    |-- libelle_mode_exercice: string (nullable = true)
|    |    |-- numero_siret_site: string (nullable = true)
|    |    |-- numero_siren_site: string (nullable = true)
|    |    |-- numero_finess_site: string (nullable = true)
|    |    |-- numero_finess_etablissement_juridique: string (nullable = true)
|    |    |-- identifiant_technique_structure: string (nullable = true)
|    |    |-- raison_sociale_site: string (nullable = true)
|    |    |-- enseigne_commerciale_site: string (nullable = true)
|    |    |-- complement_destinataire_coord_structure: string (nullable = true)
|    |    |-- complement_point_geographique_coord_structure: string (nullable = true)
|    |    |-- numero_voie_coord_structure: string (nullable = true)
|    |    |-- indice_repetition_voie_coord_structure: string (nullable = true)
|    |    |-- code_type_voie_coord_structure: string (nullable = true)
|    |    |-- libelle_type_voie_coord_structure: string (nullable = true)
|    |    |-- libelle_voie_coord_structure: string (nullable = true)
|    |    |-- mention_distribution_coord_structure: string (nullable = true)
|    |    |-- bureau_cedex_coord_structure: string (nullable = true)
|    |    |-- code_postal_coord_structure: string (nullable = true)
|    |    |-- code_commune_coord_structure: string (nullable = true)
|    |    |-- libelle_commune_coord_structure: string (nullable = true)
|    |    |-- code_pays_coord_structure: string (nullable = true)
|    |    |-- libelle_pays_coord_structure: string (nullable = true)
|    |    |-- telephone_coord_structure: string (nullable = true)
|    |    |-- telephone_2_coord_structure: string (nullable = true)
|    |    |-- telecopie_coord_structure: string (nullable = true)
|    |    |-- adresse_email_coord_structure: string (nullable = true)
|    |    |-- code_departement_structure: string (nullable = true)
|    |    |-- libelle_department_structure: string (nullable = true)
|    |    |-- ancien_identifiant_structure: string (nullable = true)
|    |    |-- autorite_enregistrement: string (nullable = true)
|    |    |-- code_secteur_activite: string (nullable = true)
|    |    |-- libelle_secteur_activite: string (nullable = true)
|    |    |-- code_section_tableau_pharmaciens: string (nullable = true)
|    |    |-- libelle_section_tableau_pharmaciens: string (nullable = true)
```
