# Projet ADEX

![ADEX acceuil](_static/adex_home.png)

```{include} ../../README.md
   :start-after: "# À propos du projet"
   :end-before: "# Table des matières"
```

```{eval-rst}
.. toctree::
   :hidden:
   :maxdepth: 1
   :glob:

   architecture.md
   traitement_donnees.md
   readme.md
   design.md
   changelog.md
   licence.md
```
