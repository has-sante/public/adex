#! /usr/bin/env python
# This script will get the dumps from Transparence Santé, and from 'annuaire ANS'
# It will then create two archives with the same structure but truncated files
# The choice was to get all lines with "BRUNO" to get some interesting examples.
import csv
import io
import os
import shutil
import tempfile
import zipfile
from os import path
from typing import Dict, List

from functions import api_ts
from functions.download_extract import download_file_from_url
from utils.constants import (
    CONSTANTS_DECLARATIONS,
    CONSTANTS_PROFESSIONALS,
    CONSTANTS_TS,
)

BENEFICIARIES = [
    {
        "firstnames": ["Benedicte", "Bénédicte"],
        "identity": "Bruno",
        "RPPS": [("10002580818", "10000000001")],
        "new_firstname": "Brigitte",
        "new_identity": "Dupont",
    },
    {
        "firstnames": ["Pierre"],
        "identity": "Martin",
        "RPPS": [
            ("10101957065", "10000000002"),
            ("10003804159", "10000000003"),
            ("10000959667", "10000000004"),
            ("10002870813", "10000000005"),
            ("10102772836", "10000000006"),
            ("319334686", "300000007"),
            ("139114334", "100000008"),
            ("10000443001", "10000000009"),
            ("382824340", "300000010"),
            ("10101635778", "10000000011"),
            ("10001920981", "10000000012"),
            ("10005530307", "10000000013"),
            ("499305498", "400000014"),
            ("10103303326", "10000000015"),
            ("10104563647", "10000000016"),
            ("10101176567", "10000000017"),
            ("10100701167", "10000000018"),
            ("692816598", "600000019"),
        ],
        "new_firstname": "Jean",
        "new_identity": "Dupont",
    },
]


def get_header_cmd(p_src: str, p_dst: str) -> str:
    return f"head -n 1 {p_src} >> {p_dst}"


def get_grep_cmd(p_src: str, p_dst: str, pattern: str) -> str:
    return f"grep -iE '{pattern}' {p_src} >> {p_dst}"


def get_all_pattern_replace():
    patterns = []
    for beneficaire in BENEFICIARIES:
        for firstname in beneficaire["firstnames"]:
            patterns.append((firstname, beneficaire["new_firstname"]))
            patterns.append(
                (beneficaire["identity"], beneficaire["new_identity"])
            )
            patterns.extend(beneficaire["RPPS"])
    return patterns


def apply_sed(filename: str) -> None:

    sed_patterns = get_all_pattern_replace()
    for pattern, replace in sed_patterns:
        cmd = f"sed -i 's/{pattern}/{replace}/gI' {filename}"
        print(f"Sed command: {cmd}")
        os.system(cmd)


#################
# Professionals #
#################


def process_professionals() -> None:
    url = CONSTANTS_PROFESSIONALS.URL
    result_name = path.join(
        "./scripts/resources", path.basename(CONSTANTS_PROFESSIONALS.P_ZIP)
    )
    tmpname_src = get_tmp_filename()
    tmpname_dst = get_tmp_filename()
    print(f"Download to: {tmpname_src}")
    print(f"Modify to: {tmpname_dst}")
    print(f"Result file will be: {result_name}")

    download_file_from_url(url=url, p_output=tmpname_src, verify_ssl=False)
    process_archive(
        p_src=tmpname_src,
        p_dst=tmpname_dst,
    )
    shutil.move(tmpname_dst, result_name)


def get_tmp_filename():
    tmpfd, tmpname = tempfile.mkstemp(dir="/tmp", suffix=".zip")
    os.close(tmpfd)
    return tmpname


def process_archive(
    p_src: str,
    p_dst: str,
) -> None:
    with zipfile.ZipFile(p_src, "r") as zin:
        with zipfile.ZipFile(p_dst, "w") as zout:
            zout.comment = zin.comment  # preserve the comment
            process_archive_files(
                zin=zin,
                zout=zout,
            )


def process_archive_files(
    zin: zipfile.ZipFile,
    zout: zipfile.ZipFile,
) -> None:
    pattern = get_rpps_grep_pattern()
    for item in zin.infolist():
        print(f"File: {item.filename}")
        if "activite" in item.filename:
            copy_file_and_grep(zin, zout, item.filename, pattern=pattern)


def get_rpps_grep_pattern():
    """
    Will return a grep cmd using regex `or` to match pattern
    |{name}|{firstname}|
    """
    patterns = []
    for beneficaire in BENEFICIARIES:
        for firstname in beneficaire["firstnames"]:
            patterns.append(f'\|{beneficaire["identity"]}\|{firstname}\|')
    return "|".join(patterns)


def copy_file_and_grep(
    zin: zipfile.ZipFile, zout: zipfile.ZipFile, filename: str, pattern: str
) -> None:
    tmpname_src = path.join("/tmp", filename)
    tmpname_dst = path.join("/tmp", filename + "_truncated")
    if os.path.isfile(tmpname_src):
        os.remove(tmpname_src)
    if os.path.isfile(tmpname_dst):
        os.remove(tmpname_dst)
    zin.extract(filename, path="/tmp")
    header_cmd = get_header_cmd(p_src=tmpname_src, p_dst=tmpname_dst)
    print(header_cmd)
    os.system(header_cmd)
    grep_cmd = get_grep_cmd(
        p_src=tmpname_src, p_dst=tmpname_dst, pattern=pattern
    )
    print(grep_cmd)
    os.system(grep_cmd)
    apply_sed(tmpname_dst)
    zout.write(tmpname_dst, arcname=filename)


######
# TS #
######
def process_ts() -> None:
    records = get_all_records()
    fieldnames = CONSTANTS_DECLARATIONS.DTYPES.keys()
    print(fieldnames)
    with open(CONSTANTS_TS.P_DEV_CSV, "w") as csv_file:
        csv_writer = csv.DictWriter(
            csv_file, fieldnames=fieldnames, delimiter=";"
        )
        csv_writer.writeheader()
        csv_writer.writerows(records)
    apply_sed(CONSTANTS_TS.P_DEV_CSV)


def get_all_records() -> List[Dict]:
    records = []
    for beneficiaire in BENEFICIARIES:
        for firstname in beneficiaire["firstnames"]:
            records.extend(
                get_beneficiaire_records(
                    firstname=firstname, lastname=beneficiaire["identity"]
                )
            )
    unique_ids = set(r["id"] for r in records)
    return [r for r in records if r["id"] in unique_ids]


def get_beneficiaire_records(firstname: str, lastname: str):
    all_records = api_ts.request_api(firstname=firstname, identity=lastname)
    all_records.extend(
        api_ts.request_api(firstname=lastname, identity=firstname)
    )
    return all_records


if __name__ == "__main__":
    # process_professionals()
    process_ts()
