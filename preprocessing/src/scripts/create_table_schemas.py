import json
from os import read

import pyarrow.parquet as pq

from functions.parquet import read_parquet
from functions.spark import get_spark
from utils.constants import (
    CONSTANTS_DECLARATIONS,
    CONSTANTS_PROFESSIONALS,
    CONSTANTS_RECIPIENTS,
)

if __name__ == "__main__":
    files = {
        "declarations": CONSTANTS_DECLARATIONS.P_PARQUET_CLEAN,
        "professionals": CONSTANTS_PROFESSIONALS.P_PROFESSIONALS_PARQUET_CLEAN,
        "recipients": CONSTANTS_RECIPIENTS.P_PARQUET,
        "recipients_groups": CONSTANTS_RECIPIENTS.P_GROUPS_PARQUET,
    }
    spark = get_spark("read_parquet")

    with open("test", "w") as file_id:
        for key, filename in files.items():
            print(filename)
            data = read_parquet(filename, spark)
            file_id.write(filename)
            file_id.write("\n")
            file_id.write(data._jdf.schema().treeString())
            file_id.write("\n")
            file_id.write("\n")
            file_id.write("\n")
            file_id.write("\n")
