"""
Cli pour le traitement des données
"""
import sys

import click

from flows.professionals_directory import create_professional_flow
from flows.transparence_sante import (
    create_ts_download_flow,
    create_ts_extract_flow,
)


@click.command()
@click.option(
    "--force-download/--no-force-download", default=False, show_default=True
)
@click.option(
    "--full-dataset/--no-full-dataset", default=False, show_default=True
)
@click.option(
    "--force-extract/--no-force-extract", default=True, show_default=True
)
@click.option(
    "--push-to-data-gouv/--no-push-to-data-gouv",
    default=False,
    show_default=True,
)
@click.option(
    "--process-professionals/--no-process-professionals",
    default=True,
    show_default=True,
)
@click.option(
    "--download-ts/--no-download-ts", default=True, show_default=True
)
@click.option("--extract-ts/--no-extract-ts", default=False, show_default=True)
def cli(
    force_download,
    full_dataset,
    force_extract,
    push_to_data_gouv,
    process_professionals,
    download_ts,
    extract_ts,
):
    process_cmd(
        force_download=force_download,
        full_dataset=full_dataset,
        force_extract=force_extract,
        push_to_data_gouv=push_to_data_gouv,
        process_professionals=process_professionals,
        download_ts=download_ts,
        extract_ts=extract_ts,
    )


def process_cmd(
    force_download: bool,
    full_dataset: bool,
    force_extract: bool,
    push_to_data_gouv: bool,
    process_professionals: bool,
    download_ts: bool,
    extract_ts: bool,
) -> None:

    if process_professionals:
        flow_professionals = create_professional_flow()
        flow_professionals_state = flow_professionals.run(
            parameters=dict(
                force_download=force_download,
                full_dataset=full_dataset,
                force_extract=force_extract,
            ),
        )
        if not flow_professionals_state.is_successful():
            sys.exit("The professionals flow execution failed")

    if download_ts:
        flow_ts = create_ts_download_flow()
        flow_ts_state = flow_ts.run(
            parameters=dict(
                force_download=force_download,
                full_dataset=full_dataset,
            ),
        )
        if not flow_ts_state.is_successful():
            sys.exit("The ts flow execution failed")
    if extract_ts:
        flow_ts = create_ts_extract_flow()
        flow_ts_state = flow_ts.run(
            parameters=dict(
                push_to_data_gouv=push_to_data_gouv,
            ),
        )
        if not flow_ts_state.is_successful():
            sys.exit("The ts flow execution failed")


if __name__ == "__main__":
    cli()
