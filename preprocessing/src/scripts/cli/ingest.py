"""
Cli pour l'ingestion des données
"""
import sys

import click

from flows.ingestion import create_ingestion_flow
from flows.roll_back import create_roll_back_flow


@click.command()
@click.option(
    "--es-ts-ingestion/--no-es-ts-ingestion",
    default=True,
    show_default=True,
)
@click.option(
    "--es-ans-ingestion/--no-es-ans-ingestion",
    default=True,
    show_default=True,
)
@click.option(
    "--es-metadata-ingestion/--no-es-metadata-ingestion",
    default=True,
    show_default=True,
)
@click.option(
    "--pg-ts-ingestion/--no-pg-ts-ingestion",
    default=True,
    show_default=True,
)
@click.option(
    "--roll-back",
    default=False,
    is_flag=True,
    show_default=True,
)
def cli(
    es_ts_ingestion,
    es_ans_ingestion,
    es_metadata_ingestion,
    pg_ts_ingestion,
    roll_back,
):
    if roll_back:
        flow_roll_back = create_roll_back_flow()
        flow_roll_back_state = flow_roll_back.run()
        if not flow_roll_back_state.is_successful():
            sys.exit("The roll back flow execution failed")
        sys.exit()

    flow_ingestion = create_ingestion_flow()
    flow_ingestion_state = flow_ingestion.run(
        parameters=dict(
            es_ingest_ans=es_ans_ingestion,
            es_ingest_ts=es_ts_ingestion,
            es_ingest_metadata=es_metadata_ingestion,
            pg_ingest_declarations=pg_ts_ingestion,
        )
    )
    if not flow_ingestion_state.is_successful():
        sys.exit("The ingestion flow execution failed")


if __name__ == "__main__":
    cli()
