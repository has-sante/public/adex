# This scripts allows to process test data raw data
from scripts.cli.process import process_cmd

if __name__ == "__main__":
    process_cmd(
        full_dataset=False,
        force_download=False,
        force_extract=True,
        push_to_data_gouv=False,
        download_ts=True,
        extract_ts=True,
        process_professionals=False,
    )
