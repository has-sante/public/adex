"""
Definition du flow d'ingestion des données dans Elasticsearch et Postgres.

Afin de faciliter le développement, et de pouvoir lancer un seul type d'ingestion, le pipeline complet prend des paramètres en entrée :

- `es_ingest_ts` : contrôle l'ingestion dans ES des groupes de bénéficiaires
- `es_ingest_ans` : contrôle l'ingestion dans ES de l'annuaire ANS
- `es_ingest_metadata` : contrôle l'ingestion dans ES des metadonnées
- `pg_ingest_declarations` : contrôle l'ingestion dans PG des déclarations

Ces 4 booléens vont alors contrôler lancer de réelles opérations,
ou bien des tâches appelées `task_dummy_true` qui renvoient juste un booléen.

Pour générer le graph du flow:

    make preprocessing-bash
    poetry run python -c 'from flows.ingestion import create_ingestion_flow; \
                create_ingestion_flow().visualize(filename="flow_ingestion", format="dot"); \
                create_ingestion_flow().visualize(filename="flow_ingestion", format="pdf");'
"""
from prefect import Flow, Parameter, case, tags, task
from prefect.tasks.control_flow.conditional import merge

import tasks.elasticsearch_ingestion as es_ingestion
import tasks.postgres_ingestion as pg_ingestion
import tasks.set_metadata as es_metadata
from tasks.spark import task_get_spark, task_stop_spark
from utils.constants import (
    CONSTANTS_DECLARATIONS,
    CONSTANTS_METADATA,
    CONSTANTS_PROFESSIONALS,
    CONSTANTS_RECIPIENTS,
    TS_COLS,
)


@task
def task_dummy_true() -> bool:
    return True


def create_ingestion_flow() -> Flow:
    with Flow("ingestion") as flow:
        es_ingest_ts = Parameter("es_ingest_ts", default=True)
        es_ingest_ans = Parameter("es_ingest_ans", default=True)
        es_ingest_metadata = Parameter("es_ingest_metadata", default=True)
        pg_ingest_declarations = Parameter(
            "pg_ingest_declarations", default=True
        )

        with tags("ingest", "es", "ans"):
            # Insertion elasticsearch professionnels
            with case(es_ingest_ans, True):
                t_es_index_name_ans = es_ingestion.task_create_index(
                    index_prefix=CONSTANTS_PROFESSIONALS.INDEX_PREFIX,
                    p_mapping=CONSTANTS_PROFESSIONALS.P_MAPPING,
                )
                t_es_ingest_ans_ingestion = es_ingestion.task_ingest(
                    index_name=t_es_index_name_ans,
                    p_parquet=CONSTANTS_PROFESSIONALS.P_PROFESSIONALS_PARQUET_CLEAN,
                    chunk_size=200,
                    id_field=CONSTANTS_PROFESSIONALS.ID_FIELD,
                )
            with case(es_ingest_ans, False):
                t_es_ingest_ans_no_ingestion = task_dummy_true()
            t_es_ingest_ans = merge(
                t_es_ingest_ans_ingestion, t_es_ingest_ans_no_ingestion
            )

        with tags("ingest", "es", "ts"):
            # Insertion elasticsearch recipient groups
            with case(es_ingest_ts, True):
                t_es_index_name_ts = es_ingestion.task_create_index(
                    index_prefix=CONSTANTS_RECIPIENTS.INDEX_GROUPS_PREFIX,
                    p_mapping=CONSTANTS_RECIPIENTS.P_GROUPS_MAPPING,
                )
                t_es_ingest_ts_ingestion = es_ingestion.task_ingest(
                    index_name=t_es_index_name_ts,
                    p_parquet=CONSTANTS_RECIPIENTS.P_GROUPS_PARQUET,
                    chunk_size=200,
                )
            with case(es_ingest_ts, False):
                t_es_ingest_ts_no_ingestion = task_dummy_true()
            t_es_ingest_ts = merge(
                t_es_ingest_ts_ingestion, t_es_ingest_ts_no_ingestion
            )

        with tags("ingest", "es", "metadata"):
            # Insertion elasticsearch metadata
            with case(es_ingest_metadata, True):
                t_es_index_metadata_name_ingestion = (
                    es_metadata.task_set_metadata(
                        index_prefix=CONSTANTS_METADATA.INDEX_PREFIX,
                    )
                )
            with case(es_ingest_metadata, False):
                t_es_index_metadata_name_no_ingestion = task_dummy_true()
            t_es_index_metadata_name = merge(
                t_es_index_metadata_name_ingestion,
                t_es_index_metadata_name_no_ingestion,
            )

        with tags("ingest", "pg"):
            # Insertion postgres des déclarations
            with case(pg_ingest_declarations, True):
                schemas_creation = pg_ingestion.task_create_schemas()

                spark = task_get_spark(app_name="adex-ingestion")
                index_creation_tasks_ingestion = []

                table_name = "ts_declarations"
                t_declarations = pg_ingestion.task_ingest_parquet_to_table(
                    CONSTANTS_DECLARATIONS.P_PARQUET_CLEAN,
                    table_name=table_name,
                    spark=spark,
                ).set_upstream(schemas_creation)
                for column in [
                    TS_COLS.DECLARATION_ID,
                    TS_COLS.HASH,
                    TS_COLS.LIEN_INTERET,
                ]:
                    task = pg_ingestion.task_create_index_on_column(
                        table_name=table_name,
                        index_name="index_" + table_name + "_" + column,
                        column=column,
                    ).set_upstream(t_declarations)
                    index_creation_tasks_ingestion.append(task)
                stop = task_stop_spark(spark)
                stop.set_upstream(t_declarations)
            with case(pg_ingest_declarations, False):
                index_creation_tasks_no_ingestion = [
                    task_dummy_true(),
                    task_dummy_true(),
                    task_dummy_true(),
                ]

        index_creation_tasks = merge(
            index_creation_tasks_ingestion, index_creation_tasks_no_ingestion
        )

        # Rollovers
        roll_over_dependencies = [
            index_creation_tasks,
            t_es_ingest_ans,
            t_es_ingest_ts,
            t_es_index_metadata_name,
        ]
        with tags("roll_over", "es", "ans"):
            with case(es_ingest_ans, True):
                t_roll_over_aliases_ans_ingestion = (
                    es_ingestion.task_roll_over_aliases(
                        index_name=t_es_index_name_ans,
                        alias_name=CONSTANTS_PROFESSIONALS.INDEX_PREFIX,
                    ).set_dependencies(upstream_tasks=roll_over_dependencies)
                )
            with case(es_ingest_ans, False):
                t_roll_over_aliases_ans_no_ingestion = task_dummy_true()
            t_roll_over_aliases_ans = merge(
                t_roll_over_aliases_ans_ingestion,
                t_roll_over_aliases_ans_no_ingestion,
            )

        with tags("roll_over", "es", "ts"):
            with case(es_ingest_ts, True):
                t_roll_over_aliases_ts_ingestion = (
                    es_ingestion.task_roll_over_aliases(
                        index_name=t_es_index_name_ts,
                        alias_name=CONSTANTS_RECIPIENTS.INDEX_GROUPS_PREFIX,
                    ).set_dependencies(upstream_tasks=roll_over_dependencies)
                )
            with case(es_ingest_ts, False):
                t_roll_over_aliases_ts_no_ingestion = task_dummy_true()
            t_roll_over_aliases_ts = merge(
                t_roll_over_aliases_ts_ingestion,
                t_roll_over_aliases_ts_no_ingestion,
            )

        with tags("roll_over", "es", "metadata"):
            with case(es_ingest_metadata, True):
                t_roll_over_aliases_metadata_ingestion = (
                    es_ingestion.task_roll_over_aliases(
                        index_name=t_es_index_metadata_name,
                        alias_name=CONSTANTS_METADATA.INDEX_PREFIX,
                    ).set_dependencies(upstream_tasks=roll_over_dependencies)
                )
            with case(es_ingest_metadata, False):
                t_roll_over_aliases_metadata_no_ingestion = task_dummy_true()
            t_roll_over_aliases_metadata = merge(
                t_roll_over_aliases_metadata_ingestion,
                t_roll_over_aliases_metadata_no_ingestion,
            )

        with case(pg_ingest_declarations, True):
            t_roll_over_schemas_ingestion = (
                pg_ingestion.task_roll_over_schemas()
            ).set_dependencies(upstream_tasks=roll_over_dependencies)
        with case(pg_ingest_declarations, False):
            t_roll_over_schemas_no_ingestion = task_dummy_true()
        t_roll_over_schemas = merge(
            t_roll_over_schemas_ingestion, t_roll_over_schemas_no_ingestion
        )

        # Clean up
        with tags("clean", "es", "ans"):
            t_delete_dangling_indices_ans = (
                es_ingestion.task_delete_dangling_indices(
                    index_prefix=CONSTANTS_PROFESSIONALS.INDEX_PREFIX
                ).set_upstream(t_roll_over_aliases_ans)
            )
        with tags("clean", "es", "ts"):
            t_delete_dangling_indices_ts = (
                es_ingestion.task_delete_dangling_indices(
                    index_prefix=CONSTANTS_RECIPIENTS.INDEX_GROUPS_PREFIX
                ).set_upstream(t_roll_over_aliases_ts)
            )
        with tags("clean", "es", "metadata"):
            t_delete_dangling_indices_metadata = (
                es_ingestion.task_delete_dangling_indices(
                    index_prefix=CONSTANTS_METADATA.INDEX_PREFIX,
                ).set_upstream(t_roll_over_aliases_metadata)
            )

        flow.set_reference_tasks(
            [
                t_roll_over_aliases_ans,
                t_roll_over_aliases_ts,
                t_roll_over_aliases_metadata,
                t_roll_over_schemas,
                t_delete_dangling_indices_ans,
                t_delete_dangling_indices_ts,
                t_delete_dangling_indices_metadata,
            ]
        )
    return flow
