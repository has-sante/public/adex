"""
Definition du flow de traitement de l'ANS.

Comme pour le pipeline de traitement de TS, le pipeline de traitement prend 3 paramêtres permettant de contrôler
l'exécution du traitement :
- `full_dataset`
- `force_download`
- `force_extract`

Pour générer le graph du flow:

    make preprocessing-bash
    poetry run python -c 'from flows.professionals_directory import create_professional_flow; \
                create_professional_flow().visualize(filename="flow_professional_directory", format="dot"); \
                create_professional_flow().visualize(filename="flow_professional_directory", format="pdf");'

"""
from prefect import Flow, Parameter, case
from prefect.tasks.control_flow import merge

from tasks.download_extract import (
    task_copy_dev_dump,
    task_download_file_from_url,
    task_extract_with_prefix,
)
from tasks.parquet import task_csv_to_parquet
from tasks.professional_processing import task_process_professional
from tasks.spark import task_get_spark, task_stop_spark
from utils.constants import CONSTANTS_PROFESSIONALS


def create_professional_flow() -> Flow:
    with Flow("download-extract-professional-directory") as flow:
        force_download = Parameter("force_download", default=False)
        full_dataset = Parameter("full_dataset", default=False)
        force_extract = Parameter("force_extract", default=True)
        # Downloads
        with case(full_dataset, True):
            download_full = task_download_file_from_url(
                url=CONSTANTS_PROFESSIONALS.URL,
                p_output=CONSTANTS_PROFESSIONALS.P_ZIP,
                force_download=force_download,
                # Their certificate isn't valid '^^
                verify_ssl=False,
            )
        with case(full_dataset, False):
            download_small = task_copy_dev_dump(
                p_dataset=CONSTANTS_PROFESSIONALS.P_DEV_DATASET,
                p_output=CONSTANTS_PROFESSIONALS.P_ZIP,
                force_download=force_download,
            )

        download = merge(download_full, download_small)
        download.set_upstream(full_dataset)

        extract_professionnals = task_extract_with_prefix(
            p_zip=CONSTANTS_PROFESSIONALS.P_ZIP,
            prefix=CONSTANTS_PROFESSIONALS.PREFIX_PROFESSIONALS,
            p_output=CONSTANTS_PROFESSIONALS.P_PROFESSIONALS_CSV,
            force_extract=force_extract,
        )
        extract_professionnals.set_upstream(download)

        to_parquet_professionals = task_csv_to_parquet(
            p_csv=CONSTANTS_PROFESSIONALS.P_PROFESSIONALS_CSV,
            p_parquet=CONSTANTS_PROFESSIONALS.P_PROFESSIONALS_PARQUET,
            dtypes=CONSTANTS_PROFESSIONALS.DTYPES_PROFESSIONALS,
            sep=CONSTANTS_PROFESSIONALS.SEP_PROFESSIONALS,
            rename_mapping=CONSTANTS_PROFESSIONALS.COLUMN_RENAMING,
        )
        to_parquet_professionals.set_upstream(extract_professionnals)

        spark = task_get_spark(app_name="adex")
        # TODO: Refacto to use task_write_parquet outside the process_professional task
        professional_processed = task_process_professional(
            p_parquet_file=CONSTANTS_PROFESSIONALS.P_PROFESSIONALS_PARQUET,
            p_parquet_destination=CONSTANTS_PROFESSIONALS.P_PROFESSIONALS_PARQUET_CLEAN,
            spark=spark,
        ).set_upstream(to_parquet_professionals)
        t_stop = task_stop_spark(spark)
        t_stop.set_upstream(professional_processed)

        flow.set_reference_tasks([professional_processed])

    return flow
