"""
Definition du flow pour le roll back des index ES et du schema PG
"""
from prefect import Flow

from tasks.elasticsearch_ingestion import task_roll_back
from tasks.postgres_ingestion import task_roll_back_schemas
from utils.constants import (
    CONSTANTS_METADATA,
    CONSTANTS_PROFESSIONALS,
    CONSTANTS_RECIPIENTS,
)


def create_roll_back_flow() -> Flow:
    with Flow("roll-back") as flow:
        task_roll_back(CONSTANTS_RECIPIENTS.INDEX_GROUPS_PREFIX)
        task_roll_back(CONSTANTS_PROFESSIONALS.INDEX_PREFIX)
        task_roll_back(CONSTANTS_METADATA.INDEX_PREFIX)
        task_roll_back_schemas()
    return flow
