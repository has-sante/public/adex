"""
Definition du flow pour le traitement de TS.

Pour faciliter le développement, le pipeline de traitement
prend 3 paramêtres permettant de contrôler l'exécution du traitement :
- `full_dataset`: Si `False`, un petit ensemble de données de développement
                  est traité plutôt que les données officielles
- `force_download`: Si `False` et que les archives sont déjà présentes,
                    les archives ne sont pas téléchargées
- `force_extract`: Si `False` et que des fichiers extraits sont déjà présents,
                   les fichiers ne sont pas extraits des archives.

Pour générer le graph du flow:

    make preprocessing-bash
    poetry run python -c 'from flows.transparence_sante import create_ts_flow; \
                create_ts_flow().visualize(filename="flow_transparence_sante", format="dot"); \
                create_ts_flow().visualize(filename="flow_transparence_sante", format="pdf");'
"""
from prefect import Flow, Parameter, case
from prefect.executors import LocalDaskExecutor
from prefect.tasks.control_flow import merge
from prefect.tasks.shell import ShellTask

from tasks.dask import task_get_dask_client, task_stop_dask_client
from tasks.data_gouv import get_data_gouv_config, update_resource_data
from tasks.download_extract import (
    task_copy_dev_dump,
    task_download_file_and_check_integrity,
    task_get_downloads_urls_and_paths,
    task_gzip_csvs,
    task_merge_csvs,
    task_prepare_download,
)
from tasks.parquet import (
    task_csv_to_parquet,
    task_read_parquet,
    task_write_parquet,
)
from tasks.spark import task_get_spark, task_stop_spark
from tasks.ts.cleaning import task_clean_declarations
from tasks.ts.linking import task_link_declarations
from tasks.ts.matchers import task_group_recipients
from tasks.ts.migration import task_migrate_declarations
from tasks.ts.recipient_processing import task_process_recipient
from utils.constants import (
    CONSTANTS_DECLARATIONS,
    CONSTANTS_RECIPIENTS,
    CONSTANTS_TS,
)


def create_ts_download_flow() -> Flow:
    with Flow("download-transparence-sante") as flow:
        force_download = Parameter("force_download", default=False)
        full_dataset = Parameter("full_dataset", default=False)
        with case(full_dataset, True):
            should_download = task_prepare_download(
                p_split_output=CONSTANTS_TS.P_SPLIT_CSVS,
                p_output=CONSTANTS_TS.P_CSV,
                force_download=force_download,
            )
            with case(should_download, True):
                urls_and_paths = task_get_downloads_urls_and_paths(
                    base_url=CONSTANTS_TS.BASE_URL,
                    p_split_output=CONSTANTS_TS.P_SPLIT_CSVS,
                )
                download_all_files = (
                    task_download_file_and_check_integrity.map(urls_and_paths)
                )
                merge_all_csvs = task_merge_csvs(
                    p_split_output=CONSTANTS_TS.P_SPLIT_CSVS,
                    p_output=CONSTANTS_TS.P_CSV,
                )
                merge_all_csvs.set_upstream(download_all_files)
            with case(should_download, False):
                downloaded = not should_download
            download_full = merge(downloaded, merge_all_csvs)
        with case(full_dataset, False):
            download_small = task_copy_dev_dump(
                p_dataset=CONSTANTS_TS.P_DEV_CSV,
                p_output=CONSTANTS_TS.P_CSV,
                force_download=force_download,
            )

        download = merge(download_full, download_small)

        flow.set_reference_tasks([download])
    flow.executor = LocalDaskExecutor(num_workers=8)
    return flow


def create_ts_extract_flow() -> Flow:
    with Flow("extract-transparence-sante") as flow:
        push_to_data_gouv = Parameter("push_to_data_gouv", default=False)

        gzip_csv = task_gzip_csvs(
            p_merged_output=CONSTANTS_TS.P_CSV,
            p_gzip_output=CONSTANTS_TS.P_GZIP_CSV,
        )
        to_parquet_declarations = task_csv_to_parquet(
            p_csv=CONSTANTS_TS.P_CSV,
            p_parquet=CONSTANTS_DECLARATIONS.P_PARQUET_RAW,
            dtypes=CONSTANTS_DECLARATIONS.DTYPES,
            chunksize=100_000,
        )
        to_parquet_declarations.set_upstream(gzip_csv)

        t_dask_client = task_get_dask_client()
        t_dask_client.set_upstream(to_parquet_declarations)

        t_migration = task_migrate_declarations(
            p_raw_declarations=CONSTANTS_DECLARATIONS.P_PARQUET_RAW,
            p_declarations=CONSTANTS_DECLARATIONS.P_PARQUET,
        )
        t_migration.set_upstream(t_dask_client)
        t_migration.set_upstream(to_parquet_declarations)

        t_stop_dask_client = task_stop_dask_client(t_dask_client)
        t_stop_dask_client.set_upstream(t_migration)

        spark = task_get_spark(app_name="adex").set_upstream(t_migration)

        # Raw declarations
        raw_declarations = task_read_parquet(
            p_parquet_file=CONSTANTS_DECLARATIONS.P_PARQUET, spark=spark
        )

        # Cleaning declarations
        clean_declarations = task_clean_declarations(
            declarations=raw_declarations,
        )
        linked_declarations = task_link_declarations(
            declarations=clean_declarations
        )

        # Writing declarations
        write_declarations = task_write_parquet(
            linked_declarations, CONSTANTS_DECLARATIONS.P_PARQUET_CLEAN
        )

        declarations = task_read_parquet(
            p_parquet_file=CONSTANTS_DECLARATIONS.P_PARQUET_CLEAN, spark=spark
        ).set_upstream(write_declarations)

        # Recipients
        recipients = task_process_recipient(
            declarations=declarations,
        )
        write_recipients = task_write_parquet(
            recipients, p_parquet_file=CONSTANTS_RECIPIENTS.P_PARQUET
        )

        # Matchers
        recipient_groups = task_group_recipients(
            recipients=recipients,
        )
        write_recipient_groups = task_write_parquet(
            recipient_groups,
            p_parquet_file=CONSTANTS_RECIPIENTS.P_GROUPS_PARQUET,
        )

        t_stop = task_stop_spark(spark)
        t_stop.set_upstream(write_declarations)
        t_stop.set_upstream(write_recipients)
        t_stop.set_upstream(write_recipient_groups)

        flow.set_reference_tasks(
            [
                write_declarations,
                write_recipients,
                write_recipient_groups,
            ]
        )
        with case(push_to_data_gouv, True):
            data_gouv_config = get_data_gouv_config()
            t_update_resource = update_resource_data(
                data_gouv_config, CONSTANTS_TS.P_GZIP_CSV
            )
            t_update_resource.set_upstream(gzip_csv)

    flow.executor = LocalDaskExecutor(num_workers=8)
    return flow
