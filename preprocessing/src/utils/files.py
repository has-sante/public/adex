import logging
from os import path


def skip_if_path_exists(
    p_file: str, force: bool, logger: logging.Logger = None
) -> bool:
    if path.exists(p_file) and (not force):
        if logger is not None:
            logger.info("Folder {} exists. Skipping".format(p_file))
            return True
    return False
