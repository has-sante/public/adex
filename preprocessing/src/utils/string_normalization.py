"""
Utilitaires pour la normalization des chaînes de caractères.
"""
import re
import string
import unicodedata


def replace_accents(any_str: str) -> str:
    return "".join(
        c
        for c in unicodedata.normalize("NFD", any_str)
        if unicodedata.category(c) != "Mn"
    )


def remove_extra_space(any_str):
    return " ".join(any_str.split())


def non_authorized_chars_remover(any_str: str) -> str:
    replacement_char = " "
    return re.sub(
        "[^{}]".format(string.ascii_letters + replacement_char),
        replacement_char,
        any_str,
    )


def normalize(any_str: str) -> str:
    """
    Normalize les chaînes de caractères.

    Est utilisé plusieurs fois comme udf lors de l'étape de nettoyage.
    """
    if any_str is None:
        any_str = ""
        return any_str
    any_str = replace_accents(any_str)
    any_str = any_str.lower()
    any_str = non_authorized_chars_remover(any_str)
    any_str = remove_extra_space(any_str)
    return any_str
