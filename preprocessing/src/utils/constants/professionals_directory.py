"""
Constantes relatives à l'annuaire de l'ANS.
"""
from os import path

from .path import P_DATA_ROOT, P_MAPPING_ROOT

#: URL où télécharger l'archive de l'ANS
URL = "https://service.annuaire.sante.fr/annuaire-sante-webservices/V300/services/extraction/PS_LibreAcces"
#: Chemin où aller chercher le dump de test
P_DEV_DATASET = "/opt/app/scripts/resources/professional_directory.zip"

#: Chemin où télécharger l'archive
P_ZIP = path.join(P_DATA_ROOT, "raw/professionals/professional_directory.zip")

PREFIX_SKILLS = "PS_LibreAcces_SavoirFaire"
P_SKILLS_CSV = path.join(
    P_DATA_ROOT, "raw/professionals/professionals_skills.csv"
)

#: Préfix du fichier de l'ans dans l'archive téléchargé
PREFIX_PROFESSIONALS = "PS_LibreAcces_Personne_activite"
#: Chemin vers le fichier csv à extraire
P_PROFESSIONALS_CSV = path.join(
    P_DATA_ROOT, "raw/professionals/professionals.csv"
)
#: Chemin vers le fichier parquet après csv_to_parquet
P_PROFESSIONALS_PARQUET = path.join(
    P_DATA_ROOT, "parquet/professionals/professionals.parquet"
)
#: Chemin vers le fichier parquet après cleaning
P_PROFESSIONALS_PARQUET_CLEAN = path.join(
    P_DATA_ROOT, "clean/professionals/professionals.parquet"
)
#: Chemin vers le mapping elasticsearch
P_MAPPING = path.join(P_MAPPING_ROOT, "mapping-professionals.json")
#: Préfix de l'index d'elasticsearch
INDEX_PREFIX = "professionals"
ID_FIELD = "identification_nationale_pp"

#: Séparateur à utiliser pour parser le fichier csv
SEP_PROFESSIONALS = "|"

#: DTYPES pour lire le csv
DTYPES_PROFESSIONALS = {
    "Type d'identifiant PP": int,
    "Identifiant PP": str,
    "Identification nationale PP": str,
    "Code civilité d'exercice": str,
    "Libellé civilité d'exercice": str,
    "Code civilité": str,
    "Libellé civilité": str,
    "Nom d'exercice": str,
    "Prénom d'exercice": str,
    "Code profession": "Int64",
    "Libellé profession": str,
    "Code catégorie professionnelle": str,
    "Libellé catégorie professionnelle": str,
    "Code type savoir-faire": str,
    "Libellé type savoir-faire": str,
    "Code savoir-faire": str,
    "Libellé savoir-faire": str,
    "Code mode exercice": str,
    "Libellé mode exercice": str,
    "Numéro SIRET site": str,
    "Numéro SIREN site": str,
    "Numéro FINESS site": str,
    "Numéro FINESS établissement juridique": str,
    "Identifiant technique de la structure": str,
    "Raison sociale site": str,
    "Enseigne commerciale site": str,
    "Complément destinataire (coord. structure)": str,
    "Complément point géographique (coord. structure)": str,
    "Numéro Voie (coord. structure)": str,
    "Indice répétition voie (coord. structure)": str,
    "Code type de voie (coord. structure)": str,
    "Libellé type de voie (coord. structure)": str,
    "Libellé Voie (coord. structure)": str,
    "Mention distribution (coord. structure)": str,
    "Bureau cedex (coord. structure)": str,
    "Code postal (coord. structure)": str,
    "Code commune (coord. structure)": str,
    "Libellé commune (coord. structure)": str,
    "Code pays (coord. structure)": str,
    "Libellé pays (coord. structure)": str,
    "Téléphone (coord. structure)": str,
    "Téléphone 2 (coord. structure)": str,
    "Télécopie (coord. structure)": str,
    "Adresse e-mail (coord. structure)": str,
    "Code Département (structure)": str,
    "Libellé Département (structure)": str,
    "Ancien identifiant de la structure": str,
    "Autorité d'enregistrement": str,
    "Code secteur d'activité": str,
    "Libellé secteur d'activité": str,
    "Code section tableau pharmaciens": str,
    "Libellé section tableau pharmaciens": str,
}

#: Dictionnaire de correspondance pour renommer les colonnes de l'ANS
COLUMN_RENAMING = {
    "Type d'identifiant PP": "type_identifiant_pp",
    "Identifiant PP": "identifiant_pp",
    "Identification nationale PP": "identification_nationale_pp",
    "Code civilité d'exercice": "code_civilite_exercice",
    "Libellé civilité d'exercice": "libelle_civilite_exercice",
    "Code civilité": "code_civilité",
    "Libellé civilité": "libelle_civilite",
    "Nom d'exercice": "nom_exercice",
    "Prénom d'exercice": "prenom_exercice",
    "Code profession": "code_profession",
    "Libellé profession": "libelle_profession",
    "Code catégorie professionnelle": "code_categorie_professionnelle",
    "Libellé catégorie professionnelle": "libelle_categorie_professionnelle",
    "Code type savoir-faire": "code_type_savoir_faire",
    "Libellé type savoir-faire": "libelle_type_savoir_faire",
    "Code savoir-faire": "code_savoir_faire",
    "Libellé savoir-faire": "libelle_savoir_faire",
    "Code mode exercice": "code_mode_exercice",
    "Libellé mode exercice": "libelle_mode_exercice",
    "Numéro SIRET site": "numero_siret_site",
    "Numéro SIREN site": "numero_siren_site",
    "Numéro FINESS site": "numero_finess_site",
    "Numéro FINESS établissement juridique": "numero_finess_etablissement_juridique",
    "Raison sociale site": "raison_sociale_site",
    "Enseigne commerciale site": "enseigne_commerciale_site",
    "Numéro Voie (coord. structure)": "numero_voie_coord_structure",
    "Indice répétition voie (coord. structure)": "indice_repetition_voie_coord_structure",
    "Code type de voie (coord. structure)": "code_type_voie_coord_structure",
    "Libellé type de voie (coord. structure)": "libelle_type_voie_coord_structure",
    "Libellé Voie (coord. structure)": "libelle_voie_coord_structure",
    "Bureau cedex (coord. structure)": "bureau_cedex_coord_structure",
    "Code postal (coord. structure)": "code_postal_coord_structure",
    "Code commune (coord. structure)": "code_commune_coord_structure",
    "Libellé commune (coord. structure)": "libelle_commune_coord_structure",
    "Code pays (coord. structure)": "code_pays_coord_structure",
    "Libellé pays (coord. structure)": "libelle_pays_coord_structure",
    "Téléphone (coord. structure)": "telephone_coord_structure",
    "Téléphone 2 (coord. structure)": "telephone_2_coord_structure",
    "Télécopie (coord. structure)": "telecopie_coord_structure",
    "Adresse e-mail (coord. structure)": "adresse_email_coord_structure",
    "Code Département (structure)": "code_departement_structure",
    "Libellé Département (structure)": "libelle_department_structure",
    "Complément destinataire (coord. structure)": "complement_destinataire_coord_structure",
    "Complément point géographique (coord. structure)": "complement_point_geographique_coord_structure",
    "Mention distribution (coord. structure)": "mention_distribution_coord_structure",
    "Identifiant technique de la structure": "identifiant_technique_structure",
    "Ancien identifiant de la structure": "ancien_identifiant_structure",
    "Autorité d'enregistrement": "autorite_enregistrement",
    "Code secteur d'activité": "code_secteur_activite",
    "Libellé secteur d'activité": "libelle_secteur_activite",
    "Code section tableau pharmaciens": "code_section_tableau_pharmaciens",
    "Libellé section tableau pharmaciens": "libelle_section_tableau_pharmaciens",
}
