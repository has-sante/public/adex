"""
Constantes relatives aux déclarations de TS
"""
from os import path

import pyarrow as pa

import utils.constants.ts_columns as TS_COLS
import utils.constants.ts_columns_old as TS_COLS_OLD

from .path import P_DATA_ROOT

#: Chemin vers le fichier parquet raw des déclarations
P_PARQUET_RAW = path.join(P_DATA_ROOT, "parquet/ts/declarations_raw.parquet")
#: Chemin vers le fichier parquet des déclarations
P_PARQUET = path.join(P_DATA_ROOT, "parquet/ts/declarations.parquet")
#: Chemin vers le fichier parquet des déclarations après nettoyage
P_PARQUET_CLEAN = path.join(P_DATA_ROOT, "clean/ts/declarations.parquet")

DTYPES = {
    TS_COLS_OLD.ID: str,
    TS_COLS_OLD.ENTREPRISE_IDENTIFIANT: str,
    TS_COLS_OLD.TOKEN: str,
    TS_COLS_OLD.LIEN_INTERET: str,
    TS_COLS_OLD.IDENTIFIANT_UNIQUE: str,
    TS_COLS_OLD.CONVENTION_LIEE: str,
    TS_COLS_OLD.MOTIF_CODE: str,
    TS_COLS_OLD.MOTIF: str,
    TS_COLS_OLD.MOTIF_AUTRE: str,
    TS_COLS_OLD.INFO_EVENEMENT: str,
    TS_COLS_OLD.MONTANT: float,
    TS_COLS_OLD.DATE: str,
    TS_COLS_OLD.DATE_DEBUT: str,
    TS_COLS_OLD.DATE_FIN: str,
    TS_COLS_OLD.SEMESTRE: str,
    TS_COLS_OLD.DATE_PUBLICATION: str,
    TS_COLS_OLD.DATE_TRANSMISSION: str,
    TS_COLS_OLD.DEMANDE_RECTIFICATION: str,  # This column should be bool, but there are some nan values
    TS_COLS_OLD.STATUS: str,
    # BENEF
    TS_COLS_OLD.BENEF_ID: str,
    TS_COLS_OLD.BENEF_IDENTITE: str,
    TS_COLS_OLD.BENEF_PRENOM: str,
    TS_COLS_OLD.BENEF_CATEGORIE_CODE: str,
    TS_COLS_OLD.BENEF_CATEGORIE: str,
    TS_COLS_OLD.BENEF_TYPE_CODE: str,
    TS_COLS_OLD.BENEF_TYPE: str,
    TS_COLS_OLD.BENEF_IDENTIFIANT: str,
    TS_COLS_OLD.BENEF_PROFESSION_CODE: str,
    TS_COLS_OLD.BENEF_PROFESSION: str,
    TS_COLS_OLD.BENEF_NOM_COM_NAME: str,
    TS_COLS_OLD.BENEF_PAYS_NAME: str,
    TS_COLS_OLD.STRUCTURE_EXERCICE: str,
    TS_COLS_OLD.PAYS_CODE: str,
    TS_COLS_OLD.ADRESSE: str,
    TS_COLS_OLD.CODE_POSTAL: str,
    TS_COLS_OLD.VILLE: str,
    # ENTREPRISE
    TS_COLS_OLD.RAISON_SOCIALE: str,
    TS_COLS_OLD.SECTEUR_ACTIVITE: str,
    TS_COLS_OLD.ENTR_VILLE: str,
    TS_COLS_OLD.ENTR_DEP_NAME: str,
    TS_COLS_OLD.ENTR_REG_NAME: str,
    TS_COLS_OLD.ENTR_PAYS: str,
    TS_COLS_OLD.MERE_ID: str,
    TS_COLS_OLD.CODE_POSTAL_ENTREPRISE: str,
    TS_COLS_OLD.SIREN: str,
    # GEO
    TS_COLS_OLD.VILLE_SANS_CEDEX: str,
    TS_COLS_OLD.GEOM: str,
    TS_COLS_OLD.COMMUNE_NAME: str,
    TS_COLS_OLD.COMMUNE_CODE: str,
    TS_COLS_OLD.EPCI_NAME: str,
    TS_COLS_OLD.EPCI_CODE: str,
    TS_COLS_OLD.DEP_CODE: str,
    TS_COLS_OLD.DEPEPCI_NAME: str,
    TS_COLS_OLD.REG_NAME: str,
    TS_COLS_OLD.REG_CODE: str,
}

#: Schema du fichier des déclarations après migration
PARQUET_SCHEMA = pa.schema(
    [
        (TS_COLS.COMPANY_ID, pa.string()),
        (TS_COLS.COMPANY_NAME, pa.string()),
        (TS_COLS.IDENTIFIANT_UNIQUE, pa.string()),
        (TS_COLS.CATEGORIE_CODE, pa.string()),
        (TS_COLS.CATEGORIE_LIBELLE, pa.string()),
        (TS_COLS.PRENOM, pa.string()),
        (TS_COLS.PROFESSION, pa.string()),
        (TS_COLS.PAYS_CODE, pa.string()),
        (TS_COLS.PAYS, pa.string()),
        (TS_COLS.TYPE_IDENTIFIANT, pa.string()),
        (TS_COLS.NUMERO_IDENTIFIANT, pa.string()),
        (TS_COLS.STRUCTURE_EXERCICE, pa.string()),
        (TS_COLS.DATE, pa.string()),
        (TS_COLS.MOTIF, pa.string()),
        (TS_COLS.MOTIF_AUTRE, pa.string()),
        (TS_COLS.DATE_DEBUT, pa.string()),
        (TS_COLS.DATE_FIN, pa.string()),
        (TS_COLS.MONTANT, pa.float64()),
        (TS_COLS.IDENTITY, pa.string()),
        (TS_COLS.CODE_POSTAL, pa.string()),
        (TS_COLS.ADRESSE, pa.string()),
        (TS_COLS.VILLE, pa.string()),
        (TS_COLS.INFO_CONVENTION, pa.string()),
        (TS_COLS.LIEN_INTERET, pa.string()),
        (TS_COLS.CONVENTION_LIEE, pa.string()),
    ]
)
