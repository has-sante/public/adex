"""
Constantes relatives aux bénéficiaires (et groupes) de TS.
"""
from os import path

from .path import P_DATA_ROOT, P_MAPPING_ROOT

#: Chemin vers le fichier parquet après extraction des bénéficiaires
P_PARQUET = path.join(P_DATA_ROOT, "prepared/recipients/recipients")
#: Chemin vers le fichier parquet pour les groupes de bénéficiaires
P_GROUPS_PARQUET = path.join(
    P_DATA_ROOT, "prepared/recipients/recipients_groups"
)

#: Préfix de l'index d'elasticsearch
INDEX_GROUPS_PREFIX = "recipients-groups"
#: Chemin vers le mapping elasticsearch
P_GROUPS_MAPPING = path.join(P_MAPPING_ROOT, "mapping-recipients-groups.json")
