"""
Constantes relatives aux métadonnées.
"""

#: Préfix de l'index d'elasticsearch
INDEX_PREFIX = "info"
