"""
Les constantes utilisées dans le projet.
"""

from . import (
    declarations as CONSTANTS_DECLARATIONS,
    metadata as CONSTANTS_METADATA,
    professionals_directory as CONSTANTS_PROFESSIONALS,
    recipients as CONSTANTS_RECIPIENTS,
    transparence_sante as CONSTANTS_TS,
    ts_columns as TS_COLS,
    ts_columns_old as TS_COLS_OLD,
)
