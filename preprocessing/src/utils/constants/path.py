"""
Chemins vers les mappings elasticsearch et la racine des fichiers à traiter
"""
import os

import fsspec

from utils.settings import STORAGE_MODE

#: Dossier où est installé ADEX
P_ADEX_APP = "/opt/app"

#: Dossier où seront créés les données (controlable via l'environnement)
P_DATA_ROOT = os.getenv("P_DATA_ROOT", "/data")
STORAGE_OPTS = {}

if STORAGE_MODE == "remote":
    P_DATA_ROOT = os.path.join("adex", P_DATA_ROOT)
    MINIO_ACCESS_KEY = os.getenv("MINIO_ACCESS_KEY")
    MINIO_SECRET_KEY = os.getenv("MINIO_SECRET_KEY")
    MINIO_URL = os.getenv("MINIO_URL")
    STORAGE_OPTS = {
        "key": MINIO_ACCESS_KEY,
        "secret": MINIO_SECRET_KEY,
        "client_kwargs": {"endpoint_url": MINIO_URL},
        "config_kwargs": {
            # minio is slow on deletion 15 minutes timeout
            "read_timeout": 15
            * 60,
        },
    }

    FS = fsspec.filesystem(
        "simplecache",
        target_protocol="s3",
        target_options=STORAGE_OPTS,
        cache_storage=os.path.join(".data-cache"),
    )
else:
    FS = fsspec.filesystem("file")

#: Dossier où sont stockés les mappings utilisés pour l'ingestion dans Elasticsearch
P_MAPPING_ROOT = os.path.join(P_ADEX_APP, "resources")
