"""
Constantes relatives aux déclarations de TS.
"""
import enum
from os import path

import utils.constants.ts_columns as TS_COLS

from .path import P_ADEX_APP, P_DATA_ROOT

#: URL où télécharger l'archive de TS
URL = "https://www.data.gouv.fr/fr/datasets/r/b65292c7-eb10-47e4-b29f-7fd229a981b1"

BASE_URL = "https://www.transparence.sante.gouv.fr/explore/dataset/declarations/download/"
#: Chemin vers le fichier csv de dev
P_DEV_CSV = path.join(P_ADEX_APP, "scripts", "resources", "declarations.csv")

#: Chemin vers le fichier csv des déclarations téléchargées et fusionnées
P_SPLIT_CSVS = path.join(P_DATA_ROOT, "raw/ts/split/")
P_CSV = path.join(P_DATA_ROOT, "raw/ts/declarations.csv")
P_GZIP_CSV = path.join(P_DATA_ROOT, "raw/ts/declarations.csv.gz")


class DeclarationType(str, enum.Enum):
    AVANTAGE = "A"
    CONVENTION = "C"
    REMUNERATION = "R"


#: Toutes les colonnes relatives à l'identité d'un bénéficiaire
BENEF_COLUMNS = [
    TS_COLS.IDENTITY,
    TS_COLS.PRENOM,
    TS_COLS.ADRESSE,
    TS_COLS.STRUCTURE_EXERCICE,
    TS_COLS.PAYS_CODE,
    TS_COLS.PAYS,
    TS_COLS.NUMERO_IDENTIFIANT,
    TS_COLS.TYPE_IDENTIFIANT,
    TS_COLS.PROFESSION,
    TS_COLS.CODE_POSTAL,
    TS_COLS.VILLE,
    TS_COLS.CATEGORIE_CODE,
    TS_COLS.CATEGORIE_LIBELLE,
]

#: Les identifiants (type, valeur) considérés comme nuls
NAN_IDS = [
    ("RPPS/ADELI", "00000000000"),
    ("RPPS/ADELI", "10000000000"),
    ("RPPS/ADELI", "[SO]"),
    ("RPPS/ADELI", "0"),
    ("RPPS/ADELI", "99999999999"),
    ("RPPS/ADELI", "INFORMATION NON DISPONIBLE"),
    ("RPPS/ADELI", "MANIPULATEUR EN RADIOLOGIE"),
    ("ORDRE", "[SO]"),
    ("ORDRE", "0"),
    ("ORDRE", "10000"),
    ("ORDRE", "SO"),
    ("ORDRE", "Ordre des Infirmiers"),
    ("ORDRE", "INFIRMIER"),
    ("ORDRE", "CNOM"),
    ("ORDRE", "ORDRE DES INFIRMIERS"),
    ("ORDRE", "IDE"),
    ("ORDRE", "Infirmier"),
    ("ORDRE", "INTERNE"),
    ("ORDRE", "Ordre des infirmiers"),
    ("ORDRE", "Masseur Kinésithérapeute"),
    ("ORDRE", "-"),
    ("ORDRE", "10000000001"),
    ("ORDRE", "SAGE FEMME"),
    ("ORDRE", "00000000000"),
    ("ORDRE", "WFRR00120014"),
    ("SIREN", "999999999"),
    ("SIREN", "000000000"),
]
