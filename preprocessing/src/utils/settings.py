"""
Utilitaire pour récupérer les credentials pour ES
"""
import os
import warnings
from typing import Tuple


def get_elasticsearch_auth() -> Tuple[str, str]:
    user = os.getenv("ELASTICSEARCH_USER")
    pwd = os.getenv("ELASTICSEARCH_PWD")
    if not user:
        warnings.warn("Env variable ELASTICSEARCH_USER not defined")
    if not pwd:
        warnings.warn("Env variable ELASTICSEARCH_PWD not defined")
    return (user, pwd)


#: Host du serveur ES
ELASTICSEARCH_HOST = os.getenv("ELASTICSEARCH_SERVER", "elasticsearch")
#: Credentials du serveur ES
ELASTICSEARCH_AUTH = get_elasticsearch_auth()

# Can be "local" (disk) or "remote" (s3)
STORAGE_MODE = os.getenv("STORAGE_MODE", "local")
