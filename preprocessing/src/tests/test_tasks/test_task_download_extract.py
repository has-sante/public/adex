from unittest.mock import MagicMock

import pytest
from pytest_mock import MockerFixture

from tasks.download_extract import (
    task_download_file_from_url,
    task_extract_with_prefix,
)


def patch_download_file_from_url(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("tasks.download_extract.download_file_from_url")


def patch_extract_with_prefix(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("tasks.download_extract.extract_with_prefix")


def patch_file_exists(
    file_exists: bool,
    mocker: MockerFixture,
) -> MagicMock:
    return mocker.patch(
        "tasks.download_extract.FS.exists", return_value=file_exists
    )


@pytest.fixture
def url() -> str:
    return "https://fake"


@pytest.fixture
def p_zip() -> str:
    return "/tmp/tests"


@pytest.fixture
def prefix() -> str:
    return "prefix_"


@pytest.fixture
def p_output() -> str:
    return "/tmp/test"


@pytest.mark.parametrize("file_exists", [False, True])
@pytest.mark.parametrize("force_download", [False, True])
def test_force_download_no_file(
    file_exists: bool,
    force_download: bool,
    mocker: MockerFixture,
    url: str,
    p_output: str,
):
    patch = patch_download_file_from_url(mocker)
    patch_file_exists(file_exists=file_exists, mocker=mocker)
    task_download_file_from_url.run(
        url=url, p_output=p_output, force_download=force_download
    )
    if force_download or not file_exists:
        patch.assert_called_once_with(url, p_output, verify_ssl=True)
    else:
        patch.assert_not_called()


@pytest.mark.parametrize("file_exists", [False, True])
@pytest.mark.parametrize("force_extract", [False, True])
def test_force_extract_no_file(
    file_exists: bool,
    force_extract: bool,
    mocker: MockerFixture,
    p_zip: str,
    prefix: str,
    p_output: str,
):
    patch = patch_extract_with_prefix(mocker)
    patch_file_exists(file_exists=file_exists, mocker=mocker)
    task_extract_with_prefix.run(
        p_zip=p_zip,
        prefix=prefix,
        p_output=p_output,
        force_extract=force_extract,
    )
    if force_extract or not file_exists:
        patch.assert_called_once_with(
            p_zip=p_zip, prefix=prefix, p_output=p_output
        )
    else:
        patch.assert_not_called()
