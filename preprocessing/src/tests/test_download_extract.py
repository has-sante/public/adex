import zipfile

from functions.download_extract import (
    extract_file,
    extract_with_prefix,
    get_filename_from_prefix,
)


def create_zip(p_zip_dir):
    p_zip = p_zip_dir / "test.zip"
    with zipfile.ZipFile(p_zip, "w") as zipf:
        zipf.writestr("file.txt", "file")
        zipf.writestr("prefixed_file.txt", "prefixed")
    return p_zip


def test_extract_file(tmp_path):
    p_zip = create_zip(tmp_path)

    with zipfile.ZipFile(p_zip, "r") as zipf:
        filename = "file.txt"
        p_output = tmp_path / "file_output.txt"
        extract_file(filename=filename, p_output=str(p_output), zip_file=zipf)
        assert p_output.read_text() == "file"


def test_get_filename_from_prefix(tmp_path):
    p_zip = create_zip(tmp_path)

    with zipfile.ZipFile(p_zip, "r") as zipf:
        prefix = "prefixed"
        filename = get_filename_from_prefix(prefix=prefix, zip_file=zipf)
        assert filename == "prefixed_file.txt"


def test_extract_with_prefix(tmp_path):
    p_zip = create_zip(tmp_path)

    prefix = "prefixed"
    p_output = tmp_path / "file_output.txt"
    extract_with_prefix(p_zip, prefix=prefix, p_output=str(p_output))
    assert p_output.read_text() == "prefixed"
