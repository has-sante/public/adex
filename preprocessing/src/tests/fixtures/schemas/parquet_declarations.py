from pyspark.sql.types import FloatType, StringType, StructField, StructType

from utils.constants import TS_COLS

PARQUET_SCHEMA = StructType(
    [
        StructField(TS_COLS.COMPANY_ID, StringType(), True),
        StructField(TS_COLS.COMPANY_NAME, StringType(), True),
        StructField(TS_COLS.IDENTIFIANT_UNIQUE, StringType(), True),
        StructField(TS_COLS.CATEGORIE_CODE, StringType(), True),
        StructField(TS_COLS.CATEGORIE_LIBELLE, StringType(), True),
        StructField(TS_COLS.PRENOM, StringType(), True),
        StructField(TS_COLS.PROFESSION, StringType(), True),
        StructField(TS_COLS.PAYS_CODE, StringType(), True),
        StructField(TS_COLS.PAYS, StringType(), True),
        StructField(TS_COLS.TYPE_IDENTIFIANT, StringType(), True),
        StructField(TS_COLS.NUMERO_IDENTIFIANT, StringType(), True),
        StructField(TS_COLS.STRUCTURE_EXERCICE, StringType(), True),
        StructField(TS_COLS.DATE, StringType(), True),
        StructField(TS_COLS.MOTIF, StringType(), True),
        StructField(TS_COLS.MOTIF_AUTRE, StringType(), True),
        StructField(TS_COLS.DATE_DEBUT, StringType(), True),
        StructField(TS_COLS.DATE_FIN, StringType(), True),
        StructField(TS_COLS.MONTANT, FloatType(), True),
        StructField(TS_COLS.IDENTITY, StringType(), True),
        StructField(TS_COLS.CODE_POSTAL, StringType(), True),
        StructField(TS_COLS.ADRESSE, StringType(), True),
        StructField(TS_COLS.VILLE, StringType(), True),
        StructField(TS_COLS.INFO_CONVENTION, StringType(), True),
        StructField(TS_COLS.LIEN_INTERET, StringType(), True),
        StructField(TS_COLS.CONVENTION_LIEE, StringType(), True),
    ]
)
