from datetime import date

from tests.conftest import get_test_spark
from tests.fixtures.schemas.clean_declarations import CLEAN_SCHEMA
from tests.fixtures.schemas.parquet_declarations import PARQUET_SCHEMA
from utils.constants import TS_COLS

DECLARATIONS_DATA = [
    {
        "src": {
            # Names
            TS_COLS.IDENTITY: "  #SulaT1 ",
            TS_COLS.PRENOM: " BeNeDicte#  ",
            # Declaration id
            TS_COLS.COMPANY_ID: "entreprise_1",
            TS_COLS.LIEN_INTERET: "avantage",
            TS_COLS.CONVENTION_LIEE: "127",
            TS_COLS.IDENTIFIANT_UNIQUE: "128",
            # Benef id
            TS_COLS.TYPE_IDENTIFIANT: "RPPS/ADELI",
            TS_COLS.NUMERO_IDENTIFIANT: " 10000012345  ",
            # Dates
            TS_COLS.DATE: "2020-03-14",
            TS_COLS.DATE_DEBUT: "2020-03-15",
            TS_COLS.DATE_FIN: "2020-03-16",
        },
        "dst": {
            # Names
            TS_COLS.IDENTITY: "  #SulaT1 ",
            TS_COLS.PRENOM: " BeNeDicte#  ",
            TS_COLS.BENEFICIARY_IDENTITY_NORMALIZED: "SULAT",
            TS_COLS.BENEFICIARY_PRENOM_NORMALIZED: "Benedicte",
            # Declaration id
            TS_COLS.COMPANY_ID: "entreprise_1",
            TS_COLS.LIEN_INTERET: "avantage",
            TS_COLS.CONVENTION_LIEE: "127",
            TS_COLS.IDENTIFIANT_UNIQUE: "128",
            TS_COLS.DECLARATION_ID: "avantage | entreprise_1 | 128",
            TS_COLS.CONVENTION_ID: "convention | entreprise_1 | 127",
            # Benef id
            TS_COLS.TYPE_IDENTIFIANT: "RPPS/ADELI",
            TS_COLS.NUMERO_IDENTIFIANT: "10000012345",
            # Dates
            TS_COLS.DATE: date(2020, 3, 14),
            TS_COLS.DATE_DEBUT: date(2020, 3, 15),
            TS_COLS.DATE_FIN: date(2020, 3, 16),
            # Other
            TS_COLS.MONTANT: 0.0,
        },
    },
]


def get_parquet_declarations(declarations_data, spark):
    data = [r["src"] for r in declarations_data]
    return spark.createDataFrame(data=data, schema=PARQUET_SCHEMA)


def get_clean_declarations(declarations_data, spark):
    data = [r["dst"] for r in declarations_data]
    return spark.createDataFrame(data=data, schema=CLEAN_SCHEMA)


PARQUET_DECLARATIONS = get_parquet_declarations(
    DECLARATIONS_DATA, get_test_spark()
)
CLEAN_DECLARATIONS = get_clean_declarations(
    DECLARATIONS_DATA, get_test_spark()
)
