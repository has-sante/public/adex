from tests.conftest import get_test_spark
from utils.constants import TS_COLS

DICT_DECLARATIONS_ID = [
    {
        TS_COLS.COMPANY_ID: "e_1",
        TS_COLS.LIEN_INTERET: "convention",
        TS_COLS.IDENTIFIANT_UNIQUE: "c_1",
        TS_COLS.DECLARATION_ID: "convention | e_1 | c_1",
    },
    {
        TS_COLS.COMPANY_ID: "e_1",
        TS_COLS.LIEN_INTERET: "avantage",
        TS_COLS.IDENTIFIANT_UNIQUE: "a_1",
        TS_COLS.DECLARATION_ID: "avantage | e_1 | a_1",
    },
    {
        TS_COLS.COMPANY_ID: "e_1",
        TS_COLS.LIEN_INTERET: "remuneration",
        TS_COLS.IDENTIFIANT_UNIQUE: "r_1",
        TS_COLS.DECLARATION_ID: "remuneration | e_1 | r_1",
    },
]


def get_df_declarations_id(declarations_id_data, spark):
    data = [
        (
            r[TS_COLS.IDENTIFIANT_UNIQUE],
            r[TS_COLS.COMPANY_ID],
            r[TS_COLS.LIEN_INTERET],
        )
        for r in declarations_id_data
    ]
    declarations_id_df = spark.createDataFrame(
        data,
        [TS_COLS.IDENTIFIANT_UNIQUE, TS_COLS.COMPANY_ID, TS_COLS.LIEN_INTERET],
    )
    return declarations_id_df


DF_DECLARATIONS_ID = get_df_declarations_id(
    DICT_DECLARATIONS_ID, get_test_spark()
)
