from tests.conftest import get_test_spark
from utils.constants import TS_COLS

DICT_CLEAN_NAMES = [
    # Simple case, Upper case on name, InitCap on surname
    {
        TS_COLS.IDENTITY: "Bruno",
        TS_COLS.PRENOM: "BeneDicte",
        TS_COLS.BENEFICIARY_IDENTITY_NORMALIZED: "BRUNO",
        TS_COLS.BENEFICIARY_PRENOM_NORMALIZED: "Benedicte",
    },
    # Ascii folding
    {
        TS_COLS.IDENTITY: "@BrUno  ",
        TS_COLS.PRENOM: "BEnéDicte-MariE",
        TS_COLS.BENEFICIARY_IDENTITY_NORMALIZED: "BRUNO",
        TS_COLS.BENEFICIARY_PRENOM_NORMALIZED: "Benedicte Marie",
    },
]


def get_df_names(dict_clean_names, spark):
    data = [(r[TS_COLS.IDENTITY], r[TS_COLS.PRENOM]) for r in dict_clean_names]
    names_df = spark.createDataFrame(
        data,
        [TS_COLS.IDENTITY, TS_COLS.PRENOM],
    )
    return names_df


DF_NAMES = get_df_names(DICT_CLEAN_NAMES, get_test_spark())
