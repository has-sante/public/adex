from tests.conftest import get_test_spark
from utils.constants import TS_COLS

DICT_CONVENTIONS_LIEES_ID = [
    {
        TS_COLS.COMPANY_ID: "e_1",
        TS_COLS.LIEN_INTERET: "convention",
        TS_COLS.DECLARATION_ID: "id_unique_convention",
        TS_COLS.CONVENTION_LIEE: None,
        TS_COLS.CONVENTION_ID: "id_unique_convention",
    },
    {
        TS_COLS.COMPANY_ID: "e_1",
        TS_COLS.LIEN_INTERET: "avantage",
        TS_COLS.DECLARATION_ID: "avantage | e_1 | a_1",
        TS_COLS.CONVENTION_LIEE: "c_2",
        TS_COLS.CONVENTION_ID: "convention | e_1 | c_2",
    },
    {
        TS_COLS.COMPANY_ID: "e_1",
        TS_COLS.LIEN_INTERET: "remuneration",
        TS_COLS.DECLARATION_ID: "remuneration | e_1 | r_1",
        TS_COLS.CONVENTION_LIEE: "c_3",
        TS_COLS.CONVENTION_ID: "convention | e_1 | c_3",
    },
    {
        TS_COLS.COMPANY_ID: "e_2",
        TS_COLS.LIEN_INTERET: "remuneration",
        TS_COLS.DECLARATION_ID: "remuneration | e_2 | r_1",
        TS_COLS.CONVENTION_LIEE: "",
        TS_COLS.CONVENTION_ID: None,
    },
    {
        TS_COLS.COMPANY_ID: "e_3",
        TS_COLS.LIEN_INTERET: "remuneration",
        TS_COLS.DECLARATION_ID: "remuneration | e_3 | r_1",
        TS_COLS.CONVENTION_LIEE: None,
        TS_COLS.CONVENTION_ID: None,
    },
]


def get_df_conventions_liees_id(dict_conventions_liees_id, spark):
    data = [
        (
            r[TS_COLS.COMPANY_ID],
            r[TS_COLS.LIEN_INTERET],
            r[TS_COLS.DECLARATION_ID],
            r[TS_COLS.CONVENTION_LIEE],
            r[TS_COLS.CONVENTION_ID],
        )
        for r in dict_conventions_liees_id
    ]
    conventions_liees_id_df = spark.createDataFrame(
        data,
        [
            TS_COLS.COMPANY_ID,
            TS_COLS.LIEN_INTERET,
            TS_COLS.DECLARATION_ID,
            TS_COLS.CONVENTION_LIEE,
            TS_COLS.CONVENTION_ID,
        ],
    )
    return conventions_liees_id_df


DF_CONVENTIONS_LIEES_ID = get_df_conventions_liees_id(
    DICT_CONVENTIONS_LIEES_ID, get_test_spark()
)
