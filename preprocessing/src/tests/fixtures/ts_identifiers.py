from tests.conftest import get_test_spark
from utils.constants import TS_COLS

DICT_IDENTIFIERS = [
    # Keep intact
    {
        TS_COLS.TYPE_IDENTIFIANT: "RPPS/ADELI",
        f"src_{TS_COLS.NUMERO_IDENTIFIANT}": "10000012345",
        f"dst_{TS_COLS.NUMERO_IDENTIFIANT}": "10000012345",
    },
    # Fill ""
    {
        TS_COLS.TYPE_IDENTIFIANT: "RPPS/ADELI",
        f"src_{TS_COLS.NUMERO_IDENTIFIANT}": "",
        f"dst_{TS_COLS.NUMERO_IDENTIFIANT}": "",
    },
    {
        TS_COLS.TYPE_IDENTIFIANT: "RPPS/ADELI",
        f"src_{TS_COLS.NUMERO_IDENTIFIANT}": None,
        f"dst_{TS_COLS.NUMERO_IDENTIFIANT}": "",
    },
    # Trim
    {
        TS_COLS.TYPE_IDENTIFIANT: "RPPS/ADELI",
        f"src_{TS_COLS.NUMERO_IDENTIFIANT}": " 10000012345  ",
        f"dst_{TS_COLS.NUMERO_IDENTIFIANT}": "10000012345",
    },
    # Ordre
    {
        TS_COLS.TYPE_IDENTIFIANT: "ORDRE",
        f"src_{TS_COLS.NUMERO_IDENTIFIANT}": " 1234  ",
        f"dst_{TS_COLS.NUMERO_IDENTIFIANT}": "1234",
    },
    # AUTRE
    {
        TS_COLS.TYPE_IDENTIFIANT: "AUTRE",
        f"src_{TS_COLS.NUMERO_IDENTIFIANT}": "  120 ",
        f"dst_{TS_COLS.NUMERO_IDENTIFIANT}": "120",
    },
    # FINESS
    {
        TS_COLS.TYPE_IDENTIFIANT: "FINESS",
        f"src_{TS_COLS.NUMERO_IDENTIFIANT}": " 123545 ",
        f"dst_{TS_COLS.NUMERO_IDENTIFIANT}": "123545",
    },
    # SIREN
    {
        TS_COLS.TYPE_IDENTIFIANT: "SIREN",
        f"src_{TS_COLS.NUMERO_IDENTIFIANT}": " 123545 ",
        f"dst_{TS_COLS.NUMERO_IDENTIFIANT}": "123545",
    },
    # Clean and fill with Nans
    {
        TS_COLS.TYPE_IDENTIFIANT: "ORDRE",
        f"src_{TS_COLS.NUMERO_IDENTIFIANT}": "0",
        f"dst_{TS_COLS.NUMERO_IDENTIFIANT}": "",
    },
    {
        TS_COLS.TYPE_IDENTIFIANT: "RPPS/ADELI",
        f"src_{TS_COLS.NUMERO_IDENTIFIANT}": "10000000000",
        f"dst_{TS_COLS.NUMERO_IDENTIFIANT}": "",
    },
]


def get_df_identifiers(dict_clean_identifiers, spark):
    data = [
        (r[TS_COLS.TYPE_IDENTIFIANT], r[f"src_{TS_COLS.NUMERO_IDENTIFIANT}"])
        for r in dict_clean_identifiers
    ]
    identifiers_df = spark.createDataFrame(
        data,
        [TS_COLS.TYPE_IDENTIFIANT, TS_COLS.NUMERO_IDENTIFIANT],
    )
    return identifiers_df


def get_dict_clean_identifiers(clean_identifiers_data):
    return [
        {
            TS_COLS.TYPE_IDENTIFIANT: r[TS_COLS.TYPE_IDENTIFIANT],
            TS_COLS.NUMERO_IDENTIFIANT: r[f"dst_{TS_COLS.NUMERO_IDENTIFIANT}"],
        }
        for r in clean_identifiers_data
    ]


DF_IDENTIFIERS = get_df_identifiers(DICT_IDENTIFIERS, get_test_spark())
DICT_CLEAN_IDENTIFIERS = get_dict_clean_identifiers(DICT_IDENTIFIERS)
