import logging

import pytest
from dask.distributed import Client

from functions.spark import get_spark


def suppress_py4j_logging():
    logger = logging.getLogger("py4j")
    logger.setLevel(logging.WARN)


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    report = outcome.get_result()

    test_fn = item.obj
    docstring = getattr(test_fn, "__doc__")
    if docstring:
        report.nodeid = docstring


def get_test_spark():
    return get_spark(app_name="tests", number_cores=3, memory_gb=1)


@pytest.fixture(scope="session")
def spark():
    return get_test_spark()


@pytest.fixture(scope="session")
def dask_client(n_workers=4):
    return Client(host="0.0.0.0", n_workers=n_workers)
