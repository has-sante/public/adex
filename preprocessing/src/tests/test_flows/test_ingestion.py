from typing import List
from unittest.mock import Mock

import prefect
import pytest
from prefect.tasks.core.function import FunctionTask
from pytest_mock import MockerFixture, MockFixture

from flows.ingestion import create_ingestion_flow


def mock_task_by_name_and_tags(
    flow: prefect.Flow, task_name: str, tags: List[str], mocker: MockFixture
) -> None:
    mocker.patch.object(
        flow.get_tasks(name=task_name, tags=tags)[0],
        "run",
    )


@pytest.mark.parametrize(
    "es_ingest_ts",
    [False, True],
)
@pytest.mark.parametrize(
    "es_ingest_ans",
    [False, True],
)
@pytest.mark.parametrize("es_ingest_metadata", [False, True])
@pytest.mark.parametrize(
    "pg_ingest_declarations",
    [False, True],
)
def test_ingestion_pipeline(
    es_ingest_ts: bool,
    es_ingest_ans: bool,
    es_ingest_metadata: bool,
    pg_ingest_declarations: bool,
    mocker: MockerFixture,
):
    """
    Ce test va d'abord patcher toutes les méthodes `run` des tâches
    du flow.

    Puis, en fonction des différents paramêtres en entrée va
    vérifier que les méthodes sont bien appelées.
    """

    flow = create_ingestion_flow()
    for task in flow.get_tasks():
        # On ne veut pas effacer le comportement de toutes les tâches de type
        # case / merge, et donc ne patcher que les tâches de type FunctionTask
        if isinstance(task, FunctionTask):
            mocker.patch.object(task, "run")

    flow.run(
        parameters=dict(
            es_ingest_ts=es_ingest_ts,
            es_ingest_ans=es_ingest_ans,
            es_ingest_metadata=es_ingest_metadata,
            pg_ingest_declarations=pg_ingest_declarations,
        )
    )

    # Ingestion ans dans ElasticSearch
    if es_ingest_ans:
        flow.get_tasks(name="task_create_index", tags=["ingest", "es", "ans"])[
            0
        ].run.assert_called_once()
        flow.get_tasks(name="task_ingest", tags=["ingest", "es", "ans"])[
            0
        ].run.assert_called_once()
        flow.get_tasks(
            name="task_roll_over_aliases", tags=["roll_over", "es", "ans"]
        )[0].run.assert_called_once()
    else:
        flow.get_tasks(name="task_create_index", tags=["ingest", "es", "ans"])[
            0
        ].run.assert_not_called()
        flow.get_tasks(name="task_ingest", tags=["ingest", "es", "ans"])[
            0
        ].run.assert_not_called()
        flow.get_tasks(
            name="task_roll_over_aliases", tags=["roll_over", "es", "ans"]
        )[0].run.assert_not_called()

    # Ingestion ts dans ElasticSearch
    if es_ingest_ts:
        flow.get_tasks(name="task_create_index", tags=["ingest", "es", "ts"])[
            0
        ].run.assert_called_once()
        flow.get_tasks(name="task_ingest", tags=["ingest", "es", "ts"])[
            0
        ].run.assert_called_once()
        flow.get_tasks(
            name="task_roll_over_aliases", tags=["roll_over", "es", "ts"]
        )[0].run.assert_called_once()
    else:
        flow.get_tasks(name="task_create_index", tags=["ingest", "es", "ts"])[
            0
        ].run.assert_not_called()
        flow.get_tasks(name="task_ingest", tags=["ingest", "es", "ts"])[
            0
        ].run.assert_not_called()
        flow.get_tasks(
            name="task_roll_over_aliases", tags=["roll_over", "es", "ts"]
        )[0].run.assert_not_called()

    # Ingestion metadata dans ElasticSearch
    if es_ingest_metadata:
        flow.get_tasks(
            name="task_set_metadata", tags=["ingest", "es", "metadata"]
        )[0].run.assert_called_once()
        flow.get_tasks(
            name="task_roll_over_aliases", tags=["roll_over", "es", "metadata"]
        )[0].run.assert_called_once()
    else:
        flow.get_tasks(
            name="task_set_metadata", tags=["ingest", "es", "metadata"]
        )[0].run.assert_not_called()
        flow.get_tasks(
            name="task_roll_over_aliases", tags=["roll_over", "es", "metadata"]
        )[0].run.assert_not_called()

    # Ingestion dans postgres
    if pg_ingest_declarations:
        flow.get_tasks(name="task_create_schemas")[0].run.assert_called_once()
        flow.get_tasks(name="task_get_spark")[0].run.assert_called_once()
        flow.get_tasks(name="task_ingest_parquet_to_table")[
            0
        ].run.assert_called_once()
        flow.get_tasks(name="task_stop_spark")[0].run.assert_called_once()
        for task in flow.get_tasks(name="task_create_index_on_column"):
            task.run.assert_called_once()
        flow.get_tasks(name="task_roll_over_schemas")[
            0
        ].run.assert_called_once()
    else:
        flow.get_tasks(name="task_create_schemas")[0].run.assert_not_called()
        flow.get_tasks(name="task_get_spark")[0].run.assert_not_called()
        flow.get_tasks(name="task_ingest_parquet_to_table")[
            0
        ].run.assert_not_called()
        flow.get_tasks(name="task_stop_spark")[0].run.assert_not_called()
        for task in flow.get_tasks(name="task_create_index_on_column"):
            task.run.assert_not_called()
        flow.get_tasks(name="task_roll_over_schemas")[
            0
        ].run.assert_not_called()

    # Dangling indices
    for dataset in ["ans", "ts", "metadata"]:
        flow.get_tasks(
            name="task_delete_dangling_indices", tags=["clean", "es", dataset]
        )[0].run.assert_called_once()
