from datetime import date

from chispa import assert_df_equality
from pyspark.sql.types import StringType, StructField, StructType

from functions.dataframe_operations import (
    create_nested_data_column,
    parse_date,
    rename_parquet,
)


def test_rename_parquet(spark):
    data = [
        ("v1", "v2"),
    ]
    df = spark.createDataFrame(data, ["c1", "c2"])
    df = rename_parquet(df, {"c1": "c1_new"})
    assert set(df.columns) == set(["c2", "c1_new"])


def test_nested_data_column(spark):
    dataframe = spark.createDataFrame([("1", "2")], ["c1", "c2"])
    nested_column_name = "nested_column"
    nested_schema = StructType(
        [
            StructField("c1", StringType(), True),
            StructField("c2", StringType(), True),
        ]
    )
    schema = StructType([StructField(nested_column_name, nested_schema, True)])

    expected_dataframe = spark.createDataFrame(
        [
            (("1", "2"),),
        ],
        schema=schema,
    )
    computed_dataframe = create_nested_data_column(
        dataframe, nested_column_name=nested_column_name
    )

    assert_df_equality(
        expected_dataframe, computed_dataframe, ignore_nullable=True
    )


def test_parse_date(spark, dicts_are_same):
    dataframe = spark.createDataFrame(
        [("2019/01/01",), ("2018/01/05",), ("0001/01/05",), ("3333/33/33",)],
        ["date"],
    )
    dataframe_computed = parse_date(
        dataframe, column="date", date_format="y/M/d"
    )

    dict_parsed_dates = dataframe_computed.toPandas().to_dict(orient="records")
    expected_dates = [
        {
            "date": date(year=2019, month=1, day=1),
        },
        {
            "date": date(year=2018, month=1, day=5),
        },
        {
            "date": date(year=1, month=1, day=5),
        },
        {
            "date": None,
        },
    ]

    dicts_are_same(dict_parsed_dates, expected_dates)
