from functions.processing.cleaning import clean_declarations
from tests.fixtures.ts_clean_declarations import (
    CLEAN_DECLARATIONS,
    PARQUET_DECLARATIONS,
)
from utils.constants import TS_COLS


def test_clean_declarations(dicts_are_same):
    """
    cleaned declarations should have all properties from source
    """
    cleaned_declarations = clean_declarations(PARQUET_DECLARATIONS)

    assert set(CLEAN_DECLARATIONS.columns) == set(cleaned_declarations.columns)

    dicts_are_same(
        cleaned_declarations.drop(TS_COLS.HASH)
        .toPandas()
        .to_dict(orient="records"),
        CLEAN_DECLARATIONS.drop(TS_COLS.HASH)
        .toPandas()
        .to_dict(orient="records"),
    )
