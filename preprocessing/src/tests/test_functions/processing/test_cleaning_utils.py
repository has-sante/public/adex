import numpy as np
import pytest
from chispa.dataframe_comparer import assert_df_equality
from pyspark.sql.types import FloatType, StructField, StructType

from functions.processing.cleaning import (
    clean_identifiers,
    clean_names,
    clean_special_chars,
    create_convention_id,
    create_declaration_id,
    delete_special_chars,
    fill_nan_montant_to_0,
    trim_identifiant,
)
from tests.fixtures.ts_convention_liee_id import (
    DF_CONVENTIONS_LIEES_ID,
    DICT_CONVENTIONS_LIEES_ID,
)
from tests.fixtures.ts_declaration_id import (
    DF_DECLARATIONS_ID,
    DICT_DECLARATIONS_ID,
)
from tests.fixtures.ts_identifiers import (
    DF_IDENTIFIERS,
    DICT_CLEAN_IDENTIFIERS,
)
from tests.fixtures.ts_names import DF_NAMES, DICT_CLEAN_NAMES
from utils.constants import TS_COLS


def test_clean_names(dicts_are_same):
    """
    Test name cleaning which does :
    - Ascii folding
    - Upper case on names
    - InitCap on surnames
    """
    df_clean_names = clean_names(DF_NAMES)
    dict_clean_names_computed = df_clean_names.toPandas().to_dict(
        orient="records"
    )
    assert dicts_are_same(dict_clean_names_computed, DICT_CLEAN_NAMES)


def test_trim_identifiant(spark):
    source_df = spark.createDataFrame(
        [
            ("10009922999",),
            (" 1a009922999",),
            ("9 ",),
            (" 9283 ",),
        ],
        [TS_COLS.NUMERO_IDENTIFIANT],
    )
    expected_df = spark.createDataFrame(
        [
            ("10009922999",),
            ("1a009922999",),
            ("9",),
            ("9283",),
        ],
        [TS_COLS.NUMERO_IDENTIFIANT],
    )
    computed_df = trim_identifiant(source_df)
    assert_df_equality(expected_df, computed_df)


def test_fill_nan_montant_to_0(spark):
    schema = StructType([StructField(TS_COLS.MONTANT, FloatType(), True)])
    source_df = spark.createDataFrame(
        [
            (123.2,),
            (None,),
            (np.NaN,),
        ],
        schema,
    )
    expected_df = spark.createDataFrame(
        [
            (123.2,),
            (0.0,),
            (0.0,),
        ],
        schema,
    )
    computed_df = fill_nan_montant_to_0(source_df)
    assert_df_equality(expected_df, computed_df, ignore_nullable=True)


@pytest.mark.parametrize(
    "string_to_replace,expected",
    [
        ("Hel\u000Blo", "Hel lo"),
        ("Hel\u0013lo", "Hel lo"),
        ("Hel\u0019lo", "Hel'lo"),
    ],
)
def test_delete_special_chars_replace(string_to_replace, expected, spark):
    source_df = spark.createDataFrame(
        [(string_to_replace,)],
        ["test_column"],
    )
    expected_df = spark.createDataFrame(
        [(expected,)],
        ["test_column"],
    )
    computed_df = delete_special_chars(source_df, columns=["test_column"])
    assert_df_equality(expected_df, computed_df)


@pytest.mark.parametrize(
    "string_to_replace,expected",
    [
        ("Hel\u000Blo", "Hel lo"),
        ("Hel\u0013lo", "Hel lo"),
        ("Hel\u0019lo", "Hel'lo"),
    ],
)
def test_clean_special_chars(string_to_replace, expected, spark):
    source_df = spark.createDataFrame(
        [[string_to_replace] * 4 + ["shoul\u000Bd not change"]],
        [
            TS_COLS.STRUCTURE_EXERCICE,
            TS_COLS.MOTIF_AUTRE,
            TS_COLS.ADRESSE,
            TS_COLS.INFO_CONVENTION,
            "shouldnt_change_column",
        ],
    )
    expected_df = spark.createDataFrame(
        [[expected] * 4 + ["shoul\u000Bd not change"]],
        [
            TS_COLS.STRUCTURE_EXERCICE,
            TS_COLS.MOTIF_AUTRE,
            TS_COLS.ADRESSE,
            TS_COLS.INFO_CONVENTION,
            "shouldnt_change_column",
        ],
    )
    computed_df = clean_special_chars(source_df)
    assert_df_equality(expected_df, computed_df)


def test_clean_identifiers(dicts_are_same):

    """
    Test id cleaning which does :
    - Trim identifiers columns (delete useless spaces)
    - Set as empty incorrect ids
    """
    df_clean_identifiers = clean_identifiers(DF_IDENTIFIERS)
    dict_clean_identifiers_computed = df_clean_identifiers.toPandas().to_dict(
        orient="records"
    )
    assert dicts_are_same(
        dict_clean_identifiers_computed, DICT_CLEAN_IDENTIFIERS
    )


def test_create_declarations_id(dicts_are_same):
    """
    Test the declaration_id function
    - Create a id {TYPE} | {entreprise_id} | {column_id}
    - If column_id value is null, the the id is set to nul
    """
    df_declarations_id_computed = create_declaration_id(
        DF_DECLARATIONS_ID,
    )

    dict_declarations_id_computed = (
        df_declarations_id_computed.toPandas().to_dict(orient="records")
    )
    assert dicts_are_same(dict_declarations_id_computed, DICT_DECLARATIONS_ID)


def test_create_conventions_liees_id(dicts_are_same):
    """
    Test the declaration_id function
    - Create a id {TYPE} | {entreprise_id} | {column_id}
    - If column_id value is null, the the id is set to nul
    """
    df_conventions_liees_id_computed = create_convention_id(
        DF_CONVENTIONS_LIEES_ID,
    )

    dict_conventions_liees_id_computed = (
        df_conventions_liees_id_computed.toPandas().to_dict(orient="records")
    )
    assert dicts_are_same(
        dict_conventions_liees_id_computed, DICT_CONVENTIONS_LIEES_ID
    )
