from chispa.dataframe_comparer import assert_df_equality

from functions.processing.linking import link_declarations
from utils.constants import TS_COLS


def get_declarations(spark):
    return spark.createDataFrame(
        [
            ("convention", "convention | c1", None, "convention | c1"),
            ("convention", "convention | c2", None, "convention | c2"),
            ("convention", "convention | c3", None, "convention | c3"),
            ("convention", "convention | c4", None, "convention | c4"),
            ("avantage", "avantage | a1", "c1", "convention | c1"),
            ("avantage", "avantage | a2", "c1", "convention | c1"),
            ("avantage", "avantage | a3", "c2", "convention | c2"),
            (
                "avantage",
                "avantage | a4",
                "c_does_not_exists",
                "convention | c_does_not_exists",
            ),
            ("avantage", "avantage | a5", None, None),
            ("avantage", "avantage | a6", "", None),
            ("remuneration", "remuneration | r1", "c3", "convention | c3"),
            ("remuneration", "remuneration | r2", "c3", "convention | c3"),
            ("remuneration", "remuneration | r3", "c4", "convention | c4"),
            (
                "remuneration",
                "remuneration | r4",
                "c_does_not_exists",
                "convention | c_does_not_exists",
            ),
            ("remuneration", "remuneration | r5", None, None),
            ("remuneration", "remuneration | r6", "", None),
        ],
        [
            TS_COLS.LIEN_INTERET,
            TS_COLS.DECLARATION_ID,
            TS_COLS.CONVENTION_LIEE,
            TS_COLS.CONVENTION_ID,
        ],
    )


def get_expected_declarations(spark):
    return spark.createDataFrame(
        [
            ("convention", "convention | c1", None, "convention | c1"),
            ("convention", "convention | c2", None, "convention | c2"),
            ("convention", "convention | c3", None, "convention | c3"),
            ("convention", "convention | c4", None, "convention | c4"),
            ("avantage", "avantage | a1", None, "convention | c1"),
            ("avantage", "avantage | a2", None, "convention | c1"),
            ("avantage", "avantage | a3", None, "convention | c2"),
            ("avantage", "avantage | a4", "c_does_not_exists", None),
            ("avantage", "avantage | a5", None, None),
            ("avantage", "avantage | a6", None, None),
            ("remuneration", "remuneration | r1", None, "convention | c3"),
            ("remuneration", "remuneration | r2", None, "convention | c3"),
            ("remuneration", "remuneration | r3", None, "convention | c4"),
            ("remuneration", "remuneration | r4", "c_does_not_exists", None),
            ("remuneration", "remuneration | r5", None, None),
            ("remuneration", "remuneration | r6", None, None),
        ],
        [
            TS_COLS.LIEN_INTERET,
            TS_COLS.DECLARATION_ID,
            TS_COLS.CONVENTION_DESCRIPTION,
            TS_COLS.CONVENTION_ID,
        ],
    )


def test_link_declarations(spark):
    declarations = get_declarations(spark)
    expected_declarations = get_expected_declarations(spark)
    linked_declarations = link_declarations(
        declarations=declarations,
    )
    assert_df_equality(
        linked_declarations.sort(TS_COLS.DECLARATION_ID),
        expected_declarations.sort(TS_COLS.DECLARATION_ID),
    )
