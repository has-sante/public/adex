from chispa.dataframe_comparer import assert_df_equality

from functions.parquet import read_parquet, write_parquet


def test_read_parquet(spark):
    p_parquet_file = "tests/data/parquet/ts/declarations_raw.parquet"
    df = read_parquet(p_parquet_file, spark)
    assert df.count() == 430
    first_row = df.select("entreprise_id").first()
    assert first_row["entreprise_id"] == "1618"


def test_write_parquet(spark, tmp_path):
    df_src = spark.createDataFrame([(a, a) for a in range(10)], ["c1", "c2"])

    p_parquet_file = str(tmp_path / "test_write_parquet.parquet")
    # Needs to be lower than the defined num_cores in spark fixture
    num_partitions = 2
    write_parquet(df_src, p_parquet_file, num_partitions)

    df_dst = spark.read.parquet(p_parquet_file)
    assert df_dst.rdd.getNumPartitions() == num_partitions
    # As dataframe has been repartitionned, we need to sort the dst dataframe
    assert_df_equality(df_src, df_dst.sort("c1"))
