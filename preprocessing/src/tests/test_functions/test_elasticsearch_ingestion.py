import datetime
import random
from unittest.mock import MagicMock, patch

from functions import elasticsearch_ingestion


def get_patch_datetime_now(requested_date: datetime.datetime) -> MagicMock:
    return patch(
        "functions.elasticsearch_ingestion.datetime.datetime",
        utcnow=lambda: requested_date,
        side_effect=lambda *args, **kw: datetime.datetime(*args, **kw),
    )


def test_new_index_name_creation():
    # We expect a datetime format "%Y-%m-%dT%H-%M-%S"
    prefix = "prefix_test"
    now_date_time = datetime.datetime(
        year=2020, month=2, day=3, hour=10, minute=20, second=3
    )
    with get_patch_datetime_now(now_date_time) as mock:
        index_name = elasticsearch_ingestion.get_new_index_name(
            index_prefix=prefix
        )
    expected_index_name = "prefix_test-2020-02-03t10.20.03"

    assert index_name == expected_index_name


def test_get_datetime_from_index_name():
    prefix = "prefix_test"
    now_date_time = datetime.datetime(
        year=2020, month=2, day=3, hour=10, minute=20, second=3
    )
    with get_patch_datetime_now(now_date_time) as mock:
        index_name = elasticsearch_ingestion.get_new_index_name(
            index_prefix=prefix
        )
    extracted_datetime = elasticsearch_ingestion.get_datetime_from_index_name(
        index_name=index_name, index_prefix=prefix
    )
    assert now_date_time == extracted_datetime


def test_index_names_are_sortable():
    """
    We test that the generated indices names are sortable
    """
    prefix = "prefix_test"
    # Creating dates
    list_datetimes = [
        datetime.datetime(year=y, month=m, day=d, hour=h, minute=mi, second=s)
        for y in [3050, 2031, 2021]
        for m in [1, 5, 12]
        for d in [1, 10, 28]
        for h in [0, 15]
        for mi in [10, 55]
        for s in [12, 33]
    ]
    # Shuffling the dates
    random.shuffle(list_datetimes)
    # Getting the indices names
    list_indices = []
    for requested_date in list_datetimes:
        with get_patch_datetime_now(requested_date) as mock:
            index_name = elasticsearch_ingestion.get_new_index_name(
                index_prefix=prefix
            )
        list_indices.append(index_name)

    sorted_list_indices = sorted(list_indices)

    assert all(
        elasticsearch_ingestion.get_datetime_from_index_name(
            index_name=index_name_1, index_prefix=prefix
        )
        < elasticsearch_ingestion.get_datetime_from_index_name(
            index_name=index_name_2, index_prefix=prefix
        )
        for index_name_1, index_name_2 in zip(
            sorted_list_indices[:-1], sorted_list_indices[1:]
        )
    )
