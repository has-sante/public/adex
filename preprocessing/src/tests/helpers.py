"""
Quelques helpers pour les tests
"""
import json
from typing import Dict, List

import dask.dataframe as dd
import pandas as pd
import pyspark
from chispa.dataframe_comparer import assert_df_equality


class DataFrameDataNotEqual(Exception):
    """Data frames are not equal"""


def load_dataframe_from_json(
    p_json_file: str,
    schema: pyspark.sql.types.StructType,
    spark: pyspark.sql.SparkSession,
) -> pyspark.sql.DataFrame:
    with open(p_json_file) as file_id:
        json_data = json.load(file_id)
    rdd = spark.sparkContext.parallelize(json_data)
    return spark.read.schema(schema).json(rdd)


def assert_df_equality_after_sort(df1, df2):
    columns = sorted(df1.columns)
    df1 = df1.select(*columns)
    df2 = df2.select(*columns)
    assert_df_equality(df1, df2, ignore_nullable=True)


def assert_df_data_equal(
    df_to_test: pyspark.sql.DataFrame,
    dict_expected: List[Dict],
    dicts_are_same,
):
    df_to_test_data = df_to_test.toPandas().to_dict(orient="records")
    assert dicts_are_same(df_to_test_data, dict_expected)


def dask_dataframe_from_records(records, npartitions=4):
    df = pd.DataFrame.from_records(records)
    return dd.from_pandas(df, npartitions=npartitions)
