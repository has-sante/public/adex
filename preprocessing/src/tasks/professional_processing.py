"""
Tâches relatives au préprocessing de l'annuaire ANS
"""
from os import path

import prefect
import pyspark
from prefect import task

from functions.parquet import read_parquet, write_parquet
from functions.processing.professional import (
    clean_professional,
    process_professionals,
)


@task
def task_process_professional(
    p_parquet_file: str,
    p_parquet_destination: str,
    spark: pyspark.sql.SparkSession,
    num_partitions: int = 100,
) -> None:
    logger = prefect.context.get("logger")
    logger.info(
        "Clean and dump {} to {}".format(p_parquet_file, p_parquet_destination)
    )

    if path.exists(p_parquet_destination):
        logger.info(
            "Folder {} exists. Overwriting".format(p_parquet_destination)
        )

    annuaire_ans = read_parquet(p_parquet_file, spark)
    annuaire_ans = clean_professional(annuaire_ans)
    annuaire_ans = process_professionals(annuaire_ans)
    write_parquet(
        annuaire_ans, p_parquet_destination, num_partitions=num_partitions
    )
