"""
Tâches relatives à l'ingestion des metadonnées
"""
import os
from datetime import datetime

import prefect
from prefect import task

import functions.elasticsearch_ingestion as es_ingestion


@task
def task_set_metadata(index_prefix: str) -> None:
    logger = prefect.context.get("logger")

    metadata = {
        "commit_sha": os.getenv("CI_COMMIT_SHORT_SHA", "noCommitHash"),
        # FIXME: better handling of datetime format ?
        "date_maj_donnees": datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
        "version": os.getenv("ADEX_VERSION", "noVersion"),
    }

    index_name = es_ingestion.get_new_index_name(index_prefix=index_prefix)

    logger.info("Creating index {}".format(index_name))
    elasticsearch_connection = es_ingestion.get_elasticsearch_instance()
    es_ingestion.upsert_document(
        index_name=index_name,
        doc_type=index_name,
        document=metadata,
        elasticsearch_connection=elasticsearch_connection,
    )
    return index_name
