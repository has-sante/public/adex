"""
Tâches relatives à l'ingestion de données dans postgres
"""
import prefect
import pyspark
from prefect import task

from functions.postgres_ingestion import (
    create_index,
    create_schema,
    ingest_parquet_to_table,
    roll_back_schemas,
    roll_over_schemas,
)


@task
def task_ingest_parquet_to_table(
    p_parquet: str,
    table_name: str,
    spark: pyspark.sql.SparkSession,
):
    logger = prefect.context.get("logger")
    logger.info("Ingest {} into postgres {}".format(p_parquet, table_name))
    ingest_parquet_to_table(
        p_parquet=p_parquet,
        table_name=table_name,
        spark=spark,
    )


@task
def task_create_index_on_column(
    table_name: str,
    index_name: str,
    column: str,
):
    logger = prefect.context.get("logger")
    logger.info(
        "Create index (index_name: {}, table_name: {}, column_name: {})".format(
            index_name, table_name, column
        )
    )
    create_index(table_name=table_name, index_name=index_name, column=column)


@task
def task_create_schemas():
    logger = prefect.context.get("logger")
    logger.info("Creating schema `old`, `new`")
    create_schema("old")
    create_schema("new")


@task
def task_roll_over_schemas():
    logger = prefect.context.get("logger")
    logger.info("Rolling over the schemas.")
    roll_over_schemas()


@task
def task_roll_back_schemas():
    logger = prefect.context.get("logger")
    logger.info("Rolling back the schemas.")
    roll_back_schemas()
