import os

import prefect
import requests
from prefect import task

from utils.constants.path import FS

API_BASE_URL = "https://www.data.gouv.fr/api/1"


class DataGouvConfig:
    def __init__(self):
        self.data_gouv_api_key = os.getenv("DATA_GOUV_API_KEY")
        self.community_id = os.getenv("DATA_GOUV_COMMUNITY_ID")
        self.dataset_id = os.getenv("DATA_GOUV_DATASET_ID")
        request_timeout_s = os.getenv("DATA_GOUV_TIMEOUT_S")
        self.request_timeout_s = request_timeout_s and int(request_timeout_s)


@task
def get_data_gouv_config() -> DataGouvConfig:
    return DataGouvConfig()


@task
def update_resource_data(
    dataset_config: DataGouvConfig,
    data_file_path: str,
):
    """
    Met à jour sur data.gouv.fr la ressource passée en paramètre avec la taille
    des données présentes sur disque au chemin passé en paramètre.
    À noter que la ressource
    doit avoir été préalablement créée, cette fonction ne réalise qu'un update.
    """
    logger = prefect.context.get("logger")
    url = f"{API_BASE_URL}/datasets/community_resources/{dataset_config.community_id}/"
    logger.info(f"Updating data gouv resource: {url} using {data_file_path}")
    headers = {
        "X-API-KEY": dataset_config.data_gouv_api_key,
    }

    file_size = FS.size(data_file_path)
    if not file_size:
        logger.warning(f"Couldn't get gzip file size, using default.")
        file_size = 1_300_000_000

    response = requests.put(
        url,
        params={"dataset": dataset_config.dataset_id},
        headers=headers,
        timeout=dataset_config.request_timeout_s,
        json={"filesize": file_size},
    )
    logger.info(f"{response.content}")
    return response
