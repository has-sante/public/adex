"""
Tâches relatives au client dask
"""
import prefect
from dask.distributed import Client
from prefect import task


@task
def task_get_dask_client() -> Client:
    return Client()


@task(trigger=prefect.triggers.always_run)
def task_stop_dask_client(client: Client) -> None:
    return client.close()
