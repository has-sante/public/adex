"""
Tâches relatives à la migration du schéma de données de TS
"""
import dask.dataframe as dd
import prefect
from prefect import task

from functions.processing.migration import process_raw_declarations
from utils.constants.declarations import PARQUET_SCHEMA
from utils.constants.path import FS, STORAGE_OPTS


@task
def task_migrate_declarations(
    p_raw_declarations: str,
    p_declarations: str,
) -> None:

    logger = prefect.context.get("logger")
    logger.info("Migration des données de transparence santé")
    raw_declarations = dd.read_parquet(
        FS.unstrip_protocol(p_raw_declarations), storage_options=STORAGE_OPTS
    )

    declarations = process_raw_declarations(raw_declarations)

    declarations.to_parquet(
        FS.unstrip_protocol(p_declarations),
        overwrite=True,
        schema=PARQUET_SCHEMA,
        storage_options=STORAGE_OPTS,
    )
