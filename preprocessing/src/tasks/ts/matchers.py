"""
Tâches relatives au regroupement des bénéficiaires de TS
"""
import pyspark
from prefect import task

from functions.matchers import group_by_id_name, merge_matches, prepare_matches


@task
def task_group_recipients(
    recipients: pyspark.sql.DataFrame,
) -> pyspark.sql.DataFrame:

    data = prepare_matches(recipients)
    grouped, non_grouped = group_by_id_name(data)
    recipient_groups = merge_matches([grouped], non_grouped)
    return recipient_groups
