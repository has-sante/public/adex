"""
Tâches relatives à la vérification des liens entre déclarations
"""
from typing import Tuple

import pyspark
from prefect import task

from functions.processing.linking import link_declarations


@task
def task_link_declarations(
    declarations: pyspark.sql.DataFrame,
) -> Tuple[
    pyspark.sql.DataFrame, pyspark.sql.DataFrame, pyspark.sql.DataFrame
]:

    declarations = link_declarations(
        declarations=declarations,
    )
    return declarations
