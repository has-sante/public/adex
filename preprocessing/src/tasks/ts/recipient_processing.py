"""
Tâches relatives à l'extraction des bénéficiaires
"""
import prefect
import pyspark
from prefect import task

from functions.processing.recipient import process_recipients


@task
def task_process_recipient(
    declarations: pyspark.sql.DataFrame,
) -> pyspark.sql.DataFrame:
    recipients = process_recipients(
        declarations=declarations,
    )
    logger = prefect.context.get("logger")
    logger.info("Recipients {}".format(recipients.count()))
    return recipients
