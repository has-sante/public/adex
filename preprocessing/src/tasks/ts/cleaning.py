"""
Tâches relatives au nettoyage des déclarations
"""
import prefect
import pyspark
from prefect import task

from functions.processing.cleaning import clean_declarations


@task
def task_clean_declarations(
    declarations: pyspark.sql.DataFrame,
) -> pyspark.sql.DataFrame:

    logger = prefect.context.get("logger")
    logger.info("Cleaning declarations")

    return clean_declarations(declarations=declarations, logger=logger)
