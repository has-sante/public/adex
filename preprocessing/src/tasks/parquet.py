"""
Tâches relatives à l'écriture / lecture des fichiers parquet.
"""
from os import path

import prefect
import pyspark
from prefect import task

from functions.parquet import csv_to_parquet, read_parquet, write_parquet
from utils.constants.path import FS


@task
def task_csv_to_parquet(
    p_csv: str,
    p_parquet: str,
    dtypes: dict,
    rename_mapping: dict = None,
    # parse_date_columns: list= None,
    sep: str = ";",
    chunksize=1_000_000,
):
    logger = prefect.context.get("logger")
    logger.info("Dump csv to parquet ({}, {})".format(p_csv, p_parquet))
    if FS.isfile(p_parquet):
        logger.info("File {} already exists. Overwritting".format(p_parquet))

    # If folder to download doesn't exists, the folder is created
    if not FS.exists(path.dirname(p_parquet)):
        logger.info("Creating folder {}".format(path.dirname(p_parquet)))
        FS.makedirs(path.dirname(p_parquet), exist_ok=True)

    csv_to_parquet(
        p_csv=p_csv,
        p_parquet=p_parquet,
        dtypes=dtypes,
        rename_mapping=rename_mapping,
        # parse_date_columns=parse_date_columns,
        sep=sep,
        chunksize=chunksize,
    )


@task
def task_read_parquet(
    p_parquet_file: str, spark: pyspark.sql.SparkSession
) -> pyspark.sql.DataFrame:

    logger = prefect.context.get("logger")
    logger.info("Loading {}".format(p_parquet_file))

    return read_parquet(p_parquet_file=p_parquet_file, spark=spark)


@task
def task_write_parquet(
    dataframe: pyspark.sql.DataFrame,
    p_parquet_file: str,
    num_partitions: int = 10,
) -> pyspark.sql.DataFrame:

    logger = prefect.context.get("logger")
    logger.info("Writing {}".format(p_parquet_file))

    if FS.exists(p_parquet_file):
        logger.info("Folder {} exists. Overwritting".format(p_parquet_file))

    return write_parquet(
        dataframe=dataframe,
        p_parquet_file=p_parquet_file,
        num_partitions=num_partitions,
    )
