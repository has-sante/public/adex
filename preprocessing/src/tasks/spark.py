"""
Tâches relatives à la création d'une instance spark
"""
import prefect
import pyspark
from prefect import task

from functions.spark import get_spark, stop_spark


@task
def task_get_spark(app_name: str) -> pyspark.sql.SparkSession:
    """
    Récupère ou crée une instance spark.
    """
    return get_spark(app_name)


@task(trigger=prefect.triggers.always_run)
def task_stop_spark(spark: pyspark.sql.SparkSession):
    """
    Stop une instance spark.
    """
    stop_spark(spark)
