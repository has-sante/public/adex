"""
Tâches relatives au téléchargement et à l'extraction des archives zip.
"""
import datetime
import gzip
import logging
import pathlib
import shutil
import time
from dataclasses import dataclass
from os import path
from typing import Dict, List

import pandas as pd
import prefect
from dateutil.relativedelta import relativedelta
from prefect import task
from prefect.engine import signals

from functions.download_extract import (
    download_file_from_url,
    extract_with_prefix,
    get_ts_nb_hits,
)
from utils.constants.declarations import DTYPES
from utils.constants.path import FS


@task
def task_download_file_from_url(
    url: str,
    p_output: str,
    force_download: bool = False,
    verify_ssl: bool = True,
) -> None:
    logger = prefect.context.get("logger")
    logger.info("Downloading {} to {}".format(url, p_output))
    if FS.exists(p_output) and (force_download):
        logger.info("File {} already exists. Overwritting".format(p_output))
    elif FS.exists(p_output) and (not force_download):
        logger.info("File {} already exists. Skipping".format(p_output))
        return

    # If folder to download doesn't exists, the folder is created
    if not FS.exists(path.dirname(p_output)):
        logger.info("Creating folder {}".format(path.dirname(p_output)))
        FS.makedirs(path.dirname(p_output), exist_ok=True)
    download_file_from_url(url, p_output, verify_ssl=verify_ssl)


@dataclass
class DownloadConfig:
    base_url: str
    p_output: str
    refine_date: str
    expected_nb_hits: int


@task(max_retries=4, retry_delay=datetime.timedelta(seconds=5))
def task_download_file_and_check_integrity(
    download_config: DownloadConfig,
) -> None:
    logger = prefect.context.get("logger")
    logger.debug(f"Base api url: {download_config.base_url}")
    logger.debug(f"Output path: {download_config.p_output}")
    logger.debug(f"Query date: {download_config.refine_date}")
    params = {
        "format": "csv",
        "use_labels_for_header": "false",
        "timezone": "Europe/Berlin",
        "refine.date": download_config.refine_date,
    }
    params["refine.date"] = download_config.refine_date
    download_file_from_url(
        url=download_config.base_url,
        p_output=download_config.p_output,
        params=params,
        logger=logger,
    )
    verify_integrity(
        nb_hits=download_config.expected_nb_hits,
        refine_date=download_config.refine_date,
        p_output=download_config.p_output,
        logger=logger,
    )


def verify_integrity(
    nb_hits: int,
    refine_date: str,
    p_output: str,
    dtypes: Dict = DTYPES,
    sep: str = ";",
    logger: logging.Logger = None,
):
    try:
        with FS.open(p_output, "r") as fp:
            result_df = pd.read_csv(fp, dtype=dtypes, sep=sep)
    except UnicodeError as e:
        logger.info(f"Got error reading the file for date {refine_date}")
        logger.info(f"Error message is {e}")
        raise signals.FAIL(message=f"File not correctly downloaded")

    if nb_hits == len(result_df):
        logger.info(f"Retrieved the {nb_hits} rows for date {refine_date}")
        raise signals.SUCCESS(message=f"Retrieved the {nb_hits} rows")
    else:
        logger.info(
            f"Expected {nb_hits} rows, but found {len(result_df)} for date {refine_date}"
        )
        raise signals.FAIL(
            message=f"Expected {nb_hits} rows, but found {len(result_df)} for date {refine_date}"
        )


@task
def task_prepare_download(
    p_split_output: str,
    p_output: str,
    force_download: bool = False,
) -> bool:
    """
    Will return True if a download is needed.
    """

    p_output = pathlib.Path(p_output)
    logger = prefect.context.get("logger")
    if FS.exists(p_output) and force_download:
        logger.info("File {} already exists. Overwritting".format(p_output))
    elif FS.exists(p_output) and not force_download:
        logger.info("File {} already exists. Skipping".format(p_output))
        return False

    # If folder to download doesn't exists, the folder is created
    if not FS.exists(p_output.parent):
        logger.info("Creating folder {}".format(path.dirname(p_output)))
        FS.makedirs(path.dirname(p_output), exist_ok=True)

    # As we will download files, we want to be sure the output folder
    # Is clean (ie we delete it and recreate it ;))
    if FS.exists(p_split_output):
        logger.info("Cleaning folder {}".format(path.dirname(p_split_output)))
        FS.rm(p_split_output, recursive=True)
    logger.info("Creating folder {}".format(path.dirname(p_split_output)))
    FS.makedirs(p_split_output, exist_ok=True)

    return True


@task
def task_get_downloads_urls_and_paths(
    base_url: str,
    p_split_output: str,
) -> List[DownloadConfig]:
    """
    Queries the API to know how big a time window will be in terms of data.
    If the default time window (monthly) contains too much data, it is split
    into daily time windows.
    """
    logger = prefect.context.get("logger")
    p_split_output = pathlib.Path(p_split_output)

    params = {
        "dataset": "declarations",
        "use_labels_for_header": "false",
        "timezone": "Europe/Berlin",
        "rows": 0,
    }

    start_date = datetime.date(year=2011, month=1, day=1)
    end_date = prefect.context.get("date").date()
    download_commands = []
    while start_date < end_date:
        monthly_date = start_date.strftime("%Y-%m")

        params["refine.date"] = monthly_date
        url = "https://www.transparence.sante.gouv.fr/api/records/1.0/search/"
        nb_hits = get_ts_nb_hits(url=url, params=params, logger=logger)

        if nb_hits < 90_000:
            output_path = p_split_output / monthly_date / f"{monthly_date}.csv"
            logger.info(
                f"Generating download cmd {monthly_date} data to {output_path}"
            )
            download_commands.append(
                DownloadConfig(
                    base_url=base_url,
                    p_output=output_path,
                    refine_date=monthly_date,
                    expected_nb_hits=nb_hits,
                )
            )
        else:
            split_start_date = start_date
            while split_start_date < start_date + relativedelta(months=1):
                daily_date = split_start_date.strftime("%Y-%m-%d")

                params["refine.date"] = daily_date
                url = "https://www.transparence.sante.gouv.fr/api/records/1.0/search/"
                nb_hits = get_ts_nb_hits(url=url, params=params, logger=logger)

                output_path = (
                    p_split_output / monthly_date / f"{daily_date}.csv"
                )
                logger.info(
                    f"Generating download cmd {daily_date} data to {output_path}"
                )
                download_commands.append(
                    DownloadConfig(
                        base_url=base_url,
                        p_output=output_path,
                        refine_date=daily_date,
                        expected_nb_hits=nb_hits,
                    )
                )
                split_start_date = split_start_date + relativedelta(days=1)
        start_date = start_date + relativedelta(months=1)
    return download_commands


@task
def task_merge_csvs(
    p_split_output: str,
    p_output: str,
):
    logger = prefect.context.get("logger")
    p_split_output = pathlib.Path(p_split_output)
    p_output = pathlib.Path(p_output)
    list_files_to_merge = sorted(FS.glob(str(p_split_output / "**.csv")))
    logger.info(
        f"Merging {len(list_files_to_merge)} files from {p_split_output} into {p_output}"
    )
    with FS.open(p_output, "wb") as outfile:
        for i, fname in enumerate(list_files_to_merge):
            with FS.open(fname, "rb") as infile:
                if i != 0:
                    infile.readline()  # Throw away header on all but first file
                # Block copy rest of file from input to output without parsing
                shutil.copyfileobj(infile, outfile)


@task
def task_gzip_csvs(
    p_merged_output: str,
    p_gzip_output: str,
):
    logger = prefect.context.get("logger")
    p_merged_output = pathlib.Path(p_merged_output)
    p_gzip_output = pathlib.Path(p_gzip_output)
    logger.info(
        f"Compressing merged file {p_merged_output} into {p_gzip_output}"
    )
    with FS.open(p_merged_output, "rb") as infile:
        with FS.open(p_gzip_output, "wb") as outfile:
            with gzip.GzipFile(fileobj=outfile) as f_out:
                shutil.copyfileobj(infile, f_out)


@task
def task_copy_dev_dump(
    p_dataset: str, p_output: str, force_download: bool = False
) -> None:
    logger = prefect.context.get("logger")
    logger.info("Downloading ts dev dumps to {}".format(p_output))
    if FS.exists(p_output) and (force_download):
        logger.info("File {} already exists. Overwritting".format(p_output))
    elif FS.exists(p_output) and (not force_download):
        logger.info("File {} already exists. Skipping".format(p_output))
        return

    # If folder to download doesn't exists, the folder is created
    if not FS.exists(path.dirname(p_output)):
        logger.info("Creating folder {}".format(path.dirname(p_output)))
        FS.makedirs(path.dirname(p_output), exist_ok=True)
    FS.put_file(p_dataset, p_output)


@task
def task_extract_with_prefix(
    p_zip: str, prefix: str, p_output: str, force_extract: bool = True
) -> None:
    logger = prefect.context.get("logger")
    logger.info(
        "Extracting with prefix {} from {} to {}".format(
            prefix, p_zip, p_output
        )
    )
    if FS.exists(p_output) and (not force_extract):
        logger.info("File {} already exists. Skipping".format(p_output))
        return

    if FS.exists(p_output):
        logger.info("File {} already exists. Overwritting".format(p_output))

    # If folder to extract doesn't exists, the folder is created
    if not FS.exists(path.dirname(p_output)):
        logger.info("Creating folder : {}".format(path.dirname(p_output)))
        FS.makedirs(path.dirname(p_output), exist_ok=True)
    extract_with_prefix(p_zip=p_zip, prefix=prefix, p_output=p_output)
