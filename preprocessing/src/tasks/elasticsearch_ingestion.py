"""
Tâches relatives à l'ingestion dans elasticsearch
"""
import prefect
from prefect import task

import functions.elasticsearch_ingestion as es_ingestion


@task
def task_create_index(index_prefix: str, p_mapping: str) -> str:
    logger = prefect.context.get("logger")

    index_name = es_ingestion.get_new_index_name(index_prefix=index_prefix)
    logger.info("Creating index {}".format(index_name))

    elasticsearch_connection = es_ingestion.get_elasticsearch_instance()

    es_ingestion.create_mapping_from_path(
        index_name=index_name,
        p_mapping=p_mapping,
        elasticsearch_connection=elasticsearch_connection,
        logger=logger,
    )

    return index_name


@task
def task_delete_dangling_indices(
    index_prefix: str, num_indices_to_keep: int = 2
) -> None:
    logger = prefect.context.get("logger")
    logger.info(
        "Cleaning up index {} by removing dangling indices".format(
            index_prefix
        )
    )

    elasticsearch_connection = es_ingestion.get_elasticsearch_instance()
    es_ingestion.delete_dangling_indices(
        index_prefix=index_prefix,
        num_indices_to_keep=num_indices_to_keep,
        elasticsearch_connection=elasticsearch_connection,
        logger=logger,
    )


@task
def task_ingest(
    index_name: str, p_parquet: str, chunk_size: int, id_field: str = None
) -> None:
    logger = prefect.context.get("logger")
    logger.info("Ingesting data into index {}".format(index_name))
    elasticsearch_connection = es_ingestion.get_elasticsearch_instance()

    settings_pre_ingestion = {
        "index": {
            "number_of_replicas": 0,
            "refresh_interval": "30s",
        }
    }
    settings_post_ingestion = {
        "index": {
            "number_of_replicas": 1,
            "refresh_interval": "1s",
        }
    }

    es_ingestion.put_settings(
        index_name=index_name,
        settings=settings_pre_ingestion,
        elasticsearch_connection=elasticsearch_connection,
    )
    actions = es_ingestion.get_all_ingestion_actions_from_parquet(
        p_parquet=p_parquet, index_name=index_name, id_field=id_field
    )
    es_ingestion.insert_parallel_bulk(
        actions=actions,
        elasticsearch_connection=elasticsearch_connection,
        chunk_size=chunk_size,
    )
    es_ingestion.put_settings(
        index_name=index_name,
        settings=settings_post_ingestion,
        elasticsearch_connection=elasticsearch_connection,
    )

    return True


@task
def task_roll_over_aliases(index_name: str, alias_name: str) -> None:
    logger = prefect.context.get("logger")
    logger.info(f"Updating alias {index_name} -> {alias_name}")
    elasticsearch_connection = es_ingestion.get_elasticsearch_instance()
    es_ingestion.roll_over_aliases(
        index_name=index_name,
        alias_name=alias_name,
        elasticsearch_connection=elasticsearch_connection,
        logger=logger,
    )


@task
def task_roll_back(index_prefix: str) -> None:
    logger = prefect.context.get("logger")

    logger.info(f"Rolling back the index {index_prefix}")

    elasticsearch_connection = es_ingestion.get_elasticsearch_instance()
    current_index = es_ingestion.list_alias_indices(
        alias_name=index_prefix,
        elasticsearch_connection=elasticsearch_connection,
    )

    new_current_index = es_ingestion.roll_back_aliases(
        index_prefix=index_prefix,
        elasticsearch_connection=elasticsearch_connection,
        logger=logger,
    )

    logger.warning(
        f"""
        *******************************************************************************************
        Pendant le rollback, l'index {current_index} a été remplacé par l'index {new_current_index}.
        Le lancement du rollback a sûrement une bonne raison, il est donc probablement NECESSAIRE
        de supprimer l'index {current_index} qui doit être corrompu.
        *******************************************************************************************
        """
    )
