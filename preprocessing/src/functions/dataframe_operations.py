"""
Quelques utilitaires pour le traitement des dataframes spark
"""
from typing import List

import pyspark
import pyspark.sql.functions as f


def rename_parquet(
    df: pyspark.sql.DataFrame, mapping: dict
) -> pyspark.sql.DataFrame:
    for src, dst in mapping.items():
        df = df.withColumn(dst, f.col(src))
        df = df.drop(src)
    return df


def replace_nans(
    df: pyspark.sql.DataFrame, columns: List[str]
) -> pyspark.sql.DataFrame:
    for column in columns:
        df = df.withColumn(
            column,
            f.when(f.isnan(f.col(column)), None).otherwise(f.col(column)),
        )
        df = df.withColumn(
            column,
            f.when(f.isnull(f.col(column)), None).otherwise(f.col(column)),
        )
    return df


def create_nested_data_column(
    dataframe: pyspark.sql.DataFrame, nested_column_name: str = "nested"
) -> pyspark.sql.DataFrame:
    columns = dataframe.columns
    dataframe = dataframe.withColumn(
        nested_column_name, f.struct(*columns)
    ).drop(*columns)
    return dataframe


def parse_date(
    dataframe: pyspark.sql.DataFrame, column: str, date_format: str
) -> pyspark.sql.DataFrame:
    dataframe = dataframe.withColumn(
        column,
        f.to_date(f.col(column), date_format),
    )
    return dataframe
