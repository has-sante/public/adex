"""
Functions relatives à l'écriture / lecture de fichiers parquets
"""
import os
import shutil

import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
import pyspark

from utils.constants.path import FS


def csv_to_parquet(
    p_csv: str,
    p_parquet: str,
    dtypes: dict,
    rename_mapping: dict = None,
    # parse_date_columns: list = None,
    sep: str = ";",
    chunksize=1_000_000,
) -> None:
    """
    As TS dumps contain multiline fields, we need to use pandas csv parser.
    It's a known limitation of spark csv parser :
    https://issues.apache.org/jira/browse/SPARK-21820
    """

    with FS.open(p_csv, "r") as fs_fp:
        df = pd.read_csv(
            fs_fp,
            sep=sep,
            usecols=dtypes.keys(),
            dtype=dtypes,
            # infer_datetime_format=True,
            # parse_dates=parse_date_columns,
            low_memory=True,
            chunksize=chunksize,
        )

        parquet_schema = dtypes_to_parquet_schema(
            dtypes,
            rename_mapping,  # parse_date_columns
        )

        if FS.isdir(p_parquet):
            FS.rm(p_parquet, recursive=True)
        elif FS.isfile(p_parquet):
            FS.rm(p_parquet)

        tmp_file_path = os.path.join("/tmp", os.path.basename(p_parquet))
        shutil.rmtree(tmp_file_path, ignore_errors=True)
        os.makedirs(tmp_file_path, exist_ok=True)

        for i, chunk in enumerate(df):
            if rename_mapping:
                chunk = chunk.rename(columns=rename_mapping)
            table = pa.Table.from_pandas(chunk, schema=parquet_schema)
            pq.write_table(table, os.path.join(tmp_file_path, f"{i}.parquet"))
        FS.put(tmp_file_path, f"{p_parquet}/", recursive=True)
        shutil.rmtree(tmp_file_path)


def dtypes_to_parquet_schema(
    dtypes: dict,
    rename_mapping: dict = None,  # , parse_date_columns: list = None
) -> pa.Schema:
    if rename_mapping:
        dtypes = rename_dtypes(dtypes, rename_mapping)
    fields = []
    for column_name, dtype in dtypes.items():
        if dtype == str:
            # if parse_date_columns and (column_name in parse_date_columns):
            #     parquet_type = pa.date32()
            # else:
            parquet_type = pa.string()
        elif dtype in [int, "Int64"]:
            parquet_type = pa.int64()
        elif dtype == float:
            parquet_type = pa.float32()
        elif dtype == bool:
            parquet_type = pa.bool_()
        else:
            raise ValueError(
                "Column %s as no type (%s)" % (column_name, dtype)
            )

        fields.append((column_name, parquet_type))
    return pa.schema(fields)


def rename_dtypes(dtypes: dict, rename_mapping: dict):
    new_dtypes = {}
    for column, dtype in dtypes.items():
        new_dtypes[rename_mapping[column]] = dtype
    return new_dtypes


def read_parquet(
    p_parquet_file: str, spark: pyspark.sql.SparkSession
) -> pyspark.sql.DataFrame:

    tmp_file_path = os.path.join(
        "/tmp/read/", os.path.basename(p_parquet_file)
    )
    # Spark doesn't really support reading s3 files directly
    # (unless with more configuration)
    shutil.rmtree(tmp_file_path, ignore_errors=True)
    FS.download(f"{p_parquet_file}/", tmp_file_path, recursive=True)
    dataframe = spark.read.parquet(tmp_file_path)

    return dataframe


def write_parquet(
    dataframe: pyspark.sql.DataFrame,
    p_parquet_file: str,
    num_partitions: int = 100,
) -> None:
    tmp_file_path = os.path.join(
        "/tmp/write/", os.path.basename(p_parquet_file)
    )
    df = dataframe.repartition(num_partitions)
    df.write.mode("overwrite").parquet(tmp_file_path)
    if FS.exists(p_parquet_file):
        FS.rm(p_parquet_file, recursive=True)
    FS.upload(tmp_file_path, f"{p_parquet_file}/", recursive=True)
    shutil.rmtree(tmp_file_path)
