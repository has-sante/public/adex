"""
Functions relatives aux regroupements des bénéficiaires de TS
"""
from typing import List

import pyspark
import pyspark.sql.functions as f

from functions.processing.recipient import get_recipients_columns
from utils.constants import TS_COLS


def prepare_matches(
    recipients: pyspark.sql.DataFrame,
) -> pyspark.sql.DataFrame:

    # We create the joined column which contains all the informations about the recipient
    columns = get_recipients_columns()

    recipients = recipients.withColumn(
        "joined",
        f.struct(*columns),
    )

    return recipients


def group_by_id_name(df: pyspark.sql.DataFrame) -> None:

    to_ignore_condition = (
        (f.col(TS_COLS.TYPE_IDENTIFIANT) == "AUTRE")
        | (f.col(TS_COLS.NUMERO_IDENTIFIANT) == "NONE")
        | (f.col(TS_COLS.BENEFICIARY_IDENTITY_NORMALIZED).isNull())
        | (f.col(TS_COLS.BENEFICIARY_PRENOM_NORMALIZED).isNull())
    )

    df_ignored_rows = df.filter(to_ignore_condition)
    df_rows_to_match = df.filter(~to_ignore_condition)

    groups = df_rows_to_match.groupby(
        TS_COLS.TYPE_IDENTIFIANT,
        TS_COLS.NUMERO_IDENTIFIANT,
        TS_COLS.BENEFICIARY_IDENTITY_NORMALIZED,
        TS_COLS.BENEFICIARY_PRENOM_NORMALIZED,
    )

    return groups, df_ignored_rows


def merge_matches(
    groups: List[pyspark.sql.DataFrame],
    non_grouped: pyspark.sql.DataFrame,
) -> None:

    columns = get_recipients_columns()

    # Hashes are unique in the recipient dataframe
    # We are juste creating the same structure as the groups
    groups.append(non_grouped.groupby(TS_COLS.HASH))

    df_union = None
    for group in groups:
        df_matched = group.agg(
            f.collect_list(f.col("joined")).alias("children"),
            f.count(f.lit(1)).alias("group_size"),
        )
        for column in columns:
            df_matched = df_matched.withColumn(
                column, df_matched["children"].getItem(0).getItem(column)
            )

        if df_union is None:
            df_union = df_matched
        else:
            df_union = df_union.union(df_matched.select(*df_union.columns))

    df_union = df_union.withColumn(
        TS_COLS.HASH,
        f.sha2(
            f.concat_ws("||", f.col("children").getItem(TS_COLS.HASH)), 256
        ),
    )

    return df_union


def write_dataframe_to_parquet(
    dataframe: pyspark.sql.DataFrame, p_parquet: str, num_partitions: int = 100
) -> None:
    dataframe.repartition(num_partitions).write.format("parquet").mode(
        "overwrite"
    ).save(p_parquet)
