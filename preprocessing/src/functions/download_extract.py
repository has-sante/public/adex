"""
Functions relatives au téléchargement et à l'extraction de fichiers depuis les archives zip.
"""
import logging
import os
import zipfile

import requests
import urllib3

from utils.constants.path import FS


def download_file_from_url(
    url: str,
    p_output: str,
    verify_ssl: bool = True,
    chunksize: int = 100,
    params: dict = None,
    logger: logging.Logger = None,
) -> None:
    if not verify_ssl:
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    if logger is not None:
        logger.info(f"Query api: {url}")

    r = requests.get(
        url,
        stream=True,
        params=params,
        verify=verify_ssl,
    )

    if logger is not None:
        logger.info(f"Query api done for: {url}")
    with FS.open(p_output, "wb") as f:
        for chunk in r.iter_content(chunk_size=chunksize):
            f.write(chunk)


def get_ts_nb_hits(
    url: str,
    verify_ssl: bool = True,
    params: dict = None,
    logger: logging.Logger = None,
) -> None:
    if not verify_ssl:
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    if logger is not None:
        logger.info(f"Query api: {url}")

    r = requests.get(
        url,
        stream=True,
        params=params,
        verify=verify_ssl,
    )

    if logger is not None:
        logger.info(f"Query api done for: {r.url}")
    return r.json()["nhits"]


def extract_with_prefix(p_zip: str, prefix: str, p_output: str) -> None:
    with FS.open(p_zip) as fs_zip_file:
        with zipfile.ZipFile(fs_zip_file) as zip_file:
            filename = get_filename_from_prefix(
                prefix=prefix, zip_file=zip_file
            )
            extract_file(
                filename=filename, p_output=p_output, zip_file=zip_file
            )


def get_filename_from_prefix(prefix: str, zip_file: zipfile.ZipFile) -> str:
    for filename in zip_file.namelist():
        if filename.startswith(prefix):
            return filename
    raise FileNotFoundError(
        "File with prefix '{}' was not found in ziparchive.".format(prefix)
    )


def extract_file(
    filename: str, p_output: str, zip_file: zipfile.ZipFile
) -> None:
    zip_file.extract(member=filename, path="/tmp")
    FS.put(os.path.join("/tmp/", filename), p_output)
    os.remove(os.path.join("/tmp/", filename))
