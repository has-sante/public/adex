"""
Functions relatives aux opérations liées à ElasticSearch
"""
import datetime
import glob
import json
import logging
import os
from os import path
from typing import Dict, Generator, List

import pandas as pd
import pyarrow.parquet as pq
from elasticsearch import Elasticsearch, helpers
from tqdm import tqdm

from utils import settings
from utils.constants.path import FS

DATETIME_FORMAT = "%Y-%m-%dt%H.%M.%S"


def get_elasticsearch_instance() -> Elasticsearch:
    """Gets a connector to elasticsearch"""

    elasticsearch_connection = Elasticsearch(
        [{"host": settings.ELASTICSEARCH_HOST}],
        http_auth=settings.ELASTICSEARCH_AUTH,
    )
    return elasticsearch_connection


def get_all_ingestion_actions_from_parquet(
    p_parquet: str,
    index_name: str,
    id_field: str = None,
) -> Generator[dict, None, None]:
    """
    First checks if p_parquet is a file or a folder, and extracts all
    insertions actions from the parquet dataset.
    Insertion actions are jsons with the index name at key _index.
    """

    if FS.isdir(p_parquet):
        return get_all_ingestion_actions_from_parquet_folder(
            p_parquet_folder=p_parquet,
            index_name=index_name,
            id_field=id_field,
        )
    if FS.isfile(p_parquet):
        return get_all_ingestion_actions_from_parquet_file(
            p_parquet_file=p_parquet, index_name=index_name, id_field=id_field
        )
    raise ValueError(
        "Parquet path {} is neither a folder nor a file" % p_parquet
    )


def get_all_ingestion_actions_from_parquet_folder(
    p_parquet_folder: str,
    index_name: str,
    id_field: str = None,
) -> Generator[dict, None, None]:
    """
    Reads all files and all tables from the parquet dataset and return ingestion actions.
    """
    files = FS.glob(os.path.join(p_parquet_folder, "*.parquet"))
    for filename in files:
        with FS.open(filename) as p_fp:
            parquet_file = pq.ParquetFile(p_fp)
            for row_group in range(parquet_file.num_row_groups):
                table = parquet_file.read_row_group(row_group)
                table_pandas = table.to_pandas()
                table_pandas = table_pandas.where(
                    pd.notnull(table_pandas), None
                )
                table_pandas["_index"] = index_name
                if id_field is not None:
                    table_pandas["_id"] = table_pandas[id_field]
                for action in table_pandas.to_dict(orient="records"):
                    yield action


def get_all_ingestion_actions_from_parquet_file(
    p_parquet_file: str,
    index_name: str,
    id_field: str,
) -> Generator[dict, None, None]:
    """
    Reads all all tables from the parquet dataset and return ingestion actions.
    """
    with FS.open(p_parquet_file) as p_fp:
        parquet_file = pq.ParquetFile(p_fp)
        for row_group in range(parquet_file.num_row_groups):
            table = parquet_file.read_row_group(row_group)
            table_pandas = table.to_pandas()
            table_pandas = table_pandas.where(pd.notnull(table_pandas), None)
            table_pandas["_index"] = index_name
            if id_field is not None:
                table_pandas["_id"] = table_pandas[id_field]
            for action in table_pandas.to_dict(orient="records"):
                yield action


def insert_parallel_bulk(
    actions: Generator[dict, None, None],
    elasticsearch_connection: Elasticsearch,
    chunk_size: int = 10000,
    thread_count: int = 3,
    queue_size: int = 10,
) -> None:
    """Inserts all records into elasticsearch"""
    bulks = helpers.parallel_bulk(
        elasticsearch_connection,
        actions,
        chunk_size=chunk_size,
        thread_count=thread_count,
        queue_size=queue_size,
    )
    failed = 0
    success = 0
    result_failed = []
    for i, rs in tqdm(enumerate(bulks)):
        if rs[0]:
            success += 1
        else:
            result_failed.append(rs)
            failed += 1
        if i % 100000 == 0:
            tqdm.write("%d / %d" % (success, failed))


def upsert_document(
    index_name: str,
    doc_type: str,
    document: Dict,
    elasticsearch_connection: Elasticsearch,
) -> None:
    """
    upsert document into index
    """
    elasticsearch_connection.update(
        index=index_name,
        doc_type=doc_type,
        id="1",
        body={"doc": document, "doc_as_upsert": True},
    )


def delete_index(
    index_name: str,
    elasticsearch_connection: Elasticsearch,
    logger: logging.Logger = None,
) -> None:
    """Deletes the index"""
    logger and logger.info(f"Deleting index {index_name}")

    elasticsearch_connection.indices.delete(index=index_name, ignore=[404])


def put_settings(
    index_name: str, settings: dict, elasticsearch_connection: Elasticsearch
) -> None:
    """Puts the settings to the index"""
    elasticsearch_connection.indices.put_settings(
        body=settings, index=index_name
    )


def get_sorted_indices_matching_prefix(
    index_prefix: str,
    elasticsearch_connection: Elasticsearch,
    from_recent_to_old: bool = False,
) -> List[str]:
    """
    Renvoie les indices matchant le pattern `index_prefix`.
    Ceux-ci sont renvoyés du plus vieux au plus récent en supposant qu'ils
    ont tous la forme f'{index_prefix}-{suffix}' où les suffix peuvent être
    ordonnés par rapport au temps en utilisant sorted
    """
    all_indices_matching_prefix = elasticsearch_connection.indices.get_alias(
        index=f"{index_prefix}-*"
    )
    return sorted(all_indices_matching_prefix, reverse=from_recent_to_old)


def create_mapping_from_path(
    index_name: str,
    p_mapping: str,
    elasticsearch_connection: Elasticsearch,
    logger: logging.Logger = None,
) -> None:
    """
    Reads the mapping file at p_mapping and create the index using this mapping
    """
    logger and logger.info(
        f"Loading mapping for {index_name} from {p_mapping}"
    )

    with open(p_mapping) as file_id:
        mapping = json.load(file_id)

    logger and logger.info(f"Creating index {index_name}")

    elasticsearch_connection.indices.create(
        index=index_name, body=mapping, master_timeout="30s", timeout="30s"
    )


def delete_dangling_indices(
    index_prefix: str,
    num_indices_to_keep: int,
    elasticsearch_connection: Elasticsearch,
    logger: logging.Logger,
):
    # Récupérations des indices
    sorted_indices_matching_prefix = get_sorted_indices_matching_prefix(
        index_prefix=index_prefix,
        elasticsearch_connection=elasticsearch_connection,
    )
    logger and logger.info(
        f"Current indices with prefix {index_prefix} : {sorted_indices_matching_prefix}"
    )

    indices_to_delete = sorted_indices_matching_prefix[:-num_indices_to_keep]
    for index_to_delete in indices_to_delete:
        delete_index(
            index_name=index_to_delete,
            elasticsearch_connection=elasticsearch_connection,
            logger=logger,
        )


def get_action_remove(
    index_name: str, alias_name: str, logger: logging.Logger = None
):
    logger and logger.info(f"Remove alias {alias_name} for index {index_name}")
    return {"remove": {"index": index_name, "alias": alias_name}}


def get_action_add(
    index_name: str,
    alias_name: str,
    logger: logging.Logger = None,
):
    logger and logger.info(f"Add alias {alias_name} for index {index_name}")
    return {"add": {"index": index_name, "alias": alias_name}}


def list_alias_indices(
    alias_name: str, elasticsearch_connection: Elasticsearch
) -> List[str]:
    return elasticsearch_connection.indices.get(alias_name).keys()


def roll_over_aliases(
    index_name: str,
    alias_name: str,
    elasticsearch_connection: Elasticsearch,
    logger: logging.Logger = None,
) -> None:

    unlink_alias_and_create_new_alias(
        index_name=index_name,
        alias_name=alias_name,
        elasticsearch_connection=elasticsearch_connection,
        logger=logger,
    )

    if logger:
        logger.info(f"Creating alias {alias_name} for index {index_name}")
    elasticsearch_connection.indices.put_alias(index_name, alias_name)


def unlink_alias_and_create_new_alias(
    index_name: str,
    alias_name: str,
    elasticsearch_connection: Elasticsearch,
    logger: logging.Logger = None,
) -> None:
    actions = []
    if elasticsearch_connection.indices.exists_alias(alias_name):
        indices_linked_to_alias = list_alias_indices(
            alias_name=alias_name,
            elasticsearch_connection=elasticsearch_connection,
        )
        for index_linked_to_alias in indices_linked_to_alias:
            actions.append(
                get_action_remove(
                    index_name=index_linked_to_alias,
                    alias_name=alias_name,
                    logger=logger,
                )
            )
    actions.append(
        get_action_add(
            index_name=index_name, alias_name=alias_name, logger=logger
        )
    )
    body = {"actions": actions}
    logger and logger.info(f"Running the following command : {body}")
    elasticsearch_connection.indices.update_aliases(body=body)


def roll_back_aliases(
    index_prefix: str,
    elasticsearch_connection: Elasticsearch,
    logger: logging.Logger = None,
) -> str:
    all_matching_indices = get_sorted_indices_matching_prefix(
        index_prefix=index_prefix,
        elasticsearch_connection=elasticsearch_connection,
        from_recent_to_old=True,
    )
    logger and logger.info(
        f"Current indices for alias {index_prefix}: {all_matching_indices}"
    )
    # We get the second most recent index
    new_current_index_name = all_matching_indices[1]
    logger and logger.info(
        f"{new_current_index_name} will be the current used index"
    )

    unlink_alias_and_create_new_alias(
        index_name=new_current_index_name,
        alias_name=index_prefix,
        elasticsearch_connection=elasticsearch_connection,
        logger=logger,
    )
    return new_current_index_name


def get_new_index_name(index_prefix: str) -> str:
    now = datetime.datetime.utcnow().strftime(DATETIME_FORMAT)
    return f"{index_prefix}-{now}"


def get_datetime_from_index_name(
    index_name: str, index_prefix: str
) -> datetime.datetime:
    formatted_date = index_name.strip(index_prefix + "-")
    return datetime.datetime.strptime(formatted_date, DATETIME_FORMAT)
