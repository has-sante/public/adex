from typing import Dict

import requests

URL = "https://www.transparence.sante.gouv.fr/api/records/1.0/search/"


def get_api_params(firstname: str, identity: str) -> Dict:
    return {
        "q": f"prenom={firstname} AND identite={identity}",
        "dataset": "declarations",
        "format": "json",
        "rows": "1000",
    }


def request_api(firstname: str, identity: str) -> Dict:
    params = get_api_params(firstname=firstname, identity=identity)
    response = requests.get(URL, params=params)
    return [r["fields"] for r in response.json()["records"]]
