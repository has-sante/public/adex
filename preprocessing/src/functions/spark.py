"""
Functions relatives à la création d'une instance spark
"""
import os
import sys

import pyspark  # pylint: disable=C0413
from pyspark.sql import SparkSession  # pylint: disable=C0413
from pyspark.sql.types import StructType

os.environ["PYSPARK_PYTHON"] = sys.executable
os.environ["PYSPARK_DRIVER_PYTHON"] = sys.executable


NUMBER_CORES = int(os.getenv("SPARK_NUMBER_CORES", default="2"))
MEMORY_GB = int(os.getenv("SPARK_MEMORY_GB", default="6"))
MAX_RESULT_SIZE_GB = int(os.getenv("SPARK_MAX_RESULT_SIZE_GB", default="4"))


def get_spark(
    app_name: str,
    number_cores: int = NUMBER_CORES,
    memory_gb: int = MEMORY_GB,
    max_result_size_gb: int = MAX_RESULT_SIZE_GB,
) -> SparkSession:
    """
    Récupère ou crée une instance spark.
    """
    spark = (
        SparkSession.builder.master("local[{}]".format(number_cores))
        .appName(app_name)
        .config("spark.driver.memory", "{}g".format(memory_gb))
        .config("spark.driver.maxResultSize", "{}g".format(max_result_size_gb))
        .config("spark.jars", "/postgresql-driver.jar")
        .config(
            "spark.sql.legacy.parquet.datetimeRebaseModeInWrite", "CORRECTED"
        )
        .config(
            "spark.driver.extraClassPath",
            "/postgresql-driver.jar",
        )
        .getOrCreate()
    )
    return spark


def stop_spark(spark: pyspark.sql.SparkSession) -> None:
    """
    Stop une instance spark.
    """
    spark.stop()


def create_empty_dataframe(
    spark: pyspark.sql.SparkSession,
) -> pyspark.sql.DataFrame:
    schema = StructType([])
    return spark.createDataframe(spark.emptyRDD(), schema)
