"""
Functions relatives à l'ingestion de données dans postgres
"""
import os
from dataclasses import dataclass

import psycopg2
import pyspark

from functions.parquet import read_parquet


@dataclass
class PostgresConnectionParams:
    postgres_server: str = os.getenv("POSTGRES_SERVER")
    postgres_port: int = int(os.getenv("POSTGRES_PORT", default="5432"))
    postgres_db: str = os.getenv("POSTGRES_DB")
    postgres_user: str = os.getenv("POSTGRES_USER")
    postgres_password: str = os.getenv("POSTGRES_PASSWORD")


def ingest_parquet_to_table(
    p_parquet: str,
    table_name: str,
    spark: pyspark.sql.SparkSession,
    schema: str = "new",
):
    df = read_parquet(p_parquet, spark)

    postgres_connection_params = PostgresConnectionParams()

    db_url = "jdbc:postgresql://{postgres_server}:{postgres_port}/{postgres_db}".format(
        postgres_server=postgres_connection_params.postgres_server,
        postgres_port=postgres_connection_params.postgres_port,
        postgres_db=postgres_connection_params.postgres_db,
    )
    df.repartition(10).write.mode("overwrite").format("jdbc").option(
        "dbtable", f"{schema}.{table_name}"
    ).option("driver", "org.postgresql.Driver").option("url", db_url).option(
        "user", postgres_connection_params.postgres_user
    ).option(
        "password", postgres_connection_params.postgres_password
    ).option(
        "batchsize", "200000"
    ).save()


def get_postgres_connection(
    connection_params: PostgresConnectionParams = PostgresConnectionParams(),
) -> psycopg2.extensions.connection:
    return psycopg2.connect(
        user=connection_params.postgres_user,
        password=connection_params.postgres_password,
        host=connection_params.postgres_server,
        port=connection_params.postgres_port,
        database=connection_params.postgres_db,
    )


def execute_and_close(
    query: str, connection: psycopg2.extensions.connection
) -> None:
    cursor = connection.cursor()
    cursor.execute(query)
    connection.commit()
    cursor.close()
    connection.close()


def create_index(
    table_name: str, index_name: str, column: str, schema_name: str = "new"
):
    sql_query = f"CREATE INDEX IF NOT EXISTS {index_name} ON {schema_name}.{table_name} ({column})"

    postgres_connection_params = PostgresConnectionParams()
    connection = get_postgres_connection(postgres_connection_params)

    execute_and_close(sql_query, connection)


def create_schema_old_current_new() -> None:
    create_schema("old")
    create_schema("new")


def create_schema(schema_name: str) -> None:
    postgres_connection_params = PostgresConnectionParams()
    connection = get_postgres_connection(postgres_connection_params)

    query = f"CREATE SCHEMA IF NOT EXISTS {schema_name}"
    execute_and_close(query, connection)


def roll_over_schemas():
    postgres_connection_params = PostgresConnectionParams()
    connection = get_postgres_connection(postgres_connection_params)

    cursor = connection.cursor()

    drop_schema(schema_name="old", cursor=cursor)
    rename_schema(
        old_schema_name="public", new_schema_name="old", cursor=cursor
    )
    rename_schema(
        old_schema_name="new", new_schema_name="public", cursor=cursor
    )

    connection.commit()
    cursor.close()
    connection.close()


def roll_back_schemas():
    postgres_connection_params = PostgresConnectionParams()
    connection = get_postgres_connection(postgres_connection_params)

    cursor = connection.cursor()

    drop_schema(schema_name="new", cursor=cursor)
    rename_schema(
        old_schema_name="public", new_schema_name="new", cursor=cursor
    )
    rename_schema(
        old_schema_name="old", new_schema_name="public", cursor=cursor
    )

    connection.commit()
    cursor.close()
    connection.close()


def rename_schema(
    old_schema_name: str,
    new_schema_name: str,
    cursor: psycopg2.extensions.cursor,
) -> None:
    query = f"ALTER SCHEMA {old_schema_name} RENAME TO {new_schema_name}"
    cursor.execute(query)


def drop_schema(schema_name: str, cursor: psycopg2.extensions.cursor) -> None:
    query = f"DROP SCHEMA IF EXISTS {schema_name} CASCADE"
    cursor.execute(query)
