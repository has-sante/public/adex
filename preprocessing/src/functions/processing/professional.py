"""
Traitement de l'annuaire ANS
"""
import pyspark
import pyspark.sql.functions as f

from functions.dataframe_operations import create_nested_data_column


def create_name_structure(
    annuaire_ans: pyspark.sql.DataFrame,
) -> pyspark.sql.DataFrame:
    annuaire_ans = annuaire_ans.withColumn(
        "nom_complet_exercice",
        f.struct(
            f.when(
                f.col("nested.libelle_civilite") == "Mademoiselle", "Madame"
            )
            .otherwise(f.col("nested.libelle_civilite"))
            .alias("libelle_civilite"),  # Replace Mademoiselle with Madame
            f.col("nested.libelle_civilite_exercice"),
            f.upper("nested.nom_exercice").alias("nom_exercice"),
            f.initcap("nested.prenom_exercice").alias("prenom_exercice"),
        ),
    )

    return annuaire_ans


def create_id_structure(
    annuaire_ans: pyspark.sql.DataFrame,
) -> pyspark.sql.DataFrame:
    # Replace Mademoiselle with Madame

    annuaire_ans = annuaire_ans.withColumn(
        "identification",
        f.struct(
            f.col("nested.type_identifiant_pp").alias(
                "code_type_identifiant_pp"
            ),
            f.when(f.col("nested.type_identifiant_pp") == 0, "ADELI")
            .when(f.col("nested.type_identifiant_pp") == 8, "RPPS")
            .otherwise("Non défini")
            .alias("libelle_type_identifiant_pp"),
            f.col("nested.identifiant_pp"),
            f.col("nested.identification_nationale_pp"),
        ),
    )
    annuaire_ans = annuaire_ans.withColumn(
        "identification_nationale_pp",
        f.col("nested.identification_nationale_pp"),
    )

    return annuaire_ans


def create_activity_place(
    annuaire_ans: pyspark.sql.DataFrame,
) -> pyspark.sql.DataFrame:

    address_columns = [
        "nested.numero_voie_coord_structure",
        "nested.indice_repetition_voie_coord_structure",
        "nested.libelle_type_voie_coord_structure",
        "nested.libelle_voie_coord_structure",
        "nested.code_postal_coord_structure",
        "nested.libelle_commune_coord_structure",
        "nested.libelle_pays_coord_structure",
        "nested.libelle_department_structure",
    ]

    annuaire_ans = annuaire_ans.withColumn(
        "lieu_activite",
        f.struct(
            f.concat_ws(" ", *address_columns).alias("adresse_complete"),
            *address_columns,
            f.col("nested.raison_sociale_site")
        ),
    )
    return annuaire_ans


def clean_professional(
    annuaire_ans: pyspark.sql.DataFrame,
) -> pyspark.sql.DataFrame:
    annuaire_ans = create_nested_data_column(annuaire_ans)
    annuaire_ans = annuaire_ans.fillna("")
    annuaire_ans = create_name_structure(annuaire_ans)
    annuaire_ans = create_id_structure(annuaire_ans)
    annuaire_ans = create_activity_place(annuaire_ans)
    return annuaire_ans


def process_professionals(
    annuaire_ans: pyspark.sql.DataFrame,
) -> pyspark.sql.DataFrame:

    grouped_annuaire_ans = annuaire_ans.groupBy(
        "identification", "identification_nationale_pp"
    ).agg(
        f.collect_set("nom_complet_exercice").alias("noms_complets_exercice"),
        f.collect_set("lieu_activite").alias("lieux_activite"),
        f.collect_set("nested.libelle_profession").alias("professions"),
        f.collect_set(
            f.when(
                f.col("nested.libelle_type_savoir_faire")
                == "Spécialité ordinale",
                f.col("nested.libelle_savoir_faire"),
            ).otherwise(None)
        ).alias("specialites"),
        f.collect_set("nested").alias("children"),
    )
    return grouped_annuaire_ans
