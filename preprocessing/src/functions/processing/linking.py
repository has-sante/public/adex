"""
Traitement du lien entre convention, avantages et rémunérations
"""
from typing import Tuple

import pyspark
import pyspark.sql.functions as f
from pyspark.sql.types import StringType

from utils.constants import TS_COLS


def link_declarations(
    declarations: pyspark.sql.DataFrame,
) -> Tuple[
    pyspark.sql.DataFrame, pyspark.sql.DataFrame, pyspark.sql.DataFrame
]:

    declarations = declarations.withColumn(
        TS_COLS.CONVENTION_DESCRIPTION, f.lit(None).cast(StringType())
    )

    temporary_convention_id_column = "existing_convention_id"

    # We create a dataframe with 2 identical columns, one as a join key, and the second to keep values
    conventions_ids = (
        declarations.filter(f.col(TS_COLS.LIEN_INTERET) == "convention")
        .select(f.col(TS_COLS.DECLARATION_ID).alias(TS_COLS.CONVENTION_ID))
        .withColumn(
            temporary_convention_id_column, f.col(TS_COLS.CONVENTION_ID)
        )
    )

    declarations = (
        declarations.join(
            conventions_ids, on=TS_COLS.CONVENTION_ID, how="left"
        )
        .withColumn(
            TS_COLS.CONVENTION_DESCRIPTION,
            f.when(
                f.col(temporary_convention_id_column).isNull()
                & (f.col(TS_COLS.CONVENTION_LIEE) != "")
                & (f.col(TS_COLS.LIEN_INTERET) != "convention"),
                f.col(TS_COLS.CONVENTION_LIEE),
            ).otherwise(None),
        )
        .drop(TS_COLS.CONVENTION_LIEE, TS_COLS.CONVENTION_ID)
        .withColumnRenamed(
            temporary_convention_id_column, TS_COLS.CONVENTION_ID
        )
    )

    return declarations
