"""
Nettoyage des déclarations de TS
"""
import logging
from typing import List

import pyspark
import pyspark.sql.functions as f
from pyspark.sql.types import StringType

from functions.dataframe_operations import parse_date
from utils.constants import TS_COLS
from utils.constants.transparence_sante import BENEF_COLUMNS, NAN_IDS
from utils.string_normalization import normalize

DATE_FORMAT = "y-M-d"


def clean_declarations(
    declarations: pyspark.sql.DataFrame, logger: logging.Logger = None
) -> pyspark.sql.DataFrame:

    df = clean_names(declarations)
    df = clean_special_chars(df)

    df = create_recipient_hash(df)

    df = clean_identifiers(df, logger)
    df = fill_nan_montant_to_0(df)
    df = parse_date(df, TS_COLS.DATE, DATE_FORMAT)
    df = parse_date(df, TS_COLS.DATE_DEBUT, DATE_FORMAT)
    df = parse_date(df, TS_COLS.DATE_FIN, DATE_FORMAT)
    df = create_declaration_id(df)
    df = create_convention_id(df)

    return df


def clean_names(df: pyspark.sql.DataFrame) -> pyspark.sql.DataFrame:
    # Normalize names
    udf_normalize = f.udf(normalize, StringType())
    df = df.withColumn(
        TS_COLS.BENEFICIARY_IDENTITY_NORMALIZED,
        f.upper(udf_normalize(TS_COLS.IDENTITY)),
    )
    df = df.withColumn(
        TS_COLS.BENEFICIARY_PRENOM_NORMALIZED,
        f.initcap(udf_normalize(TS_COLS.PRENOM)),
    )
    return df


def clean_special_chars(df: pyspark.sql.DataFrame) -> pyspark.sql.DataFrame:
    df = delete_special_chars(
        df,
        [
            TS_COLS.STRUCTURE_EXERCICE,
            TS_COLS.MOTIF_AUTRE,
            TS_COLS.ADRESSE,
            TS_COLS.INFO_CONVENTION,
        ],
    )
    return df


def delete_special_chars(
    df: pyspark.sql.DataFrame,
    columns: List[str]
    # https://en.wikipedia.org/wiki/List_of_Unicode_characters
) -> pyspark.sql.DataFrame:
    for column in columns:
        df = df.withColumn(
            column,
            f.regexp_replace(column, "\u000B|\u0013", " "),
        )
        df = df.withColumn(column, f.regexp_replace(column, "\u0019", "'"))
    return df


def trim_identifiant(df: pyspark.sql.DataFrame) -> pyspark.sql.DataFrame:
    df = df.withColumn(
        TS_COLS.NUMERO_IDENTIFIANT, f.trim(TS_COLS.NUMERO_IDENTIFIANT)
    )
    return df


def fill_nan_montant_to_0(df: pyspark.sql.DataFrame) -> pyspark.sql.DataFrame:
    return df.na.fill(value=0.0, subset=[TS_COLS.MONTANT])


def clean_identifiers(
    df: pyspark.sql.DataFrame, logger: logging.Logger = None
) -> pyspark.sql.DataFrame:
    df = trim_identifiant(df)
    temp_full_identifier_column = "full_identifier"
    df = df.withColumn(
        temp_full_identifier_column,
        f.concat(
            f.col(TS_COLS.TYPE_IDENTIFIANT),
            f.lit(" | "),
            f.col(TS_COLS.NUMERO_IDENTIFIANT),
        ),
    )

    # Setting to Nan null some values
    null_ids_str = [" | ".join((t, v)) for (t, v) in NAN_IDS]
    # Counting ids clean up
    if logger is not None:
        for bad_id_to_count in null_ids_str:
            bad_id_count = df.filter(
                f.col(temp_full_identifier_column) == bad_id_to_count
            ).count()
            logger.info(
                f"Cleaning {bad_id_to_count}: {bad_id_count} occurences"
            )

    df = df.withColumn(
        TS_COLS.NUMERO_IDENTIFIANT,
        f.when(
            f.col(temp_full_identifier_column).isin(null_ids_str), None
        ).otherwise(f.col(TS_COLS.NUMERO_IDENTIFIANT)),
    )
    df = df.drop(temp_full_identifier_column)
    df = df.fillna("", subset=[TS_COLS.NUMERO_IDENTIFIANT])

    return df


def create_declaration_id(
    df: pyspark.sql.DataFrame,
) -> pyspark.sql.DataFrame:
    df = df.withColumn(
        TS_COLS.DECLARATION_ID,
        f.concat_ws(
            " | ",
            TS_COLS.LIEN_INTERET,
            TS_COLS.COMPANY_ID,
            TS_COLS.IDENTIFIANT_UNIQUE,
        ),
    )
    return df


def create_convention_id(
    df: pyspark.sql.DataFrame,
) -> pyspark.sql.DataFrame:
    """
    Création de l'id de la convention liée.
    Pour une convention, on reprend le champ declaration_id.
    Pour les avantages et les rémunérations on reprend le champ
    covention_liee en rajoutant l'entreprise et en rajoutant 'convention'
    """
    df = df.withColumn(
        TS_COLS.CONVENTION_ID,
        f.when(
            f.col(TS_COLS.LIEN_INTERET) == "convention",
            f.col(TS_COLS.DECLARATION_ID),
        ).otherwise(
            f.when(f.col(TS_COLS.CONVENTION_LIEE) == "", None).when(
                ~f.col(TS_COLS.CONVENTION_LIEE).isNull(),
                f.concat_ws(
                    " | ",
                    f.lit("convention"),
                    TS_COLS.COMPANY_ID,
                    TS_COLS.CONVENTION_LIEE,
                ),
            ),
        ),
    )
    return df


def create_recipient_hash(df: pyspark.sql.DataFrame) -> pyspark.sql.DataFrame:
    df = df.withColumn(
        TS_COLS.HASH,
        f.sha2(f.concat_ws("||", *BENEF_COLUMNS), 256).substr(1, 12),
    )
    return df
