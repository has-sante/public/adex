"""
Migrations des raw declarations
"""

import dask.dataframe as dd

from utils.constants import TS_COLS, TS_COLS_OLD


def process_raw_declarations(raw_declarations: dd.DataFrame) -> dd.DataFrame:
    """ """
    declarations = drop_columns(raw_declarations)
    declarations = rename_declarations(declarations)
    return declarations


def rename_declarations(raw_declarations: dd.DataFrame) -> dd.DataFrame:
    return raw_declarations.rename(
        columns={
            TS_COLS_OLD.ENTREPRISE_IDENTIFIANT: TS_COLS.COMPANY_ID,
            TS_COLS_OLD.RAISON_SOCIALE: TS_COLS.COMPANY_NAME,
            TS_COLS_OLD.IDENTIFIANT_UNIQUE: TS_COLS.IDENTIFIANT_UNIQUE,
            TS_COLS_OLD.BENEF_CATEGORIE_CODE: TS_COLS.CATEGORIE_CODE,
            TS_COLS_OLD.BENEF_CATEGORIE: TS_COLS.CATEGORIE_LIBELLE,
            TS_COLS_OLD.BENEF_PRENOM: TS_COLS.PRENOM,
            TS_COLS_OLD.BENEF_PROFESSION: TS_COLS.PROFESSION,
            TS_COLS_OLD.PAYS_CODE: TS_COLS.PAYS_CODE,
            TS_COLS_OLD.BENEF_PAYS_NAME: TS_COLS.PAYS,
            TS_COLS_OLD.BENEF_TYPE_CODE: TS_COLS.TYPE_IDENTIFIANT,
            TS_COLS_OLD.BENEF_IDENTIFIANT: TS_COLS.NUMERO_IDENTIFIANT,
            TS_COLS_OLD.STRUCTURE_EXERCICE: TS_COLS.STRUCTURE_EXERCICE,
            TS_COLS_OLD.DATE: TS_COLS.DATE,
            TS_COLS_OLD.MOTIF: TS_COLS.MOTIF,
            TS_COLS_OLD.MOTIF_AUTRE: TS_COLS.MOTIF_AUTRE,
            TS_COLS_OLD.DATE_DEBUT: TS_COLS.DATE_DEBUT,
            TS_COLS_OLD.DATE_FIN: TS_COLS.DATE_FIN,
            TS_COLS_OLD.MONTANT: TS_COLS.MONTANT,
            TS_COLS_OLD.BENEF_IDENTITE: TS_COLS.IDENTITY,
            TS_COLS_OLD.CODE_POSTAL: TS_COLS.CODE_POSTAL,
            TS_COLS_OLD.ADRESSE: TS_COLS.ADRESSE,
            TS_COLS_OLD.VILLE_SANS_CEDEX: TS_COLS.VILLE,
            TS_COLS_OLD.INFO_EVENEMENT: TS_COLS.INFO_CONVENTION,
            TS_COLS_OLD.LIEN_INTERET: TS_COLS.LIEN_INTERET,
            TS_COLS_OLD.CONVENTION_LIEE: TS_COLS.CONVENTION_LIEE,
        }
    )


def drop_columns(declarations: dd.DataFrame) -> dd.DataFrame:
    return declarations.drop(
        columns=[
            TS_COLS_OLD.TOKEN,
            TS_COLS_OLD.MOTIF_CODE,
            TS_COLS_OLD.SEMESTRE,
            TS_COLS_OLD.DATE_PUBLICATION,
            TS_COLS_OLD.DATE_TRANSMISSION,
            TS_COLS_OLD.DEMANDE_RECTIFICATION,
            TS_COLS_OLD.STATUS,
            TS_COLS_OLD.BENEF_ID,
            TS_COLS_OLD.BENEF_TYPE,
            TS_COLS_OLD.BENEF_PROFESSION_CODE,
            TS_COLS_OLD.BENEF_NOM_COM_NAME,
            TS_COLS_OLD.SECTEUR_ACTIVITE,
            TS_COLS_OLD.ENTR_VILLE,
            TS_COLS_OLD.ENTR_DEP_NAME,
            TS_COLS_OLD.ENTR_REG_NAME,
            TS_COLS_OLD.ENTR_PAYS,
            TS_COLS_OLD.MERE_ID,
            TS_COLS_OLD.SIREN,
            TS_COLS_OLD.GEOM,
            TS_COLS_OLD.COMMUNE_NAME,
            TS_COLS_OLD.COMMUNE_CODE,
            TS_COLS_OLD.EPCI_NAME,
            TS_COLS_OLD.EPCI_CODE,
            TS_COLS_OLD.DEP_CODE,
            TS_COLS_OLD.DEPEPCI_NAME,
            TS_COLS_OLD.REG_NAME,
            TS_COLS_OLD.REG_CODE,
            TS_COLS_OLD.VILLE,
        ]
    )
