"""
Extraction des bénéficiaires de TS
"""
from typing import List

import pyspark

from utils.constants import TS_COLS
from utils.constants.transparence_sante import BENEF_COLUMNS


def get_recipients_columns() -> List:
    return BENEF_COLUMNS + [
        TS_COLS.HASH,
        TS_COLS.BENEFICIARY_IDENTITY_NORMALIZED,
        TS_COLS.BENEFICIARY_PRENOM_NORMALIZED,
    ]


def process_recipients(
    declarations: pyspark.sql.DataFrame,
) -> pyspark.sql.DataFrame:

    to_select = get_recipients_columns()

    # Merge dataframes
    recipients = declarations.select(to_select)

    # Drop duplicates
    unique_recipients = recipients.drop_duplicates([TS_COLS.HASH])
    return unique_recipients
